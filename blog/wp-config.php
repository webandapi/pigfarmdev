<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dev_pig');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D~MtND78+</Vvz_mRy8oeo];[oDd.dIc}iH#F2i1.G;N?S>pp0{ct_=AOYYQYlW|');
define('SECURE_AUTH_KEY',  '([}mbQ5vDr=@#|X$A1[_36G;6rx[#[s65()|vu*Y+QWy`Al{zgZ5;|S&p(rPFX&3');
define('LOGGED_IN_KEY',    'Z8]SU$I~v%xsf@CRJ2rg+>)-#{U*wkJqSS;yf0*}@&aE@p#:/QIeVMEA{JHmo,D$');
define('NONCE_KEY',        '<uLNcox@:9G([^U+{G8}aTtulSHBdRi(SYO)Rlc_@r3v?]<iD&/)AgujPe@F$)/B');
define('AUTH_SALT',        '32TUdVad:p9k,_UQaBWS =yeJ<AuBQ}x@ZT^rVL5x/d}Z-~z;9H3Pr$ONbJ/7{$u');
define('SECURE_AUTH_SALT', ';E2eu/-~%mi^5Q5wc:h~c,ld#c~M~neMY}]0*UHwL{itpZ!4utb{Q*NOa4wPjDnR');
define('LOGGED_IN_SALT',   '!NQE,y}ER|4$g+jMAkR@$>XHjk<K[^;!W?A{g +?yb5FTev[UIrBpI1^lA<oRDJ3');
define('NONCE_SALT',       '(UsZu)/;f8k`;;[46M-|8sEGr0Q9U6+op}1}{96qU4/H/{z~E0D4UtHq&V^;H?.^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
