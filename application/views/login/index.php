<!-- Content Wrapper. Contains page content -->
<div class="container-fluid">
    <div class="row section-box">
        <div class="col-sm-4 col-sm-offset-4 txt-register"> Đăng Nhập </div>
        <div class="col-sm-4 col-sm-offset-4">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="ft-logo">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="Logo" /> 
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <?php echo form_open(base_url(), 'id="frmlogin"'); ?>
                    <form id="frmadministratorlogin" method="POST">
                        <?php if ($this->session->flashdata('reponse')) { ?>
                            <div class="form-group">
                                <p class="btn btn-block btn-social btn-google">
                                    <i class="fa fa-warning"></i>
                                    <span><?php echo $this->session->flashdata('reponse'); ?></span>
                                </p>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <input type="text" id="username" name="username" class="form-control" placeholder="Tên đăng nhập" value="<?php echo $this->session->userdata('re_username') ? $this->session->userdata('re_username') : set_value('username'); ?>" >
                            <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                        </div>

                        <div class="form-group">
                            <input type="password" id="pwd" name="pwd" class="form-control" placeholder="Mật khẩu" value="<?php echo $this->session->userdata('re_password') ? $this->session->userdata('re_password') : set_value('pwd'); ?>" >
                            <?php echo form_error('pwd', '<p class="error">', '</p>'); ?>
                        </div>
                        <!-- /.box-body -->
                        <div class="form-group form-check section-remmember">
                            <div class="remember-password">
                                <input type="checkbox" name="remember" value="1" class="form-check-input">
                                <label class="form-check-label" for="remember">Ghi nhớ mật khẩu</label>
                            </div>
                            <div class="forgot-password"><a href="/forgot-password">Quên mật khẩu</a></div>
                        </div>       
                        <?php echo form_submit('submit', 'Đăng nhập', array('class' => 'btn btn-register btn-primary btn-lg btn-block')); ?>
                        <?php echo form_close(); ?>
                </div>
                <div class="other-register">
                    <div class="txt-other">Hoặc đăng nhập với</div>
                    <div class="btn-other">
                        <a href="#" title="Facebook" class="btn btn-facebook btn-lg"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
                        <a href="#" title="Google+" class="btn btn-googleplus btn-lg"><i class="fa fa-google-plus fa-fw"></i> Google+</a>
                    </div>
                    <div class="account-exists">Chưa có tài khoản <a href="/register">Đăng Ký</a></div>
                </div>
            </div>
        </div>
    </div>
</div>