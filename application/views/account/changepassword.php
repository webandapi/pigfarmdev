<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Đổi mật khẩu</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Đổi mật khẩu</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <?php $this->load->view('message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <section class="content">

                            <div class="row">
                                <div class="col-md-3">

                                    <!-- Profile Image -->
                                    <div class="box box-primary">
                                        <div class="box-body box-profile">
                                            <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->config->config['assets_path'] . '/images/icons/logo.png'; ?>" alt="">
                                            <h3 class="profile-username text-center"><?php echo $current['fullname']; ?></h3>
                                            <p class="text-muted text-center">(<?php echo $current['username']; ?>)</p>
                                            <table class="table dataTable">
                                                <?php if ($current['phone']) { ?>
                                                    <tr>
                                                        <td><small>Điện thoại:</small></td>
                                                        <td><small><i><?php echo $current['phone']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['email']) { ?>
                                                    <tr>
                                                        <td><small>Email:</small></td>
                                                        <td><small><i><?php echo $current['email']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['birthday']) { ?>
                                                    <tr>
                                                        <td><small>Ngày sinh:</small></td>
                                                        <td><small><i><?php echo $current['birthday']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['gender']) { ?>
                                                    <tr>
                                                        <td><small>Giới tính:</small></td>
                                                        <td><?php echo (!isset($current['gender'])) ? "_" : ($current['gender'] == 1 ? "<a title='Nam'><img height='20' src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img height='20' src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                            <a href="<?php echo $this->config->config['base_url'] . '/logout'; ?>" class="btn btn-warning btn-flat btn-block"><b>Đăng xuất</b></a>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!-- /.col -->
                                <div class="col-md-9">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li><a href="<?php echo $this->config->config['base_url'] . '/account'; ?>">Thông tin tài khoản</a></li>
                                            <li class="active"><a href="#changepass">Đổi mật khẩu</a></li>
                                            <li><a href="<?php echo $this->config->config['base_url'] . '/account/update'; ?>">Cập nhật tài khoản</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="changepass">
                                                <?php echo form_open(base_url('account/changepassword'), array('id' => 'frmchangepassword', 'class' => 'form-horizontal')); ?>
                                                <div class="form-group">
                                                    <label for="oldpass" class="col-sm-3 control-label label-normal">Mật khẩu cũ <span class="text-danger">*</span></label>
                                                    <div class="col-sm-5">
                                                        <input type="password" class="form-control" name="oldpass" id="oldpass" value="<?php echo set_value('oldpass'); ?>" placeholder="Mật khẩu cũ">
                                                        <?php echo form_error('oldpass', '<p class="error">', '</p>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="newpass" class="col-sm-3 control-label label-normal">Mật khẩu mới <span class="text-danger">*</span></label>
                                                    <div class="col-sm-5">
                                                        <input type="password" class="form-control" name="newpass" id="newpass" value="<?php echo set_value('newpass'); ?>" placeholder="Mật khẩu mới">
                                                        <?php echo form_error('newpass', '<p class="error">', '</p>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="cfnewpass" class="col-sm-3 control-label label-normal">Nhập lại mật khẩu mới <span class="text-danger">*</span></label>
                                                    <div class="col-sm-5">
                                                        <input type="password" class="form-control" id="cfnewpass" name="cfnewpass" value="<?php echo set_value('cfnewpass'); ?>" placeholder="Nhập lại mật khẩu mới">
                                                        <?php echo form_error('cfnewpass', '<p class="error">', '</p>'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-5">
                                                        <?php
                                                        echo form_submit('submit', 'Đổi mật khẩu', array('class' => 'btn btn-flat btn-success'));
                                                        echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- /.nav-tabs-custom -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                        </section>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
