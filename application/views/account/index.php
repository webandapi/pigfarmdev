<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-user-circle-o"></i> Thông tin tài khoản</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Thông tin tài khoản</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <?php $this->load->view('message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <section class="content">

                            <div class="row">
                                <div class="col-md-3">

                                    <!-- Profile Image -->
                                    <div class="box box-primary">
                                        <div class="box-body box-profile">
                                            <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->config->config['assets_path'] . '/images/icons/logo.png'; ?>" alt="">

                                            <h3 class="profile-username text-center"><?php echo $current['fullname']; ?></h3>

                                            <p class="text-muted text-center">(<?php echo $current['username']; ?>)</p>
                                            <table class="table dataTable">
                                                <?php if ($current['phone']) { ?>
                                                    <tr>
                                                        <td><small>Điện thoại:</small></td>
                                                        <td><small><i><?php echo $current['phone']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['email']) { ?>
                                                    <tr>
                                                        <td><small>Email:</small></td>
                                                        <td><small><i><?php echo $current['email']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['birthday']) { ?>
                                                    <tr>
                                                        <td><small>Ngày sinh:</small></td>
                                                        <td><small><i><?php echo $current['birthday']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['gender']) { ?>
                                                    <tr>
                                                        <td><small>Giới tính:</small></td>
                                                        <td><?php echo (!isset($current['gender'])) ? "_" : ($current['gender'] == 1 ? "<a title='Nam'><img height='20' src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img height='20' src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                            <a href="<?php echo $this->config->config['base_url'] . '/logout'; ?>" class="btn btn-warning btn-flat btn-block"><b>Đăng xuất</b></a>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!-- /.col -->
                                <div class="col-md-9">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#account" data-toggle="tab">Thông tin tài khoản</a></li>
                                            <li><a href="<?php echo $this->config->config['base_url'] . '/account/changepassword'; ?>">Đổi mật khẩu</a></li>
                                            <li><a href="<?php echo $this->config->config['base_url'] . '/account/update'; ?>">Cập nhật tài khoản</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="account">
                                                <div class="box-body no-padding">
                                                    <table class="table table-striped dataTable">
                                                        <tr>
                                                            <td style="width: 30%;">Tên đăng nhập:</td>
                                                            <td><i><?php echo $current['username']; ?></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Họ tên:</td>
                                                            <td><?php echo $current['fullname'] ? "<i>{$current['fullname']}</i>" : "<a title='Chưa cập nhật'><img height='20' src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Địa chỉ:</td>
                                                            <td><?php echo $current['address'] ? "<i>{$current['address']}</i>" : "<a title='Chưa cập nhật'><img height='20' src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email:</td>
                                                            <td><?php echo $current['email'] ? "<i>{$current['email']}</i>" : "<a title='Chưa cập nhật'><img height='20' src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Điện thoại:</td>
                                                            <td><?php echo $current['phone'] ? "<i>{$current['phone']}</i>" : "<a title='Chưa cập nhật'><img height='20' src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Loại người dùng:</td>
                                                            <td><i>Nông dân</i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Trạng thái:</td>
                                                            <td><?php echo ($current['status'] == 1) ? "<a title='Kích hoạt'><img height='20' src='{$this->config->config['assets_path']}/images/icons/checked.png' /></a>" : "<a title='Chưa kích hoạt'><img height='20' src='{$this->config->config['assets_path']}/images/icons/unchecked.png' /></a>"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ngày sinh:</td>
                                                            <td><?php echo $current['birthday'] ? "<i>{$current['birthday']}</i>" : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Giới tính:</td>
                                                            <td><?php echo (!isset($current['gender'])) ? "_" : ($current['gender'] == 1 ? "<a title='Nam'><img height='20' src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img height='20' src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Ngày đăng ký:</td>
                                                            <td><i><?php echo $current['created']; ?></i></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Lĩnh vực:</td>
                                                            <td><?php echo $current['userfield']; ?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- /.nav-tabs-custom -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                        </section>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
