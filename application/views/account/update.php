<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Cập nhật tài khoản</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Cập nhật tài khoản</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <?php $this->load->view('message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <section class="content">

                            <div class="row">
                                <div class="col-md-3">

                                    <!-- Profile Image -->
                                    <div class="box box-primary">
                                        <div class="box-body box-profile">
                                            <img class="profile-user-img img-responsive img-circle" src="<?php echo $this->config->config['assets_path'] . '/images/icons/logo.png'; ?>" alt="">
                                            <h3 class="profile-username text-center"><?php echo $data['current']['fullname']; ?></h3>
                                            <p class="text-muted text-center">(<?php echo $data['current']['username']; ?>)</p>
                                            <table class="table">
                                                <?php if ($current['phone']) { ?>
                                                    <tr>
                                                        <td><small>Điện thoại:</small></td>
                                                        <td><small><i><?php echo $current['phone']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['email']) { ?>
                                                    <tr>
                                                        <td><small>Email:</small></td>
                                                        <td><small><i><?php echo $current['email']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['birthday']) { ?>
                                                    <tr>
                                                        <td><small>Ngày sinh:</small></td>
                                                        <td><small><i><?php echo $current['birthday']; ?></i></small></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($current['gender']) { ?>
                                                    <tr>
                                                        <td><small>Giới tính:</small></td>
                                                        <td><?php echo (!isset($current['gender'])) ? "_" : ($current['gender'] == 1 ? "<a title='Nam'><img height='20' src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img height='20' src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                            <a href="<?php echo $this->config->config['base_url'] . '/logout'; ?>" class="btn btn-warning btn-flat btn-block"><b>Đăng xuất</b></a>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!-- /.box -->

                                </div>
                                <!-- /.col -->
                                <div class="col-md-9">
                                    <div class="nav-tabs-custom">
                                        <ul class="nav nav-tabs">
                                            <li><a href="<?php echo $this->config->config['base_url'] . '/account'; ?>">Thông tin tài khoản</a></li>
                                            <li><a href="<?php echo $this->config->config['base_url'] . '/account/changepassword'; ?>">Đổi mật khẩu</a></li>
                                            <li class="active"><a href="#updateaccount" data-toggle="tab">Cập nhật tài khoản</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="active tab-pane" id="updateaccount">
                                                <?php echo form_open(base_url('account/update'), array('id' => 'frmaccountupdate', 'class' => 'form-horizontal')); ?>
                                                <div class="form-group">
                                                    <label for="username" class="col-sm-2 control-label label-normal">Tên đăng nhập</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="username" name="username" placeholder="Tên đăng nhập" value="<?php echo $data['current']['username']; ?>" readonly="">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname" class="col-sm-2 control-label label-normal">Họ tên</label>
                                                    <div class="col-sm-10">
                                                        <div class="form-group">
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo $data['current']['lastname'] ? $data['current']['lastname'] : set_value('lastname'); ?>" placeholder="Họ">
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo $data['current']['firstname'] ? $data['current']['firstname'] : set_value('firstname'); ?>" placeholder="Tên">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="address" class="col-sm-2 control-label label-normal">Địa chỉ</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="address" value="<?php echo $data['current']['address'] ? $data['current']['address'] : set_value('address'); ?>" name="address" placeholder="Địa chỉ">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="email" class="col-sm-2 control-label label-normal">Email</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="email" name="email" value="<?php echo $data['current']['email'] ? $data['current']['email'] : set_value('email'); ?>" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone" class="col-sm-2 control-label label-normal">Điện thoại</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $data['current']['phone'] ? $data['current']['phone'] : set_value('phone'); ?>" placeholder="Điện thoại">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="birthday" class="col-sm-2 control-label label-normal">Ngày sinh</label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group date">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" id="birthday" name="birthday" class="form-control pull-right datepicker" value="<?php echo $data['current']['birthday'] ? $data['current']['birthday'] : set_value('birthday'); ?>" placeholder="Ngày sinh" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group box-label">
                                                    <label class="col-sm-2 control-label label-normal">Giới tính</label>
                                                    <div class="col-sm-10">
                                                        <label for="male">
                                                            <input type="radio" name="gender" id="male" name="gender" class="minimal-red" value="1" <?php echo $data['current']['gender'] == '1' ? 'checked="checked"' : set_radio('gender', '1', TRUE); ?>> Nam
                                                        </label>
                                                        <label for="female">
                                                            <input type="radio" name="gender" id="female" name="gender" class="minimal-red" value="0" <?php echo $data['current']['gender'] == '0' ? 'checked="checked"' : set_radio('gender', '0'); ?>> Nữ
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label label-normal">Lĩnh vực</label>
                                                    <div class="col-sm-10">
                                                        <select class="form-control select2" multiple="multiple" data-live-search="true" name="userfield[]">
                                                            <option value="">-- Chọn lĩnh vực --</option>
                                                            <?php
                                                            if ($data['userfield']):
                                                                foreach ($data['userfield'] as $val) :
                                                                    printf('<option value="%d" %s>%s</option>', $val->id, in_array($val->id, $data['fieldcold']) ? 'selected="selected"' : set_select('userfield', $val->id), $val->name);
                                                                endforeach;
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <br/>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <?php
                                                        echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-success'));
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php echo form_close(); ?>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div>
                                    <!-- /.nav-tabs-custom -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                        </section>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
