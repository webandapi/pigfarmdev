<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-life-ring"></i> Quản lý thiết bị</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý thiết bị</li>
        </ol>
    </section>
      <!-- Main content -->
      <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <a href="<?php echo $this->config->config['base_url'] . '/supportdevice/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-life-ring"></i> Thêm thiết bị</a>
                        </div>
                        <div class="form-group">
                            <?php $this->load->view('administrator/message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="users-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thiết bị</th>
                                        <th>IP máy chủ</th>
                                        <th>Người hỗ trợ</th>
                                        <th>Trang trại</th>
                                        <th>Tên đăng nhập</th>
                                        <th>User Mysql</th>
                                        <th class="text-center">Ngày kích hoạt</th>
                                        <th>Thời gian bảo hành</th>
                                        <th>Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($devices):
                                        $i = 0;
                                        foreach ($devices as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $val->deviceid; ?></td>
                                                <td><?php echo $val->ipserver; ?></td>
                                                <td><?php echo $val->supporter ? $val->supporter : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td><?php echo $val->name ? $val->name : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td><?php echo $val->username; ?></td>
                                                <td><?php echo $val->usersql; ?></td>
                                                <td><?php echo $val->dateactive ? date('d/m/Y', strtotime($val->dateactive)) : '_'; ?></td>
                                                <td><?php echo $val->dateactive ? date('d/m/Y', strtotime($val->warantytime)) : '_'; ?></td>
                                                <td><?php echo ($val->isdefault == 1) ? "<a title='Mặc định'><img src='{$this->config->config['assets_path']}/images/icons/checked.png' /></a>" : "<a title='Không mặc định'><img src='{$this->config->config['assets_path']}/images/icons/unchecked.png' /></a>"; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/supportdevice/delete/{$val->id}"; ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/supportdevice/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thiết bị</th>
                                        <th>IP máy chủ</th>
                                        <th>Người hỗ trợ</th>
                                        <th>Trang trại</th>
                                        <th>Tên đăng nhập</th>
                                        <th>User Mysql</th>
                                        <th class="text-center">Ngày kích hoạt</th>
                                        <th>Thời gian bảo hành</th>
                                        <th>Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>   
