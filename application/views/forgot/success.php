<div class="container-fluid">
    <div class="row section-box">
        <div class="col-sm-4 col-sm-offset-4 txt-register"> Email Đã Gởi </div>
        <div class="col-sm-4 col-sm-offset-4">
            <div class="box box-info">
                <div class="icon-confirm">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/icon-confirm.png" alt="Logo"> 
                </div>

                <div class="box-body">
                    <div class="verify-password-description">Chúng tôi vừa gởi cho bạn email hướng dẫn cách khôi phục mật khẩu. Vui lòng mở email và làm theo hướng dẫn.</div>
                    <div class="other-register">
                        <div class="account-exists">Quay lại <a href="/">Đăng Nhập</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>       
</div>