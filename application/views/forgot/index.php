<div class="container-fluid">
    <div class="row section-box">
        <div class="col-sm-4 col-sm-offset-4 txt-register"> Quên Mật Khẩu </div>
        <div class="col-sm-4 col-sm-offset-4">
            <div class="box box-info">
                <div class="ft-logo">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="Logo"> 
                </div>

                <div class="box-body">
                    <div class="verify-password-description">Nhập email của bạn và chúng tôi sẽ gởi bạn hướng dẫn để khôi phục mật khẩu</div>
                    <?php echo form_open(base_url().'forgot-password', 'id="frmForgotPassword"'); ?>
                        <div class="form-group">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                            <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                        </div>
                    <?php echo form_submit('submit', 'Gởi Email', array('class' => 'btn btn-register btn-primary btn-lg btn-block')); ?>
                    <?php echo form_close(); ?>
                    <div class="other-register">
                        <div class="account-exists">Quay lại <a href="/">Đăng Nhập</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>       
</div>