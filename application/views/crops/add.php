<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/crops'; ?>">Quản lý cây trồng, vật nuôi</a></li>
            <li class="active">Thêm cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/crops'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 cây trồng, vật nuôi.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $this->load->view('message'); ?>
                        <div class="form-wrap farm-wrap <?php echo $checkfull ? 'unvisibility' : ''; ?>">
                            <?php echo form_open(base_url('crops/add'), 'id="frmaddcrops"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Tên cây trồng, vật nuôi <span class="text-danger">*</span> <i class="text-success" style="font-weight: normal">(Bạn có thể chọn tên cây trồng, vật nuôi có sẵn từ gợi ý bên dưới)</i></label>
                                    <input type="text" class="form-control" list="cropsdatalist" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Tên cây trồng, vật nuôi" />
                                    <?php echo form_error('name', '<p class="error">', '</p>'); ?>
                                    <?php
                                    if ($cropsdatalist) {
                                        echo '<datalist id="cropsdatalist">';
                                        foreach ($cropsdatalist as $dtl)
                                            printf('<option value="%s">', $dtl->name);
                                        echo '</datalist>';
                                    }
                                    ?>

                                </div>
                                <div class="form-group">
                                    <label>Loại cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-tree"></i> 
                                        <select class="form-control select2" name="field">
                                            <option value="">-- Chọn loại cây trồng, vật nuôi --</option>
                                            <?php
                                            if ($fields):
                                                foreach ($fields as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->id, set_select('field', $f->id), $f->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('field', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Trang trại <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-home"></i> 
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->id, set_select('farm', $f->id), $f->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="note">Ghi chú</label>
                                    <textarea rows="5" class="form-control" id="note" name="note"  placeholder="Ghi chú" ><?php echo set_value('note'); ?></textarea>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
