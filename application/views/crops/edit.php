<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-pencil-square-o"></i> Cập nhật cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/crops'; ?>">Quản lý cây trồng, vật nuôi</a></li>
            <li class="active">Cập nhật cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/crops'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/crops/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php if ($permission) { ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Access denied!</h4>
                                    <?php echo $permission; ?>
                                </div>
                            <?php } else { ?>
                                <?php echo form_open(base_url('crops/edit'), 'id="frmeditcrops"', array('id' => $data->id)); ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Tên cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $data->name ? $data->name : set_value('name'); ?>" placeholder="Tên cây trồng, vật nuôi" />
                                        <?php echo form_error('name', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Loại cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                        <a class="btn btn-block btn-social btn-google">
                                            <i class="fa fa-tree"></i> 
                                            <select class="form-control select2" name="field">
                                                <option value="">-- Chọn loại cây trồng, vật nuôi --</option>
                                                <?php
                                                if ($fields):
                                                    foreach ($fields as $f) :
                                                        printf('<option value="%d" %s>%s</option>', $f->id, ($data->field_id == $f->id) ? 'selected="selected"' : set_select('field', $f->id), $f->name);
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </a>
                                        <?php echo form_error('field', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Trang trại <span class="text-danger">*</span></label>
                                        <a class="btn btn-block btn-social btn-tumblr">
                                            <i class="fa fa-home"></i> 
                                            <select class="form-control select2" name="farm">
                                                <option value="">-- Chọn trang trại --</option>
                                                <?php
                                                if ($farms):
                                                    foreach ($farms as $f) :
                                                        printf('<option value="%d" %s>%s</option>', $f->id, ($data->farm_id == $f->id) ? 'selected="selected"' : set_select('farm', $f->id), $f->name);
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </a>
                                        <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="note">Ghi chú</label>
                                        <textarea rows="5" class="form-control" id="note" name="note"  placeholder="Ghi chú" ><?php echo $data->note ? $data->note : set_value('note'); ?></textarea>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <?php
                                //box-footer
                                echo'<div class="box-footer">';
                                echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                                echo '</div>';
                                ?>
                                <?php echo form_close(); ?>
                            <?php } ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
