<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-tree"></i> Quản lý cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php if (!$checkfull) { ?>
                        <div class="box-header">
                            <div class="form-group">
                                <a href="<?php echo $this->config->config['base_url'] . '/crops/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm cây trồng, vật nuôi mới</a>
                            </div>
                            <div class="form-group">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                    <?php } ?>   

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 cây trồng, vật nuôi.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="box-filter">
                            <form id="frmfilter" method="get">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $val) :
                                                    printf('<option value="%d" %s>%s</option>', $val->id, ($_GET['farm'] == $val->id) ? 'selected="selected"' : '', $val->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="submit" class="btn btn-flat btn-success" value="Lọc cây trồng, vật nuôi" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="crops-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên cây trồng,vật nuôi</th>
                                        <th>Trang trại</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($crops):
                                        $i = 0;
                                        foreach ($crops as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <?php echo $val->name; ?>
                                                    <p class="sub-item">Loại cây trồng, vật nuôi: (<?php echo $val->field->name; ?>)</p>
                                                </td>
                                                <td><?php echo $val->farm->name; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/crops/delete/{$val->id}?f={$val->farm->id}"; ?>" onclick="return confirm('Một khi bạn đã xóa cây trồng, vật nuôi, tất cả các thông tin liên quan đến cây trồng, vật nuôi này cũng sẽ bị xóa. Bạn có chắc chắn muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên cây trồng,vật nuôi</th>
                                        <th>Trang trại</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
