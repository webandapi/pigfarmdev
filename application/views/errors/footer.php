        </div>
        <!-- ./wrapper -->


        <!-- jQuery 3 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/select2/dist/js/select2.full.min.js"></script>
        <!-- InputMask -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- date-range-picker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/moment/min/moment.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- bootstrap color picker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- SlimScroll -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/adminlte.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/myscripts.js"></script>
        <!-- Page script -->
        
        
        <!--Define base url-->
        <script type="text/javascript">
            var base_url = '<?php echo $this->config->config['base_url']; ?>';
        </script>
    </body>
</html>