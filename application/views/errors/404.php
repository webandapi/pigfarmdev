<!-- Content Wrapper. Contains page content -->
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="ft-logo">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="Logo" /> 
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title text-success">Hệ thống quản lý trang trại</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body text-center">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/404.jpg" alt="404" />
                    <h4 class="text-danger text-uppercase">Không tìm thấy trang bạn yêu cầu !</h4>
                </div>

                <div class="box-footer">
                    <div class="form-group">
                        <div class="login-footer text-center">
                            <a href="<?php echo $this->config->config['base_url']; ?>">Quay lại trang chủ</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>