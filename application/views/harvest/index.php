<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-truck"></i> Quản lý lần thu hoạch</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý lần thu hoạch</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php if (!$checkfull) { ?>
                        <div class="box-header">
                            <div class="form-group">
                                <a href="<?php echo $this->config->config['base_url'] . '/harvest/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm lần thu hoạch mới</a>
                            </div>
                            <div class="form-group">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 lần thu hoạch.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="table-responsive">
                            <table id="harvest-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thời gian đóng gói</th>
                                        <th>Nhân viên đóng gói</th>
                                        <th>Nhà phân phối</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($harvest):
                                        $i = 0;
                                        foreach ($harvest as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <?php echo date('d/m/Y', strtotime($val->packaging_date)); ?>
                                                    <p class="sub-item text-danger">Qrcode:  <?php echo $val->qrcode ? "[ " . number_format($val->qrcode->qr_from, '0', ',', '.') . " <i class='fa  fa-angle-double-right '></i> " . number_format($val->qrcode->qr_to, '0', ',', '.') . "] " : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' alt='Chưa cập nhật'/></a>"; ?></p>
                                                    <p class="sub-item text-success">Vụ cây trồng, vật nuôi:  (<?php echo $val->planting; ?>)</p>
                                                </td>
                                                <td><?php echo $val->user ? $val->user : _; ?></td>
                                                <td>
                                                    <?php
                                                    if ($val->distributor):
                                                        echo "<span class='text-success'><small>Số lượng: " . count($val->distributor) . "</small></span>";
                                                        echo '<ul class="sub-lst">';
                                                        foreach ($val->distributor as $dis) {
                                                            printf('<li><small>- %s</small></li>', $dis);
                                                        }
                                                        echo '</ul>';
                                                    else:
                                                        echo "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' alt='Chưa cập nhật'/></a>";
                                                    endif;
                                                    ?>
                                                </td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/harvest/delete/{$val->id}"; ?>" onclick="return confirm('Một khi lần thu hoạch được xóa, tất cả thông tin liên quan tới lần thu hoạch này sẽ bị xóa hết. Bạn có chắc muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/harvest/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Thời gian đóng gói</th>
                                        <th>Nhân viên đóng gói</th>
                                        <th>Nhà phân phối</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
