<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm lần thu hoạch</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/harvest'; ?>">Quản lý lần thu hoạch</a></li>
            <li class="active">Thêm lần thu hoạch</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/harvest'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 lần thu hoạch.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $this->load->view('message'); ?>
                        <div class="form-wrap farm-wrap <?php echo $checkfull ? 'unvisibility' : ''; ?>">
                            <?php echo form_open(base_url('harvest/add'), 'id="frmaddharvest"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="packaging-date">Ngày đóng gói <span class="text-danger">*</span></label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="packaging-date" name="packaging_date" class="form-control pull-right datepicker" value="<?php echo set_value('packaging_date'); ?>" placeholder="Ngày đóng gói" />
                                    </div>
                                    <?php echo form_error('packaging_date', '<p class="error">', '</p>'); ?>
                                </div> 
                                <div class="form-group">
                                    <label>Nhân viên đóng gói</label>
                                    <a class="btn btn-block btn-social btn-twitter">
                                        <i class="fa fa-user"></i>
                                        <select class="form-control select2" name="userfarm">
                                            <option value="">-- Chọn nhân viên --</option>
                                            <?php
                                            if ($userfarm):
                                                foreach ($userfarm as $uf) :
                                                    printf('<option value="%d" %s>%s</option>', $uf->id, set_select('userfarm', $uf->id), $uf->username . " ({$uf->fullname})");
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label>Vụ cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-product-hunt"></i> 
                                        <select class="form-control select2" name="planting">
                                            <option value="">-- Chọn vụ cây trồng, vật nuôi --</option>
                                            <?php
                                            if ($planting):
                                                foreach ($planting as $p) :
                                                    printf('<option value="%d" %s>%s</option>', $p->id, set_select('planting', $p->id), $p->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('planting', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Nhà phân phối</label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-home"></i> 
                                        <select class="form-control select2" multiple="multiple" data-live-search="true" name="distributors[]">
                                            <option value="">-- Chọn nhà phân phối --</option>
                                            <?php
                                            if ($distributors):
                                                foreach ($distributors as $d) :
                                                    printf('<option value="%d" %s>%s</option>', $d->id, set_select('distributors', $d->id), $d->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-6">
                                        <label for="qrfrom">Mã Qrcode từ</label>
                                        <input type="number" class="form-control" id="qrfrom" name="qrfrom" value="<?php echo set_value('qrfrom'); ?>" placeholder="Từ">
                                        <?php echo form_error('qrfrom', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="qrto">Mã QRcode đến</label>
                                        <input type="number" class="form-control" id="qrto" name="qrto" value="<?php echo set_value('qrto'); ?>" placeholder="Đến">
                                        <?php echo form_error('qrto', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
