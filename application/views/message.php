<?php if ($this->session->flashdata('success')) { ?>
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php } elseif ($this->session->flashdata('failed')) { ?>
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> Thông báo!</h4>
        <?php echo $this->session->flashdata('failed'); ?>
    </div>
    <?php
}