<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm nhật ký</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/diary'; ?>">Quản lý nhật ký xử lý</a></li>
            <li class="active">Thêm nhật ký</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/diary'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php echo form_open(base_url('diary/add'), 'id="frmadddiary"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title">Tiêu đề <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo set_value('title'); ?>" placeholder="Xử lý sâu bệnh | Xử lý phân bón" />
                                    <?php echo form_error('title', '<p class="error">', '</p>'); ?>
                                </div> 
                                <div class="form-group">
                                    <label for="content">Nội dung xử lý <span class="text-danger">*</span></label>
                                    <textarea class="form-control" id="content" rows="10" cols="80" name="content" value="<?php echo set_value('content'); ?>" placeholder="Nội dung xử lý"></textarea>
                                    <?php echo form_error('content', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <label for="date">Thời gian (ngày) <span class="text-danger">*</span></label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="date" class="form-control pull-right datepicker" id="date" value="<?php echo set_value('date'); ?>">
                                        </div>
                                        <?php echo form_error('date', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="bootstrap-timepicker">
                                            <label for="time">Thời gian (giờ) <span class="text-danger">*</span></label>
                                            <div class="input-group">
                                                <input type="text" id="time" name="time" class="form-control timepicker">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-clock-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo form_error('time', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Vụ cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-product-hunt"></i> 
                                        <select class="form-control select2" name="planting">
                                            <option value="">-- Chọn vụ cây trồng, vật nuôi --</option>
                                            <?php
                                            if ($planting):
                                                foreach ($planting as $p) :
                                                    printf('<option value="%d" %s>%s</option>', $p->id, set_select('planting', $p->id), $p->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('planting', '<p class="error">', '</p>'); ?>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
