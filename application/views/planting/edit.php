<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-pencil-square-o"></i> Cập nhật vụ cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>">Quản lý vụ cây trồng, vật nuôi</a></li>
            <li class="active">Cập nhật vụ cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/planting/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php if ($permission) { ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Access denied!</h4>
                                    <?php echo $permission; ?>
                                </div>
                            <?php } else { ?>
                                <?php echo form_open(base_url('planting/edit'), 'id="frmeditplanting"', array('id' => $data->id)); ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="name">Tên vụ cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="name" name="name" value="<?php echo $data->name ? $data->name : set_value('name'); ?>" placeholder="Tên vụ cây trồng, vật nuôi" />
                                    </div>
                                    <div class="form-group">
                                        <label for="sowing-time">Thời gian bắt đầu</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="sowing-time" name="sowing_time" class="form-control pull-right datepicker" value="<?php echo $data->sowing_time ? date('d/m/Y', strtotime($data->sowing_time)) : set_value('sowing_time'); ?>" placeholder="Thời gian bắt đầu" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="expected-harvest-time">Thời gian dự kiến kết thúc</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" id="expected-harvest-time" name="expected_harvest_time" class="form-control pull-right datepicker" value="<?php echo $data->expected_harvest_time ? date('d/m/Y', strtotime($data->expected_harvest_time)) : set_value('expected_harvest_time'); ?>" placeholder="Thời gian bắt đầu" />
                                        </div>
                                    </div>  
                                    <div class="form-group box-label">
                                        <label class="control-label">Trạng thái</label><br/>
                                        <label for="stt-pl-notactice">
                                            <input type="radio" name="status" id="stt-pl-notactice" name="status" class="minimal-red" value="0" <?php echo ($data->status == 0) ? 'checked="checked"' : set_radio('status', 0, TRUE); ?>> Chưa kết thúc
                                        </label>
                                        <label for="stt-pl-actice">
                                            <input type="radio" name="status" id="stt-pl-actice" name="status" class="minimal-red" value="1" <?php echo ($data->status == 1) ? 'checked="checked"' : set_radio('status', 1); ?>> Đã kết thúc
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Giống cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                        <a class="btn btn-block btn-social btn-tumblr">
                                            <i class="fa fa-database"></i> 
                                            <select class="form-control select2" name="seeds">
                                                <option value="">-- Chọn giống cây trồng, vật nuôi --</option>
                                                <?php
                                                if ($seeds):
                                                    foreach ($seeds as $s) :
                                                        printf('<option value="%d" %s>%s</option>', $s->id, ($data->seeds_id == $s->id) ? 'selected="selected"' : set_select('seeds', $s->id), $s->name);
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </a>
                                        <?php echo form_error('seeds', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Trang trại <span class="text-danger">*</span> <span class="text-danger"><small style="font-weight: normal">(Trang trại không được phép thay đổi)</small></span></label>
                                        <a class="btn btn-block btn-social btn-google">
                                            <i class="fa fa-home"></i> 
                                            <select class="form-control select2" name="farm" disabled="">
                                                <option value="">-- Chọn trang trại --</option>
                                                <?php
                                                if ($farms):
                                                    foreach ($farms as $f) :
                                                        printf('<option value="%d" %s>%s</option>', $f->id, ($data->farm_id == $f->id) ? 'selected="selected"' : set_select('farm', $f->id), $f->name);
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </a>
                                        <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <?php
                                //box-footer
                                echo'<div class="box-footer">';
                                echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                                echo '</div>';
                                ?>
                                <?php echo form_close(); ?>
                            <?php } ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
