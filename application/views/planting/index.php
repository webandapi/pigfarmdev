<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-leaf"></i> Quản lý vụ cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý vụ cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php if (!$checkfull) { ?>
                        <div class="box-header">
                            <div class="form-group">
                                <a href="<?php echo $this->config->config['base_url'] . '/planting/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm vụ cây trồng, vật nuôi mới</a>
                            </div>
                            <div class="form-group">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 vụ cây trồng, vật nuôi.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="box-filter">
                            <form id="frmfilter" method="get">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $val) :
                                                    printf('<option value="%d" %s>%s</option>', $val->id, ($_GET['farm'] == $val->id) ? 'selected="selected"' : '', $val->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="submit" class="btn btn-flat btn-success" value="Lọc vụ cây trồng, vật nuôi" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="planting-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên vụ cây trồng, vật nuôi</th>
                                        <th>Thời gian bắt đầu</th>
                                        <th>Thời gian thu hoạch</th>
                                        <th>Nhật ký xử lý</th>
                                        <th>Sự cố</th>
                                        <th>Giống</th>
                                        <th>Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($planting):
                                        $i = 0;
                                        foreach ($planting as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <?php echo $val->name; ?>
                                                    <p class="sub-item"><i class="fa fa-home"></i>: <?php echo $val->farm; ?></p>
                                                </td>
                                                <td><?php echo date('d/m/Y', strtotime($val->sowing_time)); ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($val->expected_harvest_time)); ?></td>
                                                <td>
                                                    <?php
                                                    if ($val->diaries):
                                                        echo "<a class='text-success' href='{$this->config->config['base_url']}/diary'>Danh sách nhật ký: (" . count($val->diaries) . ")</a>";
                                                        echo '<ul class="sub-lst">';
                                                        foreach ($val->diaries as $di) {
                                                            printf('<li><a href="%s">- %s</a></li>', $this->config->config['base_url'] . "/diary/edit/{$di->id}", $di->title);
                                                        }
                                                        echo '</ul>';
                                                        echo "<a class='btn btn-primary btn-flat btn-sm' href='{$this->config->config['base_url']}/diary/add'>Thêm mới</a>";
                                                    else:
                                                        echo '_';
                                                    endif;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($val->incidents):
                                                        echo "<a class='text-success' href='{$this->config->config['base_url']}/incidents'>Danh dách sự cố: (" . count($val->incidents) . ")</a>";
                                                        echo '<ul class="sub-lst">';
                                                        foreach ($val->incidents as $inc) {
                                                            printf('<li><a href="%s">- %s</a></li>', $this->config->config['base_url'] . "/incidents/edit/{$inc->id}", $inc->title);
                                                        }
                                                        echo '</ul>';
                                                        echo "<a class='btn btn-primary btn-flat btn-sm' href='{$this->config->config['base_url']}/incidents/add'>Thêm mới</a>";
                                                    else:
                                                        echo '_';
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><?php echo $val->seeds; ?></td>
                                                <td><?php echo $val->status == 1 ? 'Đã kết thúc' : 'Chưa kết thúc'; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/planting/delete/{$val->id}"; ?>" onclick="return confirm('Một khi bạn xóa vụ cây trồng, vật nuôi, tất cả thông tin liên quan đến vụ cây trồng, vật nuôi này sẽ bị xóa hết. Bạn có chắc muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/planting/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên vụ cây trồng, vật nuôi</th>
                                        <th>Thời gian bắt đầu</th>
                                        <th>Thời gian thu hoạch</th>
                                        <th>Nhật ký xử lý</th>
                                        <th>Sự cố</th>
                                        <th>Giống</th>
                                        <th>Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
