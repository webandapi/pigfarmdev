<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm vụ cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>">Quản lý vụ cây trồng, vật nuôi</a></li>
            <li class="active">Thêm vụ cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 vụ cây trồng, vật nuôi.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $this->load->view('message'); ?>
                        <div class="form-wrap farm-wrap <?php echo $checkfull ? 'unvisibility' : ''; ?>">
                            <?php echo form_open(base_url('planting/add'), 'id="frmaddplanting"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Tên vụ cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Tên vụ cây trồng, vật nuôi" />
                                    <?php echo form_error('name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="sowing-time">Thời gian bắt đầu</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="sowing-time" name="sowing_time" class="form-control pull-right datepicker" value="<?php echo set_value('sowing_time'); ?>" placeholder="Thời gian bắt đầu" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="expected-harvest-time">Thời gian dự kiến kết thúc</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" id="expected-harvest-time" name="expected_harvest_time" class="form-control pull-right datepicker" value="<?php echo set_value('expected_harvest_time'); ?>" placeholder="Thời gian bắt đầu" />
                                    </div>
                                </div>  
                                <div class="form-group box-label">
                                    <label class="control-label">Trạng thái</label><br/>
                                    <label for="stt-notactice">
                                        <input type="radio" id="stt-notactice" name="status" class="minimal-red" value="0" <?php echo set_radio('status', 0, TRUE); ?>> Chưa kết thúc
                                    </label>
                                    <label for="stt-actice">
                                        <input type="radio" id="stt-actice" name="status" class="minimal-red" value="1" <?php echo set_radio('status', 1); ?> disabled> Đã kết thúc
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Giống cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-database"></i> 
                                        <select class="form-control select2" name="seeds">
                                            <option value="">-- Chọn giống cây trồng, vật nuôi --</option>
                                            <?php
                                            if ($seeds):
                                                foreach ($seeds as $s) :
                                                    printf('<option value="%d" %s>%s</option>', $s->id, set_select('seeds', $s->id), $s->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('seeds', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Trang trại <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-home"></i> 
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->id, set_select('farm', $f->id), $f->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
