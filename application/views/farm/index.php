<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-home"></i> Quản lý trang trại</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý trang trại</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php if (!$checkfull) { ?>
                        <div class="box-header">
                            <div class="form-group">
                                <a href="<?php echo $this->config->config['base_url'] . '/farm/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm trang trại mới</a>
                            </div>
                            <div class="form-group">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 trang trại.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="table-responsive">
                            <table id="farms-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên trang trại</th>
                                        <th>Vị trí</th>
                                        <th>Sản phẩm</th>
                                        <th>Chứng nhận</th>
                                        <th>Loại trang trại</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($farms):
                                        $i = 0;
                                        foreach ($farms as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <p class="text-primary"><strong><?php echo $val->name; ?></strong></p>
                                                    <p class="sub-item"><i title='Mã trang trại' class="fa fa-qrcode"></i> <?php echo $val->code ? $val->code : "Chưa cập nhật"; ?></p>
                                                </td>
                                                <td><?php echo $val->location ? $val->location : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' alt='Chưa cập nhật'/></a>"; ?></td>
                                                <td>
                                                    <?php
                                                    if ($val->products):
                                                        echo "<span class='text-success'><small>Số lượng: " . count($val->products) . "</small></span>";
                                                        echo '<ul class="sub-lst">';
                                                        foreach ($val->products as $p) {
                                                            printf('<li><small>- %s</small></li>', $p->name);
                                                        }
                                                        echo '</ul>';
                                                    else:
                                                        echo "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' alt='Chưa cập nhật'/></a>";
                                                    endif;
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($val->certificate):
                                                        echo "<span class='text-success'><small>Số lượng: " . count($val->certificate) . "</small></span>";
                                                        echo '<ul class="sub-lst">';
                                                        foreach ($val->certificate as $ce) {
                                                            printf('<li><small>- %s</small></li>', $ce->name);
                                                        }
                                                        echo '</ul>';
                                                    else:
                                                        echo "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' alt='Chưa cập nhật'/></a>";
                                                    endif;
                                                    ?>
                                                </td>
                                                <td><?php echo $val->farmtype ? $val->farmtype : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' alt='Chưa cập nhật'/></a>"; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/farm/delete/{$val->id}"; ?>" onclick="return confirm('Một khi bạn đồng ý xóa trang trại, mọi thông tin liên quan tới trang trại này sẽ đều bị xóa. Bạn có chắc chắn muốn xóa ?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/farm/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên trang trại</th>
                                        <th>Vị trí</th>
                                        <th>Sản phẩm</th>
                                        <th>Chứng nhận</th>
                                        <th>Loại trang trại</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
