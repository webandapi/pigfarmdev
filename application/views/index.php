<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard/'; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo number_format($data['farm'], 0, ',', '.'); ?></h3>

                        <p>Trang trại</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-home"></i>
                    </div>
                    <a href="<?php echo $this->config->config['base_url'] . '/farm'; ?>" class="small-box-footer">Quản lý trang trại <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo number_format($data['userfarm'], 0, ',', '.'); ?></h3>

                        <p>Nhân viên</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="<?php echo $this->config->config['base_url'] . '/userfarm/'; ?>" class="small-box-footer">Quản lý nhân viên <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo number_format($data['planting'], 0, ',', '.'); ?></h3>

                        <p>Vụ cây trồng, vật nuôi</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-document"></i>
                    </div>
                    <a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>" class="small-box-footer">Quản lý vụ cây trồng, vật nuôi <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo number_format($data['incidents'], 0, ',', '.'); ?></h3>

                        <p>Sự cố</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-document"></i>
                    </div>
                    <a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>" class="small-box-footer">Quản lý sự cố <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">
                <!-- Custom tabs (Charts with tabs)-->
                <div class="nav-tabs-custom">
                    <!-- Tabs within a box -->
                    <ul class="nav nav-tabs pull-right">
                        <li class="active"><a href="#farm-chart" data-toggle="tab">Trang trại</a></li>
                        <li><a href="#incidents-chart" data-toggle="tab">Sự cố</a></li>
                        <li><a href="#diary-chart" data-toggle="tab">Nhật ký xử lý</a></li>
                        <li class="pull-left header"><i class="fa fa-inbox"></i> Thống kê</li>
                    </ul>
                    <div class="tab-content no-padding">
                        <!-- Morris chart - Sales -->
                        <div class="chart tab-pane active" id="farm-chart">
                            <div class="box-body">
                                <?php if ($farms): ?>
                                    <table id="crops-lst" class="table table-bordered table-striped table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th class="text-center">STT</th>
                                                <th>Trang trại</th>
                                                <th class="text-center">Vụ cây trồng, vật nuôi - Giống</th>
                                                <th class="text-center">Tổng kinh phí dự kiến</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $i = 0;
                                            foreach ($farms as $val) :
                                                $i++;
                                                ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $i; ?></td>
                                                    <td>
                                                        <?php echo $val->name; ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php
                                                        if ($val->planting):
                                                            echo "<span class='text-primary'><small>Số lượng: " . count($val->planting) . "</small></span>";
                                                            echo '<ul class="sub-lst">';
                                                            foreach ($val->planting as $p) {
                                                                printf('<li>+ %s <p class="sub-item text-success"><i>Giống: %s</i> <i> - Kinh phí: %s vnđ</i></p></li>', $p->name, $p->seeds->name, number_format($p->seeds->total_cost, '0', ',', '.'));
                                                            }
                                                            echo '</ul>';
                                                        else:
                                                            echo "_";
                                                        endif;
                                                        ?>
                                                    </td>
                                                    <td class="text-center"><?php echo $val->total ? number_format($val->total, '0', ',', '.') . ' vnđ' : '_'; ?></td>
                                                </tr>
                                                <?php
                                            endforeach;
                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="text-center">STT</th>
                                                <th>Trang trại</th>
                                                <th class="text-center">Vụ cây trồng, vật nuôi - Giống</th>
                                                <th class="text-center">Tổng kinh phí dự kiến</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                    <?php
                                else:
                                    echo '<p>Bạn chưa có trang trại.</p>';
                                endif;
                                ?>
                            </div>
                        </div>
                        <div class="chart tab-pane" id="incidents-chart">
                            <div class="box-body">
                                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                                <?php if ($incidents) { ?>
                                    <ul class="todo-list">
                                        <?php
                                        $i = 0;
                                        foreach ($incidents as $val) {
                                            $i++;
                                            ?>
                                            <li>
                                                <!-- drag handle -->
                                                <span class="handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                <!-- checkbox -->
                                                <input type="checkbox" value="">
                                                <!-- todo text -->
                                                <span class="text"><?php echo $val->title; ?></span>
                                                <small style="margin-left: 20px"> - <i>Vụ cây trồng, vật nuôi: <?php echo $val->planting; ?></i> </small>
                                                <!-- Emphasis label -->
                                                <!-- General tools such as edit or delete-->
                                                <div class="tools">
                                                    <a href="<?php echo $this->config->config['base_url'] . "/incidents/edit/{$val->id}"; ?>" class="text-danger" title="Chỉnh sửa"><i class="fa fa-edit"></i></a>
                                                    <a href="<?php echo $this->config->config['base_url'] . "/incidents/delete/{$val->id}"; ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="text-danger" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <p>Không tìm thấy sự cố</p>
                                <?php } ?>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix no-border">
                                <a href="<?php echo $this->config->config['base_url'] . '/incidents/add'; ?>" class="btn btn-success btn-flat pull-right"><i class="fa fa-plus-circle"></i> Thêm sự cố mới</a>
                            </div>
                        </div>
                        <div class="chart tab-pane" id="diary-chart">
                            <div class="box-body">
                                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                                <?php if ($diaries) { ?>
                                    <ul class="todo-list">
                                        <?php
                                        $i = 0;
                                        foreach ($diaries as $val) {
                                            $i++;
                                            ?>
                                            <li>
                                                <!-- drag handle -->
                                                <span class="handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                <!-- checkbox -->
                                                <input type="checkbox" value="">
                                                <!-- todo text -->
                                                <span class="text"><?php echo $val->title; ?></span>
                                                <!-- Emphasis label -->
                                                <small class="label <?php echo ($i % 2 == 0) ? 'label-info' : 'label-danger'; ?>"><i class="fa fa-clock-o"></i> <?php echo date('H:i A - d/m/Y', strtotime($val->date)); ?></small>

                                                <small> - <i>Vụ cây trồng, vật nuôi: <?php echo $val->planting; ?></i> </small>
                                                <!-- General tools such as edit or delete-->
                                                <div class="tools">
                                                    <a href="<?php echo $this->config->config['base_url'] . "/diary/edit/{$val->id}"; ?>" class="text-danger" title="Chỉnh sửa"><i class="fa fa-edit"></i></a>
                                                    <a href="<?php echo $this->config->config['base_url'] . "/diary/delete/{$val->id}"; ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="text-danger" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                <?php } else { ?>
                                    <p>Không tìm thấy nhật ký xử lý</p>
                                <?php } ?>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer clearfix no-border">
                                <a href="<?php echo $this->config->config['base_url'] . '/diary/add'; ?>" class="btn btn-success btn-flat pull-right"><i class="fa fa-plus-circle"></i> Thêm nhật ký xử lý mới</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.nav-tabs-custom -->
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable">
                <div class="select-device">
                    
                    <form action="" method="post">                 
                        <select id="option-select-device" name="item-select-device" aria-controls="crops-lst" class="form-control item-select-device" onchange="this.form.submit()">
                        <?php 
                            if ($devices) { 
                                foreach ($devices as $val) { ?>
                                <option value='<?php echo $val->deviceid."---".$val->ipserver ?>' <?php if ($device[0] == $val->deviceid) echo "selected" ?> ><?= $val->deviceid ?></option>
                            <?php    
                                }
                            }
                        ?>               
                        </select>
                    </form>
                </div>
                <div id="load-chart-by-device">
                    <div class="chart-temperature">
                        <div id="chart_temperature">
                            <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/loading.gif" class="loading-chart">
                        </div>
                        <div class="txt-temperature">
                            Nhiệt độ hiện tại
                            <div id="current-temperature"></div> 
                        </div>
                    </div>
                    <div class="chart-humidity">
                        <div id="chart_humidity">
                            <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/loading.gif" class="loading-chart">
                        </div>
                        <div class="txt-humidity"> 
                            Độ ẩm hiện tại
                        <div id="current-humidity"></div>             
                        </div>       
                    </div>
                </div>        
                <!-- /.box -->
                <div class="btn-device">
                    <a href="/devicechart" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-info-circle"></i> Chi tiết</a>
                </div>
            </section>
            <!-- right col -->
            <div class="col-lg-12">
                   <!-- Map box -->
                <div class="box box-solid bg-light-blue-gradient" style="height: 400px;z-index: 999; margin-bottom: 100px">
                    <div id="map" class="farmlist" style="height: 100%;width: 100%"></div>
                    <!-- /.box-body-->
                </div>                  
            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
</div>
<!-- /.content-wrapper -->
<script>
    var locations = <?= json_encode($location) ?>;

    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: new google.maps.LatLng(locations[0][1], locations[0][2]),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });

        google.maps.event.addListener(marker, 'click', (function (marker, i) {
            return function () {
                infowindow.setContent(locations[i][0]);
                infowindow.open(map, marker);
            }
        })(marker, i));
    }

    // Begin Chart
    var mqtt;
    var reconnectTimeout = 2000;
    var host = <?php echo ($device) ? "'" . $device[1] . "'" : 'farmtech.ubddns.org'; ?>;
    var port = 8080;
    var topic = <?php echo ($device) ? "'" . $device[0] . "'" : 'LoRaGw/00ad02ffff008259/Node/76FE48FFFF000007/Val'; ?>;		// topic to subscribe to
    var useTLS = false;
    var username = null;
    var password = null;
    var cleansession = true;
    var selectedDevice = false;

    function MQTTconnect() {
	if (typeof path == "undefined") {
		path = '/mqtt';
	}
	mqtt = new Paho.MQTT.Client(
			host,
			port,
			path,
			"web_" + parseInt(Math.random() * 100, 10)
	);
        var options = {
            timeout: 3,
            useSSL: useTLS,
            cleanSession: cleansession,
            onSuccess: onConnect,
            onFailure: function (message) {
                $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
                setTimeout(MQTTconnect, reconnectTimeout);
            }
        };

        mqtt.onConnectionLost = onConnectionLost;
        mqtt.onMessageArrived = onMessageArrived;

        if (username != null) {
            options.userName = username;
            options.password = password;
        }
        mqtt.connect(options);
        console.log("Test " + selectedDevice);
    }

    function onConnect() {
        $('#status').val('Connected to ' + host + ':' + port + path);
        // Connection succeeded; subscribe to our topic
        mqtt.subscribe(topic, {qos: 0});
        $('#topic').val(topic);
    }

    function onConnectionLost(response) {
        setTimeout(MQTTconnect, reconnectTimeout);
        $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

    };

    function onMessageArrived(message) {
        var topic = message.destinationName;
        var payload = message.payloadString;
        var data = JSON.parse(payload);
        drawChartTemperature(data);
        drawChartHumidity(data);
    };

    // Gauge Chart for Temperature
    var dataGauge = [['Label', 'Value']];
    google.charts.load('current', {'packages':['gauge']});
    google.charts.setOnLoadCallback(drawChartTemperature);
    
    function drawChartTemperature(dataTemperature) {
        var chartGauge = new google.visualization.Gauge(document.getElementById('chart_temperature'));
        if (dataTemperature  && dataTemperature.payload_field) {
            var dt = dataTemperature.payload_field;
            for (i = 0; i < dt.length; i++) {
                var x = dt[i][2];
                if (x.indexOf('+') > -1) {
                    x = x.substr(1);
                    dataGauge.push([
                        '', parseFloat(x)
                    ]);
                    $('#current-temperature').html(x + '°C');
                }
            }
        } else if (dataTemperature && dataTemperature.Temp) {
            var x = dataTemperature.Temp[0];
            dataGauge.push([
                '', parseFloat(x)
            ]);
            $('#current-temperature').html(x + '°C');
        }
        var data = google.visualization.arrayToDataTable(dataGauge);

        var options = {
          width: 200, height: 168,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };
        chartGauge.draw(data, options);
        dataGauge = [['Label', 'Value']];
    }

    function drawChartHumidity(dataHumidity) {
        var chartGauge = new google.visualization.Gauge(document.getElementById('chart_humidity'));
        if (dataHumidity && dataHumidity.payload_field) {
            var dt = dataHumidity.payload_field;
            for (i = 0; i < dt.length; i++) {
                var y = dt[i][1];
                if (y.indexOf('Humidity') > -1){
                    dataGauge.push([
                        '', parseFloat(dt[i][2])
                    ]);
                    $('#current-humidity').html(dt[i][2] + '%');
                }
            }
        } else if (dataHumidity && dataHumidity.Hum) {
            var y = dataHumidity.Hum[0];
            dataGauge.push([
                '', parseFloat(y)
            ]);
            $('#current-humidity').html(y + '%');
        }
        var data = google.visualization.arrayToDataTable(dataGauge);

        var options = {
          width: 200, height: 168,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };
        chartGauge.draw(data, options);
        dataGauge = [['Label', 'Value']];
    }

    $(document).ready(function() {
        MQTTconnect();     
    });

</script>
