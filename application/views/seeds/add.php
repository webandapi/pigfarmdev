<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm giống cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/seeds'; ?>">Quản lý giống cây trồng, vật nuôi</a></li>
            <li class="active">Thêm giống cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/seeds'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 giống cây trồng, vật nuôi.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $this->load->view('message'); ?>
                        <div class="form-wrap farm-wrap seeds-wrap <?php echo $checkfull ? 'unvisibility' : ''; ?>">
                            <?php echo form_open(base_url('seeds/add'), 'id="frmaddseeds"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Tên giống cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Tên giống cây trồng, vật nuôi" />
                                    <?php echo form_error('name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-tree"></i> 
                                        <select class="form-control select2" name="crops">
                                            <option value="">-- Chọn cây trồng, vật nuôi --</option>
                                            <?php
                                            if ($crops):
                                                foreach ($crops as $c) :
                                                    printf('<option value="%d" %s>%s</option>', $c->id, set_select('crops', $c->id), $c->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('crops', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Trang trại <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-home"></i> 
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->id, set_select('farm', $f->id), $f->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="origin">Xuất xứ</label>
                                    <input type="text" class="form-control" id="origin" name="origin" value="<?php echo set_value('origin'); ?>" placeholder="Xuất xứ" />
                                </div>
                                <div class="form-group">
                                    <label for="date-of-purchase">Ngày mua</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="date_of_purchase" class="form-control pull-right datepicker" value="<?php echo set_value('date_of_purchase'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="expiration-date">Ngày hết hạn</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="expiration_date" class="form-control pull-right datepicker" value="<?php echo set_value('expiration_date'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="amount">Số lượng</label>
                                    <input type="text" class="form-control" id="amount" name="amount" value="<?php echo set_value('amount') ?>" placeholder="Số lượng">
                                </div>
                                <div class="form-group">
                                    <label for="seed-cost">Chi phí giống (1/ha) <i style="font-weight: normal">(vnđ)</i><i class="text-danger">(Chi phí tính cho 1 vụ trồng trọt hoặc nuôi)</i></label>
                                    <input type="text" class="form-control opeinput" id="seed-cost" name="seed_cost" value="<?php echo set_value('seed_cost'); ?>" placeholder="Chi phí giống (1/ha)">
                                </div>
                                <div class="form-group">
                                    <label for="pesticide_costs">Chi phí chăm sóc <i style="font-weight: normal">(vnđ)</i></label>
                                    <input type="text" class="form-control opeinput" id="pesticide-costs" name="pesticide_costs" value="<?php echo set_value('pesticide_costs'); ?>" placeholder="Chi phí chăm sóc">
                                </div>
                                <div class="form-group">
                                    <label for="fertilizer-cost">Chi phí nhân công <i style="font-weight: normal">(vnđ)</i></label>
                                    <input type="text" class="form-control opeinput" id="fertilizer-cost" name="fertilizer_cost" value="<?php echo set_value('fertilizer_cost') ?>" placeholder="Chi phí nhân công">
                                </div>
                                <div class="form-group">
                                    <label for="harvesting-costs">Chi phí thu hoạch <i style="font-weight: normal">(vnđ)</i></label>
                                    <input type="text" class="form-control opeinput" id="harvesting-costs" name="harvesting_costs" value="<?php echo set_value('harvesting_costs') ?>" placeholder="Chi phí thu hoạch">
                                </div>
                                <div class="form-group">
                                    <label for="expected-productivity">Năng suất kỳ vọng</label>
                                    <input type="text" class="form-control" id="expected-productivity" name="expected_productivity" value="<?php echo set_value('expected_productivity') ?>" placeholder="Năng suất kỳ vọng">
                                </div>
                                <h5 id="cost-message" class="text-success"></h5>
                                <input type="hidden" id="total-cost" name="total_cost" />
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
