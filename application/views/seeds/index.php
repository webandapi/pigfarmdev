<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-pagelines"></i> Quản lý giống cây trồng, vật nuôi</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý giống cây trồng, vật nuôi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php if (!$checkfull) { ?>
                        <div class="box-header">
                            <div class="form-group">
                                <a href="<?php echo $this->config->config['base_url'] . '/seeds/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm giống cây trồng, vật nuôi mới</a>
                            </div>
                            <div class="form-group">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 giống cây trồng, vật nuôi.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="box-filter">
                            <form id="frmfilter" method="get">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $val) :
                                                    printf('<option value="%d" %s>%s</option>', $val->id, ($_GET['farm'] == $val->id) ? 'selected="selected"' : '', $val->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="submit" class="btn btn-flat btn-success" value="Lọc giống cây trồng, vật nuôi" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="seeds-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên giống cây trồng, vật nuôi</th>
                                        <th>Xuất xứ</th>
                                        <th>Ngày mua</th>
                                        <th>Ngày hết hạn</th>
                                        <th>Số lượng</th>
                                        <th>Chi phí giống (vnđ)</th>
                                        <th>Chi phí chăm sóc (vnđ)</th>
                                        <th>Chi phí nhân công (vnđ)</th>
                                        <th>Chi phí thu hoạch (vnđ)</th>
                                        <th>Năng suất kì vọng</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($seeds):
                                        $i = 0;
                                        foreach ($seeds as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <?php echo $val->name; ?>
                                                    <p class="sub-item"><i class="fa fa-tree" title="Loại cây trồng"></i> <?php echo $val->crops; ?></p>
                                                    <p class="sub-item"><i class="fa fa-home" title="Trang trại"></i> <?php echo $val->farm; ?></p>
                                                </td>
                                                <td><?php echo $val->origin ? $val->origin : '_'; ?></td>
                                                <td><?php echo strtotime($val->date_of_purchase) ? date('d/m/Y', strtotime($val->date_of_purchase)) : '_'; ?></td>
                                                <td><?php echo strtotime($val->expiration_date) ? date('d/m/Y', strtotime($val->expiration_date)) : '_'; ?></td>
                                                <td><?php echo $val->amount ? number_format($val->amount, '0', ',', '.') : '_'; ?></td>
                                                <td><?php echo $val->seed_cost ? number_format($val->seed_cost, '0', ',', '.') : "_"; ?></td>
                                                <td><?php echo $val->pesticide_costs ? number_format($val->pesticide_costs, '0', ',', '.') : '_'; ?></td>
                                                <td><?php echo $val->fertilizer_cost ? number_format($val->fertilizer_cost, '0', ',', '.') : '_' ?></td>
                                                <td><?php echo $val->harvesting_costs ? number_format($val->harvesting_costs, '0', ',', '.') : '_'; ?></td>
                                                <td><?php echo $val->expected_productivity ? $val->expected_productivity : '_'; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/seeds/delete/{$val->id}"; ?>" onclick="return confirm('Một khi bạn đã xóa giống cây trồng, vật nuôi, tất cả thông tin liên quan đến giống cây trồng, vật nuôi này sẽ bị xóa hết. Bạn có chắc muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/seeds/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên giống cây trồng, vật nuôi</th>
                                        <th>Xuất xứ</th>
                                        <th>Ngày mua</th>
                                        <th>Ngày hết hạn</th>
                                        <th>Số lượng</th>
                                        <th>Chi phí giống</th>
                                        <th>Chi phí chăm sóc</th>
                                        <th>Chi phí nhân công</th>
                                        <th>Chi phí thu hoạch</th>
                                        <th>Năng suất kì vọng</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
