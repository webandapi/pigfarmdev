<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-pencil-square-o"></i> Cập nhật nhân viên</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/userfarm'; ?>">Quản lý nhân viên</a></li>
            <li class="active">Cập nhật nhân viên</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/userfarm'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/userfarm/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php if ($permission) { ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Access denied!</h4>
                                    <?php echo $permission; ?>
                                </div>
                            <?php } else { ?>
                                <?php echo form_open(base_url('userfarm/edit'), 'id="frmedituserfarm"', array('id' => $data->id)); ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="username">Tên đăng nhập <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="username" name="username" readonly value="<?php echo $data->username ? $data->username : set_value('username'); ?>" placeholder="Tên đăng nhập">
                                        <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <div class="form-group">
                                            <label for="fullname">Họ tên <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" id="fullname" name="fullname" value="<?php echo $data->fullname ? $data->fullname : set_value('fullname'); ?>" placeholder="Họ tên">
                                            <?php echo form_error('fullname', '<p class="error">', '</p>'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email <span class="text-danger">*</span></label>
                                        <input type="email" class="form-control" id="email" name="email" value="<?php echo $data->email ? $data->email : set_value('email'); ?>" placeholder="Email">
                                        <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="phone">Điện thoại</label>
                                        <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $data->phone ? $data->phone : set_value('phone') ?>" placeholder="Điện thoại">
                                    </div>
                                    <div class="form-group">
                                        <label>Trang trại <span class="text-danger">*</span></label>
                                        <a class="btn btn-block btn-social btn-google">
                                            <i class="fa fa-home"></i>
                                            <select class="form-control select2" name="farm">
                                                <option value="">-- Chọn trang trại --</option>
                                                <?php
                                                if ($farms):
                                                    foreach ($farms as $f) :
                                                        printf('<option value="%d" %s>%s</option>', $f->id, ($data->farm_id == $f->id) ? 'selected="selected"' : set_select('usertype', $f->id), $f->name);
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </a>
                                        <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Chức năng</label>
                                        <div class="modulelst">
                                            <?php
                                            if ($modules):
                                                $modules_in = $data->modules_in ? json_decode($data->modules_in) : array();
                                                foreach ($modules as $m) :
                                                    ?>
                                                    <label for="module-<?php echo $m->id; ?>"> 
                                                        <input type="checkbox" class="minimal-red" name="modules[]" id="module-<?php echo $m->id; ?>" value="<?php echo $m->id; ?>" <?php echo in_array($m->id, $modules_in) ? 'checked="checked"' : '' ?> />
                                                        <?php echo $m->name; ?>
                                                    </label>
                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group box-label">
                                        <label class="control-label">Trạng thái</label><br/>
                                        <label for="stt-actice">
                                            <input type="radio" name="status" id="stt-actice" class="minimal-red" value="1" <?php echo ($data->status == 1) ? 'checked="checked"' : set_radio('status', 1, TRUE); ?>> Kích hoạt
                                        </label>
                                        <label for="stt-notactice">
                                            <input type="radio" name="status" id="stt-notactice" class="minimal-red" value="-1" <?php echo ($data->status == -1) ? 'checked="checked"' : set_radio('status', -1); ?>> Không kích hoạt
                                        </label>
                                    </div>
                                    <div class="form-group box-label">
                                        <label class="control-label">Giới tính</label><br/>
                                        <label for="male">
                                            <input type="radio" name="gender" id="male" name="gender" class="minimal-red" value="1" <?php echo ($data->gender == 1) ? 'checked="checked"' : set_radio('gender', '1', TRUE); ?>> Nam
                                        </label>
                                        <label for="female">
                                            <input type="radio" name="gender" id="female" name="gender" class="minimal-red" value="0" <?php echo ($data->gender == 0) ? 'checked="checked"' : set_radio('gender', '0'); ?>> Nữ
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label>Ngày sinh:</label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="birthday" class="form-control pull-right" value="<?php echo $data->date_of_birth ? date('d/m/Y', strtotime($data->date_of_birth)) : set_value('birthday'); ?>" id="datepicker">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <?php
                                //box-footer
                                echo'<div class="box-footer">';
                                echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                                echo '<a href="' . $this->config->config['base_url'] . '/userfarm/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                                echo '</div>';
                                ?>
                                <?php echo form_close(); ?>
                            <?php } ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
