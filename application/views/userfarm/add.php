<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm nhân viên</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/userfarm'; ?>">Quản lý nhân viên</a></li>
            <li class="active">Thêm nhân viên</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/userfarm'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 nhân viên.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php $this->load->view('message'); ?>
                        <div class="form-wrap farm-wrap <?php echo $checkfull ? 'unvisibility' : ''; ?>">
                            <?php echo form_open(base_url('userfarm/add'), 'id="frmadduserfarm"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="username">Tên đăng nhập <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="Tên đăng nhập">
                                    <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-5">
                                        <label for="password">Mật khẩu <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Mật khẩu">
                                        <?php echo form_error('password', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="col-xs-7">
                                        <label for="cf-password">Nhập lại mật khẩu <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="cf-password" name="cf_password" value="<?php echo set_value('cf_password'); ?>" placeholder="Nhập lại mật khẩu">
                                        <?php echo form_error('cf_password', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="fullname">Họ tên <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="fullname" name="fullname" value="<?php echo set_value('fullname'); ?>" placeholder="Họ tên">
                                    <?php echo form_error('fullname', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email">
                                    <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Điện thoại</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone') ?>" placeholder="Điện thoại">
                                </div>
                                <div class="form-group">
                                    <label>Trang trại <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-home"></i>
                                        <select class="form-control select2" name="farm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($farms):
                                                foreach ($farms as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->id, set_select('usertype', $f->id), $f->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('farm', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Chức năng</label>
                                    <div class="modulelst">
                                        <?php
                                        if ($modules):
                                            foreach ($modules as $m) :
                                                ?>
                                                <label for="module-<?php echo $m->id; ?>"> 
                                                    <input type="checkbox" class="minimal-red" name="modules[]" id="module-<?php echo $m->id; ?>" value="<?php echo $m->id; ?>" />
                                                    <?php echo $m->name; ?>
                                                </label>
                                                <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Trạng thái</label><br/>
                                    <label for="stt-actice">
                                        <input type="radio" name="status" id="stt-actice" name="status" class="minimal-red" value="1" <?php echo set_radio('status', 1, TRUE); ?>> Kích hoạt
                                    </label>
                                    <label for="stt-notactice">
                                        <input type="radio" name="status" id="stt-notactice" name="status" class="minimal-red" value="0" <?php echo set_radio('status', 0); ?>> Không kích hoạt
                                    </label>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Giới tính</label><br/>
                                    <label for="male">
                                        <input type="radio" name="gender" id="male" name="gender" class="minimal-red" value="1" <?php echo set_radio('gender', '1', TRUE); ?>> Nam
                                    </label>
                                    <label for="female">
                                        <input type="radio" name="gender" id="female" name="gender" class="minimal-red" value="0" <?php echo set_radio('gender', '0'); ?>> Nữ
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="birthday" class="form-control pull-right" value="<?php echo set_value('birthday'); ?>" id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
