<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-users"></i> Quản lý nhân viên</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý nhân viên</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <?php if (!$checkfull) { ?>
                        <div class="box-header">
                            <div class="form-group">
                                <a href="<?php echo $this->config->config['base_url'] . '/userfarm/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm nhân viên mới</a>
                            </div>
                            <div class="form-group">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php if ($checkfull) { ?>
                            <div class="info-box bg-red">
                                <span class="info-box-icon"><i class="fa fa-warning"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Thông báo !</span>
                                    <span class="info-box-number text-uppercase">Bạn đang ở chế độ dùng thử ứng dụng quản lý trang trại</span>

                                    <div class="progress">
                                        <div class="progress-bar" style="width: 70%"></div>
                                    </div>
                                    <span class="progress-description">
                                        Tài khoản của bạn chỉ được phép tạo tối đa 1 nhân viên.
                                    </span>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="table-responsive">
                            <table id="employees-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên đăng nhập</th>
                                        <th>Họ tên</th>
                                        <th>Email</th>
                                        <th class="text-center">Điện thoại</th>
                                        <th class="text-center">Giới tính</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Ngày đăng ký</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($users):
                                        $i = 0;
                                        foreach ($users as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $val->username; ?></td>
                                                <td><?php echo $val->fullname; ?></td>
                                                <td><?php echo $val->email ? "<a href='mailto:{$val->email}'>{$val->email}</a>" : '_'; ?></td>
                                                <td class="text-center"><?php echo $val->phone ? $val->phone : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td class="text-center"><?php echo (!isset($val->gender)) ? "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>" : ($val->gender == 1 ? "<a title='Nam'><img src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                <td class="text-center"><?php echo ($val->status == 1 || $val->status == 0) ? "<a title='Kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/checked.png' /></a>" : "<a title='Chưa kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/unchecked.png' /></a>"; ?></td>
                                                <td><?php echo $val->created ? date('d/m/Y', strtotime($val->created)) : '_'; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/userfarm/delete/{$val->id}"; ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/userfarm/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên đăng nhập</th>
                                        <th>Họ tên</th>
                                        <th>Email</th>
                                        <th class="text-center">Điện thoại</th>
                                        <th class="text-center">Giới tính</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Ngày đăng ký</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
