<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-sticky-note"></i> Quản lý sự cố</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý sự cố</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <a href="<?php echo $this->config->config['base_url'] . '/incidents/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm sự cố mới</a>
                        </div>
                        <div class="form-group">
                            <?php $this->load->view('message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="incidents-lst" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tiêu đề</th>
                                        <th>Nội dung</th>
                                        <th>Thiệt hại</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($incidents):
                                        $i = 0;
                                        foreach ($incidents as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td>
                                                    <?php echo $val->title; ?>
                                                    <p class="sub-item">Vụ cây trồng, vật nuôi: <?php echo $val->planting; ?></p>
                                                </td>
                                                <td><?php echo $val->content; ?></td>
                                                <td><?php echo $val->damages ? $val->damages : "_"; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/incidents/delete/{$val->id}"; ?>" onclick="return confirm('Bạn có chắc muốn xóa?');" class="btn btn-danger btn-flat btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/incidents/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tiêu đề</th>
                                        <th>Nội dung</th>
                                        <th>Thiệt hại</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
