<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-pencil-square-o"></i> Cập nhật sự cố</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>">Quản lý sự cố</a></li>
            <li class="active">Cập nhật sự cố</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/incidents/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php if ($permission) { ?>
                                <div class="alert alert-danger alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> Access denied!</h4>
                                    <?php echo $permission; ?>
                                </div>
                            <?php } else { ?>
                                <?php echo form_open(base_url('incidents/edit'), 'id="frmeditincidents"', array('id' => $data->id)); ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Tiêu đề <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="title" name="title" value="<?php echo $data->title ? $data->title : set_value('title'); ?>" placeholder="Tên sự cố" />
                                        <?php echo form_error('title', '<p class="error">', '</p>'); ?>
                                    </div> 
                                    <div class="form-group">
                                        <label for="content">Nội dung <span class="text-danger">*</span></label>
                                        <?php
                                        $content = array(
                                            'name' => 'content',
                                            'id' => 'content',
                                            'class' => 'ckeditor',
                                            'value' => $data->content ? $data->content : set_value('content')
                                        );
                                        echo form_textarea($content);
                                        echo form_error('content', '<p class="error">', '</p>');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="damages">Thiệt hại</label>
                                        <?php
                                        $damages = array(
                                            'name' => 'damages',
                                            'id' => 'damages',
                                            'class' => 'ckeditor',
                                            'value' => $data->damages ? $data->damages : set_value('damages')
                                        );
                                        echo form_textarea($damages);
                                        echo form_error('damages', '<p class="error">', '</p>');
                                        ?>
                                    </div>
                                    <div class="form-group">
                                        <label>Vụ cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                        <a class="btn btn-block btn-social btn-google">
                                            <i class="fa fa-product-hunt"></i> 
                                            <select class="form-control select2" name="planting">
                                                <option value="">-- Chọn vụ cây trồng, vật nuôi --</option>
                                                <?php
                                                if ($planting):
                                                    foreach ($planting as $p) :
                                                        printf('<option value="%d" %s>%s</option>', $p->id, ($data->planting_id == $p->id) ? 'selected="selected"' : set_select('planting', $p->id), $p->name);
                                                    endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </a>
                                        <?php echo form_error('planting', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->

                                <?php
                                //box-footer
                                echo'<div class="box-footer">';
                                echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
//                                echo '<a href="' . $this->config->config['base_url'] . '/incidents/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                                echo '</div>';
                                ?>
                                <?php echo form_close(); ?>
                            <?php } ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
