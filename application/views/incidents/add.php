<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm sự cố</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>">Quản lý sự cố</a></li>
            <li class="active">Thêm sự cố</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php echo form_open(base_url('incidents/add'), 'id="frmaddincidents"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title">Tiêu đề <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="title" name="title" value="<?php echo set_value('title'); ?>" placeholder="Tên sự cố" />
                                    <?php echo form_error('title', '<p class="error">', '</p>'); ?>
                                </div> 
                                <div class="form-group">
                                    <label for="content">Nội dung <span class="text-danger">*</span></label>
                                    <?php
                                    $content = array(
                                        'name' => 'content',
                                        'id' => 'content',
                                        'class' => 'ckeditor',
                                        'value' => set_value('content')
                                    );
                                    echo form_textarea($content);
                                    echo form_error('content', '<p class="error">', '</p>');
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label for="damages">Thiệt hại</label>
                                    <?php
                                    $damages = array(
                                        'name' => 'damages',
                                        'id' => 'damages',
                                        'class' => 'ckeditor',
                                        'value' => set_value('damages')
                                    );
                                    echo form_textarea($damages);
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label>Vụ cây trồng, vật nuôi <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-product-hunt"></i> 
                                        <select class="form-control select2" name="planting">
                                            <option value="">-- Chọn vụ cây trồng, vật nuôi --</option>
                                            <?php
                                            if ($planting):
                                                foreach ($planting as $p) :
                                                    printf('<option value="%d" %s>%s</option>', $p->id, set_select('planting', $p->id), $p->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('planting', '<p class="error">', '</p>'); ?>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
