<div class="container-fluid">
        <div class="row section-box">
            <div class="col-sm-4 col-sm-offset-4 txt-register"> Đăng Ký </div>
            <div class="col-sm-4 col-sm-offset-4">
                <div class="box box-info">
                    <div class="ft-logo">
                        <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="Logo" />
                    </div>

                    <div class="box-body">
                        <?php echo form_open(base_url().'register', 'id="frmRegister"'); ?>
                            <?php if ($this->session->flashdata('failed')) { ?>
                                <div class="form-group">
                                    <p class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-warning"></i>
                                        <span><?php echo $this->session->flashdata('failed'); ?></span>
                                    </p>
                                </div>
                            <?php } ?>
                            <div class="form-group input-group group-yourname">
                                <input type="text" class="form-control txt-lastname" name="lastname" placeholder="Họ">
                                <input type="text" class="form-control txt-firstname" name="firstname" placeholder="Tên">
                                <?php echo form_error('lastname', '<p class="error">', '</p>'); ?>
                                <?php echo form_error('firstname', '<p class="error">', '</p>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="text" id="username" name="username" class="form-control" placeholder="Tên đăng nhập" value="" >
                                <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="pw" id="pw" placeholder="Mật Khẩu">
                                <?php echo form_error('pw', '<p class="error">', '</p>'); ?>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" name="pw-confirm" id="pw-password" placeholder="Nhập Lại Mật Khẩu">
                                <?php echo form_error('pw-confirm', '<p class="error">', '</p>'); ?>
                            </div>
                            <?php echo form_submit('submit', 'Đăng ký', array('class' => 'btn btn-register btn-primary btn-lg btn-block')); ?>
                        <?php echo form_close(); ?>
                        <div class="other-register">
                            <div class="txt-other">Hoặc đăng ký với</div>
                            <div class="btn-other">
                                    <a href="#" title="Facebook" class="btn btn-facebook btn-lg"><i class="fa fa-facebook fa-fw"></i> Facebook</a>
                                    <a href="#" title="Google+" class="btn btn-googleplus btn-lg"><i class="fa fa-google-plus fa-fw"></i> Google+</a>
                            </div>
                            <div class="account-exists">Đã có tài khoản <a href="/">Đăng Nhập</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
    </div>