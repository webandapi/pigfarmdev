<div class="container-fluid">
    <div class="row section-box">
        <div class="col-sm-4 col-sm-offset-4 txt-register"> Xác Nhận Email </div>
        <div class="col-sm-4 col-sm-offset-4">
            <div class="box box-info">
                <div class="verify-email-icon">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/icon-verify.png" alt="Logo"> 
                </div>

                <div class="box-body">
                    <div class="txt-verify"> Cảm ơn bạn đã tạo tài khoản Fman. Xin hãy xác nhận email để hoàn tất việc đăng ký.</div>
                    <div class="account-exists">Đã có tài khoản <a href="/">Đăng Nhập</a></div>
                </div>
            </div>
        </div>
    </div>       
</div>