<!-- Content Wrapper. Contains page content -->
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="ft-logo">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="Logo" /> 
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title text-success">Lấy lại mật khẩu</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php echo form_open(base_url(), 'id="frmforgotpass"'); ?>
                    <form id="frmadministratorlogin" method="POST">
                        <?php if ($this->session->flashdata('reponse')) { ?>
                            <div class="form-group">
                                <p class="btn btn-block btn-social btn-google">
                                    <i class="fa fa-warning"></i>
                                    <span><?php echo $this->session->flashdata('reponse'); ?></span>
                                </p>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </div>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Nhập email để lấy lại mật khẩu" value="<?php echo set_value('email'); ?>" >
                            </div>
                            <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo form_submit('submit', 'Gửi', array('class' => 'btn btn-success btn-flat pull-right')); ?>
                        </div>
                        <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>