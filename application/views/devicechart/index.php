<div class="content-wrapper" style="min-height: 601.594px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
       <div class="row">
            <div class="col-lg-3 txt-header">Trang trại</div>
            <div class="col-lg-3 txt-header">Thiết bị</div>
            <div class="col-lg-6"></div>
        </div>
        <div class="row">
            <form action="" method="post">
                <div class="col-lg-3">
                    <select id="option-select-farm" name="item-farm" aria-controls="crops-lst" class="form-control item-select-device">
                    <?php 
                        if ($farms) { 
                            foreach ($farms as $val) { ?>
                            <option value='<?php echo $val->id ?>' <?php //if ($device[0] == $val->deviceid) echo "selected" ?> ><?= $val->name ?></option>
                        <?php    
                            }
                        }
                    ?>               
                    </select>
                </div>
                <div class="col-lg-3">
                    <select id="option-select-device" name="item-select-device" aria-controls="crops-lst" class="form-control item-select-device" onchange="this.form.submit()">
                    <?php 
                        if ($devices) { 
                            foreach ($devices as $val) { ?>
                            <option value='<?php echo $val->deviceid."---".$val->ipserver ?>' <?php if ($device[0] == $val->deviceid) echo "selected" ?> ><?= $val->deviceid ?></option>
                        <?php    
                            }
                        }
                    ?>               
                    </select>
                </div>
                <div class="col-lg-6 link-header">
                    <a href="/devicechart/detail"> Xem lịch sử thiết bị </a> | <a href="/devicechart/detail"> Xem lịch sử trong vòng 24h </a></div>
            </form>
       </div>
       <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/header-1-enviroment.png">
    </section>

    <!-- Main content -->
    <section class="content">
 
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <section class="col-lg-9 connectedSortable ui-sortable">
                <div class="enviroment-chart-temperature">
                    <div class="txt-chart-sin-temperature">Nhiệt Độ</div>
                    <div id="temperature_sin_chart">
                        <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/loading.gif" class="loading-chart">
                    </div>
                </div>
                <div class="enviroment-chart-humidity">
                    <div class="txt-chart-sin-humidity">Độ Ẩm</div>
                    <div id="humidity_sin_chart">
                        <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/loading.gif" class="loading-chart">
                    </div>
                </div>
            </section>
            <!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-3 connectedSortable ui-sortable">
                <div class="enviroment-chart-temperature">
                    <div class="txt-chart-view">
                        Nhiệt độ hiện tại
                    </div>
                    <div id="chart_temperature_view">
                        <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/loading.gif" class="loading-chart">
                    </div>
                </div>
                <div class="enviroment-chart-humidity">
                    <div class="txt-chart-view"> 
                        Độ ẩm hiện tại  
                    </div> 
                    <div id="chart_humidity_view">
                        <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/loading.gif" class="loading-chart">
                    </div>
                </div>
            </section>
            <!-- right col -->
        </div>
        <!-- /.row (main row) -->

    </section>
</div>

<script>
    // Begin Chart
    var mqtt;
    var reconnectTimeout = 2000;
    var host = <?php echo ($device) ? "'" . $device[1] . "'" : 'farmtech.ubddns.org'; ?>;
    var port = 8080;
    var topic = <?php echo ($device) ? "'" . $device[0] . "'" : 'LoRaGw/00ad02ffff008259/Node/76FE48FFFF000007/Val'; ?>;		// topic to subscribe to
    var useTLS = false;
    var username = null;
    var password = null;
    var cleansession = true;

    function MQTTconnect() {
	if (typeof path == "undefined") {
		path = '/mqtt';
	}
	mqtt = new Paho.MQTT.Client(
			host,
			port,
			path,
			"web_" + parseInt(Math.random() * 100, 10)
	);
        var options = {
            timeout: 3,
            useSSL: useTLS,
            cleanSession: cleansession,
            onSuccess: onConnect,
            onFailure: function (message) {
                $('#status').val("Connection failed: " + message.errorMessage + "Retrying");
                setTimeout(MQTTconnect, reconnectTimeout);
            }
        };

        mqtt.onConnectionLost = onConnectionLost;
        mqtt.onMessageArrived = onMessageArrived;

        if (username != null) {
            options.userName = username;
            options.password = password;
        }
        mqtt.connect(options);
    }

    function onConnect() {
        $('#status').val('Connected to ' + host + ':' + port + path);
        // Connection succeeded; subscribe to our topic
        mqtt.subscribe(topic, {qos: 0});
        $('#topic').val(topic);
    }

    function onConnectionLost(response) {
        setTimeout(MQTTconnect, reconnectTimeout);
        $('#status').val("connection lost: " + responseObject.errorMessage + ". Reconnecting");

    };

    function onMessageArrived(message) {

        var topic = message.destinationName;
        var payload = message.payloadString;
        var data = JSON.parse(payload);
        drawChartTemperatureSin(data);
        drawChartHumiditySin(data);
        drawChartTemperature(data);
        drawChartHumidity(data);
    };

       // Chart for Temperature
       var dataPoints = [['Time', 'Temperature']];

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChartTemperatureSin);

    function drawChartTemperatureSin(data) {
        var d = new Date();
        var timeChart = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        if (data && data.payload_field) {
            var dt = data.payload_field;
            for (i = 0; i < dt.length; i++) {
                var x = dt[i][2];
                if (x.indexOf('+') > -1){
                    x = x.substr(1);
                    dataPoints.push([
                        timeChart, parseFloat(x)
                    ]);
                }   
            } 
            var dataChart = google.visualization.arrayToDataTable(dataPoints);
            var options = {
                curveType: 'function',
                legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('temperature_sin_chart'));
            chart.draw(dataChart, options); 
        } else if (data && data.Temp) {
            var x = data.Temp[0];
            dataPoints.push([
                    timeChart, parseFloat(x)
            ]); 
            var dataChart = google.visualization.arrayToDataTable(dataPoints);
            var options = {
                curveType: 'function',
                legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('temperature_sin_chart'));
            chart.draw(dataChart, options);
        }
    }

    // Chart for Humidity
    var dataPointsHumidity = [['Time', 'Humidity']];

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChartHumiditySin);

    function drawChartHumiditySin(data) {
        var d = new Date();
        var timeChart = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
        if (data && data.payload_field) {
            var dt = data.payload_field;
            for (i = 0; i < dt.length; i++) {
                var x = dt[i][1];
                if (x.indexOf('Humidity') > -1){
                    dataPointsHumidity.push([
                        timeChart, parseFloat(dt[i][2])
                    ]);
                }   
            }
            var dataChart = google.visualization.arrayToDataTable(dataPointsHumidity);
            var options = {
                curveType: 'function',
                legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('humidity_sin_chart'));
            chart.draw(dataChart, options);
        } else if (data && data.Hum) {
            var x = data.Hum[0];
            dataPointsHumidity.push([
                timeChart, parseFloat(x)
            ]); 
            var dataChart = google.visualization.arrayToDataTable(dataPointsHumidity);
            var options = {
                curveType: 'function',
                legend: { position: 'bottom' }
            };

            var chart = new google.visualization.LineChart(document.getElementById('humidity_sin_chart'));
            chart.draw(dataChart, options);
        }
    }

    // Gauge Chart for Temperature
    var dataGauge = [['Label', 'Value']];
    google.charts.load('current', {'packages':['gauge']});
    google.charts.setOnLoadCallback(drawChartTemperature);
    
    function drawChartTemperature(dataTemperature) {
        var chartGauge = new google.visualization.Gauge(document.getElementById('chart_temperature_view'));
        if (dataTemperature  && dataTemperature.payload_field) {
            var dt = dataTemperature.payload_field;
            for (i = 0; i < dt.length; i++) {
                var x = dt[i][2];
                if (x.indexOf('+') > -1) {
                    x = x.substr(1);
                    dataGauge.push([
                        '', parseFloat(x)
                    ]);
                }
            }
        } else if (dataTemperature && dataTemperature.Temp) {
            var x = dataTemperature.Temp[0];
            dataGauge.push([
                '', parseFloat(x)
            ]);
        }
        var data = google.visualization.arrayToDataTable(dataGauge);

        var options = {
          width: 250, height: 200,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };
        chartGauge.draw(data, options);
        dataGauge = [['Label', 'Value']];
    }

    function drawChartHumidity(dataHumidity) {
        var chartGauge = new google.visualization.Gauge(document.getElementById('chart_humidity_view'));
        if (dataHumidity && dataHumidity.payload_field) {
            var dt = dataHumidity.payload_field;
            for (i = 0; i < dt.length; i++) {
                var y = dt[i][1];
                if (y.indexOf('Humidity') > -1){
                    dataGauge.push([
                        '', parseFloat(dt[i][2])
                    ]);
                }
            }
        } else if (dataHumidity && dataHumidity.Hum) {
            var y = dataHumidity.Hum[0];
            dataGauge.push([
                '', parseFloat(y)
            ]);
        }
        var data = google.visualization.arrayToDataTable(dataGauge);

        var options = {
          width: 250, height: 200,
          redFrom: 90, redTo: 100,
          yellowFrom:75, yellowTo: 90,
          minorTicks: 5
        };
        chartGauge.draw(data, options);
        dataGauge = [['Label', 'Value']];
    }

    $(document).ready(function() {
        MQTTconnect();
    });

</script>