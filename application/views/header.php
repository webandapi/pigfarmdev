<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $pageTitle; ?> | FARMTECH VIETNAM.,JSC</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

        <link href="<?php echo $this->config->config['assets_path']; ?>/images/icons/favicon.png" rel="shortcut icon">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/Ionicons/css/ionicons.min.css">

        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-daterangepicker/daterangepicker.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/plugins/iCheck/all.css">

        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/datatables.net-bs/css/dataTables.bootstrap.min.css">

        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/library/select2/dist/css/select2.min.css">

        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/plugins/timepicker/bootstrap-timepicker.min.css">

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/css/AdminLTE.min.css">

        <!-- my-styles -->
        <link rel="stylesheet" href="<?php echo $this->config->config['assets_path']; ?>/css/mystyles.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH7bEOG4frBd5rjTaBiVMBZD-nD4bF6YA&language=vi"></script>
        <!-- Begin Chart -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/mqttws31.js" type="text/javascript"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo $this->config->config['base_url']; ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>DB</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Fman Cpanel</b></span>
                </a>
                <!--Nav at here-->
                <?php $this->load->view('user-nav'); ?>
            </header>
            <!-- Left side column. contains the logo and sidebar -->


            <!--Sidebar at here-->
            <?php $this->load->view('sidebar'); ?>