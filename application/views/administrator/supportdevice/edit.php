<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Cập nhật thiết bị</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/supportdevice'; ?>">Quản lý thiết bị</a></li>
            <li class="active">Cập nhật thiết bị</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/supportdevice'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open(base_url('administrator/supportdevice/edit'), 'id="frmeditsupportdevice"', array('id' => $data->id)); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="deviceid">Tên thiết bị <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="deviceid" name="deviceid" value="<?php echo $data->deviceid ? $data->deviceid : set_value('deviceid'); ?>" placeholder="Tên thiết bị">
                                    <?php echo form_error('deviceid', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="ipserver">Địa chỉ máy chủ <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="ipserver" name="ipserver" value="<?php echo $data->ipserver ? $data->ipserver : set_value('ipserver'); ?>" placeholder="Địa chỉ máy chủ">
                                    <?php echo form_error('ipserver', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Người hỗ trợ <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-user-circle"></i> 
                                        <select class="form-control select2" name="idsupport">
                                            <option value="">-- Chọn người hỗ trợ --</option>
                                            <?php
                                            if ($entry['supporter']):
                                                foreach ($entry['supporter'] as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->id, ($data->idsupport == $f->id) ? 'selected="selected"' : set_select('idsupport', $f->id), $f->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('idsupport', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Trang trại <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-cog"></i> 
                                        <select class="form-control select2" name="idfarm">
                                            <option value="">-- Chọn trang trại --</option>
                                            <?php
                                            if ($entry['farms']):
                                                foreach ($entry['farms'] as $ft) :
                                                    printf('<option value="%d" %s>%s</option>', $ft->id, ($data->idfarm == $ft->id) ? 'selected="selected"' : set_select('idfarm', $ft->id), $ft->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('idfarm', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                <label for="ipserver">Username</label>
                                <input type="text" class="form-control" id="username" name="username" value="<?php echo $data->username ? $data->username : set_value('username'); ?>" placeholder="Username">
                            </div>

                            <div class="form-group">
                                <label for="ipserver">Mật khẩu</label>
                                <input type="password" class="form-control" id="password" name="password" readonly value="<?php echo $data->password ? $data->password : set_value('password'); ?>" placeholder="Mật khẩu">
                            </div>

                            <div class="form-group">
                                <label for="ipserver">User MySQL</label>
                                <input type="text" class="form-control" id="usersql" name="usersql" value="<?php echo $data->usersql ? $data->usersql : set_value('usersql'); ?>" placeholder="User MySQL">
                            </div>

                            <div class="form-group">
                                <label for="ipserver">Mật khẩu MySQL</label>
                                <input type="password" class="form-control" id="passsql" name="passsql" readonly value="<?php echo $data->passsql ? $data->passsql : set_value('passsql'); ?>" placeholder="Mật khẩu MySQL">
                            </div>

                            <div class="form-group box-label">
                                <label class="control-label">Device Mặc định</label><br/>
                                <label for="default">
                                    <input type="radio" name="isdefault" id="default" name="isdefault" class="minimal-red" value="1" <?php echo ($data->isdefault == 1) ? 'checked="checked"' : set_radio('isdefault', '1', TRUE); ?> > Đúng
                                </label>
                                <label for="undefault">
                                    <input type="radio" name="isdefault" id="undefault" name="isdefault" class="minimal-red" value="0" <?php echo ($data->isdefault == 0) ? 'checked="checked"' : set_radio('isdefault', '0'); ?>> Sai
                                </label>
                            </div>

                            <div class="form-group">
                                <label>Ngày kích hoạt:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="dateactive" class="form-control pull-right" value="<?php echo $data->dateactive ? date('d/m/Y', strtotime($data->dateactive)) : set_value('dateactive'); ?>" id="datepicker">
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Thời gian bảo hành:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="warantytime" class="form-control pull-right" value="<?php echo $data->warantytime ? date('d/m/Y', strtotime($data->warantytime)) : set_value('warantytime'); ?>" id="datepicker1">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                                //box-footer
                                echo'<div class="box-footer">';
                                echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                                echo '<a href="' . $this->config->config['base_url'] . '/administrator/supportdevice/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                                echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
