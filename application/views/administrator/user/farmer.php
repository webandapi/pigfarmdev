<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-users"></i> Quản lý nông dân</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý nông dân</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/user/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm nông dân mới</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/user/export'; ?>" class="btn btn-flat btn-warning btn-backward"><i class="fa fa-align-justify"></i> Export</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <?php $this->load->view('administrator/message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-filter">
                            <form id="frmfilter" method="get">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="status">
                                            <option value="">-- Chọn trạng thái --</option>
                                            <option value="1" <?php echo ($_GET['status'] == 1) ? 'selected="selected"' : ''; ?>>Kích hoạt</option>
                                            <option value="-1" <?php echo ($_GET['status'] == -1) ? 'selected="selected"' : ''; ?>>Chưa kích hoạt</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="flags">
                                            <option value="">-- Dùng thử admin --</option>
                                            <option value="1" <?php echo ($_GET['flags'] == '1') ? 'selected="selected"' : ''; ?>>Full</option>
                                            <option value="0" <?php echo ($_GET['flags'] == '0') ? 'selected="selected"' : ''; ?>>Dùng thử</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <button type="button" class="btn btn-info btn-flat" id="daterange-btn">
                                                <span>
                                                    <i class="fa fa-calendar"></i> <?php echo $_GET['date'] ? $_GET['date'] : 'Chọn thời gian'; ?>
                                                </span>
                                                <input type="hidden" name="date" value="" />
                                                <i class="fa fa-caret-down"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="submit" class="btn btn-flat btn-success" value="Lọc nông dân" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="farmers-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên đăng nhập</th>
                                        <th>Họ tên</th>
                                        <th>Email</th>
                                        <th class="text-center">Điện thoại</th>
                                        <th class="text-center">Giới tính</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Dùng thử admin</th>
                                        <th>Ngày đăng ký</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($farmers):
                                        $i = 0;
                                        foreach ($farmers as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $val->username; ?></td>
                                                <td><?php echo ($val->lastname || $val->firstname) ? $val->lastname . ' ' . $val->firstname : "_"; ?></td>
                                                <td><?php echo $val->email ? "<a href='mailto:{$val->email}'>{$val->email}</a>" : "_"; ?></td>
                                                <td class="text-center"><?php echo $val->phone ? $val->phone : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td class="text-center"><?php echo (!isset($val->gender)) ? "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>" : ($val->gender == 1 ? "<a title='Nam'><img src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                <td class="text-center"><?php echo ($val->status == 1 || $val->status == 0) ? "<a title='Kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/checked.png' /></a>" : "<a title='Chưa kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/unchecked.png' /></a>"; ?></td>
                                                <td><?php echo $val->flags == '1' ? 'Full' : 'Dùng thử'; ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($val->created)); ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/administrator/user/delete/{$val->user_id}"; ?>" class="btn btn-danger btn-sm" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/administrator/user/edit/{$val->user_id}"; ?>" class="btn btn-success btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên đăng nhập</th>
                                        <th>Họ tên</th>
                                        <th>Email</th>
                                        <th class="text-center">Điện thoại</th>
                                        <th class="text-center">Giới tính</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Dùng thử admin</th>
                                        <th>Ngày đăng ký</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
