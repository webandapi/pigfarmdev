<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm người dùng</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/user'; ?>">Quản lý người dùng</a></li>
            <li class="active">Thêm người dùng</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/user'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open(base_url('administrator/user/add'), 'id="frmadduser"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="username">Tên đăng nhập <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo set_value('username'); ?>" placeholder="Tên đăng nhập">
                                    <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-5">
                                        <label for="password">Mật khẩu <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password'); ?>" placeholder="Mật khẩu">
                                        <?php echo form_error('password', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="col-xs-7">
                                        <label for="cf-password">Nhập lại mật khẩu <span class="text-danger">*</span></label>
                                        <input type="password" class="form-control" id="cf-password" name="cf_password" value="<?php echo set_value('cf_password'); ?>" placeholder="Nhập lại mật khẩu">
                                        <?php echo form_error('cf_password', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-5">
                                        <label for="lastname">Họ</label>
                                        <input type="text" class="form-control" id="lastname" name="lastname" value="<?php echo set_value('lastname'); ?>" placeholder="Họ">
                                    </div>
                                    <div class="col-xs-7">
                                        <label for="firstname">Tên</label>
                                        <input type="text" class="form-control" id="firstname" name="firstname" value="<?php echo set_value('firstname'); ?>" placeholder="Tên">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address">Địa chỉ</label>
                                    <input type="text" class="form-control" id="address" name="address" value="<?php echo set_value('address'); ?>" placeholder="Địa chỉ">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email">
                                    <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Điện thoại</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone') ?>" placeholder="Điện thoại">
                                </div>
                                <div class="form-group">
                                    <label>Loại người dùng <span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="usertype">
                                        <option value="">-- Chọn loại người dùng --</option>
                                        <?php
                                        if ($usertype):
                                            foreach ($usertype as $type) :
                                                printf('<option value="%d" %s>%s</option>', $type->id, set_select('usertype', $type->id), $type->name);
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                    <?php echo form_error('usertype', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Lĩnh vực</label>
                                    <select class="form-control select2" multiple="multiple" data-live-search="true" name="userfield[]">
                                        <option value="">-- Chọn lĩnh vực --</option>
                                        <?php
                                        if ($userfield):
                                            foreach ($userfield as $val) :
                                                printf('<option value="%d" %s>%s</option>', $val->id, set_select('userfield', $val->id), $val->name);
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Trạng thái</label><br/>
                                    <label for="stt-actice">
                                        <input type="radio" name="status" id="stt-actice" class="minimal-red" value="1" <?php echo set_radio('status', 1, TRUE); ?>> Kích hoạt
                                    </label>
                                    <label for="stt-notactice">
                                        <input type="radio" name="status" id="stt-notactice" class="minimal-red" value="0" <?php echo set_radio('status', 0); ?>> Không kích hoạt
                                    </label>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Giới tính</label><br/>
                                    <label for="male">
                                        <input type="radio" name="gender" id="male" class="minimal-red" value="1" <?php echo set_radio('gender', '1', TRUE); ?>> Nam
                                    </label>
                                    <label for="female">
                                        <input type="radio" name="gender" id="female" class="minimal-red" value="0" <?php echo set_radio('gender', '0'); ?>> Nữ
                                    </label>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Dùng thử admin</label><br/>
                                    <label for="flags">
                                        <input type="radio" name="flags" id="flags"  class="minimal-red" value="0" <?php echo set_radio('flags', 0, TRUE); ?>> Dùng thử
                                    </label>
                                    <label for="noflags">
                                        <input type="radio" name="flags" id="noflags" class="minimal-red" value="1" <?php echo set_radio('flags', 1); ?>> Full
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="birthday" class="form-control pull-right" value="<?php echo set_value('birthday'); ?>" id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
