<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Cập nhật trang trại</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/farm'; ?>">Quản lý trang trại</a></li>
            <li class="active">Cập nhật trang trại</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/farm'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/farm/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i>Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap farm-wrap">
                            <?php echo form_open_multipart(base_url('administrator/farm/edit'), 'id="frmeditfarm"', array('id' => $data->id)); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="farm-name">Tên trang trại <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="farm-name" name="farm_name" value="<?php echo $data->name ? $data->name : set_value('farm_name'); ?>" placeholder="Tên trang trại">
                                    <?php echo form_error('farm_name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Chủ trang trại <span class="text-danger">*</span></label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-user-circle"></i> 
                                        <select class="form-control select2" name="farm_boss">
                                            <option value="">-- Chọn chủ trang trại --</option>
                                            <?php
                                            if ($entry['farmes']):
                                                foreach ($entry['farmes'] as $f) :
                                                    printf('<option value="%d" %s>%s</option>', $f->user_id, ($data->user_id == $f->user_id) ? 'selected="selected"' : set_select('farm_boss', $f->user_id), $f->username);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('farm_boss', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Loại trang trại</label>
                                    <a class="btn btn-block btn-social btn-tumblr">
                                        <i class="fa fa-cog"></i> 
                                        <select class="form-control select2" name="farmtype">
                                            <option value="">-- Chọn loại trang trại --</option>
                                            <?php
                                            if ($entry['farmtype']):
                                                foreach ($entry['farmtype'] as $ft) :
                                                    printf('<option value="%d" %s>%s</option>', $ft->id, ($data->farmtype_id == $ft->id) ? 'selected="selected"' : set_select('farmtype', $ft->id), $ft->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label>Sản phẩm</label>
                                    <a class="btn btn-block btn-social btn-google">
                                        <i class="fa fa-product-hunt"></i> 
                                        <select class="form-control select2" multiple="multiple" data-live-search="true" name="products[]">
                                            <option value="">-- Chọn sản phẩm --</option>
                                            <?php
                                            if ($entry['products']):
                                                foreach ($entry['products'] as $p) :
                                                    printf('<option value="%d" %s>%s</option>', $p->id, in_array($p->id, $data->products) ? 'selected="selected"' : set_select('products', $p->id), $p->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label>Chứng nhận</label>
                                    <a class="btn btn-block btn-social btn-twitter">
                                        <i class="fa fa-certificate"></i> 
                                        <select class="form-control select2" multiple="multiple" data-live-search="true" name="certificate[]">
                                            <option value="">-- Chọn chứng nhận --</option>
                                            <?php
                                            if ($entry['certificate']):
                                                foreach ($entry['certificate'] as $ce) :
                                                    printf('<option value="%d" %s>%s</option>', $ce->id, in_array($ce->id, $data->certificate) ? 'selected="selected"' : set_select('certificate', $ce->id), $ce->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label for="certificate-picture">Giấy chứng nhận <small><i>(Nếu có)</i></small></label>
                                    <input type="file" name="fileToUpload" id="certificate-picture">
                                    <?php echo $data->certificate_picture ? "<img width='100' height='100' style='margin: 10px 0' src=" . $this->config->config['upload_path'] . '/certificate/' . $data->certificate_picture . " alt='{$data->name}' />" : ''; ?>
                                    <p class="help-block">Upload giấy chứng nhận</p>
                                </div>
                                <div class="form-group">
                                    <label for="farm-size">Diện tích (m<sup>2</sup>) <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control" id="farm-size" name="farm_size" value="<?php echo $data->size ? $data->size : set_value('farm_size'); ?>" placeholder="Diện tích">
                                    <?php echo form_error('farm_size', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="farm-note">Ghi chú</label>
                                    <textarea class="form-control" id="farm-note" rows="5" name="farm_note" placeholder="Ghi chú"><?php echo $data->note ? $data->note : set_value('farm_note'); ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="water-indicator">Chỉ tiêu nước</label>
                                    <input type="text" class="form-control" id="water-indicator" name="water_indicator" value="<?php echo $data->water_indicator ? $data->water_indicator : set_value('water_indicator'); ?>" placeholder="Chỉ tiêu nước">
                                </div>
                                <div class="form-group">
                                    <label for="index-of-photosynthesis">Chỉ số quang hợp</label>
                                    <input type="text" class="form-control" id="index-of-photosynthesis" name="index_of_photosynthesis" value="<?php echo $data->index_of_photosynthesis ? $data->index_of_photosynthesis : set_value('index_of_photosynthesis'); ?>" placeholder="Chỉ số quang hợp">
                                </div>

                                <div class="form-group">
                                    <label for="farm-slug">Slug</label>
                                    <input type="text" class="form-control" id="farm-slug" name="farm_slug" value="<?php echo $data->slug ? $data->slug : set_value('farm_slug'); ?>" placeholder="Slug">
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Hiển thị thông tin vụ cây trồng, vật nuôi</label><br/>
                                    <label for="show-harvest">
                                        <input type="radio" id="show-harvest" name="show_harvest" class="minimal-red" value="1" <?php echo ($data->show_harvest == 1) ? 'checked="checked"' : set_radio('status', 1, TRUE); ?> checked=""> Cho phép
                                    </label>
                                    <label for="notshow-harvest">
                                        <input type="radio" id="notshow-harvest" name="show_harvest" class="minimal-red" value="0" <?php echo ($data->show_harvest == 0) ? 'checked="checked"' : set_radio('status', 0); ?> > Không cho phép
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Tỉnh/TP <span class="text-danger">*</span> <i class="text-danger">(Tỉnh/TP sẽ không thể được cập nhật sau khi đã thêm trang trại)</i></label>
                                    <a class="btn btn-block btn-social btn-twitter">
                                        <i class="fa fa-certificate"></i> 
                                        <select class="form-control select2"name="province" > <!--disabled="false"-->
                                            <option value="">-- Chọn Tỉnh/TP --</option>
                                            <?php
                                            if ($entry['provinces']):
                                                foreach ($entry['provinces'] as $prov) :
                                                    printf('<option value="%d" %s>%s</option>', $prov->id, $data->province_id == $prov->id ? 'selected = "selected"' : set_select('province', $prov->id), $prov->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </a>
                                    <?php echo form_error('province', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="farm-location">Vị trí <span class="text-danger">*</span> <i class="text-danger">(Vị trị sẽ không thể được cập nhật sau khi đã thêm trang trại)</i></label>
                                    <input type="text" class="form-control" id="maps_address" name="farm_location" value="<?php echo set_value('farm_location'); ?>" placeholder="Nhập vị trí trang trại ..." > <!--disabled=""-->
                                    <?php echo form_error('farm_location', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div id="maps_maparea">
                                            <div id="maps_mapcanvas"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="maps_maplat">Vĩ độ</label>
                                        <input type="text" class="form-control lat" name="latitude" id="maps_maplat" value="{maps_maplat}" readonly="readonly">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="maps_maplng">Kinh độ</label>
                                        <input type="text" class="form-control long" name="longitude" id="maps_maplng" value="{maps_maplng}" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                            echo '<a href="' . $this->config->config['base_url'] . '/administrator/farm/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    var lati = <?php echo $data->latitude; ?>;
    var longi = <?php echo $data->longitude; ?>;

    var map, ele, mapH, mapW, addEle, mapL, mapN, mapZ;

    ele = 'maps_mapcanvas';
    addEle = 'maps_address';
    mapLat = 'maps_maplat';
    mapLng = 'maps_maplng';
    mapZ = 'maps_mapzoom';
    mapArea = 'maps_maparea';
    mapCenLat = 'maps_mapcenterlat';
    mapCenLng = 'maps_mapcenterlng';

// Call Google MAP API
    if (!document.getElementById('googleMapAPI')) {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.id = 'googleMapAPI';
        s.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDH7bEOG4frBd5rjTaBiVMBZD-nD4bF6YA&language=vi&sensor=false&libraries=places&callback=controlMap';
        document.body.appendChild(s);
    } else {
        controlMap();
    }

// Creat map and map tools
    function initializeMap() {
        var zoom = parseInt($("#" + mapZ).val()), lat = parseFloat($("#" + mapLat).val()), lng = parseFloat($("#" + mapLng).val()), Clat = parseFloat($("#" + mapCenLat).val()), Clng = parseFloat($("#" + mapCenLng).val());
        Clat || (Clat = lati, $("#" + mapCenLat).val(Clat));
        Clng || (Clng = longi, $("#" + mapCenLng).val(Clng));
        lat || (lat = Clat, $("#" + mapLat).val(lat));
        lng || (lng = Clng, $("#" + mapLng).val(lng));
        zoom || (zoom = 15, $("#" + mapZ).val(zoom));

        mapW = $('#' + ele).innerWidth();
        mapH = mapW * 3 / 4;

        // Init MAP
        $('#' + ele).width(mapW).height(mapH > 500 ? 500 : mapH);
        map = new google.maps.Map(document.getElementById(ele), {
            zoom: zoom,
            center: {
                lat: Clat,
                lng: Clng
            }
        });

        // Init default marker
        var markers = [];
        markers[0] = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(lati, longi),
            draggable: true,
            animation: google.maps.Animation.DROP
        });
        markerdragEvent(markers);

        // Init search box
        var searchBox = new google.maps.places.SearchBox(document.getElementById(addEle));

        google.maps.event.addListener(searchBox, 'places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            markers = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var marker = new google.maps.Marker({
                    map: map,
                    position: place.geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP
                });

                markers.push(marker);
                bounds.extend(place.geometry.location);
            }

            markerdragEvent(markers);
            map.fitBounds(bounds);
            console.log(places);
        });

        // Add marker when click on map
        google.maps.event.addListener(map, 'click', function (e) {
            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            markers = [];
            markers[0] = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(e.latLng.lat(), e.latLng.lng()),
                draggable: true,
                animation: google.maps.Animation.DROP
            });

            markerdragEvent(markers);
        });

        // Event on zoom map
        google.maps.event.addListener(map, 'zoom_changed', function () {
            $("#" + mapZ).val(map.getZoom());
        });

        // Event on change center map
        google.maps.event.addListener(map, 'center_changed', function () {
            $("#" + mapCenLat).val(map.getCenter().lat());
            $("#" + mapCenLng).val(map.getCenter().lng());
            console.log(map.getCenter());
        });
    }

// Show, hide map on select change
    function controlMap(manual) {
        $('#' + mapArea).slideDown(100, function () {
            initializeMap();
        });

        return !1;
    }

// Map Marker drag event
    function markerdragEvent(markers) {
        for (var i = 0, marker; marker = markers[i]; i++) {
            $("#" + mapLat).val(marker.position.lat());
            $("#" + mapLng).val(marker.position.lng());

            google.maps.event.addListener(marker, 'drag', function (e) {
                $("#" + mapLat).val(e.latLng.lat());
                $("#" + mapLng).val(e.latLng.lng());
            });
        }
    }
</script>
