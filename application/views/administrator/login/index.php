<!-- Content Wrapper. Contains page content -->
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-sm-offset-4">
            <!-- Horizontal Form -->
            <div class="box box-info">
                <div class="ft-logo">
                    <img src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="Logo" /> 
                </div>

                <div class="box-header with-border">
                    <h3 class="box-title text-success">Farmtech Administrator</h3>
                </div>
                <!-- /.box-header -->

                <div class="box-body">
                    <?php echo form_open(base_url('administrator/login'), 'id="frmadministratorlogin"'); ?>
                    <form id="frmadministratorlogin" method="POST">
                        <?php if ($this->session->flashdata('reponse')) { ?>
                            <div class="form-group">
                                <p class="btn btn-block btn-social btn-google">
                                    <i class="fa fa-warning"></i>
                                    <span><?php echo $this->session->flashdata('reponse'); ?></span>
                                </p>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label" for="username">Tên đăng nhập</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                </div>
                                <input type="text" id="username" name="username" class="form-control" placeholder="Tên đăng nhập" value="<?php echo set_value('username'); ?>" >
                            </div>
                            <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="pwd">Mật khẩu</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-key" aria-hidden="true"></i>
                                </div>
                                <input type="password" id="pwd" name="pwd" class="form-control" placeholder="Mật khẩu" value="<?php echo set_value('pwd'); ?>" >
                            </div>
                            <?php echo form_error('pwd', '<p class="error">', '</p>'); ?>
                        </div>
                        <!-- /.box-body -->
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" class="minimal-red"> Ghi nhớ mật khẩu
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <?php echo form_submit('submit', 'Đăng nhập', array('class' => 'btn btn-success btn-flat pull-right')); ?>
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                </div>
                <div class="box-footer">
                    <div class="form-group">
                        <div class="login-footer">
                            <a href="#">Quên mật khẩu</a> | 
                            <a href="<?php echo $this->config->config['ft_url']; ?>" target="_blank">Đăng ký tài khoản</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>