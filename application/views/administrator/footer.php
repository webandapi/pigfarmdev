            <footer class="main-footer">
                <!--<div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>-->
                <strong>Copyright &copy; <?php echo date('Y'); ?> <a target="_blank" href="<?php echo $this->config->config['ft_url']; ?>">Fman</a>.</strong> All rights
                reserved.
            </footer>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/raphael/raphael.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/moment/min/moment.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/pages/dashboard.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/select2/dist/js/select2.full.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/iCheck/icheck.min.js"></script>
        
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/ckeditor/ckeditor.js"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/demo.js"></script>
        <script>
            jQuery(function ($) {
                jQuery('.box-body table').DataTable({
                            "oLanguage": {
                                "oPaginate": {
                                "sFirst": "Trang đầu", // This is the link to the first page
                                "sPrevious": "Trước", // This is the link to the previous page
                                "sNext": "Tiếp", // This is the link to the next page
                                "sLast": "Trang cuối" // This is the link to the last page
                                },
                                "sSearch": "Tìm kiếm:",
                                "sZeroRecords": "Không tìm thấy thông tin",
                                "sInfo": "Hiển thị (_START_ - _END_) / _TOTAL_",
                                "sLengthMenu": "Hiển thị _MENU_ dòng"
                            }
                            });
                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy'
                });
                //Date picker
                $('#datepicker1').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy'
                });
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                  {
                    ranges   : {
                      'Hôm nay': [moment(), moment()],
                      'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                      '7 ngày trước': [moment().subtract(6, 'days'), moment()],
                      'Tháng trước': [moment().subtract(29, 'days'), moment()],
                      'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                  },
                  function (start, end) {
                    $('#daterange-btn span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                    $('#daterange-btn > input').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'))
                  }
                )
                 //Initialize Select2 Elements
                $('.select2').select2();
                
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                  checkboxClass: 'icheckbox_minimal-red',
                  radioClass   : 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                  checkboxClass: 'icheckbox_flat-green',
                  radioClass   : 'iradio_flat-green'
                });
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('ckeditor');
            });
        </script>
    </body>
</html>
