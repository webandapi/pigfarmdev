<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Cập nhật loại trang trại</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/farmtype'; ?>">Loại trang trại</a></li>
            <li class="active">Cập nhật loại trang trại</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/farmtype'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/farmtype/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php
                            echo form_open(base_url('administrator/farmtype/edit'), 'id="frmeditfarmtype"', array('farmtype_id' => $data->id));
                            echo '<div class="box-body">';

                            //form-group
                            echo '<div class="form-group">';
                            echo form_label('Tên loại trang trại <span class="text-danger">*</span>', 'farmtype-name');
                            $name = array(
                                'name' => 'farmtype_name',
                                'id' => 'farmtype-name',
                                'value' => $data->name ? $data->name : set_value('farmtype_name'),
                                'class' => 'form-control',
                                'placeholder' => 'Tên loại trang trại'
                            );
                            echo form_input($name);
                            echo form_error('farmtype_name', '<p class="error">', '</p>');
                            echo '</div>';

                            //form-group
                            echo '<div class="form-group">';
                            echo form_label('Mô tả', 'farmtype-des');
                            $des = array(
                                'rows' => 6,
                                'name' => 'farmtype_des',
                                'id' => 'farmtype-des',
                                'value' => $data->note ? $data->note : set_value('farmtype_des'),
                                'class' => 'form-control',
                                'placeholder' => 'Mô tả'
                            );
                            echo form_textarea($des);
                            echo '</div>';
                            echo '</div>';

                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                            echo '<a href="' . $this->config->config['base_url'] . '/administrator/farmtype/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                            echo '</div>';
                            // form close
                            echo form_close();
                            ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
