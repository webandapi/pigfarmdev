<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-user-plus"></i> Quản trị viên</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý quản trị viên</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <a href="<?php echo $this->config->config['base_url'] . '/administrator/admin/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm quản trị viên mới</a>
                        </div>
                        <div class="form-group">
                            <?php $this->load->view('administrator/message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-filter">
                            <form id="frmfilter" method="get">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="status">
                                            <option value="">-- Chọn trạng thái --</option>
                                            <option value="1" <?php echo ($_GET['status'] == '1') ? 'selected="selected"' : ''; ?>>Kích hoạt</option>
                                            <option value="0" <?php echo ($_GET['status'] == '0') ? 'selected="selected"' : ''; ?>>Chưa kích hoạt</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <select class="form-control select2" name="role">
                                            <option value="">-- Chọn Role --</option>
                                            <?php
                                            if ($roles):
                                                foreach ($roles as $val) :
                                                    printf('<option value="%d" %s>%s</option>', $val->id, ($_GET['role'] == $val->id) ? 'selected="selected"' : '', $val->name);
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <input type="submit" class="btn btn-flat btn-success" value="Lọc quản trị viên" />
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="table-responsive">
                            <table id="admin-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên đăng nhập</th>
                                        <th>Họ tên</th>
                                        <th>Email</th>
                                        <th class="text-center">Điện thoại</th>
                                        <th class="text-center">Giới tính</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Admin Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($administrators):
                                        $i = 0;
                                        foreach ($administrators as $val) :
                                            $i++;
                                            ?>
                                            <tr class="<?php echo $val->id == 1 ? 'disabled' : ''; ?>">
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $val->username; ?></td>
                                                <td><?php echo $val->fullname ? $val->fullname : '_'; ?></td>
                                                <td><?php echo $val->email ? "<a href='mailto:{$val->email}'>{$val->email}</a>" : '_'; ?></td>
                                                <td class="text-center"><?php echo $val->phone ? $val->phone : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td class="text-center"><?php echo (!isset($val->gender)) ? "_" : ($val->gender == 1 ? "<a title='Nam'><img src='{$this->config->config['assets_path']}/images/icons/male.png' /></a>" : "<a title='Nữ'><img src='{$this->config->config['assets_path']}/images/icons/female.png' /></a>"); ?></td>
                                                <td class="text-center"><?php echo ($val->status == 1) ? "<a title='Kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/checked.png' /></a>" : "<a title='Chưa kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/unchecked.png' /></a>"; ?></td>
                                                <td><?php echo $val->role ? $val->role : '_'; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['base_url'] . "/administrator/admin/delete/{$val->id}"; ?>" class="btn btn-danger btn-flat btn-sm" onclick="return confirm('Bạn có chắc muốn xóa?');" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/administrator/admin/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên đăng nhập</th>
                                        <th>Họ tên</th>
                                        <th>Email</th>
                                        <th class="text-center">Điện thoại</th>
                                        <th class="text-center">Giới tính</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Admin Role</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
