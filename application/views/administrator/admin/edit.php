<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Cập nhật quản trị viên</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/admin'; ?>">Quản lý quản trị viên</a></li>
            <li class="active">Cập nhật quản trị viên</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/admin'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/admin/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open(base_url('administrator/admin/edit'), 'id="frmeditadmin"', array('admin_id' => $data->id)); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="username">Tên đăng nhập <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $data->username ? $data->username : set_value('username'); ?>" placeholder="Tên đăng nhập">
                                    <?php echo form_error('username', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="fullname">Họ tên <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="fullname" name="fullname" value="<?php echo $data->fullname ? $data->fullname : set_value('fullname'); ?>" placeholder="Họ tên">
                                    <?php echo form_error('fullname', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="address">Địa chỉ</label>
                                    <input type="text" class="form-control" id="address" name="address" value="<?php echo $data->address ? $data->address : set_value('address'); ?>" placeholder="Địa chỉ">
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $data->email ? $data->email : set_value('email'); ?>" placeholder="Email">
                                    <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Điện thoại <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $data->phone ? $data->phone : set_value('phone'); ?>" placeholder="Điện thoại">
                                    <?php echo form_error('phone', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label>Loại quản trị viên <span class="text-danger">*</span></label>
                                    <select class="form-control select2" name="adminrole">
                                        <option value="">-- Chọn loại quản trị viên --</option>
                                        <?php
                                        if ($roles):
                                            foreach ($roles as $val) :
                                                printf('<option value="%d" %s>%s</option>', $val->id, ($data->adminrole_id == $val->id) ? 'selected="selected"' : set_select('adminrole', $val->id), $val->name);
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                    <?php echo form_error('adminrole', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Trạng thái</label><br/>
                                    <label for="stt-actice">
                                        <input type="radio" name="status" id="stt-actice" name="status" class="minimal-red" value="1" <?php echo ($data->status == 1) ? 'checked="checked"' : set_radio('status', 1, TRUE); ?>> Kích hoạt
                                    </label>
                                    <label for="stt-notactice">
                                        <input type="radio" name="status" id="stt-notactice" name="status" class="minimal-red" value="0" <?php echo ($data->status == 0) ? 'checked="checked"' : set_radio('status', 0); ?>> Không kích hoạt
                                    </label>
                                </div>
                                <div class="form-group box-label">
                                    <label class="control-label">Giới tính</label><br/>
                                    <label for="male">
                                        <input type="radio" name="gender" id="male" name="gender" class="minimal-red" value="1" <?php echo ($data->gender == 1) ? 'checked="checked"' : set_radio('gender', '1', TRUE); ?>> Nam
                                    </label>
                                    <label for="female">
                                        <input type="radio" name="gender" id="female" name="gender" class="minimal-red" value="0" <?php echo ($data->gender == 0) ? 'checked="checked"' : set_radio('gender', '0'); ?>> Nữ
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Ngày sinh:</label>

                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" name="birthday" class="form-control pull-right" value="<?php echo $data->date_of_birth ? date('d/m/Y', strtotime($data->date_of_birth)) : set_value('birthday'); ?>" id="datepicker">
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                            echo '<a href="' . $this->config->config['base_url'] . '/administrator/admin/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
