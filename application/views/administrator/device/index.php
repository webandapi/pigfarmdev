<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-database"></i> Quản lý thiết bị</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý thiết bị</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <a href="<?php echo $this->config->config['base_url'] . '/administrator/device/add'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm thiết bị mới</a>
                        </div>
                        <div class="form-group">
                            <?php $this->load->view('administrator/message'); ?>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="devices-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên thiết bị</th>
                                        <th>Loại thiết bị</th>
                                        <th>Nông trại</th>
                                        <th>Serial</th>
                                        <th>Ngày hết hạn bảo hành</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($devices):
                                        $i = 0;
                                        foreach ($devices as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><?php echo $val->name; ?></td>
                                                <td><?php echo $val->device_type->name ? $val->device_type->name : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td><?php echo $val->farm->name ? $val->farm->name : "<a title='Chưa cập nhật'><img src='{$this->config->config['assets_path']}/images/icons/undefine.png' /></a>"; ?></td>
                                                <td><?php echo $val->serial; ?></td>
                                                <td><?php echo date('d/m/Y', strtotime($val->date_end_warranty)); ?></td>
                                                <td class="text-center"><?php echo ($val->status == 1) ? "<a title='Kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/checked.png' /></a>" : "<a title='Chưa kích hoạt'><img src='{$this->config->config['assets_path']}/images/icons/unchecked.png' /></a>"; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="#" class="btn btn-danger btn-sm" onclick="return confirm('Bạn có chắc muốn xóa?');" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                                        <a href="<?php echo $this->config->config['base_url'] . "/administrator/device/edit/{$val->id}"; ?>" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên thiết bị</th>
                                        <th>Loại thiết bị</th>
                                        <th>Nông trại</th>
                                        <th>Serial</th>
                                        <th>Ngày hết hạn bảo hành</th>
                                        <th class="text-center">Trạng thái</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
