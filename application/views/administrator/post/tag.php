<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-tags"></i> Quản lý tags</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý tags</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group row">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['ft_url'] . '/blog/wp-admin/edit-tags.php?taxonomy=post_tag'; ?>" target="_blank" class="btn btn-flat btn-primary"><i class="fa fa-plus-circle"></i> Thêm tag mới</a>
                            </div>
                            <div class="col-xs-4">
                                <a href="<?php echo $this->config->config['ft_url'] . '/blog/wp-admin/edit-tags.php?taxonomy=post_tag'; ?>" target="_blank" class="btn btn-flat btn-success btn-backward"><i class="fa fa-tags"></i> Quản lý tags trong Wordpress</a>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="tags-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên</th>
                                        <th>Slug</th>
                                        <th class="text-center">Số bài viết</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($tags):
                                        $i = 0;
                                        foreach ($tags as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><a href="<?php echo $this->config->config['ft_url'] . "/post/tag/{$val->slug}"; ?>" target="_blank"><?php echo $val->name; ?></a></td>
                                                <td><?php echo $val->slug; ?></td>
                                                <td class="text-center"><?php echo number_format($val->count, 0, ',', '.'); ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['ft_url'] . "/blog/wp-admin/term.php?taxonomy=post_tag&tag_ID={$val->term_id}&post_type=post"; ?>" target="_blank" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên</th>
                                        <th>Slug</th>
                                        <th class="text-center">Số bài viết</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
