<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-sitemap"></i> Quản lý chuyên mục</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý chuyên mục</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group row">
                            <div class="col-xs-3">
                                <a href="<?php echo $this->config->config['ft_url'] . '/blog/wp-admin/edit-tags.php?taxonomy=category'; ?>" target="_blank" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm chuyên mục mới</a>
                            </div>
                            <div class="col-xs-4">
                                <a href="<?php echo $this->config->config['ft_url'] . '/blog/wp-admin/edit-tags.php?taxonomy=category'; ?>" target="_blank" class="btn btn-flat btn-success btn-backward"><i class="fa fa-sitemap"></i> Quản lý chuyên mục trong Wordpress</a>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="categories-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên chuyên mục</th>
                                        <th>Slug</th>
                                        <th>Mô tả</th>
                                        <th class="text-center">Số bài viết</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($categories):
                                        $i = 0;
                                        foreach ($categories as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><a target="_blank" href="<?php echo $this->config->config['ft_url'] . "/post/category/{$val->slug}"; ?>" target="_blank"><?php echo $val->name; ?></a></td>
                                                <td><?php echo $val->slug; ?></td>
                                                <td><?php echo $val->description; ?></td>
                                                <td class="text-center"><?php echo number_format($val->count, 0, ',', '.'); ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['ft_url'] . "/blog/wp-admin/term.php?taxonomy=category&tag_ID={$val->term_id}&post_type=post"; ?>" target="_blank" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên chuyên mục</th>
                                        <th>Slug</th>
                                        <th>Mô tả</th>
                                        <th class="text-center">Số bài viết</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
