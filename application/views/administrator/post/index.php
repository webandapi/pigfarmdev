<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-database"></i> Quản lý bài viết</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Quản lý bài viết</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group row">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['ft_url'] . '/blog/wp-admin/post-new.php'; ?>" target="_blank" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-plus-circle"></i> Thêm bài viết mới</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/post/category'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-sitemap"></i> Quản lý chuyên mục</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/post/tag'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-tags"></i> Quản lý tags</a>
                            </div>
                            <div class="col-xs-4">
                                <a target="_blank" href="<?php echo $this->config->config['ft_url'] . '/blog/wp-admin/edit.php'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-database"></i> Quản lý bài viết trong Wordpress</a>
                            </div>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="posts-lst" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tiêu đề</th>
                                        <th>Tác giả</th>
                                        <th>Chuyên mục</th>
                                        <th>Lượt xem</th>
                                        <th>Ngày đăng</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if ($posts):
                                        $i = 0;
                                        foreach ($posts as $val) :
                                            $i++;
                                            ?>
                                            <tr>
                                                <td><?php echo $i; ?></td>
                                                <td><a href="<?php echo $val->url; ?>" target="_blank"><?php echo $val->title; ?></a></td>
                                                <td><?php echo $val->author; ?></td>
                                                <td><?php echo $val->fields; ?></td>
                                                <td><?php echo $val->view; ?></td>
                                                <td><?php echo $val->date; ?></td>
                                                <td>
                                                    <div class="mailbox-controls">
                                                        <!-- Check all button -->
                                                        <a href="<?php echo $this->config->config['ft_url'] . "/blog/wp-admin/post.php?post={$val->id}&action=edit"; ?>" target="_blank" class="btn btn-success btn-flat btn-sm" title="Chỉnh sửa"><i class="fa fa-pencil"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tiêu đề</th>
                                        <th>Tác giả</th>
                                        <th>Chuyên mục</th>
                                        <th>Lượt xem</th>
                                        <th>Ngày đăng</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
