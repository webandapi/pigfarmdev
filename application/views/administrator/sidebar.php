<?php
$admin_root = $admin = $admin_add = $adminrole_root = $adminrole = $adminrole_add = '';
$user_root = $user = $user_add = $expert = $farmer = $usertype_root = $usertype = $usertype_add = '';
$post_root = $post = $post_category = $post_tag = '';
$farm_root = $farm = $farm_add = $farmtype_root = $farmtype = $farmtype_add = '';
$product_root = $product = $product_add = '';
$field_root = $field = $field_add = '';
$device_root = $device = $device_add = $devicetype_root = $devicetype = $devicetype_add = '';
$option_root = $option = $option_add = '';

// Active admin nav
if (in_array($path_info, array('administrator/admin', 'administrator/admin/add', 'administrator/admin/edit', 'administrator/adminrole', 'administrator/adminrole/add', 'administrator/adminrole/edit')))
    $admin_root = 'active';

if ($path_info == 'administrator/admin')
    $admin = 'active';

if ($path_info == 'administrator/admin/add')
    $admin_add = 'active';

if (in_array($path_info, array('administrator/adminrole', 'administrator/adminrole/add', 'administrator/adminrole/edit')))
    $adminrole_root = 'active';

if ($path_info == 'administrator/adminrole')
    $adminrole = 'active';

if ($path_info == 'administrator/adminrole/add')
    $adminrole_add = 'active';

// Active user nav
if (in_array($path_info, array('administrator/user', 'administrator/user/add', 'administrator/user/edit', 'administrator/user/expert', 'administrator/user/farmer', 'administrator/usertype', 'administrator/usertype/add', 'administrator/usertype/edit')))
    $user_root = 'active';

if ($path_info == 'administrator/user')
    $user = 'active';

if ($path_info == 'administrator/user/add')
    $user_add = 'active';

if ($path_info == 'administrator/user/expert')
    $expert = 'active';

if ($path_info == 'administrator/user/farmer')
    $farmer = 'active';

if (in_array($path_info, array('administrator/usertype', 'administrator/usertype/add', 'administrator/usertype/edit')))
    $usertype_root = 'active';

if ($path_info == 'administrator/usertype')
    $usertype = 'active';

if ($path_info == 'administrator/usertype/add')
    $usertype_add = 'active';

// Active post nav
if (in_array($path_info, array('administrator/post', 'administrator/post/category', 'administrator/post/tag')))
    $post_root = 'active';

if ($path_info == 'administrator/post')
    $post = 'active';

if ($path_info == 'administrator/post/category')
    $post_category = 'active';

if ($path_info == 'administrator/post/tag')
    $post_tag = 'active';

// Active farm nav
if (in_array($path_info, array('administrator/farm', 'administrator/farm/add', 'administrator/farm/edit', 'administrator/farmtype', 'administrator/farmtype/add')))
    $farm_root = 'active';

if ($path_info == 'administrator/farm')
    $farm = 'active';

if ($path_info == 'administrator/farm/add')
    $farm_add = 'active';

if ($path_info == 'administrator/farmtype' || $path_info == 'administrator/farmtype/add')
    $farmtype_root = 'active';

if ($path_info == 'administrator/farmtype')
    $farmtype = 'active';

if ($path_info == 'administrator/farmtype/add')
    $farmtype_add = 'active';

// Active provinces nav
if (in_array($path_info, array('administrator/province', 'administrator/province/add', 'administrator/province/edit')))
    $province_root = 'active';

if ($path_info == 'administrator/province')
    $province = 'active';

if ($path_info == 'administrator/province/add')
    $province_add = 'active';

// Active Distributor nav
if (in_array($path_info, array('administrator/distributor', 'administrator/distributor/add', 'administrator/distributor/edit')))
    $distributor_root = 'active';

if ($path_info == 'administrator/distributor')
    $distributor = 'active';

if ($path_info == 'administrator/distributor/add')
    $distributor_add = 'active';

// Active Certificate nav
if (in_array($path_info, array('administrator/certificate', 'administrator/certificate/add', 'administrator/certificate/edit')))
    $certificate_root = 'active';

if ($path_info == 'administrator/certificate')
    $certificate = 'active';

if ($path_info == 'administrator/certificate/add')
    $certificate_add = 'active';

// Active product nav
if (in_array($path_info, array('administrator/product', 'administrator/product/add', 'administrator/product/edit')))
    $product_root = 'active';

if ($path_info == 'administrator/product')
    $product = 'active';

if ($path_info == 'administrator/product/add')
    $product_add = 'active';

// Active field nav
if (in_array($path_info, array('administrator/field', 'administrator/field/add', 'administrator/field/edit')))
    $field_root = 'active';

if ($path_info == 'administrator/field')
    $field = 'active';

if ($path_info == 'administrator/field/add')
    $field_add = 'active';

// Active qa  nav
if (in_array($path_info, array('administrator/qa', 'administrator/qa/add', 'administrator/qa/edit')))
    $qa_root = 'active';

if ($path_info == 'administrator/qa')
    $qa = 'active';

if ($path_info == 'administrator/qa/add')
    $qa_add = 'active';

// Active module  nav
if (in_array($path_info, array('administrator/module', 'administrator/module/add', 'administrator/module/edit')))
    $module_root = 'active';

if ($path_info == 'administrator/module')
    $module = 'active';

if ($path_info == 'administrator/module/add')
    $module_add = 'active';

// Active device nav
if (in_array($path_info, array('administrator/device', 'administrator/device/add', 'administrator/device/edit', 'administrator/devicetype', 'administrator/devicetype/add', 'administrator/devicetype/edit', 'administrator/sensor', 'administrator/sensor/add', 'administrator/sensor/edit')))
    $device_root = 'active';

if ($path_info == 'administrator/device')
    $device = 'active';

if ($path_info == 'administrator/device/add')
    $device_add = 'active';

if (in_array($path_info, array('administrator/devicetype', 'administrator/devicetype/add', 'administrator/devicetype/edit')))
    $devicetype_root = 'active';

if ($path_info == 'administrator/devicetype')
    $devicetype = 'active';

if ($path_info == 'administrator/devicetype/add')
    $devicetype_add = 'active';

if (in_array($path_info, array('administrator/sensor', 'administrator/sensor/add', 'administrator/sensor/edit')))
    $sensor_root = 'active';

if ($path_info == 'administrator/sensor')
    $sensor = 'active';

if ($path_info == 'administrator/sensor/add')
    $sensor_add = 'active';

// Active report nav
if (in_array($path_info, array('administrator/report/post')))
    $report_root = 'active';

if ($path_info == 'administrator/report/post')
    $report = 'active';

// Active setting nav
if (in_array($path_info, array('administrator/option', 'administrator/option/add', 'administrator/option/edit')))
    $option_root = 'active';

if ($path_info == 'administrator/option')
    $option = 'active';

if ($path_info == 'administrator/option/add')
    $option_add = 'active';
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $current['avatar']; ?>" class="img-circle" alt="Avatar">
            </div>
            <div class="pull-left info">
                <p><?php echo $current['fullname']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user-circle-o"></i>
                    <span>Tài khoản cá nhân</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
                    <li><a href="#"><i class="fa fa-user-circle-o"></i> Cập nhật tài khoản</a></li>
                    <li><a href="#"><i class="fa fa-key"></i> Đổi mật khẩu</a></li>
                    <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/logout'; ?>"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
                </ul>
            </li>
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] == '1'): ?>
                <li class="treeview <?php echo $admin_root; ?>">
                    <a href="#">
                        <i class="fa fa-user-plus"></i> <span>Quản trị viên</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu <?php echo $adminrole_root; ?>">
                        <li class="<?php echo $admin; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/admin'; ?>"><i class="fa fa-folder-open"></i> Quản lý quản trị viên</a></li>
                        <li class="<?php echo $admin_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/admin/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        <li class="treeview <?php echo $adminrole_root; ?>">
                            <a href="#"><i class="fa fa-cogs"></i> Phân quyền quản trị viên
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $adminrole; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/adminrole'; ?>"><i class="fa fa-folder-open"></i> Quản lý phân quyền</a></li>
                                <li class="<?php echo $adminrole_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/adminrole/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $user_root; ?>">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>Người dùng</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $user; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/user'; ?>"><i class="fa fa-folder-open"></i> Tất cả người dùng</a></li>
                        <li class="<?php echo $expert; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/user/expert'; ?>"><i class="fa fa-folder-open"></i> Danh sách chuyên gia</a></li>
                        <li class="<?php echo $farmer; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/user/farmer'; ?>"><i class="fa fa-folder-open"></i> Danh sách nông dân</a></li>
                        <li class="<?php echo $user_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/user/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        <li class="treeview <?php echo $usertype_root; ?>">
                            <a href="#"><i class="fa fa-cogs"></i> Phân quyền người dùng
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $usertype; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/usertype'; ?>"><i class="fa fa-folder-open"></i> Quản lý phân quyền</a></li>
                                <li class="<?php echo $usertype_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/usertype/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if ($current['adminrole_id']): ?>
                <li class="treeview <?php echo $post_root; ?>">
                    <a href="#">
                        <i class="fa fa-database"></i>
                        <span>Bài viết</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $post; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/post'; ?>"><i class="fa fa-folder-open"></i> Quản lý bài viết</a></li>
                        <li class="<?php echo $post_category; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/post/category'; ?>"><i class="fa fa-sitemap"></i> Quản lý chuyên mục</a></li>
                        <li class="<?php echo $post_tag; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/post/tag'; ?>"><i class="fa fa-tags"></i> Quản lý tags</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $farm_root; ?>">
                    <a href="#">
                        <i class="fa fa-home"></i>
                        <span>Trang trại</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $farm; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/farm'; ?>"><i class="fa fa-folder-open"></i> Quản lý trang trại</a></li>
                        <li class="<?php echo $farm_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/farm/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        <li class="treeview <?php echo $farmtype_root; ?>">
                            <a href="#"><i class="fa fa-cogs"></i> Loại trang trại
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $farmtype; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/farmtype'; ?>"><i class="fa fa-folder-open"></i> Quản lý loại trang trại</a></li>
                                <li class="<?php echo $farmtype_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/farmtype/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $province_root; ?>">
                    <a href="#">
                        <i class="fa fa-location-arrow"></i>
                        <span>Tỉnh/TP</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $province; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/province'; ?>"><i class="fa fa-folder-open"></i> Quản lý Tỉnh/TP</a></li>
                        <li class="<?php echo $province_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/province/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $distributor_root; ?>">
                    <a href="#">
                        <i class="fa fa-user-secret"></i>
                        <span>Nhà phân phối</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $distributor; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/distributor'; ?>"><i class="fa fa-folder-open"></i> Quản lý nhà phân phối</a></li>
                        <li class="<?php echo $distributor_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/distributor/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $certificate_root; ?>">
                    <a href="#">
                        <i class="fa fa-certificate"></i>
                        <span>Chứng nhận</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $certificate; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/certificate'; ?>"><i class="fa fa-folder-open"></i> Quản lý chứng nhận</a></li>
                        <li class="<?php echo $certificate_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/certificate/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $product_root; ?>">
                    <a href="#">
                        <i class="fa fa-product-hunt"></i>
                        <span>Sản phẩm</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $product; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/product'; ?>"><i class="fa fa-folder-open"></i> Quản lý sản phẩm</a></li>
                        <li class="<?php echo $product_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/product/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $field_root; ?>">
                    <a href="#">
                        <i class="fa fa-building"></i>
                        <span>Lĩnh vực</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $field; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/field'; ?>"><i class="fa fa-folder-open"></i> Quản lý lĩnh vực</a></li>
                        <li class="<?php echo $field_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/field/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-qrcode"></i>
                        <span>QR Code</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-folder-open"></i> QR Code trang trại</a></li>
                        <li><a href="#"><i class="fa fa-folder-open"></i> QR Code sản phẩm</a></li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $qa_root; ?>">
                    <a href="#">
                        <i class="fa fa-question-circle-o"></i>
                        <span>Câu hỏi thường gặp</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $qa; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/qa'; ?>"><i class="fa fa-folder-open"></i> Quản lý câu hỏi</a></li>
                        <li class="<?php echo $qa_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/qa/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] == '1'): ?>
                <li class="treeview <?php echo $module_root; ?>">
                    <a href="#">
                        <i class="fa fa-address-card"></i>
                        <span>Modules</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $module; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/module'; ?>"><i class="fa fa-folder-open"></i> Quản lý Modules</a></li>
                        <li class="<?php echo $module_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/module/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $device_root; ?>">
                    <a href="#">
                        <i class="fa fa-database"></i> <span>Thiết bị</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $device; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/device'; ?>"><i class="fa fa-folder-open"></i> Quản lý thiết bị</a></li>
                        <li class="<?php echo $device_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/device/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        <li class="treeview <?php echo $devicetype_root; ?>">
                            <a href="#"><i class="fa fa-cogs"></i> Loại thiết bị
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $devicetype; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/devicetype'; ?>"><i class="fa fa-folder-open"></i> Quản lý loại thiết bị</a></li>
                                <li class="<?php echo $devicetype_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/devicetype/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                            </ul>
                        </li>
                        <li class="treeview <?php echo $sensor_root; ?>">
                            <a href="#"><i class="fa fa-cogs"></i> Sensor
                                <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="<?php echo $sensor; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/sensor'; ?>"><i class="fa fa-folder-open"></i> Quản lý Sensor</a></li>
                                <li class="<?php echo $sensor_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/sensor/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if ($current['adminrole_id']): ?>
                <li class="treeview <?php echo $report_root; ?>">
                    <a href="#">
                        <i class="fa fa-area-chart"></i>
                        <span>Thống kê</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $report; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/report/post'; ?>"><i class="fa fa-folder-open"></i> Thống kê bài viết</a></li>
                    </ul>
                </li>
            <?php endif; ?>
            <!--Device Supporter-->
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $notifycation_root; ?>">
                    <a href="#">
                        <i class="fa fa-wpexplorer"></i>
                        <span>Thiết Bị & Người Hỗ Trợ</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $notifycation; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/supportdevice'; ?>"><i class="fa fa-map-pin"></i> Quản lý thiết bị</a></li>
                        <li class="<?php echo $notifycation_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/supportdevice/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        <li class="<?php echo $notifycation; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/supporter'; ?>"><i class="fa fa-life-ring"></i> Quản lý người hỗ trợ</a></li>
                        <li class="<?php echo $notifycation_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/supporter/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>

            <!--Thông báo-->
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] !== '3'): ?>
                <li class="treeview <?php echo $notifycation_root; ?>">
                    <a href="#">
                        <i class="fa fa-bell"></i>
                        <span>Thông báo</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $notifycation; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/notifycation'; ?>"><i class="fa fa-folder-open"></i> Quản lý thông báo</a></li>
                        <li class="<?php echo $notifycation_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/notifycation/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>
            <?php if (isset($current['adminrole_id']) && $current['adminrole_id'] == '1'): ?>
                <li class="treeview <?php echo $option_root; ?>">
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        <span>Setting</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $option; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/option'; ?>"><i class="fa fa-folder-open"></i> Quản lý cài đặt</a></li>
                        <li class="<?php echo $option_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/administrator/option/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
            <?php endif; ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>