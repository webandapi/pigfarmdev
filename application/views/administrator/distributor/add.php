<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm nhà phân phối</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/distributor'; ?>">Quản lý nhà phân phối</a></li>
            <li class="active">Thêm nhà phân phối</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/distributor'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open_multipart(base_url('administrator/distributor/add'), 'id="frmadddistributor"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Tên nhà phân phối <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo set_value('name'); ?>" placeholder="Tên nhà phân phối">
                                    <?php echo form_error('name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="logo">Logo <small><i>(Kích thước: 100px x 100px)</i></small></label>
                                    <input type="file" name="fileToUpload" id="logo">
                                    <?php echo form_error('fileToUpload', '<p class="error">', '</p>'); ?>
                                    <p class="help-block">Chọn logo nhà phân phối</p>
                                </div>
                                <div class="form-group">
                                    <label for="code">Mã số doanh nghiệp <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="code" name="code" value="<?php echo set_value('code'); ?>" placeholder="Mã số doanh nghiệp">
                                    <?php echo form_error('code', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Điện thoại <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone'); ?>" placeholder="Điện thoại">
                                    <?php echo form_error('phone', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo set_value('email'); ?>" placeholder="Email">
                                    <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="maps_address">Địa chỉ <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="maps_address" name="location" value="<?php echo set_value('location'); ?>" placeholder="Địa chỉ">
                                    <?php echo form_error('location', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-12">
                                        <div id="maps_maparea">
                                            <div id="maps_mapcanvas"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="maps_maplat">Vĩ độ</label>
                                        <input type="text" class="form-control lat" name="latitude" id="maps_maplat" value="{maps_maplat}" readonly="readonly">
                                    </div>
                                    <div class="col-xs-6">
                                        <label for="maps_maplng">Kinh độ</label>
                                        <input type="text" class="form-control long" name="longitude" id="maps_maplng" value="{maps_maplng}" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
    var lati = 12.238791465759;
    var longi = 109.19674682617;

    var map, ele, mapH, mapW, addEle, mapL, mapN, mapZ;

    ele = 'maps_mapcanvas';
    addEle = 'maps_address';
    mapLat = 'maps_maplat';
    mapLng = 'maps_maplng';
    mapZ = 'maps_mapzoom';
    mapArea = 'maps_maparea';
    mapCenLat = 'maps_mapcenterlat';
    mapCenLng = 'maps_mapcenterlng';

// Call Google MAP API
    if (!document.getElementById('googleMapAPI')) {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.id = 'googleMapAPI';
        s.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDH7bEOG4frBd5rjTaBiVMBZD-nD4bF6YA&language=vi&sensor=false&libraries=places&callback=controlMap';
        document.body.appendChild(s);
    } else {
        controlMap();
    }

// Creat map and map tools
    function initializeMap() {
        var zoom = parseInt($("#" + mapZ).val()), lat = parseFloat($("#" + mapLat).val()), lng = parseFloat($("#" + mapLng).val()), Clat = parseFloat($("#" + mapCenLat).val()), Clng = parseFloat($("#" + mapCenLng).val());
        Clat || (Clat = lati, $("#" + mapCenLat).val(Clat));
        Clng || (Clng = longi, $("#" + mapCenLng).val(Clng));
        lat || (lat = Clat, $("#" + mapLat).val(lat));
        lng || (lng = Clng, $("#" + mapLng).val(lng));
        zoom || (zoom = 15, $("#" + mapZ).val(zoom));

        mapW = $('#' + ele).innerWidth();
        mapH = mapW * 3 / 4;

        // Init MAP
        $('#' + ele).width(mapW).height(mapH > 500 ? 500 : mapH);
        map = new google.maps.Map(document.getElementById(ele), {
            zoom: zoom,
            center: {
                lat: Clat,
                lng: Clng
            }
        });

        // Init default marker
        var markers = [];
        markers[0] = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(lati, longi),
            draggable: true,
            animation: google.maps.Animation.DROP
        });
        markerdragEvent(markers);

        // Init search box
        var searchBox = new google.maps.places.SearchBox(document.getElementById(addEle));

        google.maps.event.addListener(searchBox, 'places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            markers = [];
            var bounds = new google.maps.LatLngBounds();
            for (var i = 0, place; place = places[i]; i++) {
                var marker = new google.maps.Marker({
                    map: map,
                    position: place.geometry.location,
                    draggable: true,
                    animation: google.maps.Animation.DROP
                });

                markers.push(marker);
                bounds.extend(place.geometry.location);
            }

            markerdragEvent(markers);
            map.fitBounds(bounds);
            console.log(places);
        });

        // Add marker when click on map
        google.maps.event.addListener(map, 'click', function (e) {
            for (var i = 0, marker; marker = markers[i]; i++) {
                marker.setMap(null);
            }

            markers = [];
            markers[0] = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(e.latLng.lat(), e.latLng.lng()),
                draggable: true,
                animation: google.maps.Animation.DROP
            });

            markerdragEvent(markers);
        });

        // Event on zoom map
        google.maps.event.addListener(map, 'zoom_changed', function () {
            $("#" + mapZ).val(map.getZoom());
        });

        // Event on change center map
        google.maps.event.addListener(map, 'center_changed', function () {
            $("#" + mapCenLat).val(map.getCenter().lat());
            $("#" + mapCenLng).val(map.getCenter().lng());
            console.log(map.getCenter());
        });
    }

// Show, hide map on select change
    function controlMap(manual) {
        $('#' + mapArea).slideDown(100, function () {
            initializeMap();
        });

        return !1;
    }

// Map Marker drag event
    function markerdragEvent(markers) {
        for (var i = 0, marker; marker = markers[i]; i++) {
            $("#" + mapLat).val(marker.position.lat());
            $("#" + mapLng).val(marker.position.lng());

            google.maps.event.addListener(marker, 'drag', function (e) {
                $("#" + mapLat).val(e.latLng.lat());
                $("#" + mapLng).val(e.latLng.lng());
            });
        }
    }
</script>
