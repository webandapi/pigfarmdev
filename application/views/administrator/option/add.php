<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-cogs"></i> Thêm cài đặt</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/option'; ?>"><i class="fa fa-dashboard"></i> Quản lý cài đặt</a></li>
            <li class="active">Thêm cài đặt</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/option'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open(base_url('administrator/option/add'), 'id="frmaddoption"'); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="setting-name">Tên cài đặt <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="setting-name" name="setting_name" value="<?php echo set_value('setting_name'); ?>" placeholder="Tên cài đặt">
                                    <?php echo form_error('setting_name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-5">
                                        <label for="key">Key <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="key" name="setting_key" value="<?php echo set_value('setting_key'); ?>" placeholder="Key">
                                        <?php echo form_error('setting_key', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="col-xs-7">
                                        <label for="value">Giá trị <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="value" name="setting_value" value="<?php echo set_value('setting_value'); ?>" placeholder="Giá trị">
                                        <?php echo form_error('setting_value', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
