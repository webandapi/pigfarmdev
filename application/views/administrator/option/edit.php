<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Cập nhật cài đặt</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/optionistrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/optionistrator/option'; ?>">Quản lý cài đặt</a></li>
            <li class="active">Cập nhật cài đặt</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/option'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/option/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open(base_url('administrator/option/edit'), 'id="frmeditoption"',array('option_id' => $data->id)); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="setting-name">Tên cài đặt <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="setting-name" name="setting_name" value="<?php echo $data->name ? $data->name : set_value('setting_name'); ?>" placeholder="Tên cài đặt">
                                    <?php echo form_error('setting_name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-xs-5">
                                        <label for="key">Key <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="key" name="setting_key" value="<?php echo $data->slug ? $data->slug : set_value('setting_key'); ?>" placeholder="Key">
                                        <?php echo form_error('setting_key', '<p class="error">', '</p>'); ?>
                                    </div>
                                    <div class="col-xs-7">
                                        <label for="value">Giá trị <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" id="value" name="setting_value" value="<?php echo $data->value ? $data->value : set_value('setting_value'); ?>" placeholder="Giá trị">
                                        <?php echo form_error('setting_value', '<p class="error">', '</p>'); ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                            echo '<a href="' . $this->config->config['base_url'] . '/administrator/option/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
