<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Cập nhật người hỗ trợ</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/supporter'; ?>">Quản lý người hỗ trợ</a></li>
            <li class="active">Cập nhật người hỗ trợ</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/supporter'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                            <div class="col-xs-2">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/supporter/add'; ?>" class="btn btn-flat btn-success btn-backward"><i class="fa fa-plus-circle"></i> Thêm mới</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php echo form_open(base_url('administrator/supporter/edit'), 'id="frmaeditsupporter"', array('id' => $data->id)); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="name">Họ và tên <span class="text-danger">*</span></label>
                                    <input type="text" class="form-control" id="name" name="name" value="<?php echo $data->name ? $data->name : set_value('name'); ?>" placeholder="Họ và tên">
                                    <?php echo form_error('name', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email </label>
                                    <input type="text" class="form-control" id="email" name="email" value="<?php echo $data->email ? $data->email : set_value('email'); ?>" placeholder="Email">
                                    <?php echo form_error('email', '<p class="error">', '</p>'); ?>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Điện thoại</label>
                                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo $data->phone ? $data->phone : set_value('phone'); ?>" placeholder="Điện thoại">
                                </div>
                                <div class="form-group">
                                    <label>Ghi Chú:</label>
                                    <div>
                                        <textarea name="description" id="description" rows="5" cols="82" class="large-text"><?php echo $data->description ? $data->description : set_value('description'); ?></textarea> 
                                    </div>
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <?php
                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Cập nhật', array('class' => 'btn btn-flat btn-primary'));
                            echo '<a href="' . $this->config->config['base_url'] . '/administrator/supporter/delete/' . $data->id . '" class="btn btn-flat btn-danger btn-backward"><i class="fa fa-trash"></i> Xóa</a>';
                            echo '</div>';
                            ?>
                            <?php echo form_close(); ?>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
