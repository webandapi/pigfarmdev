<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-plus-circle"></i> Thêm câu hỏi mới</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li><a href="<?php echo $this->config->config['base_url'] . '/administrator/qa'; ?>">Quản lý câu hỏi</a></li>
            <li class="active">Thêm câu hỏi mới</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <a href="<?php echo $this->config->config['base_url'] . '/administrator/qa'; ?>" class="btn btn-flat btn-primary btn-backward"><i class="fa fa-backward"></i> Quay lại danh sách</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?php $this->load->view('administrator/message'); ?>
                            </div>
                        </div>
                        <div class="form-wrap">
                            <?php
                            echo form_open(base_url('administrator/qa/add'), 'id="frmaddqa"');
                            echo '<div class="box-body">';

                            //form-group
                            echo '<div class="form-group">';
                            echo form_label('Câu hỏi <span class="text-danger">*</span>', 'title');
                            $title = array(
                                'name' => 'title',
                                'id' => 'title',
                                'value' => set_value('title'),
                                'class' => 'form-control',
                                'placeholder' => 'Câu hỏi'
                            );
                            echo form_input($title);
                            echo form_error('title', '<p class="error">', '</p>');
                            echo '</div>';

                            //form-group
                            echo '<div class="form-group">';
                            echo form_label('Nội dung trả lời <span class="text-danger">*</span>', 'description');
                            $des = array(
                                'name' => 'description',
                                'id' => 'description',
                                'value' => set_value('description'),
                                'class' => 'ckeditor',
                                'placeholder' => 'Nội dung trả lời'
                            );
                            echo form_textarea($des);
                            echo form_error('description', '<p class="error">', '</p>');
                            echo '</div>';
                            echo '</div>';

                            //box-footer
                            echo'<div class="box-footer">';
                            echo form_submit('submit', 'Thêm mới', array('class' => 'btn btn-flat btn-primary'));
                            echo form_reset('reset', 'Hủy', array('class' => 'btn btn-flat btn-default'));
                            echo '</div>';
                            // form close
                            echo form_close();
                            ?>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
