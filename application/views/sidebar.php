<?php
// Active account nav
if (in_array($path_info, array('account', 'account/update', 'account/changepassword')))
    $account_root = 'active';
if ($path_info == 'account')
    $account = 'active';
if ($path_info == 'account/update')
    $account_update = 'active';
if ($path_info == 'account/changepassword')
    $account_changepass = 'active';

// Active device nav
if (in_array($path_info, array('devicechart', 'devicechart/detail', 'supportdevice')))
    $devicechart = 'active';

if ($path_info == 'supportdevice')
    $supportdevice = 'active';

if ($path_info == 'devicechart')
    $deviced = 'active';

if ($path_info == 'devicechart/detail')
    $device_detail = 'active';

// Active farm nav
if (in_array($path_info, array('farm', 'farm/add', 'farm/edit')))
    $farm_root = 'active';

if ($path_info == 'farm')
    $farm = 'active';

if ($path_info == 'farm/add')
    $farm_add = 'active';

// Active userfarm nav
if (in_array($path_info, array('userfarm', 'userfarm/add', 'userfarm/edit')))
    $userfarm_root = 'active';

if ($path_info == 'userfarm')
    $userfarm = 'active';

if ($path_info == 'userfarm/add')
    $userfarm_add = 'active';

// Active crops nav
if (in_array($path_info, array('crops', 'crops/add', 'crops/edit')))
    $crops_root = 'active';

if ($path_info == 'crops')
    $crops = 'active';

if ($path_info == 'crops/add')
    $crops_add = 'active';

// Active seeds nav
if (in_array($path_info, array('seeds', 'seeds/add', 'seeds/edit')))
    $seeds_root = 'active';

if ($path_info == 'seeds')
    $seeds = 'active';

if ($path_info == 'seeds/add')
    $seeds_add = 'active';

// Active planting nav
if (in_array($path_info, array('planting', 'planting/add', 'planting/edit', 'diary', 'diary/add', 'diary/edit', 'incidents', 'incidents/add', 'incidents/edit')))
    $planting_root = 'active';

if ($path_info == 'planting')
    $planting = 'active';

if ($path_info == 'planting/add')
    $planting_add = 'active';

// Active diary nav
if (in_array($path_info, array('diary', 'diary/add', 'diary/edit')))
    $diary_root = 'active';

if ($path_info == 'diary')
    $diary = 'active';

if ($path_info == 'diary/add')
    $diary_add = 'active';

// Active incidents nav
if (in_array($path_info, array('incidents', 'incidents/add', 'incidents/edit')))
    $incidents_root = 'active';

if ($path_info == 'incidents')
    $incidents = 'active';

if ($path_info == 'incidents/add')
    $incidents_add = 'active';

// Active harvest nav
if (in_array($path_info, array('harvest', 'harvest/add', 'harvest/edit')))
    $harvest_root = 'active';

if ($path_info == 'harvest')
    $harvest = 'active';

if ($path_info == 'harvest/add')
    $harvest_add = 'active';

// Important
if ($path_info == 'info/important')
    $important = 'active';

// User manuals
if ($path_info == 'info/user_manuals')
    $user_manuals = 'active';

// QA
if ($path_info == 'info/qa')
    $qa = 'active';
?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $current['avatar']; ?>" class="img-circle" alt="Avatar">
            </div>
            <div class="pull-left info">
                <p><a href="<?php echo $this->config->config['base_url'] . '/account'; ?>"><?php echo $current['fullname']; ?></a></p>
                <a href="<?php echo $this->config->config['base_url'] . '/account'; ?>"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Tìm kiếm...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview <?php echo $account_root; ?>">
                <a href="#">
                    <i class="fa fa-user-circle-o"></i>
                    <span>Tài khoản cá nhân</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $account; ?>"><a href="<?php echo $this->config->config['base_url'] . '/account'; ?>"><i class="fa fa-angle-double-right"></i> Thông tin tài khoản</a></li>
                    <li class="<?php echo $account_update; ?>"><a href="<?php echo $this->config->config['base_url'] . '/account/update'; ?>"><i class="fa fa-angle-double-right"></i> Cập nhật tài khoản</a></li>
                    <li class="<?php echo $account_changepass; ?>"><a href="<?php echo $this->config->config['base_url'] . '/account/changepassword'; ?>"><i class="fa fa-angle-double-right"></i> Đổi mật khẩu</a></li>
                    <li><a href="<?php echo $this->config->config['base_url'] . '/logout'; ?>"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
                </ul>
            </li>
            <li class="<?php echo $important; ?>">
                <a style="color: #00c0ef;" href="<?php echo $this->config->config['base_url'] . '/info/important'; ?>">
                    <i class="fa fa-star-half-full"></i> <span>Thông tin quan trọng</span>
                </a>
            </li>
            <li class="treeview <?php echo $devicechart; ?>">
                <a href="#">
                    <i class="fa fa-map-pin"></i>
                    <span>Thiết bị</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $supportdevice; ?>"><a href="<?php echo $this->config->config['base_url'] . '/supportdevice'; ?>"><i class="fa fa-map-pin"></i> Danh sách thiết bị</a></li>
                    <li class="<?php echo $deviced; ?>"><a href="<?php echo $this->config->config['base_url'] . '/devicechart'; ?>"><i class="fa fa-map-pin"></i> Biểu đồ</a></li>
                    <li class="<?php echo $device_detail; ?>"><a href="<?php echo $this->config->config['base_url'] . '/devicechart/detail'; ?>"><i class="fa fa-map-pin"></i> Chi tiết</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo $farm_root; ?>">
                <a href="#">
                    <i class="fa fa-home"></i>
                    <span>Danh sách trang trại</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $farm; ?>"><a href="<?php echo $this->config->config['base_url'] . '/farm'; ?>"><i class="fa fa-folder-open"></i> Quản lý trang trại</a></li>
                    <li class="<?php echo $farm_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/farm/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo $userfarm_root; ?>">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Danh sách nhân viên</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $userfarm; ?>"><a href="<?php echo $this->config->config['base_url'] . '/userfarm'; ?>"><i class="fa fa-folder-open"></i> Quản lý nhân viên</a></li>
                    <li class="<?php echo $userfarm_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/userfarm/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>
            <!-- List -->
            <li class="treeview <?php echo $planting_root; ?>">
                <a href="#">
                    <i class="fa fa-bars"></i>
                    <span>Danh mục</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="treeview <?php echo $crops_root; ?>">
                        <a href="#">
                            <i class="fa fa-tree"></i>
                            <span>Cây trồng, vật nuôi</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?php echo $crops; ?>"><a href="<?php echo $this->config->config['base_url'] . '/crops'; ?>"><i class="fa fa-folder-open"></i> Danh sách</a></li>
                            <li class="<?php echo $crops_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/crops/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?php echo $seeds_root; ?>">
                        <a href="#">
                            <i class="fa fa-pagelines"></i>
                            <span>Giống cây trồng, vật nuôi</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?php echo $seeds; ?>"><a href="<?php echo $this->config->config['base_url'] . '/seeds'; ?>"><i class="fa fa-folder-open"></i> Danh sách</a></li>
                            <li class="<?php echo $seeds_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/seeds/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?php echo $planting_root; ?>">
                    <a href="#">
                        <i class="fa fa-leaf"></i>
                        <span>Danh sách vụ mùa</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="<?php echo $planting; ?>"><a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>"><i class="fa fa-folder-open"></i> Danh sách</a></li>
                        <li class="<?php echo $planting_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/planting/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    </ul>
                </li>
                </ul>
            </li>
            <li class="treeview <?php echo $diary_root; ?>">
                <a href="#"><i class="fa fa-history"></i> Nhật ký sản xuất
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $diary; ?>"><a href="<?php echo $this->config->config['base_url'] . '/diary'; ?>"><i class="fa fa-folder-open"></i> Quản lý nhật ký</a></li>
                    <li class="<?php echo $diary_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/diary/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo $incidents_root; ?>">
                <a href="#"><i class="fa fa-sticky-note"></i> Quản lý sự cố
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $incidents; ?>"><a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>"><i class="fa fa-folder-open"></i> Quản lý sự cố</a></li>
                    <li class="<?php echo $incidents_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/incidents/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo $harvest_root; ?>">
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>Lần thu hoạch</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $harvest; ?>"><a href="<?php echo $this->config->config['base_url'] . '/harvest'; ?>"><i class="fa fa-folder-open"></i> Quản lý lần thu hoạch</a></li>
                    <li class="<?php echo $harvest_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/harvest/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>

            <!--<li class="treeview <?php echo $crops_root; ?>">
                <a href="#">
                    <i class="fa fa-tree"></i>
                    <span>Cây trồng, vật nuôi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $crops; ?>"><a href="<?php echo $this->config->config['base_url'] . '/crops'; ?>"><i class="fa fa-folder-open"></i> Danh sách</a></li>
                    <li class="<?php echo $crops_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/crops/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>
            <li class="treeview <?php echo $seeds_root; ?>">
                <a href="#">
                    <i class="fa fa-pagelines"></i>
                    <span>Giống cây trồng, vật nuôi</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $seeds; ?>"><a href="<?php echo $this->config->config['base_url'] . '/seeds'; ?>"><i class="fa fa-folder-open"></i> Danh sách</a></li>
                    <li class="<?php echo $seeds_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/seeds/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>

            <li class="treeview <?php echo $planting_root; ?>">
                <a href="#">
                    <i class="fa fa-leaf"></i>
                    <span>Danh sách vụ mùa</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $planting; ?>"><a href="<?php echo $this->config->config['base_url'] . '/planting'; ?>"><i class="fa fa-folder-open"></i> Danh sách</a></li>
                    <li class="<?php echo $planting_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/planting/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                    <li class="treeview <?php echo $diary_root; ?>">
                        <a href="#"><i class="fa fa-history"></i> Nhật ký sản xuất
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?php echo $diary; ?>"><a href="<?php echo $this->config->config['base_url'] . '/diary'; ?>"><i class="fa fa-folder-open"></i> Quản lý nhật ký</a></li>
                            <li class="<?php echo $diary_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/diary/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        </ul>
                    </li>
                    <li class="treeview <?php echo $incidents_root; ?>">
                        <a href="#"><i class="fa fa-sticky-note"></i> Sự cố
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="<?php echo $incidents; ?>"><a href="<?php echo $this->config->config['base_url'] . '/incidents'; ?>"><i class="fa fa-folder-open"></i> Quản lý sự cố</a></li>
                            <li class="<?php echo $incidents_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/incidents/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo $harvest_root; ?>">
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>Lần thu hoạch</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo $harvest; ?>"><a href="<?php echo $this->config->config['base_url'] . '/harvest'; ?>"><i class="fa fa-folder-open"></i> Quản lý lần thu hoạch</a></li>
                    <li class="<?php echo $harvest_add; ?>"><a href="<?php echo $this->config->config['base_url'] . '/harvest/add'; ?>"><i class="fa fa-plus-circle"></i> Thêm mới</a></li>
                </ul>
            </li>-->
            <li class="<?php echo $user_manuals; ?>">
                <a href="<?php echo $this->config->config['base_url'] . '/info/user_manuals'; ?>">
                    <i class="fa fa-sticky-note-o"></i> <span>Hướng dẫn sử dụng</span>
                </a>
            </li>
            <li class="<?php echo $qa; ?>">
                <a href="<?php echo $this->config->config['base_url'] . '/info/qa'; ?>">
                    <i class="fa fa-question-circle-o"></i> <span>Câu hỏi thường gặp</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>