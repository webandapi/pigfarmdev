            <footer class="main-footer">
                <!--<div class="pull-right hidden-xs">
                    <b>Version</b> 2.4.0
                </div>-->
                <strong>Copyright &copy; <?php echo date('Y'); ?> <a target="_blank" href="<?php echo $this->config->config['base_url']; ?>">Fman - Beta</a>.</strong> All rights
                reserved.
            </footer>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script>
       
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/raphael/raphael.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- daterangepicker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/moment/min/moment.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- Slimscroll -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/pages/dashboard.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/select2/dist/js/select2.full.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/iCheck/icheck.min.js"></script>
        <!-- CK Editor -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/library/ckeditor/ckeditor.js"></script>
        
        <!-- bootstrap time picker -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/plugins/timepicker/bootstrap-timepicker.min.js"></script>

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo $this->config->config['assets_path']; ?>/js/demo.js"></script>
         <script src="<?php echo $this->config->config['assets_path']; ?>/js/myscripts.js"></script>
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script>
            jQuery(function ($) {
                jQuery('.box-body table.dataTable').DataTable({
                            "oLanguage": {
                                "oPaginate": {
                                "sFirst": "Trang đầu", // This is the link to the first page
                                "sPrevious": "Trước", // This is the link to the previous page
                                "sNext": "Tiếp", // This is the link to the next page
                                "sLast": "Trang cuối" // This is the link to the last page
                                },
                                "sSearch": "Tìm kiếm:",
                                "sZeroRecords": "Không tìm thấy thông tin",
                                "sInfo": "Hiển thị (_START_ - _END_) / _TOTAL_",
                                "sLengthMenu": "Hiển thị _MENU_ dòng"
                            }
                            });
                //Date picker
                $('#datepicker, .datepicker').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy'
                });

                $('#datepicker1, .datepicker').datepicker({
                    autoclose: true,
                    format: 'dd/mm/yyyy'
                });
                //Timepicker
                $('.timepicker').timepicker({
                  showInputs: false
                });              
                 //Initialize Select2 Elements
                $('.select2').select2();
                
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                  checkboxClass: 'icheckbox_minimal-red',
                  radioClass   : 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                  checkboxClass: 'icheckbox_flat-green',
                  radioClass   : 'iradio_flat-green'
                });
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('ckeditor');
            });
        </script>
        <!--<script type="text/javascript">(function(){var isHttps=false;if(window.location.protocol==='https:') {isHttps=true;}window.abKiteAsyncInit=function(){abKiteSDK.init({appId:'7de454bb29548',abKiteServer:isHttps?'kite.antbuddy.com':'sdk.antin.co',insert2Selector:'body'});};(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}js=d.createElement(s);js.id=id;if(isHttps){js.src='//kite.antbuddy.com/sdk/v0.0.0/sdk.js';}else{js.src='//sdk.antin.co/sdk/v0.0.0/sdk.js';}fjs.parentNode.insertBefore(js, fjs);}(document,'script','ab-kite-jssdk'));})();</script>-->
        <!--New Kite chat-->
        <script type="text/javascript">(function(){var isHttps=false;if(window.location.protocol==='https:') {isHttps=true;}window.abKiteAsyncInit=function(){abKiteSDK.init({appId:'7de454bb29548',abKiteServer:isHttps?'kite.antbuddy.com':'sdk.antin.co',insert2Selector:'body'});};(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;}js=d.createElement(s);js.id=id;if(isHttps){js.src='//kite.antbuddy.com/sdk/v0.0.0/sdk.js';}else{js.src='//sdk.antin.co/sdk/v0.0.0/sdk.js';}fjs.parentNode.insertBefore(js, fjs);}(document,'script','ab-kite-jssdk'));})();</script>
        <!--New Kite call-->
        <script type="text/javascript">(function(){window.abKiteCallAsyncInit=function(){abKiteCallSDK.init({credential:'13DKForswE9HzxyzwFwt',realm:'farmtech.anttel-pro.ab-kz-02.antbuddy.com',kiteRoom:'Kite_vE5H3unp8MzwvqzytrHvFtqJLC3Izy',abKiteServer:'kite.antbuddy.com',isHttps:"https:"===window.location.protocol});};(function(d, s, id){var js, fjs=d.getElementsByTagName(s)[0];if(d.getElementById(id)){return;} js=d.createElement(s);js.id=id;js.src='https://kite.antbuddy.com/call/sdk/v0.0.0/sdk.js';fjs.parentNode.insertBefore(js, fjs);}(document,'script','ab-call-kite-jssdk'));})();</script>
    </body>
</html>
