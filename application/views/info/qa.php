<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-edit"></i> Câu hỏi thường gặp</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Câu hỏi thường gặp</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <section class="content">
                            <?php
                            if ($qa):
                                foreach ($qa as $val) :
                                    ?>
                                    <div class="box box-default collapsed-box">
                                        <div class="box-header with-border">
                                            <div class="user-block">
                                                <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/qa.jpg" alt="QA">
                                                <span class="username"><?php echo $val->title; ?></span>
                                                <span class="description">Câu hỏi thường gặp</span>
                                            </div>

                                            <div class="box-tools pull-right">
                                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                                </button>
                                            </div>
                                            <!-- /.box-tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="box-footer box-comments">
                                                <div class="box-comment">
                                                    <p><?php echo $val->description; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <?php
                                endforeach;
                            else:
                                echo '<p>Đang cập nhật...</p>';
                            endif;
                            ?>                          
                        </section>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
