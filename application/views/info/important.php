<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-star-half-full"></i> Thông tin quan trọng</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Thông tin quan trọng</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <section class="content">
                            <!-- Box Comment -->
                            <div class="box box-widget">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="">
                                        <span class="username"><a href="#">Cung cấp thông tin xác nhận loại hình trang trại</a></span>
                                        <span class="description">Thông tin quan trọng</span>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <!-- /.box-body -->
                                <div class="box-footer box-comments">
                                    <div class="box-comment">
                                        <!-- User image -->
                                        <img class="img-circle img-sm" style="float:left !important;max-width:10%;" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/qa.jpg" alt="">
                                        <div class="comment-text">
                                            <p>Sau khi thêm trang trại, bạn cần phải cung cấp các thông tin liên quan để Fcheck xác nhận loại hình trang trại của bạn.</p>
                                            <p>Để xác nhận loại hình trang trại, vui lòng liên hệ ban quản trị để được hỗ trợ.</p>
                                            <p><strong class="text-danger"><i>Hotline</i></strong>: <strong>0258 730 6899</strong></p>
                                            <p><strong class="text-danger"><i>Mobile</i></strong>: <strong>0909666306</strong></p>
                                            <p><strong class="text-danger"><i>Email</i></strong>: <strong>info@farmtech.vn</strong></p>
                                        </div>
                                        <!-- /.comment-text -->
                                    </div>
                                    <!-- /.box-comment -->
                                    <!-- /.box-comment -->
                                </div>
                            </div>
                            <!-- /.box -->

                            <!-- Box Comment -->
                            <div class="box box-widget">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="">
                                        <span class="username"><a href="#">Cung cấp thông tin xác nhận chứng chỉ trang trại</a></span>
                                        <span class="description">Thông tin quan trọng</span>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <!-- /.box-body -->
                                <div class="box-footer box-comments">
                                    <div class="box-comment">
                                        <!-- User image -->
                                        <img class="img-circle img-sm" style="float:left !important;max-width:10%;" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/qa.jpg" alt="">
                                        <div class="comment-text">
                                            <p>Sau khi thêm trang trại, bạn cần phải cung cấp các thông tin liên quan để Fcheck xác nhận chứng chỉ trang trại của bạn.</p>
                                            <p>Để xác nhận chứng chỉ trang trại, vui lòng liên hệ ban quản trị để được hỗ trợ.</p>
                                            <p><strong class="text-danger"><i>Hotline</i></strong>: <strong>0258 730 6899</strong></p>
                                            <p><strong class="text-danger"><i>Mobile</i></strong>: <strong>0909666306</strong></p>
                                            <p><strong class="text-danger"><i>Email</i></strong>: <strong>info@farmtech.vn</strong></p>
                                        </div>
                                        <!-- /.comment-text -->
                                    </div>
                                    <!-- /.box-comment -->
                                    <!-- /.box-comment -->
                                </div>
                            </div>
                            <!-- /.box -->

                            <!-- Box Comment -->
                            <div class="box box-widget">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="">
                                        <span class="username"><a href="#">Nhận hỗ trợ trực tuyến từ Farmtech</a></span>
                                        <span class="description">Thông tin quan trọng</span>
                                    </div>
                                </div>
                                <!-- /.box-header -->
                                <!-- /.box-body -->
                                <div class="box-footer box-comments">
                                    <div class="box-comment">
                                        <!-- User image -->
                                        <div class="col-xs-12">
                                        <div class="row">
                                            <img class="img-circle img-sm" style="float:left !important;max-width:10%;" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/qa.jpg" alt="">
                                            <div class="comment-text">
                                                <p>Sau khi bạn đăng nhập vào hệ thống. Để có thể nhận được <font color="red">hỗ trợ trực tuyến từ Farmtech</font>, bạn vui lòng nhấn vào icon ở góc dưới bên phải màn hình.</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <img class="helpimage" alt="huongdan5" style="max-width:10%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan5.png"/>
                                        </div>
                                        <div class="row">
                                            <div class="comment-text">
                                                <p>Chúc các bạn thành công.</p>
                                            </div>
                                        </div>
                                        </div>
                                        <!-- /.comment-text -->
                                    </div>
                                    <!-- /.box-comment -->
                                    <!-- /.box-comment -->
                                </div>
                            </div>
                            <!-- /.box -->
                        </section>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
