<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 class="text-uppercase"><i class="fa fa-sticky-note-o"></i> Hướng dẫn sử dụng</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->config->config['base_url'] . '/dashboard'; ?>"><i class="fa fa-dashboard"></i> Trang chủ</a></li>
            <li class="active">Hướng dẫn sử dụng</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                        <section class="content">

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">GIỚI THIỆU</span>
                                        <span class="description">Giới thiệu phần mềm truy xuất nguồn gốc fman</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <p>Fman là một hệ sinh thái hỗ trợ nhà nông bao gồm : Mạng xã hội nhà nông Fman, phần
                                            mềm quản lý trang trại, các thiết bị phần cứng IoT, Phần mềm truy xuất nguồn gốc
                                            <b><font color="red">FCHECK</font></b>.</p>
                                            <p>
                                            Trong tài liệu này chúng tôi hướng dẫn các bạn cách đăng kí và sử dụng phần mềm quản
                                            lý trang trại Fman và phân hệ truy xuất nguồn gốc FCHECK.
                                            Phần mềm quản lý trang trại Fman chạy trên môi trường Web dễ dàng sử dụng trên mọi
                                            thiết bị với các tính năng :
                                            <ul class="dash-list">
                                                <li>Quản lý nhiều trang trại</li>
                                                <li>Quản lý nhân sự trang trại</li>
                                                <li>Quản lý loại cây trồng</li>
                                                <li>Quản lý giống cây trồng</li>
                                                <li>Quản lý vụ trồng</li>
                                                <li>Nhật kí trồng cây</li>
                                                <li>Nhật kí sự cố</li>
                                                <li>Công bố chất lượng</li>
                                                <li>Truy xuất nguồn gốc <b><font color="red">FCHECK</font></b></li>
                                                <li>Tiện ích :
                                                    <ul>
                                                        <li>Theo dõi thời tiết</li>
                                                        <li>Kết nối chuyên gia</li>
                                                        <li>Gợi ý quy trình trồng</li>
                                                        <li>Theo dõi giá thị trường</li>
                                                        <li>Đọc bài viết hướng dẫn</li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            </p>
                                            <p>
                                            Trên nền tảng phần mềm quản lý trang trại Fman chúng tôi sẵn sàng triển khai các hình thức
                                            nâng cao thương hiệu nông sản của trang trại bằng cách :
                                            <ul class="dash-list">
                                                <li>Triển khai phân hệ truy xuất nguồn gốc <b><font color="red">FCHECK</font></b> bằng QR code</li>
                                                <li>Cung cấp các dịch vụ in ấn mã QR Code</li>
                                                <li>Dịch vụ Digital maketting</li>
                                                <li>Theo dõi môi trường trang trại bằng thiết bị IoT</li>
                                            </ul>
                                            </p>
                                            <p>
                                            Truy xuất nguồn gốc <b><font color="red">FCHECK</font></b> là cách truy xuất quy trình dành riêng cho sản phẩm nông
                                            nghiệp. Doanh nghiệp tăng giá trị thương hiệu bằng cách đảm bảo người dùng xem, kiểm tra
                                            toàn bộ thông tin nông sản từ nơi sản xuất ban đầu đến thành phẩm cuối cùng, các công đoạn
                                            từ sản xuất, chế biến và phân phối.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">GIỚI THIỆU PHẦN MỀM FMAN TRÊN WEB</span>
                                        <span class="description">Giới thiệu trang admin.farmtech.vn</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chúng ta chọn vào đường dẫn: <a style="cursor:pointer;" target="_blank" href="http://admin.farmtech.vn/">admin.farmtech.vn</a></p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan1" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan1.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Chúng ta sử dụng tài khoản được tạo ở ứng dụng Fman để đăng nhập, hoặc nếu chưa có chúng ta bấm vào nút Đăng ký tài khoản để tạo tài khoản mới.</p>
                                                </div>
                                                <div class="row">
                                                    <p>Giao diện chính của Fman</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan2" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan2.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Bên trái là menu: Nơi tổng hợp các tính năng của trang web</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan3" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan3.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Chúng ta có 4 ô màu đại diện cho các tính năng của trang web. Chúng ta có thể xem được các số liệu từ trang trại</p>
                                                </div>
                                                <div class="row">
                                                    <img class="helpimage" alt="huongdan4" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan4.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để nhận sự trợ giúp của Farmtech, bạn bấm vào biểu tượng ở góc phải của màn hình</t></p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan5" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan5.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Để chỉnh sửa tài khoản chúng ta nhấp chọn vào</p>
                                                </div>
                                                <div class="row">
                                                    <img class="helpimage" alt="huongdan6" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan6.png"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">TÀI KHOẢN CÁ NHÂN</span>
                                        <span class="description">Hướng dẫn sử dụng tài khoản cá nhân trên trang admin.farmtech.vn</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Tài khoản cá nhân" nằm ở menu trái</p>
                                                    <p>Chọn "Thông tin tài khoản"</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan7" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan7.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là thanh menu giúp chúng ta thực hiện các chỉnh sửa với tài khoản:</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan8" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan8.png"/>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan9" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan9.png"/>
                                                </div>
                                                <div class="row">
                                                    <p><b>Đổi mật khẩu:</b></p>
                                                    <p>Bạn nhấp vào tùy chọn "Đổi mật khẩu" ở menu trên</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan10" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan10.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p><b>Cập nhật tài khoản</b></p>
                                                    <p>Bạn nhấp vào tùy chọn "Cập nhật tài khoản" ở menu trên</p>
                                                </div>
                                                <div class="row">
                                                    <img class="helpimage" alt="huongdan11" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan11.png"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">TRANG TRẠI</span>
                                        <span class="description">Hướng dẫn sử dụng và quản lý các nông trại trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Trang trại" nằm ở menu trái</p>
                                                    <p>Chọn "Quản lý trang trại"</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan12" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan12.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm một trang trại mới, bạn nhấp vào "Thêm mới" ở menu trái hoặc bấm vào nút "Thêm trang trại mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan13" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan13.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font>*</font> bắt buộc phải nhập</p>
                                                    <p>Tại mục Sản phẩm, chúng ta có thể chọn tất cả sản phẩm của nông trại mình đang có</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan14" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan14.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan15" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan15.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p><font color="red"><b>Lưu ý: Sau khi lưu lại chúng ta không thể chỉnh sửa lại vị trí của trang trại</b></font></p>
                                                    <p>Ta hoàn thành việc thêm trang trại hoặc hủy việc thêm trang trại bằng 2 nút này</p>
                                                </div>
                                                <div class="row">
                                                    <img class="helpimage" alt="huongdan16" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan16.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Chúng ta đã thêm thành công</p>
                                                </div>
                                                <div class="row">
                                                    <img class="helpimage" alt="huongdan17" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan17.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm:</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img class="helpimage" alt="huongdan18" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan18.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> trang trại, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin trang trại, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Nếu các bạn <b>có chứng nhận và thông tin về loại trang trại</b>, hãy gửi các giấy tờ chứng mình của trang trại của các bạn cho chúng tôi theo email <i>info@farmtech.vn</i> . Việc bổ sung các thông tin trên sẽ giúp tăng sự uy tín của trang trại đối với khách hàng.</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img class="helpimage" alt="huongdan21" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan21.png"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">NHÂN VIÊN</span>
                                        <span class="description">Hướng dẫn sử dụng mục quản lý nhân viên trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Nhân viên" nằm ở menu trái</p>
                                                    <p>Chọn "Quản lý nhân viên"</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan22" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan22.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm nhân viên mới, các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm nhân viên mới"</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan23" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan23.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                    <p>Mục <b>Trang trại</b>, chúng ta chọn chính xác nơi làm việc của nhân viên</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan24" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan24.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm nhân viên hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan26" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan26.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan27" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan27.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> nhân viên, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin nhân viên, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">CÂY TRỒNG, VẬT NUÔI</span>
                                        <span class="description">Hướng dẫn cách quản lý cây trồng và vật nuôi trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Cây trồng, vật nuôi" nằm ở menu trái</p>
                                                    <p>Chọn "Danh sách"</p>
                                                </div>
                                                <div class="row">
                                                    <img alt="huongdan28" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan28.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm cây trồng, vật nuôi mới, các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm cây trồng, vật nuôi mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan29" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan29.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan30" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan30.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm <b>cây trồng, vật nuôi</b> hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:30%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan31" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan31.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan32" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan32.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> cây trồng, vật nuôi, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin cây trồng, vật nuôi, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">GIỐNG CÂY TRỒNG, VẬT NUÔI</span>
                                        <span class="description">Hướng dẫn cách quản lý giống cây trồng và vật nuôi trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Giống cây trồng, vật nuôi" nằm ở menu trái</p>
                                                    <p>Chọn "Danh sách"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan33" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan33.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Chúng ta có thể lọc những giống cây trồng, vật nuôi ở một trang trại cụ thể</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan34" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan34.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm giống cây trồng, vật nuôi mới, các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm giống cây trồng, vật nuôi mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan35" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan35.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                    <p>Ở đây ta có thể chọn các loại hạt giống và vật nuôi ứng với các loại cây trồng trong trang trại trong mục <b>Cây trồng, vật nuôi</b></p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan36" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan36.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm <b>giống cây trồng, vật nuôi</b> hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:30%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan37" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan37.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan38" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan38.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> giống cây trồng, vật nuôi, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin giống cây trồng, vật nuôi, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">VỤ CÂY TRỒNG, VẬT NUÔI</span>
                                        <span class="description">Hướng dẫn cách quản lý vụ cây trồng và vật nuôi trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Vụ cây trồng, vật nuôi" nằm ở menu trái</p>
                                                    <p>Chọn "Danh sách"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan39" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan39.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Chúng ta có thể lọc vụ cây trồng, vật nuôi ở một trang trại cụ thể</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan40" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan40.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm thông tin vụ cây trồng, vật nuôi mới, các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm vụ cây trồng, vật nuôi mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan41" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan41.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan42" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan42.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm <b>vụ cây trồng, vật nuôi</b> hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:30%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan43" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan43.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan44" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan44.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> vụ cây trồng, vật nuôi, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin vụ cây trồng, vật nuôi, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Cột <b>Trạng thái</b> được kích hoạt sau khi thêm vào, muốn <b>thay đổi trạng thái vụ trồng</b> ta bấm nút chỉnh sửa</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">NHẬT KÝ XỬ LÝ</span>
                                        <span class="description">Hướng dẫn cách sử dụng nhật ký xử lý trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Vụ cây trồng, vật nuôi" nằm ở menu trái</p>
                                                    <p>Chọn "Nhật ký xử lý"</p>
                                                    <p>Chọn "Quản lý nhật ký"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan45" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan45.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm nhật ký xử lý mới các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm nhật ký xử lý mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan46" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan46.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan47" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan47.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm <b>nhật ký</b> hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:30%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan48" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan48.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan49" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan49.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> nhật ký, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin nhật ký, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">SỰ CỐ</span>
                                        <span class="description">Hướng dẫn cách sử dụng mục sự cố trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Vụ cây trồng, vật nuôi" nằm ở menu trái</p>
                                                    <p>Chọn "Sự cố"</p>
                                                    <p>Chọn "Quản lý sự cố"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan50" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan50.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm sự cố mới các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm sự cố mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan51" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan51.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan52" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan52.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm <b>sự cố</b> hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:30%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan53" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan53.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan54" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan54.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> sự cố, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin sự cố, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                            <!-- Help -->
                            <div class="box box-default collapsed-box">
                                <div class="box-header with-border">
                                    <div class="user-block">
                                        <img class="img-circle" src="<?php echo $this->config->config['assets_path']; ?>/images/icons/logo.png" alt="FMAN">
                                        <span class="username">LẦN THU HOẠCH</span>
                                        <span class="description">Hướng dẫn cách sử dụng mục lần thu hoạch trên trang</span>
                                    </div>

                                    <div class="box-tools pull-right">
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <!-- /.box-tools -->
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-footer box-comments">
                                        <div class="box-comment">
                                            <div class="col-xs-12">
                                                <div class="row">
                                                    <p>Chọn tùy chọn "Lần thu hoạch" nằm ở menu trái</p>
                                                    <p>Chọn "Quản lý lần thu hoạch"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan55" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan55.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Để thêm lần thu hoạch mới các bạn nhấp vào "Thêm mới" ở menu trái hoặc nhấp vào nút "Thêm lần thu hoạch mới"</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan56" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan56.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Ta điền thông tin vào các mục, các mục có dấu <font color="red">*</font> bắt buộc phải nhập</p>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 text-center">
                                                        <img class="helpimage" alt="huongdan57" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan57.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Để hoàn thành việc thêm <b>lần thu hoạch</b> hoặc hủy bỏ việc đang làm bằng 2 nút này</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan25" style="max-width:30%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan25.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <p>Sau khi thêm thành công</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan58" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan58.png"/>
                                                </div>
                                                <div class="row">
                                                    <p><b><font color="red">Chú ý: Nhập lần thu hoạch tiếp theo chúng ta phải để mã QR lớn hơn lần trước</font></b></p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan60" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan60.png"/>
                                                </div>
                                                <div class="row">
                                                    <p>Đây là kết quả sau khi chúng ta đã thêm</p>
                                                </div>
                                                <div class="row text-center">
                                                    <img alt="huongdan59" style="max-width:100%;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan59.png"/>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>xóa</b> lần thu hoạch, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan19" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan19.png"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <p>Muốn <b>sửa</b> thông tin lần thu hoạch, bạn chỉ cần ấn</p>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <img class="helpimage" alt="huongdan20" style="max-width:15%; float:left !important;" src="<?php echo $this->config->config['assets_path']; ?>/images/tutorial/huongdan20.png"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <!--End help-->

                        </section>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
