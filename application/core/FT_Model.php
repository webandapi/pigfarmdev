<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FT_Model extends CI_Model {

    // Ten table
    var $table = '';
    // Key chinh cua table
    var $key = 'id';
    // Key chinh cua table
    var $adminkey = 'username';
    var $userkey = 'user_id';
    // Order mac dinh (VD: $order = array('id', 'desc))
    var $order = '';
    // Cac field select mac dinh khi get_list (VD: $select = 'id, name')
    var $select = '';

    /**
     * Them row moi
     * $data : du lieu ma ta can them
     */
    function create($data = array()) {
        if ($this->db->insert($this->table, $data)) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }

    /**
     * Cap nhat row tu id
     * $id : khoa chinh cua bang can sua
     * $data : mang du lieu can sua
     */
    function update($id, $data) {
        if (!$id) {
            return FALSE;
        }
        $this->check_param($data, $this->params);

        $where = array();
        $where[$this->key] = $id;
        $this->update_rule($where, $data);

        return TRUE;
    }

    /**
     * Cap nhat row tu dieu kien
     * $where : dieu kien
     * $data : mang du lieu can cap nhat
     */
    function update_rule($where, $data) {
        if (!$where) {
            return FALSE;
        }

        $this->db->where($where);
        $this->db->set($data);
        $this->db->update($this->table, $data);

        return TRUE;
    }

    /**
     * Xoa row tu id
     * $id : gia tri cua khoa chinh
     */
    function delete($id) {
        if (!$id) {
            return FALSE;
        }
        //neu la so
        if (is_numeric($id)) {
            $where = array($this->key => $id);
        } else {
            //$id = 1,2,3..
            $where = $this->key . " IN (" . $id . ") ";
        }
        $this->del_rule($where);

        return TRUE;
    }

    /**
     * Delete data from table (by where)
     * @param type $where
     * @return boolean
     */
    function del_rule($where = array()) {
        if ($where) {
            $this->db->where($where);
            $this->db->delete($this->table);
        }
        return FALSE;
    }

    /**
     * Query SQL
     * @param type $sql
     * @return type
     */
    function query($sql) {
        $rows = $this->db->query($sql);
        return $rows->result;
    }

    /**
     * Lay thong tin cua row tu id
     * $id : id can lay thong tin
     * $field : cot du lieu ma can lay
     */
    function get_user_field_info($user_id, $field) {
        if (!$user_id || !$field) {
            return FALSE;
        }

        $where = array();
        $where[$this->key1] = $user_id;
        $where[$this->key2] = $field;
        return $this->get_info_rule($where);
    }

    function get_rate_info($farmer_id, $expert_id) {
        if (!$farmer_id || !$expert_id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key1] = $farmer_id;
        $where[$this->key2] = $expert_id;
        return $this->get_info_rule($where);
    }

    function get_post_rate_info($user_id, $post_id) {
        if (!$user_id || !$post_id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key1] = $user_id;
        $where[$this->key2] = $post_id;
        return $this->get_info_rule($where);
    }

    /**
     * Lay thong tin cua row tu id
     * $id : id can lay thong tin
     * $field : cot du lieu ma can lay
     */
    function get_info($id, $field = '') {
        if (!$id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key] = $id;

        return $this->get_info_rule($where, $field);
    }

    /**
     * Lay thong tin cua admin tu username
     * $username : username can lay thong tin
     * $field : cot du lieu ma can lay
     */
    function get_admin_info($username, $field = '') {
        if (!$username) {
            return FALSE;
        }

        $where = array();
        $where[$this->adminkey] = $username;

        return $this->get_info_rule($where, $field);
    }

    /**
     * Lay thong tin cua row tu dieu kien
     * $where: Mảng điều kiện
     * $field: Cột muốn lấy dữ liệu
     */
    function get_info_rule($where = array(), $field = '') {
        if ($field) {
            $this->db->select($field);
        }
        $this->db->where($where);
        $query = $this->db->get($this->table);
        if ($query->num_rows()) {
            return $query->row();
        }

        return FALSE;
    }

    /**
     * Lay tong so
     */
    function get_total($input = array()) {
        $this->get_list_set_input($input);

        $query = $this->db->get($this->table);

        return $query->num_rows();
    }

    /**
     * Lay tat ca
     */
    function get_all($input = array()) {
        $this->get_list_set_input($input);

        $query = $this->db->get($this->table);

        return $query->row();
    }

    /**
     * Lay tong so
     * $field: cot muon tinh tong
     */
    function get_sum($field, $where = array()) {
        $this->db->select_sum($field); //tinh rong
        $this->db->where($where); //dieu kien
        $this->db->from($this->table);

        $row = $this->db->get()->row();
        foreach ($row as $f => $v) {
            $sum = $v;
        }
        return $sum;
    }

    /**
     * Lay 1 row
     */
    function get_row($input = array()) {
        $this->get_list_set_input($input);

        $query = $this->db->get($this->table);

        return $query->row();
    }

    /**
     * Lay danh sach
     * $input : mang cac du lieu dau vao
     */
    function get_list($input = array()) {
        //xu ly ca du lieu dau vao
        $this->get_list_set_input($input);

        //thuc hien truy van du lieu
        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        return $query->result();
    }

    function my_get_list($input = array(), $field = '') {
        $this->db->where($input);
        if ($field) {
            $this->db->select($field);
        }
        $query = $this->db->get($this->table);
        return $query->result();
    }

    /**
     * Gan cac thuoc tinh trong input khi lay danh sach
     * $input : mang du lieu dau vao
     */
    protected function get_list_set_input($input = array()) {

        // Thêm điều kiện cho câu truy vấn truyền qua biến $input['where'] 
        //(vi du: $input['where'] = array('email' => 'hocphp@gmail.com'))
        if ((isset($input['where'])) && $input['where']) {
            $this->db->where($input['where']);
        }

        //tim kiem like
        // $input['like'] = array('name' => 'abc');
        if ((isset($input['like'])) && $input['like']) {
            echo $input['like'][0] . '_' . $input['like'][1];
            $this->db->like($input['like'][0], $input['like'][1]);
        }

        // Thêm sắp xếp dữ liệu thông qua biến $input['order'] 
        //(ví dụ $input['order'] = array('id','DESC'))
        if (isset($input['order'][0]) && isset($input['order'][1])) {
            $this->db->order_by($input['order'][0], $input['order'][1]);
        } else {
            //mặc định sẽ sắp xếp theo id giảm dần 
            $order = ($this->order == '') ? array($this->table . '.' . $this->key, 'DESC') : $this->order;
            $this->db->order_by($order[0], $order[1]);
        }

        // Thêm điều kiện limit cho câu truy vấn thông qua biến $input['limit'] 
        //(ví dụ $input['limit'] = array('10' ,'0')) 
        if (isset($input['limit'][0]) && isset($input['limit'][1])) {
            $this->db->limit($input['limit'][0], $input['limit'][1]);
        }
    }

    /**
     * kiểm tra sự tồn tại của dữ liệu theo 1 điều kiện nào đó
     * $where : mang du lieu dieu kien
     */
    function check_exists($where = array()) {
        $this->db->where($where);
        //thuc hien cau truy van lay du lieu
        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function my_check_exists($value) {

        $this->db->where(array($this->key => $value));
        //thuc hien cau truy van lay du lieu
        $query = $this->db->get($this->table);
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {

            return FALSE;
        }
    }

    function check_param($values, $params = '') {
        if (!$params)
            $params = $this->params;
        foreach ($values as $key => $value) {
            if (!in_array($key, $params)) {
                echo json_encode(array('messageCode' => 404, 'message' => $key . ' is invailid param!'));
                exit();
            }
        }
        return true;
    }

    function check_param2($values, $params = '') {
        foreach ($values as $key => $value) {
            if (!in_array($value, $params)) {
                echo json_encode(array('messageCode' => 500, 'message' => $value . ' is invailid param!'));
                exit();
            }
        }
        return true;
    }

//------------------------------------------------------------------------------------------------------------------------
    // These constants may be changed without breaking existing hashes.
    const FT_Model_HASH_ALGORITHM = "sha256";
    const FT_Model_ITERATIONS = 1000;
    const FT_Model_SALT_BYTES = 24;
    const FT_Model_HASH_BYTES = 24;
    const HASH_SECTIONS = 4;
    const HASH_ALGORITHM_INDEX = 0;
    const HASH_ITERATION_INDEX = 1;
    const HASH_SALT_INDEX = 2;
    const HASH_FT_Model_INDEX = 3;

    /**
     * Creates a hash for the given password
     *
     * @param string $password    the password to hash
     * @return string             the hashed password in format "algorithm:iterations:salt:hash"
     */
    public function create_hash($password) {
        $salt = base64_encode(mcrypt_create_iv(FT_Model::FT_Model_SALT_BYTES, MCRYPT_DEV_URANDOM));

        return FT_Model::FT_Model_HASH_ALGORITHM . ":" . FT_Model::FT_Model_ITERATIONS . ":" . $salt . ":" .
                base64_encode($this->hash(
                                FT_Model::FT_Model_HASH_ALGORITHM, $password, $salt, FT_Model::FT_Model_ITERATIONS, FT_Model::FT_Model_HASH_BYTES, true
        ));
    }

    /**
     * Checks if the given password matches the given hash created by PBKDF::create_hash( string )
     *
     * @param string $password     the password to check
     * @param string $good_hash    the hash which should be match the password
     * @return boolean             true if $password and $good_hash match, false otherwise
     *
     * @see FT_Model::create_hash
     */
    public function validate_password($password, $good_hash) {
        $params = explode(":", $good_hash);
        if (count($params) < FT_Model::HASH_SECTIONS)
            return false;
        $FT_Model = base64_decode($params[FT_Model::HASH_FT_Model_INDEX]);
        return $this->slow_equals(
                        $FT_Model, $this->hash(
                                $params[FT_Model::HASH_ALGORITHM_INDEX], $password, $params[FT_Model::HASH_SALT_INDEX], (int) $params[FT_Model::HASH_ITERATION_INDEX], strlen($FT_Model), true
                        )
        );
    }

    /**
     * Compares two strings $a and $b in length-constant time
     *
     * @param string $a    the first string
     * @param string $b    the second string
     * @return boolean     true if they are equal, false otherwise
     */
    public function slow_equals($a, $b) {
        $diff = strlen($a) ^ strlen($b);
        for ($i = 0; $i < strlen($a) && $i < strlen($b); $i++) {
            $diff |= ord($a[$i]) ^ ord($b[$i]);
        }
        return $diff === 0;
    }

    /**
     * FT_Model key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
     *
     * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
     *
     * This implementation of FT_Model was originally created by https://defuse.ca
     * With improvements by http://www.variations-of-shadow.com
     * Added support for the native PHP implementation by TheBlintOne
     *
     * @param string $algorithm                                 the hash algorithm to use. Recommended: SHA256
     * @param string $password                                  the Password
     * @param string $salt                                      a salt that is unique to the password
     * @param int $count                                        iteration count. Higher is better, but slower. Recommended: At least 1000
     * @param int $key_length                                   the length of the derived key in bytes
     * @param boolean $raw_output [optional] (default false)    if true, the key is returned in raw binary format. Hex encoded otherwise
     * @return string                                           a $key_length-byte key derived from the password and salt,
     *                                                          depending on $raw_output this is either Hex encoded or raw binary
     * @throws Exception                                        if the hash algorithm are not found or if there are invalid parameters
     */
    public function hash($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {
        $algorithm = strtolower($algorithm);
        if (!in_array($algorithm, hash_algos(), true))
            throw new Exception('FT_Model ERROR: Invalid hash algorithm.');
        if ($count <= 0 || $key_length <= 0)
            throw new Exception('FT_Model ERROR: Invalid parameters.');

        // use the native implementation of the algorithm if available
        if (function_exists("hash_FT_Model")) {
            return hash_FT_Model($algorithm, $password, $salt, $count, $key_length, $raw_output);
        }

        $hash_length = strlen(hash($algorithm, "", true));
        $block_count = ceil($key_length / $hash_length);

        $output = "";
        for ($i = 1; $i <= $block_count; $i++) {
            // $i encoded as 4 bytes, big endian.
            $last = $salt . pack("N", $i);
            // first iteration
            $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
            // perform the other $count - 1 iterations
            for ($j = 1; $j < $count; $j++) {
                $xorsum ^= ( $last = hash_hmac($algorithm, $last, $password, true) );
            }
            $output .= $xorsum;
        }

        if ($raw_output)
            return substr($output, 0, $key_length);
        else
            return bin2hex(substr($output, 0, $key_length));
    }

    public function test_hash($raw_password, $salt) {
        return base64_encode($this->hash(
                        FT_Model::FT_Model_HASH_ALGORITHM, $password, $salt, FT_Model::FT_Model_ITERATIONS, FT_Model::FT_Model_HASH_BYTES, true
        ));
    }

}
