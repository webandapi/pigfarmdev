<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class FT_Controller extends CI_Controller {

    var $accessToken;
    var $fieldKey = 'id';
    var $messageCode = 'messageCode';
    var $message = 'message';
    var $NullObj = '';
    var $NullArray = [];
    var $secretKey = 'ecee85869dbb8dc0e35a260af4f01202';
    var $codeSuccess = 200; //OK : The request is OK (this is the standard response for successful HTTP requests)
    var $codeCreated = 201; //Created :The request has been fulfilled, and a new resource is created 
    var $codeAccepted = 202; //Accepted : The request has been accepted for processing, but the processing has not been completed
    var $codeNonAuthoritativeInformation = 203; //Non-Authoritative Information : The request has been successfully processed, but is returning information that may be from another source
    var $codeNoContent = 204; //No Content : The request has been successfully processed, but is not returning any content
    var $codeResetContent = 205; //Reset Content : The request has been successfully processed, but is not returning any content, and requires that the requester reset the document view
    var $codePartialContent = 206; //Partial Content : The server is delivering only part of the resource due to a range header sent by the client
    var $codeUpdated = 210;
    var $codeNotFound = 404;
    var $codeDuplicated = 409;
    var $codeFailed = 500;
    var $admin;
    var $system;
    //message code for Token
    var $codeTokenSuccess = 600;
    var $codeTokenExpired = 601;
    var $codeTokenInvalid = 602;
    var $fbUrlHeader = '';
    var $language = '';

    /**
     * __construct
     */
    function __construct() {

        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->model('rate_model');
        $this->load->model('profile_model');
        $this->load->model('userfield_model');
        $this->load->model('field_model');
        $this->load->model('usertype_model');
        $this->load->model('adminrole_model');
        $this->load->model('language_model');
        $this->load->model('level_model');
        $this->load->model('post_rate_model');
        $this->load->model('token_model');
        $this->load->model('user_model');
        $this->load->model('farm_model');
        $this->load->model('farmtype_model');
        $this->load->model('product_model');
        $this->load->model('slug_model');
        $this->load->model('options_model');
        $this->load->model('post_model');
        $this->load->model('farmproduct_model');
        $this->load->model('topic_model');
        $this->load->model('readpost_model');
        $this->load->model('mail_model');
        $this->load->model('contact_model');
        $this->load->model('address_model');
        $this->load->model('userconnected_model');
        $this->load->model('wp_term_taxonomy_model');
        $this->load->model('device_model');
        $this->load->model('devicetype_model');
        $this->load->model('devicetype_sensor_model');
        $this->load->model('sensor_model');
        $this->load->model('admin_model');
        $this->load->model('searchrank_model');
        $this->load->model('saleproduct_model');
        $this->load->model('regisproduct_model');
        $this->load->model('module_model');
        $this->load->model('userfarm_model');
        $this->load->model('crops_model');
        $this->load->model('seeds_model');
        $this->load->model('planting_model');
        $this->load->model('harvest_model');
        $this->load->model('diary_model');
        $this->load->model('incidents_model');
        $this->load->model('qrcode_model');
        $this->load->model('distributor_model');
        $this->load->model('harvest_distributor_model');
        $this->load->model('certificate_model');
        $this->load->model('farm_certificate_model');
        $this->load->model('province_model');
        $this->load->model('qa_model');
        $this->load->model('farm_crops_model');
        $this->load->model('supporter_model');
        $this->load->model('supportdevice_model');
        //Notify model
        $this->load->model('notifycation_model');
        $this->load->library('session');
        $this->load->helper('file');
        $this->load->helper('url');
        $this->load->helper('form');
        
        $this->load->dbutil();
        $controller = $this->uri->segment(1);
        $controller = strtolower($controller);
        $function = $this->uri->segment(2);
        $function = strtolower($function);

        if ($this->language == '') {
            if (!$this->session->userdata('language'))
                $this->language = 'vietnamese';
            else {
                $this->language = $this->session->userdata('language');
            }
            if ($this->language == 'english')
                $this->lang->load('sms_lang', 'english');
            else
                $this->lang->load('sms_lang', 'vietnamese');
            $this->load->helper('html');
            $this->system = $this->user_model->get_user_info('system');
            $this->admin = $this->user_model->get_user_info('admin');
        }
    }

    public function strip_images($content) {
        $content = preg_replace('/<img[^>]+./', '<img>', $content);
        $content = str_replace('<', "&lt", $content);
        $content = str_replace('>', "&gt", $content);
        return $content = preg_replace('/\[avatar user="admin" \/\]/', '', $content);
    }

    public function getPostCategories($id = '') {
        $post_categories = wp_get_post_categories($id);

        $cats = array();

        foreach ($post_categories as $c) {
            $cat = get_category($c);
            $cats[] = array('name' => $cat->name, 'slug' => $cat->slug, 'id' => $this->field_model->get_field_id_by_term_id($cat->term_id));
        }
        return $cats;
    }

    public function getPostThumbnail($id) {
        return get_the_post_thumbnail_url($id);
    }

    public function getAttachmentImage($id = '') {
        if ($id) {
            $img = array();
            $attachments = get_posts(array(
                'post_type' => 'attachment',
                'numberposts' => -1,
                'post_status' => "",
                'post_parent' => $id
            ));

            if ($attachments) {
                foreach ($attachments as $attachment) {
                    //$imgInfo = wp_get_attachment_image_src(  $attachment->ID, array('150', '150'), "", array( "class" => "img-responsive" ));
                    $imgInfo = wp_get_attachment_image_src($attachment->ID, "", array("class" => "img-responsive"));
                    $img[] = array('link' => $imgInfo[0], 'title' => $attachment->post_title);
                }
            }
        }
        return $img;
    }

    //convert properties of model to string if it is numeric
    function to_int($model) {
        if (is_array($model)) {
            foreach ($model as $key => $value) {
                if (is_numeric($value) && $key != 'phone' && $key != 'username' && $key != 'address')
                    $model[$key] = (int) $value;
            }
        }else {
            foreach ($model as $key => $value) {
                if (is_numeric($value) && $key != 'phone' && $key != 'username' && $key != 'address')
                    $model->$key = (int) $value;
            }
        }

        return $model;
    }

    //update star for expert profile after rating.
    function update_star($expert_id) {
        if ($expert_id) {
            $rateCol = $this->rate_model->get_rate_of_expert_list($expert_id);
            $totalStar = 0;
            foreach ($rateCol as $key => $value) {
                $totalStar += $value->star;
            }
            if ($this->profile_model->update($expert_id, array('total_rate' => count($rateCol), 'total_star' => ($totalStar))))
                return true;
            else {

                echo json_encode(array('messageCode' => 500, 'message' => $this->lang->line('failed')));
                exit();
            }
        }
    }

    //get rate info
    function get_farmer_rate_expert_info($farmer_id, $expert_id) {
        $profile = $this->profile_model->get_info($expert_id);
        $rateCol = $this->rate_model->get_rate_of_expert_list($expert_id);
        $expertStar;
        if ($profile->total_rate)
            $expertStar = round((float) ($profile->total_star / ($profile->total_rate)), 1);
        $rate = $this->rate_model->get_rate_info($farmer_id, $expert_id);
        if (!$rate) {
            return false;
        }
        return array('averageStar' => $expertStar, 'star' => $rate->star, 'comment' => $rate->comment, 'time' => $rate->time);
    }

    function get_all_custom_user_info($user_id) {
        $user = $this->user_model->get_info($user_id);
        if ($user) {
            $fields = array();
            foreach ($this->userfield_model->getFieldsByUserId($user->user_id) as $key => $value) {
                $fields[] = $this->field_model->get_info($value);
            }
            $this->to_int($fields);
            $user->field = $fields;
            $user->usertype = $this->usertype_model->get_info($user->usertype_id);

            $user->language = $this->language_model->get_info($user->language);

            //var_dump($user);
            $this->null_filter($user);
            $this->to_int($user);
            $profile = $this->get_custom_expert_profile($user);
            unset($user->pw);
            unset($user->user_id);
            unset($user->usertype_id);
        }
        return array('user' => $user, 'expert_profile' => $profile);
    }

    function get_user_basic_info($user_id) {
        $user = $this->user_model->get_info($user_id);
        foreach ($user as $key => $value) {
            if ($key != 'username' && $key != 'firstname' && $key != 'lastname' && $key != 'avatar')
                unset($user->$key);
        }
        return $user;
    }

    function get_custom_expert_profile($user) {
        $this->null_filter($user);
        $this->to_int($user);
        $profile = '';
        if ($user->usertype_id == 2) {
            $profile = $this->profile_model->get_info($user->user_id);
            if ($profile) {
                $profile->username = $user->username;
                $profile->level = ($this->level_model->get_info($profile->level_id) == false ? "" : $this->level_model->get_info($profile->level_id));
                unset($profile->user_id);
                unset($profile->level_id);
            } else
                $profile = "";
        }
        return $profile;
    }

    function null_filter($object) {
        //$object = array($object);
        foreach ($object as $key => $value) {
            if ($value == '')
                $object->$key = '';
        }
        return $object;
    }

    function getUserRatedUpList($id) {
        //expertRateList for post
        $userRatedUp = $this->post_rate_model->get_user_rated_up($id);
        //var_dump($userRatedUp);
        //$userRatedUpList=array();
        foreach ($userRatedUp as $key => $value) {
            $value->user = $this->get_user_basic_info($value->user_id);
            unset($value->post_id);
            unset($value->user_id);
            unset($value->rate);
        }
        return $userRatedUp;
    }

    function getUserRatedDownList($id) {
        $userRatedDown = $this->post_rate_model->get_user_rated_down($id);
        //$userRatedDownList=array();
        foreach ($userRatedDown as $key => $value) {
            $value->user = $this->get_user_basic_info($value->user_id);
            unset($value->post_id);
            unset($value->user_id);
            unset($value->rate);
        }
        return $userRatedDown;
    }

    function getUserRatedUpList_web($id) {
        //expertRateList for post
        $userRatedUp = $this->post_rate_model->get_user_rated_up($id);
        //var_dump($userRatedUp);
        //$userRatedUpList=array();
        foreach ($userRatedUp as $key => $value) {
            $value->user = $this->user_model->get_info($value->user_id);
            //unset($value->post_id);
            //unset($value->user_id);
            unset($value->rate);
        }
        return $userRatedUp;
    }

    function getUserRatedDownList_web($id) {
        $userRatedDown = $this->post_rate_model->get_user_rated_down($id);
        //$userRatedDownList=array();
        foreach ($userRatedDown as $key => $value) {
            $value->user = $this->user_model->get_info($value->user_id);
            //unset($value->post_id);
            //unset($value->user_id);
            unset($value->rate);
        }
        return $userRatedDown;
    }

    function getUserRatedUpListCount($id) {
        //expertRateList for post
        $userRatedUp = $this->post_rate_model->get_user_rated_up($id);
        //$userRatedUp=array($userRatedUp);
        return count($userRatedUp);
    }

    function getUserRatedDownListCount($id) {
        $userRatedDown = $this->post_rate_model->get_user_rated_down($id);
        //$userRatedDown=array($userRatedDown);
        return count($userRatedDown);
    }

    function getUserRateUpCollection($id) {
        $userRate = $this->post_rate_model->get_user_rate_up_collection($id);
        //$userRatedDownList=array();
        foreach ($userRate as $key => $value) {
            $value->user = $this->get_user_basic_info($value->user_id);
            // unset($value->post_id);
            unset($value->user_id);
            unset($value->rate);
        }
        return $userRate;
    }

    function getUserRateDownCollection($id) {
        $userRate = $this->post_rate_model->get_user_rate_down_collection($id);
        //$userRatedDownList=array();
        foreach ($userRate as $key => $value) {
            $value->user = $this->get_user_basic_info($value->user_id);
            // unset($value->post_id);
            unset($value->user_id);
            unset($value->rate);
        }
        return $userRate;
    }

    /**
     * Check upload media
     * @return boolean
     */
    public function check_upload_image() {
        if ($_FILES['fileToUpload']['name']) {
            $check = TRUE;

            $extension = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);
            $allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "GIF", "PNG"); // file extension are allowed
            $allowedTypes = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF);
//            $detectedType = exif_imagetype($_FILES['fileToUpload']['tmp_name']);
            // check image content
//            if (!in_array($detectedType, $allowedTypes)) {
//                $this->form_validation->set_message('check_upload_image', 'Invalid Image Content!');
//                $check = FALSE;
//            }
            // Check if file already exists
            // 
            // check file size
            if (filesize($_FILES['fileToUpload']['tmp_name']) > 1000000) {
                $this->form_validation->set_message('check_upload_image', 'Kích thước ảnh không lớn hơn 10MB.');
                $check = FALSE;
            }

            // Allow certain file formats
            if (!in_array($extension, $allowedExts)) {
                $this->form_validation->set_message('check_upload_image', "Định dạng (.{$extension}) không phải là hình ảnh.");
                $check = FALSE;
            }

            return $check;
        }

        return TRUE;
    }

    /**
     * Upload media
     * @param type $target
     * @param type $target_id
     * @param type $_FILES_
     */
    public function do_upload($target, $target_id, $_FILES_) {

        $target_dir = './uploads/' . $target . '/';
        $target_file = $target_dir . basename($_FILES_["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);


        $data = array();
        date_default_timezone_set('UTC');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $filename = date("dmyhs") . $target_id . '-' . basename($_FILES_["fileToUpload"]["name"]);
        $target_file_ = $target_dir . $filename;

        if (move_uploaded_file($_FILES_["fileToUpload"]["tmp_name"], $target_file_)) {
            switch ($target) {
                case 'user':
                    $username = $this->session->userdata('user');
                    $data['avatar'] = $filename;
                    $this->user_model->user_update($username, $data);
                    $this->session->set_userdata(array('avatar' => $filename));

                    break;
                case 'farm':
                    $data['cover'] = $filename;
                    $this->farm_model->update($target_id, $data);
                    break;

                case 'distributors':
                    $data['logo'] = $filename;
                    $this->distributor_model->update($target_id, $data);
                    break;

                case 'certificate':
                    $data['certificate_picture'] = $filename;
                    $this->farm_model->update($target_id, $data);
                    break;
                default:
                    # code...
                    break;
            }
        } else {
            $this->session->set_flashdata('failed', 'Không thể upload hình ảnh!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    /* -------for API-------- */

//message for api
    #not exists user by username
    function api_message_not_exists_user() {
        $message = json_encode(array('messageCode' => 404, 'message' => $this->lang->line('message_not_exists_user')));
        echo ($message);
    }

    #username is null

    function api_message_username_null() {
        $message = json_encode(array('messageCode' => 404, 'message' => $this->lang->line('message_username_null')));
        echo ($message);
    }

    #access token is null

    function api_message_token_is_null() {
        $message = json_encode(array('messageCode' => 604, 'message' => $this->lang->line('message_token_null')));
        echo ($message);
    }

    function api_message_token_is_invalid() {
        $message = json_encode(array('messageCode' => 604, 'message' => $this->lang->line('message_token_invalid')));
        echo ($message);
    }

    function api_message_refresh_key_is_null() {
        $message = json_encode(array('messageCode' => 404, 'message' => $this->lang->line('message_refresh_key_is_null')));
        echo ($message);
    }

    //for session
    function check_session() {
        if ($this->session->userdata('user') != '') {
            return true;
        }
        return false;
    }

    function get_user_by_session() {
        return $this->user_model->get_user_info($this->session->userdata('user'));
    }

    function aes128Encrypt($key, $data) {
        if (16 !== strlen($key))
            $key = hash('MD5', $key, true);
        $padding = 16 - (strlen($data) % 16);
        $data .= str_repeat(chr($padding), $padding);
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16)));
    }

    function aes128Decrypt($key, $data) {
        $data = base64_decode($data);
        if (16 !== strlen($key))
            $key = hash('MD5', $key, true);
        $data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, str_repeat("\0", 16));
        $padding = ord($data[strlen($data) - 1]);
        return substr($data, 0, -$padding);
    }

    /**
     * Get token
     * @param type $alg
     * @param type $typ
     * @param type $payload
     * @return boolean
     */
    function get_token($alg, $typ, $payload) {
        $payloadJson = json_decode($payload);

        if (isset($payloadJson->password))
            $payloadJson->password = md5(uniqid($payloadJson->password));
        else
            $payloadJson->password = md5(uniqid($payloadJson->username));

        $params = array('alg' => $alg, 'typ' => $typ, 'payload' => $payload);
        foreach ($params as $key => $value) {
            if (!$value) {
                $message = json_encode(array('messageCode' => 404, 'message' => 'Missing ' . $key . '!'));
                echo ($message);
                return false;
            }
        }
        if (!isset($payloadJson->username)) {
            $this->api_message_token_is_invalid();
            return false;
        }

        $header = array("alg" => $alg, "typ" => $typ);
        $headerB64 = base64_encode(json_encode($header));
        $payloadB64 = base64_encode(json_encode($payloadJson));

        $signatureB64 = hash_hmac('sha256', base64_encode(json_encode($header)) + "." + $payloadJson, 'farmtechvietnam230317');
        $token = $headerB64 . '.' . $payloadB64 . '.' . $signatureB64;

        return (array('access_token' => $token, 'refresh_key' => md5(uniqid($payloadJson->username)), 'token_type' => 'bearer'));
    }

    function check_token() {
        $accessToken = $_SERVER['HTTP_ACCESS_TOKEN'];
        $this->accessToken = $accessToken;
        /* ex:{
          "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ICJmdDAxIn0=.5e0f9633b89a046714cd246c012c8591f24ec83d6583dfcb8150531e7a0db4d6"
          } */
        // $tokenArray=explode('.', $token);
        // var_dump($tokenArray);
        if ($accessToken) {

            if ($this->token_model->check_exists(array('access_token' => $accessToken))) {

                $token = $this->token_model->get_info_rule(array('access_token' => $accessToken));
                if ($token->expire_at < date('Y-m-d H:i:s')) {
                    $message = json_encode(array('messageCode' => $this->codeTokenExpired, 'message' => $this->lang->line('message_token_out_of_date'), 'data' => ''));
                    echo ($message);
                    return false;
                }
                return true;
            } else {
                $message = json_encode(array('messageCode' => $this->codeTokenInvalid, 'message' => $this->lang->line('message_token_invalid')));
                echo ($message);
                return false;
            }
        } else {
            $this->api_message_token_is_null();
            return false;
        }
    }

    function check_token_reset_pass($accessToken) {
        $this->accessToken = $accessToken;
        if ($accessToken) {

            if ($this->token_model->check_exists(array('access_token' => $accessToken))) {

                $token = $this->token_model->get_info_rule(array('access_token' => $accessToken));
                if ($token->expire_at < date('Y-m-d H:i:s')) {
                    $message = json_encode(array('messageCode' => $this->codeTokenExpired, 'message' => $this->lang->line('message_token_out_of_date')));
                    return $message;
                }
                return true;
            } else {
                $message = json_encode(array('messageCode' => $this->codeTokenInvalid, 'message' => $this->lang->line('message_token_invalid')));
                return $message;
            }
        } else {
            $this->api_message_token_is_null();
            return false;
        }
    }

    function get_user_by_accesstoken() {
        //$accessToken = $_SERVER['HTTP_ACCESS_TOKEN'];`
        if (!$this->accessToken) {
            $message = json_encode(array('messageCode' => 404, 'message' => $this->lang->line('message_token_null')));
            echo ($message);
            exit();
        }
        $token = $this->token_model->get_user_id_by_accesstoken($this->accessToken);
        if ($token->user_id == '') {
            $message = json_encode(array('messageCode' => 404, 'message' => $this->lang->line('message_token_invalid')));
            echo ($message);
            exit();
        } else {
            if (!$this->user_model->check_exists(array('user_id' => $token->user_id))) {
                $this->api_message_token_is_invalid();
                exit();
            }
        }
        return $this->token_model->get_user_id_by_accesstoken($this->accessToken);
    }

    function get_user_by_accesstoken_reset_pass($accessToken) {
        //$accessToken = $_SERVER['HTTP_ACCESS_TOKEN'];`
        if (!$accessToken) {
            $message = json_encode(array('messageCode' => 404, 'message' => $this->lang->line('message_token_null')));
            echo ($message);
            exit();
        }
        return $this->token_model->get_user_id_by_accesstoken($accessToken)->user_id;
    }

    // get_slug make_slug
    function slugify($text) {
        return $this->slug_model->stripUnicode($text);
    }

    // get_slug make-slug
    function slugify2($text) {
        return $this->slug_model->stripUnicode2($text);
    }

    function get_payload_data() {
        $postJson = file_get_contents('php://input');
        $data = json_decode($postJson); //androi api
        if (!$data)
            $data = $_POST; //for jquery ajax
        else {
            //androi api
            $data = (array) $data;
        }
        unset($data['username']);
        $user = $this->user_model->get_info($this->get_user_by_accesstoken()->user_id);
        //echo $username;
        if (!$user) {
            $this->api_message_username_null();
            exit();
        }
        $this->user_model->check_exists_($user->username);
        unset($user->pw);
        return array('data' => $data, 'user' => $user);
    }

    //for web
    function get_expert_list_of_current_user_by_fields() {
        $username = $this->session->userdata('user');
        $user = $this->user_model->get_user_info($username);
        $userFieldsCol = $this->userfield_model->getFieldsByUserId($user->user_id);
        return $this->user_model->get_expert_list_by_fields($userFieldsCol);
    }

    public function send_mail($to, $subject, $body, $altBody = '') {
        require __DIR__ . '/PHPMailer-master/class.phpmailer.php';
        require __DIR__ . '/PHPMailer-master/class.smtp.php';

        $this->mail = new PHPMailer();
        $this->mail->IsSMTP(); // set mailer to use SMTP


        $this->mail->Host = "smtp.office365.com"; // specify main and backup server
        $this->mail->Port = 587; // set the port to use
        $this->mail->SMTPAuth = true; // turn on SMTP authentication
        $this->mail->SMTPSecure = "tls";
        $this->mail->Username = "noreply@farmtech.vn"; // your SMTP username or your gmail username
        $this->mail->Password = "Zaqwsx1212"; // your SMTP password or your gmail password


        $from = "noreply@farmtech.vn"; // Reply to this email
        $this->mail->From = $from;
        $this->mail->FromName = "Fman"; // Name to indicate where the email came from when
        $this->mail->AddAddress($to, 'name');
        $this->mail->AddReplyTo($this->mail->From, "Fman");
        $this->mail->WordWrap = 50; // set word wrap
        $this->mail->IsHTML(true); // send as HTML
        $this->mail->CharSet = 'UTF-8';
        $this->mail->Subject = $subject;
        $this->mail->Body = $body; //HTML Body
        $this->mail->AltBody = $altBody; //Text Body

        if (!$this->mail->Send()) {
            $message = json_encode(array($this->messageCode => 404, $this->message => $this->lang->line('send_mail_error') . $this->mail->ErrorInfo));
            echo ($message);
            exit();
        } else {
            return true;
        }
    }

    public function mail_form($value = '') {
        return $mailContent = '<table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">  
      <tbody>
      <tr style="height: 52.5pt;">   
      <td width="100%" valign="top" style="width: 100%; background: #f1f1f1; padding: 5px; height: 52.5pt;">
      <img src="http://farmtech.vn/library/images/logo.png">  
      </td>  
      </tr>
      <tr>
      <td>' . $value . '</td>
      </tr> 
      </tbody>
      </table> ';
    }

    public function reset_email_content($name, $email, $urlResetPass) {
        return $mailContent = '<table cellpadding="0" class="cf An" id="undefined"><tbody><tr><td class="Aq">&nbsp;</td><td class="Ap"><div id=":mj" class="Ar As aoE" style="display: none;"><div class="At"><textarea id=":l1" class="Ak aXjCH" style="" aria-label="Nội dung thư" spellcheck="true" itacorner="6,7:1,1,0,0" tabindex="1" form="nosend"></textarea></div></div><div id=":r0" class="Ar Au" style="display: block;"><div id=":mn" class="Am Al editable LW-avf" hidefocus="true" aria-label="Nội dung thư" g_editable="true" role="textbox" contenteditable="true" tabindex="1" style="direction: ltr; min-height: 95px;" itacorner="6,7:1,1,0,0"><table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="700" style="width: 525pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;"><tbody><tr> <td width="100%" valign="top" style="width: 100%; padding: 0in;"> <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">  <tbody><tr style="height: 52.5pt;">   <td width="100%" valign="top" style="width: 100%; background: #f1f1f1; padding: 5px; height: 52.5pt;"><img src="http://farmtech.vn/library//imgs/logo.png">  </td>  </tr> </tbody></table> </td></tr><tr> <td width="100%" valign="top" style="width: 100%; padding: 0in;"> <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">  <tbody><tr>   <td width="20" style="width: 15pt; padding: 0in;"></td>   <td valign="top" style="padding: 0in;">   <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 19pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">Tạo mật khẩu mới</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>   </td>   <td width="20" style="width: 15pt; padding: 0in;"></td>  </tr> </tbody></table> </td></tr><tr> <td width="100%" valign="top" style="width: 100%; background: rgb(240, 240, 240); padding: 0in;"> <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">  <tbody><tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;"></td>  </tr>  <tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;">   <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">    <tbody><tr>     <td width="20" style="width: 15pt; padding: 0in;"></td>     <td valign="top" style="padding: 0in;">     <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 13.5pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">' . $name . ' thân mến,</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>     <p class="MsoNormal" style="line-height: 18pt;"><span style="font-size: 12pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">Fman vừa nhận được yêu cầu thay đổi mật khẩu đăng nhập vào hệ thống Fman cho tài khoản email<a href="mailto:' . $email . '" target="_blank"> ' . $email . '</a>.</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>     </td>     <td width="20" style="width: 15pt; padding: 0in;"></td>    </tr>   </tbody></table>   </td>  </tr>  <tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;"></td>  </tr> </tbody></table> </td></tr><tr> <td width="100%" valign="top" style="width: 100%; padding: 0in;"> <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">  <tbody><tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;"></td>  </tr>  <tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;">   <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">    <tbody><tr>     <td width="20" style="width: 15pt; padding: 0in;"></td>     <td valign="top" style="padding: 0in;">     <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">      <tbody><tr>       <td width="30" style="width: 22.5pt; padding: 0in;"></td>       <td width="15" style="width: 11.25pt; padding: 0in;"></td>       <td style="padding: 0in;">       <p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 13.5pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">Bước tiếp theo</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>       </td>      </tr>     </tbody></table>     </td>     <td width="20" style="width: 15pt; padding: 0in;"></td>    </tr>    <tr>     <td width="20" style="width: 15pt; padding: 0in;"></td>     <td valign="top" style="padding: 0in;"></td>     <td width="20" style="width: 15pt; padding: 0in;"></td>    </tr>    <tr>     <td width="20" style="width: 15pt; padding: 0in;"></td>     <td valign="top" style="padding: 0in;">     <p class="MsoNormal" style="line-height: 18pt;"><span style="font-size: 12pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">Bạn hãy&nbsp;<a href="' . $urlResetPass . '" target="_blank"><span style="color: rgb(51, 162, 178);">BẤM VÀO ĐÂY</span></a>&nbsp;để thực hiện thiết lập mật khẩu mới cho tài khoản của bạn.</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>     </td>     <td width="20" style="width: 15pt; padding: 0in;"></td>    </tr>   </tbody></table>   </td>  </tr>  <tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;"></td>  </tr> </tbody></table> </td></tr><tr> <td style="padding: 0in;"> <p class="MsoNormal" align="center" style="text-align: center; line-height: normal;"><i><span style="font-size: 9.5pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">(Đây là email tự động, bạn vui lòng không trả lời email này)</span></i><span style="font-size: 9.5pt; font-family: Arial, sans-serif;"><span></span></span></p> </td></tr><tr> <td width="100%" valign="top" style="width: 100%; background: rgb(240, 240, 240); padding: 0in;"> <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">  <tbody><tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;"></td>  </tr>  <tr>   <td width="100%" valign="top" style="width: 100%; padding: 0in;">   <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">    <tbody><tr>     <td width="20" style="width: 15pt; padding: 0in;"></td>     <td valign="top" style="padding: 0in;">     <table class="gmail-MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="100%" style="width: 100%;">      <tbody><tr>       <td width="30" style="width: 22.5pt; padding: 0in;"></td>       <td width="15" style="width: 11.25pt; padding: 0in;"></td>       <td style="padding: 0in;">       <p class="MsoNormal" style="margin-bottom: 0.0001pt; line-height: normal;"><span style="font-size: 13.5pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">Lưu ý</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>       </td>      </tr>      <tr>       <td width="30" style="width: 22.5pt; padding: 0in;"></td>       <td width="15" style="width: 11.25pt; padding: 0in;"></td>       <td valign="top" style="padding: 0in;"></td>      </tr>     </tbody></table>     </td>     <td width="20" style="width: 15pt; padding: 0in;"></td>    </tr>    <tr>     <td width="20" style="width: 15pt; padding: 0in;"></td>     <td valign="top" style="padding: 0in;">     <p class="MsoNormal" style="line-height: normal;"><span style="font-size: 12pt; font-family: Helvetica, sans-serif; color: rgb(32, 32, 32);">Bạn có thể liên hệ với Fman tại <a href="http://farmtech.vn/welcome#lien-he" target="_blank"><span style="color: rgb(51, 162, 178);">Đây</span></a>&nbsp;hoặc gửi thư đến&nbsp;<a href="mailto:info@farmtech.vn" target="_blank"><span style="color: rgb(51, 162, 178);">đây</span></a>.</span><span style="font-size: 12pt; font-family: Arial, sans-serif;"><span></span></span></p>     </td>     <td style="padding: 0in;"></td>    </tr>   </tbody></table>   </td>  </tr> </tbody></table> </td></tr></tbody></table><p class="MsoNormal"><span>&nbsp;</span></p></div></div></td><td class="Aq">&nbsp;</td></tr></tbody></table>';
    }

    /**
     * Check admin login
     */
    public function check_admin_log() {
        if (!$this->session->has_userdata('admin')) {
            redirect($this->config->config['base_url'] . '/administrator/login');
            exit();
        }
    }

    /**
     * Check user login
     */
    public function check_user_log() {
        if (!$this->session->has_userdata('user')) {
            redirect($this->config->config['base_url']);
            exit();
        }
    }

    /**
     * Get current admin info
     * @return boolean
     */
    public function get_current_admin() {
        if ($this->session->has_userdata('admin')) {
            $user = $this->admin_model->get_admin_info($this->session->userdata('admin'));
            $user_data = array();
            $user_data['username'] = $user->username;
            $user_data['adminrole_id'] = $user->adminrole_id;
            $user_data['fullname'] = ($user->fullname) ? $user->fullname : $user->username;
            $user_data['avatar'] = $this->config->config['assets_path'] . '/images/icons/logo.png';
            $user_data['created'] = date('d/m/Y', strtotime($user->created));
            return $user_data;
        }
        return false;
    }

    /**
     * Get current user info
     * @return boolean
     */
    public function get_current_user() {
        if ($this->session->has_userdata('user')) {
            $user = $this->user_model->get_user_info($this->session->userdata('user'));
            $user_data = array();
            $user_data['user_id'] = $user->user_id;
            $user_data['username'] = $user->username;
            $user_data['usertype_id'] = $user->usertype_id;
            $user_data['lastname'] = $user->lastname;
            $user_data['firstname'] = $user->firstname;
            $user_data['fullname'] = ($user->lastname || $user->firstname) ? $user->lastname . ' ' . $user->firstname : '';
            $user_data['address'] = $user->address;
            $user_data['email'] = $user->email;
            $user_data['phone'] = $user->phone;
            $user_data['status'] = $user->status;
            $user_data['birthday'] = date('d/m/Y', strtotime($user->date_of_birth));
            $user_data['gender'] = $user->gender;
            $user_data['avatar'] = $this->config->config['assets_path'] . '/images/icons/logo.png';
            $user_data['created'] = date('d/m/Y', strtotime($user->created));
            $user_data['flags'] = $user->flags;
            return $user_data;
        }
        return false;
    }

    /**
     * Check admin full
     * @return boolean
     */
    public function check_admin_full() {
        if ($this->session->has_userdata('user')) {
            $user = $this->user_model->get_user_info($this->session->userdata('user'), array('flags'));
            if ($user->flags == 1)
                return true;
            return false;
        }
        return false;
    }

}
