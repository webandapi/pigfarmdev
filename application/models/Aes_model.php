<?php
class Aes_model extends FT_Model
{
    // Credit should be given to;
    // https://gist.github.com/bradbernard/7789c2dd8d17c2d8304b#file-php-encyrption
    // https://gist.github.com/krzyzanowskim/043be69ab3ba9fd5ba58#file-encryptaeswithphp
    
    function aesEncrypt($data, $key) {
        $data = $this->addPadding($data);
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $cipherText = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $data, MCRYPT_MODE_CBC, $iv);
        $result = base64_encode($iv.$cipherText);
        return $result;
    }
    function aesDecrypt($base64encodedCipherText, $key) {
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $cipherText = base64_decode($base64encodedCipherText);
        if (strlen($cipherText) < $ivSize) {
            throw new Exception('Missing initialization vector');
        } 
        $iv = substr($cipherText, 0, $ivSize);
        $cipherText = substr($cipherText, $ivSize);
        
        $result = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $cipherText, MCRYPT_MODE_CBC, $iv);
        $result = $this->stripPadding(rtrim($result, chr(0x0b)));
        return $result;
    }
    function addPadding($value){
        $pad = 16 - (strlen($value) % 16);
            return $value.str_repeat(chr($pad), $pad);
    }
     
    function stripPadding($value){
        $pad = ord($value[($len = strlen($value)) - 1]);
        return $this->paddingIsValid($pad, $value) ? substr($value, 0, $len - $pad) : $value;
    }
     
    function paddingIsValid($pad, $value){
            $beforePad = strlen($value) - $pad;
            return substr($value, $beforePad) == str_repeat(substr($value, -1), $pad);
    }

    


}
