<?php

class Distributor_model extends FT_Model {

    //ten bang du lieu
    public $table = 'distributors';
    var $key = 'id';
    var $params = array('name', 'logo', 'location', 'latitude', 'longitude', 'phone', 'email', 'code');

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Insert data
     * @param type $data
     * @return type
     */
    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Check exists
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

}
