<?php

class Token_model extends FT_Model {

    public $table = 'fm_token';
    var $key = 'id';
    var $params = array('id', 'user_id', 'access_token', 'start', 'expire_at', 'refresh_key');

    function create_($user_id, $token, $refresh_key) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $data = array();
        $data['user_id'] = $user_id;
        $data['access_token'] = $token;
        $data['start'] = date('Y-m-d H:i:s');
        $data['expire_at'] = date('Y-m-d H:i:s', strtotime('+2 month'));
        $data['refresh_key'] = $refresh_key;
        return $this->create($data);
    }

    function create_reset_pass($user_id, $token) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $data = array();
        $data['user_id'] = $user_id;
        $data['access_token'] = $token;
        $data['start'] = date('Y-m-d H:i:s');
        $data['expire_at'] = date('Y-m-d H:i:s', strtotime('+1 hour'));
        return $this->create($data);
    }

    function update_($token, $refresh_key) {
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        if ($this->check_exists(array('access_token' => $token, 'refresh_key' => $refresh_key))) {
            $data = array();
            //$data['start'] = date('Y-m-d H:i:s');
            $data['expire_at'] = date('Y-m-d H:i:s', strtotime('+2 month'));
            return $this->update_rule(array('access_token' => $token), $data);
        }
        return false;
    }

    function get_user_id_by_accesstoken($access_token) {
        //echo $access_token;
        $result = $this->get_info_rule(array('access_token' => $access_token), 'user_id');
        //var_dump($result);
        return $result;
    }

    function get_token_info($access_token) {
        if (!$access_token) {
            return FALSE;
        }

        $where = array();
        $where['access_token'] = $access_token;

        $this->db->where($where);
        $query = $this->db->get($this->table);
        if ($query->num_rows()) {
            $result = $query->row();
            unset($result->user_id);
            unset($result->id);
            return $result;
        }
        return false;
    }

}
