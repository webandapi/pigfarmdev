<?php

class Farmproduct_model extends FT_Model {
    #product_id=term_id
    //ten bang du lieu

    public $table = 'farm_product';
    public $key1 = 'farm_id';
    public $key2 = 'product_id';
    var $params = array('farm_id', 'product_id');

    /**
     * Insert data
     * @param type $farm_id
     * @param type $data
     * @return boolean
     */
    function create($farm_id = '', $data = array()) {
        if ($farm_id) {
            foreach ($data as $val) {
                $product_id = gettype($val) == 'object' ? $val->id : $val;

                // check product is exists
                if (!$this->product_model->check_exists(array('id' => $product_id))) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'Không tìm thấy sản phẩm yêu cầu!'));
                    exit();
                }

                // check farm is exists
                if (!$this->farm_model->check_exists(array('id' => $farm_id))) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'Không tìm thấy trang trại được yêu cầu!'));
                    exit();
                }

                // check farm - certificate is exists
                if ($this->farmproduct_model->check_exists(array($this->key1 => $farm_id, $this->key2 => $product_id))) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'Thông tin trang trại - sản phẩm bị trùng lặp!'));
                    exit();
                }

                // insert data
                $this->db->insert($this->table, array('farm_id' => $farm_id, 'product_id' => $product_id));
            }
        }
        return false;
    }

    /**
     * Update data
     * @param type $farm_id
     * @param type $data
     * @return boolean|int
     */
    public function update($farm_id = '', $data = array()) {
        if ($farm_id) {
            $this->del_rule(array('farm_id' => $farm_id));
            if ($data) {
                $this->farmproduct_model->create($farm_id, $data);
            }
        }
        return false;
    }

    function get_products_by_farm_id($farm_id = '') {
        $farmproductCol = $this->get_farm_product_list($farm_id);
        $farmproductData = array();
        foreach ($farmproductCol as $key => $value) {
            $farmproductData[] = $this->product_model->get_info_rule(array('id' => $value->product_id), array('name'));
        }
        return $farmproductData;
    }

    function get_farm_product_list($farm_id = '') {
        $this->db->where(array($this->key1 => $farm_id));
        $query = $this->db->get($this->table);
        return $query->result();
    }

    function check_exists_($product_id, $farm_id) {
        if ($product_id) {
            if (!$this->check_exists(array('product_id' => $product_id, 'farm_id' => $farm_id))) {
                echo json_encode(array('messageCode' => 500, 'message' => $this->lang->line('message_this_regisproduct_not_belong_to_user')));
                exit();
            }
        }
        return true;
    }

}
