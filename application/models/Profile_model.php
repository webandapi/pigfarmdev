<?php
class Profile_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'profile';
    var $key = 'user_id';
    var $params=array('user_id', 'certificate', 'level_id', 'note', 'exp', 'office', 'total_star', 'total_rate');
    
    function get_info($user_id, $field = '')
	{
		if (!$user_id)
		{
			return FALSE;
		}
	 	
	 	$where = array();
	 	$where['user_id'] = $user_id;
	 	
	 	return $this->get_info_rule($where, $field);
	}
}