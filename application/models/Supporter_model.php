<?php

class Supporter_model extends FT_Model {

    public $table = 'supporter';
    var $key = 'id';
    var $params = array('id', 'name', 'email', 'phone', 'description', 'created', 'updated');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get supporter info
     * @param type $id
     * @return boolean
     */
    function get_supporter_info($id) {
        if ($id) {
            $this->db->reset_query();
            return $this->get_info($id, '*');
        }
        return false;
    }

}
