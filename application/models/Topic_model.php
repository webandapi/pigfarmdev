<?php
class Topic_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'topic';
    var $key = 'id';
    var $params=array('id','name', 'created', 'device_id', 'pw','qos');
    
    function create_($data = array())
    {
        if($this->db->insert($this->table, $data))
        {
          return $insert_id = $this->db->insert_id();
           return TRUE; 
        }else{
            return FALSE;
        }
    }

    function get_topic_list_by_user_id($user_id)
    {
        $this->db->where(array('user_id'=>$user_id));
        $query = $this->db->get($this->table);
        $result=$query->result();
        foreach ($result as $key => $value) {
            $value->username=$this->user_model->get_username_by_userid($value->user_id);
            unset($value->user_id);
        }
        return $result;
    }
}