<?php

class Admin_model extends FT_Model {

    //ten bang du lieu
    public $table = 'admin';
    var $key = 'id';
    var $params = array('id', 'username', 'fullname', 'email', 'phone', 'address', 'status', 'date_of_birth', 'pw', 'adminrole_id', 'gender', 'create');

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Insert data
     * @param type $data
     * @return type
     */
    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Check exists
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get data info
     * @param type $where
     * @param type $field
     * @return type
     */
    public function get_info_rule($where = array(), $field = '') {
        return parent::get_info_rule($where, $field);
    }

    function delete_user_references_value_except_users() {
        $userList = array(1, 2);
        $tables = array('user_field', 'post_rate', 'read_post', 'connected_post', 'token', 'profile');
        $this->db->where_not_in('user_id', $userList);
        $this->db->delete($tables);
        $this->db->reset_query();
        $this->db->where_not_in('expert_id', $userList);
        $this->db->delete('rate');
        $this->db->reset_query();
        $this->db->where_not_in('farmer_id', $userList);
        $this->db->delete('rate');
        foreach ($userList as $key => $value) {
            $farmList = array();
            $farmCol = $this->farm_model->get_farm_list_basic_info_by_user_id($value);
            foreach ($farmCol as $key => $value_) {
                $farmList[] = $value_->id;
            }
            if ($farmList) {
                $tables_ = array('farm_product');
                $this->db->where_not_in('farm_id', $farmList);
                $this->db->delete($tables_);

                $this->db->reset_query();
                $tables__ = array('farm');
                $this->db->where_not_in('user_id', $userList);
                $this->db->delete($tables__);
            }
        }

        return true;
    }

    function reset_all_user_except_custom_user() {
        $userList = array(1, 2);
        $tables = array('user');
        $this->db->where_not_in('user_id', $userList);
        $this->db->delete($tables);
        return true;
    }

    function create_500_devices_test() {
        for ($i = 1; $i <= 500; $i++) {
            if (!$this->device_model->check_exists(array('serial' => 'DVTEST' . $i)))
                echo 'DVTEST' . $i;
            $this->device_model->create(array('name' => 'DVTEST' . $i, 'device_type_id' => 1, 'serial' => 'DVTEST' . $i));
        }
        return true;
    }

}
