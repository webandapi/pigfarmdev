<?php

class Province_model extends FT_Model {

    public $table = 'provinces';
    var $key = 'id';
    var $params = array('id', 'name', 'zipcode');

}
