<?php

class Qa_model extends FT_Model {

    public $table = 'qa';
    var $key = 'id';
    var $params = array('title', 'description');

    /**
     * Get list data
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Insert data
     * @param type $data
     * @return type
     */
    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

}
