<?php
class Wp_term_taxonomy_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'wp_term_taxonomy';
    var $key = 'term_taxonomy_id';
    var $params=array('term_taxonomy_id', 'term_id', 'taxonomy', 'description', 'parent', 'count');
    
    public function generate_term_description($value='')
    {
        $termList=$this->get_list(array('where'=>array('taxonomy'=>'category')));
        //echo json_encode($termList);
        //lọc ra các term là category
        foreach ($termList as $key => $value) {
        	//tạo description dựa trên name của category nếu rỗng
        	if(!$value->description)
        		{
        			$termObj='';
					$this->db->where(array('term_id' =>$value->term_id));
					$query = $this->db->get('wp_terms');
					//echo $value->term_id;
        			$this->db->select('name');
					if ($query->num_rows())
					{
						$termObj= $query->row();
					}
					echo json_encode($termObj);
					//echo json_encode($query);
					$this->update($value->term_taxonomy_id,array('description'=>$termObj->name));
        			
        		}
        }
        return 0;
    }


}
