<?php
class Post_rate_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'post_rate';
    public $key1 = 'user_id';
    public $key2 = 'post_id';//post_id
    var $params=array('user_id', 'post_id', 'rate', 'comment');
    function create_($user_id,$post_id,$value,$comment='')
	{
		//echo $user_id.'_'.$post_id.'_'.$value;
		if($user_id!='' && $post_id!='' &&$value!=='')
		{
			{
				$postRateObj = $this->get_post_rate_info($user_id,$post_id);
				//echo $user_id.$post_id;
				//var_dump($postRateObj);
				if($postRateObj)
					{
						 if($comment!='')
						 	$this->post_rate_update($user_id,$post_id,array('rate'=>$value,'comment'=>$comment));
						 else 
						 	$this->post_rate_update($user_id,$post_id,array('rate'=>$value));
					}
				else 
				{
			        $post_rate = array(
			        "user_id" => $user_id,
			        "post_id" => $post_id,
			        "rate"   =>$value,
			        "comment" =>$comment);
			        //echo 123;
			        $this->db->insert('post_rate',$post_rate);
	    		}
	        }
	        return true;
		}
		//else echo "Null";
		return false;
        
	}
	public function add_4_expert_to_all_posts($value='')
	{
		$expertList = array(136,137,138);
		$postList = $this->post_model->my_get_list(array('post_type'=>'post','post_status'=>'publish'),'ID');

        foreach ($postList as $key => $post)
        	foreach ($expertList as $key => $user_id) 
        	{
				$postRateObj = $this->get_post_rate_info($user_id,$post->ID);
				//echo $user_id.$post_id;
				//var_dump($postRateObj);
				if($postRateObj)
				{
				}
				else
				{
			        $post_rate = array(
			        "user_id" => $user_id,
			        "post_id" => $post->ID,
			        "rate"   =>1
			        );
			        //echo 123;
			        $this->db->insert('post_rate',$post_rate);
	    		}

	    	}
	        return true;
	}
	function post_rate_update($user_id, $post_id,$data)
	{
		if (!$user_id)
		{
			return FALSE;
		}
		
		$where = array();
	 	$where['user_id'] = $user_id;
	 	$where['post_id'] = $post_id;
	    $this->update_rule($where, $data);
	 	
	 	return TRUE;
	}
	function get_user_rated_up($post_id)
	{
		$this->db->where(array('post_id'=>$post_id,'rate'=>1));
		$query = $this->db->get($this->table);
		return $query->result();
	}
	function get_user_rated_down($post_id)
	{
		$this->db->where(array('post_id'=>$post_id,'rate'=>0));
		$query = $this->db->get($this->table);
		return $query->result();
	}

	function get_user_rate_up_collection($user_id)
	{
		$this->db->where(array('user_id'=>$user_id,'rate'=>1));
		$query = $this->db->get($this->table);
		return $query->result();
	}
	function get_user_rate_down_collection($user_id)
	{
		$this->db->where(array('user_id'=>$user_id,'rate'=>0));
		$query = $this->db->get($this->table);
		return $query->result();
	}
	function get_user_rate_collection($user_id,$limit=10,$offset=0)
	{
		// $this->db->where(array('user_id'=>$user_id));
		// $this->db->limit($limit+6, $offset);
		// $this->db->order_by('time');
		// $query = $this->db->get($this->table);
		// return $query->result();

		 $this->db->select('*')
                 ->from($this->table)
                 ->limit($limit, $offset)
                 ->where(array('user_id'=>$user_id));
        $query = $this->db->get();

        return $query->result();
	}
	
}