<?php

class Farm_model extends FT_Model {

    public $table = 'farm';
    var $key = 'id';
    var $params = array('id', 'name', 'code', 'farmtype_id', 'water_indicator', 'index_of_photosynthesis', 'user_id', 'province_id', 'cover', 'certificate_picture', 'size', 'location', 'note', 'latitude', 'longitude', 'slug', 'show_harvest', 'order');

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Get farm basic info
     * @param type $farm_id
     * @return string
     */
    public function get_basic_info($farm_id) {
        if ($farm_id) {
            $field = array('id', 'name', 'cover', 'user_id');
            $farm = $this->get_info_rule(array('id' => $farm_id), $field);
            $farm->username = $this->user_model->get_username_by_userid($farm->user_id);
            unset($farm->user_id);
            return $farm;
        }
        return '';
    }

    /**
     * Get farm list by user_id
     * @param type $user_id
     * @return boolean
     */
    function get_farmlist_by_userid($username, $field = array()) {
        $user = $this->user_model->get_user_info($username);
        if ($user->user_id) :
            $query = $this->db->select($field)
                    ->get_where($this->table, array('user_id' => $user->user_id));
            $result = $query->result();
            return $result;
        endif;
        return false;
    }

    /**
     * Get products by farm
     * @param type $farm_id
     * @return boolean
     */
    public function get_farm_products($farm_id = '') {
        if ($farm_id) {
            $sql = "SELECT p.* FROM farm f JOIN farm_product fp ON f.id = fp.farm_id JOIN product p ON fp.product_id = p.id WHERE f.id = {$farm_id}";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }

    public function get_farm_certificate($farm_id = '') {
        if ($farm_id) {
            $sql = "SELECT c.* FROM farm f JOIN farm_certificate fc ON f.id = fc.farm_id JOIN certificate c ON fc.certificate_id = c.id WHERE f.id = {$farm_id}";
            $query = $this->db->query($sql);
            return $query->result();
        }
        return false;
    }

}
