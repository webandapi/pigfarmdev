<?php

class Module_model extends FT_Model {

    //ten bang du lieu
    public $table = 'modules';
    var $key = 'id';
    var $params = array('id', 'name', 'note');
    
    
    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    public function get_info_($id) {
        $deviceType = $this->get_info($id);
        $sensorList = $this->devicetype_sensor_model->my_get_list(array('devicetype_id' => $id));
        foreach ($sensorList as $key => $sensor) {
            $sensorList[$key] = $this->sensor_model->get_info($sensor->sensor_id);
        }
        if ($sensorList)
            $deviceType->sensor = $sensorList;
        return $deviceType;
    }

    function check_exists_($id) {
        if (!$this->check_exists(array('id' => $id))) {
            echo json_encode(array($this->messageCode => $this->codeNotFound, 'message' => $this->lang->line('message_not_exists_devicetype')));
            exit();
        }
        return true;
    }

    public function get_list_() {
        $deviceTypeList = $this->my_get_list();
        foreach ($deviceTypeList as $key => $device) {
            $sensorList = $this->devicetype_sensor_model->my_get_list(array('devicetype_id' => $device->id));
            foreach ($sensorList as $key => $sensor) {
                $sensorList[$key] = $this->sensor_model->get_info($sensor->sensor_id);
            }
            $device->sensor = $sensorList;
        }
        return $deviceTypeList;
    }

}
