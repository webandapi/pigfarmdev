<?php

class Userconnected_model extends FT_Model {
    #post_id=term_id
    //ten bang du lieu

    public $table = 'user_connected';
    public $key1 = 'farmer_id';
    public $key2 = 'expert_id';
    var $params = array('farmer_id', 'expert_id', 'post_id', 'created');

    //for API method
    function my_create($farmer_id, $expert_id, $post_id = array()) {
        if ($farmer_id) {
            if (!$this->post_model->my_check_exists($post_id))
                ;
            if ($farmer_id != '' && $expert_id != '' && $post_id != '') {
                if ($this->check_exists(array('farmer_id' => $farmer_id, 'expert_id' => $expert_id, 'post_id' => $post_id))) {
                    echo json_encode(array('messageCode' => 500, 'message' => 'Already exist!'));
                    exit();
                }
                $user_connected = array(
                    "farmer_id" => $farmer_id,
                    "expert_id" => $expert_id,
                    "post_id" => $post_id
                );
                $this->db->insert('user_connected', $user_connected);
                return true;
            } else {
                echo json_encode(array('messageCode' => 404, 'message' => 'check input user_id and post_id!'));
                exit();
            }
        }
        return false;
    }

    function check_user_connected($farmer_id = '', $post_id = array()) {
        if ($farmer_id != '' && $post_id != '') {
            if ($this->check_exists(array('farmer_id' => $farmer_id, 'post_id' => $post_id))) {
                return true;
            }
        } else {
            echo json_encode(array('messageCode' => 404, 'message' => 'check input user_id and post_id!!'));
            exit();
        }
        return false;
    }

    public function get_expert_list_by_farmer_and_post($farmer_id, $post_id) {
        $this->db->where(array('farmer_id' => $farmer_id, 'post_id' => $post_id));
        $this->db->select('expert_id');
        $query = $this->db->get($this->table);
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key] = $this->user_model->get_user_basic_info($value->expert_id);
        }
        return $result;
    }

    function get_post_what_user_connected_list($farmer_id, $limit = 10, $offset = 0, $os = '', $version = '') {

        $this->db->where(array('farmer_id' => $farmer_id));
        $this->db->limit($limit, $offset);
        $this->db->group_by('post_id');
        $query = $this->db->get($this->table);
        $connectedPostList = $query->result();
        $result = array();
        foreach ($connectedPostList as $key => $value) {

            $post = $this->post_model->get_post_basic_info('', $value->post_id, $os, $version);
            $post['expert_connect'] = $this->get_expert_list_by_farmer_and_post($farmer_id, $value->post_id);

            $checkbookmark = $this->bookmark_model->check_exists_($value->post_id, $farmer_id);
            $post['bookmark'] = ($checkbookmark == 1) ? 1 : 0;

            $result[] = $post;
        }

        return $result;
    }

    public function getPostCategories($id = '') {
        $post_categories = wp_get_post_categories($id);

        $cats = array();

        foreach ($post_categories as $c) {
            $cat = get_category($c);
            $cats[] = array('name' => $cat->name, 'slug' => $cat->slug, 'id' => $this->field_model->get_field_id_by_term_id($cat->term_id));
        }
        return $cats;
    }

}
