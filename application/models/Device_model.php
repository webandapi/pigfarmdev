<?php

class Device_model extends FT_Model {

    //ten bang du lieu
    public $table = 'device';
    var $key = 'id';
    var $params = array('id', 'name', 'device_type_id', 'farm_id', 'serial', 'date_active', 'date_end_warranty', 'status', 'latitude', 'longitude', 'sensor_on');

    /**
     * Insert data
     * @param type $data
     * @return type
     */
    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Check exists
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        $result = parent::get_list($input);
        $data = [];
        foreach ($result as $key => $value) {
            $this->db->reset_query();
            $data[] = $this->device_model->get_basic_info($value->id);
        }
        return $data;
    }

    function get_info_rule_($where = array(), $field = '') {
        if ($field) {
            $this->db->select($field);
        }
        $this->db->where($where);
        $query = $this->db->get($this->table);

        if ($query->num_rows()) {
            $result = $query->row();
            $result->farm = $this->farm_model->get_basic_info($result->farm_id);
            $result->device_type = $this->devicetype_model->get_info_($result->device_type_id);
            $result->sensor_on = explode(',', $result->sensor_on);
            unset($result->farm_id);
            unset($result->device_type_id);
            return $result;
        }
        return FALSE;
    }

    function active($device_id, $farm_id) {
        $device = $this->get_info($device_id);
        $this->device_model->check_exists_($device_id);
        if ($device->date_active == '') {
            $date_active = date('Y-m-d H:i:s');
            $deviceTypeWarranty = $this->devicetype_model->get_info($device->device_type_id)->warranty;
            $date_end_warranty = date('Y-m-d H:i:s', strtotime('+' . $deviceTypeWarranty));
            return $this->update($device_id, array('farm_id' => $farm_id, 'status' => 1, 'date_active' => $date_active, 'date_end_warranty' => $date_end_warranty));
        }
        return $this->update($device_id, array('farm_id' => $farm_id, 'status' => 1));
    }

    function is_current_user($device_id, $user_id) {
        if (isset($device_id) && isset($user_id)) {
            if ($this->get_info($device_id)->farm_id) {
                if ($this->farm_model->check_exists(array('id' => $this->get_info($device_id)->farm_id, 'user_id' => $user_id))) {
                    return true;
                }
            }
            return false;
        } else {
            echo json_encode(array($this->messageCode => $this->codeNotFound, 'message' => $this->lang->line('message_missing_param') . ': Device ID or User ID'));
            exit();
        }
    }

    function is_current_user_by_serial($serial, $user_id) {
        if (isset($serial) && isset($user_id)) {
            if ($this->get_info_rule(array('serial' => $serial))->farm_id) {
                if ($this->farm_model->check_exists(array('id' => $this->get_info_rule(array('serial' => $serial))->farm_id, 'user_id' => $user_id))) {
                    return true;
                }
            }
            return false;
        } else {
            echo json_encode(array($this->messageCode => $this->codeNotFound, 'message' => $this->lang->line('message_missing_param') . ': Device ID or User ID'));
            exit();
        }
    }

    /**
     * Get devices info
     * @param type $id
     * @return type
     */
    public function get_basic_info($id) {
        $device = $this->device_model->get_info($id, array('*'));

        $device_type = $this->devicetype_model->get_info($device->device_type_id, array('name','mfg_date'));

        $farm = $this->farm_model->get_info($device->farm_id, array('name'));

        if ($device_type)
            $device->device_type = $device_type;

        if ($farm)
            $device->farm = $farm;

        unset($device->device_type_id);

        return $device;
    }

    public function get_device_id_by_serial($serial) {
        return $this->get_info_rule(array("serial" => $serial))->id;
    }

    function check_device_in_any_farm($device_id, $farm_id) {
        if ($this->check_exists(array('id' => $device_id, 'farm_id' => $farm_id))) {
            echo json_encode(array($this->messageCode => $this->codeFailed, 'message' => $this->lang->line('message_this_device_is_already_in_a_farm')));
            exit();
        }
        return true;
    }

    public function check_active($device_id) {
        if ($this->get_info($device_id)->status == 1) {
            echo json_encode(array($this->messageCode => $this->codeFailed, 'message' => $this->lang->line('message_this_device_is_already_active')));
            exit();
        }
        return true;
    }

    function check_exists_($device_id) {
        if (!$this->check_exists(array('id' => $device_id))) {
            echo json_encode(array($this->messageCode => $this->codeNotFound, 'message' => $this->lang->line('message_not_exists_device')));
            exit();
        }
        return true;
    }

    function check_exists_serial($serial) {
        if (!$this->check_exists(array('serial' => $serial))) {
            echo json_encode(array($this->messageCode => $this->codeNotFound, 'message' => $this->lang->line('message_not_exists_device')));
            exit();
        }
        return true;
    }

    function get_device_list_by_user_id($user_id) {
        $farmList = $this->farm_model->get_farm_list_basic_info_by_user_id($user_id);
        $deviceList = array();
        $data = [];
        $defaultFarm = $this->farm_model->get_basic_info2(45);
        $defaultFarm->username = $this->farm_model->get_username_by_farm_id($defaultFarm->id);
        $defaultFarm->device[] = $this->get_basic_info(3);
        $defaultFarm->device[] = $this->get_basic_info(1062);
        $data[] = array('farm' => $defaultFarm);
        foreach ($farmList as $key => $farm) {
            //echo count($farm->id).'_';
            if ($this->check_exists(array('farm_id' => $farm->id))) {
                $farm->username = $this->farm_model->get_username_by_farm_id($farm->id);
                $farm->device = $this->get_device_list_by_farm_id($farm->id);
                $data[] = array('farm' => $farm);
            }
        }
        return $data;
        //return array("farm"=>$data);
    }

    /**
     * Get devices list
     * @return type
     */
    function get_devices_list() {
        $query = $this->db->get($this->table);
        $result = $query->result();
        $data = [];
        foreach ($result as $key => $value) {
            $this->db->reset_query();
            $data[] = $this->device_model->get_basic_info($value->id);
        }
        return $data;
    }

    // FOR WEB CLIENT SIDE
    function web_check_exists_($device_id) {
        if (!$this->check_exists(array('id' => $device_id))) {
            return false;
        }
        return true;
    }

    function web_check_exists_serial($serial) {
        if (!$this->check_exists(array('serial' => $serial))) {
            return false;
        }
        return true;
    }

}
