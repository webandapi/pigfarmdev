<?php
class Sensor_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'sensor';
    var $key = 'id';
    var $params=array('id', 'chart_id', 'name', 'description', 'unit', 'min_val', 'max_val','slug','image','color');

    public function get_info_($id)
    {
    	return $this->get_info($id);
    }
    function check_exists_($id)
    {
        if(!$this->check_exists(array('id'=>$id))){
            echo json_encode(array($this->messageCode=>$this->codeNotFound,'message'=>$this->lang->line('message_not_exists_sensor')));
            exit();
        }
        return true;
    }
}