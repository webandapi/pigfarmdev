<?php

class Field_model extends FT_Model {

    //ten bang du lieu
    public $table = 'field';
    #dùng chính category của wordpress làm field(lĩnh vực) của hệ thống
    //public $table = 'wp_terms';
    //var $key = 'term_id';
    var $key = 'id';
    var $params = array('id', 'name', 'desc', 'slug', 'cover', 'temp_id', 'level', 'priority');

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    //var $params=array('term_id', 'name', 'slug', 'term_group');
    public function fieldslug_to_fieldid($data = '') {
        foreach ($data as $key => $value) {
            $data[$key] = $this->get_info_rule(array('slug'))->term_id;
        }
    }

    public function get_info($id, $field = '') {
        if (!$id) {
            return FALSE;
        }
        $where = array();
        $where[$this->key] = $id;
        $result = $this->get_info_rule($where, $field);
        if ($result) {
            unset($result->term_id);
        }
        return $result;
    }

    public function get_basic_info($id, $field = '') {
        if (!$id) {
            return FALSE;
        }
        $where = array();
        $where[$this->key] = $id;
        $result = $this->get_info_rule($where, $field);
        if ($result) {
            unset($result->term_id);
            unset($result->desc);
        }
        return $result;
    }

    function get_list_($input = array()) {
        //xu ly ca du lieu dau vao
        $this->get_list_set_input($input);

        //thuc hien truy van du lieu
        $query = $this->db->get($this->table);
        //echo $this->db->last_query();
        $result = $query->result();
        foreach ($result as $key => $value) {
            if ($value) {
                //$value->field_id=$value->term_id;
                unset($value->term_id);
                unset($value->term_group);
            }
        }
        return $result;
    }

    function get_field_collection_by_user($user_id) {
        $fieldList = $this->userfield_model->get_user_field_list($user_id);
        $fieldCol = array();
        foreach ($fieldList as $key => $value) {
            if ($value) {
                //var_dump($this->get_info($value->field_id));
                if ($this->get_info($value->field_id))
                    $fieldCol[] = $this->get_info($value->field_id);
            }
        }
        return $fieldCol;
    }

    function get_field_id_by_term_id($term_id) {
        return $this->get_info_rule(array('term_id' => $term_id))->id;
    }

    function get_term_id_by_field_id($field_id) {
        return $this->get_info_rule(array('id' => $field_id))->term_id;
    }

    function get_term_id_by_field_id_list($fields = array()) {
        if (gettype($fields) == 'array') {
            $terms = array();
            foreach ($fields as $key => $value) {
                $terms[] = $this->get_term_id_by_field_id($value);
            }
            return $terms;
        } else {
            echo json_encode(array($this->messageCode => $this->codeFailed, $this->message => 'Input variable is not array'));
            exit();
        }
    }

    function create_field_with_wpterms($value = '') {
        $this->db->where(array('taxonomy' => 'category'));
        $query = $this->db->get('wp_term_taxonomy');

        $termTaxonomyList = $query->result();

        //$termTaxonomyList=$this->wp_term_taxonomy_model->get_list(array('where'=>array('taxonomy'=>'category'),'order'=>array('term_id','asc')));
        foreach ($termTaxonomyList as $key => $value) {
            $where = array();
            $where['term_id'] = $value->term_id;
            $result = $this->get_info_rule($where);
            if (!$result) {
                //echo $value->term_id.',';
                $this->db->reset_query();
                $termObj = '';
                $this->db->where(array('term_id' => $value->term_id));
                $query = $this->db->get('wp_terms');
                //echo $value->term_id;
                $this->db->select('name');
                if ($query->num_rows()) {
                    $termObj = $query->row();
                }
                //echo $value->description."_".$value->term_id."_".$termObj->slug;
                $this->db->reset_query();
                $level = 1;
                //var_dump($value);
                if ($value->parent != 0) {
                    $termObj2 = $this->wp_term_taxonomy_model->get_info_rule(array('term_id' => $value->parent));
                    //var_dump($termObj2);
                    if ($termObj2->parent != 0) {
                        $level = 3;
                    } else
                        $level = 2;
                }
                //echo $level;
                $this->create(array('name' => $value->description, 'term_id' => $value->term_id, 'slug' => $termObj->slug, 'level' => $level));
            }
        }
    }

    function slugify($text) {
        return $this->slug_model->stripUnicode($text);
    }

}
