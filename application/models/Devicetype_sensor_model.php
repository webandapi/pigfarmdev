<?php
class Devicetype_sensor_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'devicetype_sensor';
    var $key = 'id';
    var $params=array('devicetype_id', 'sensor_id');

    public function get_info_($id)
    {
    	return $this->get_info($id);
    }
    function check_exists_($devicetype_id,$sensor_id)
    {
        {
            if(!$this->devicetype_sensor_model->check_exists(array('devicetype_id'=>$devicetype_id,'sensor_id'=>$sensor_id))){
            echo json_encode(array($this->messageCode=>$this->codeFailed,'message'=>$this->lang->line('message_this_sensor_not_belong_to_devicetype')));
            exit();
            }
        }
        return true;
    }
    
}