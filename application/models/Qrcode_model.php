<?php

class Qrcode_model extends FT_Model {

    public $table = 'qrcode';
    var $key = 'id';
    var $params = array('planting_id', 'harvest_id', 'farm_code', 'qr_from', 'qr_to');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get max qrcode by farm code
     * @param type $farm_code
     * @return boolean
     */
    public function get_maxqrcode_by_farmcode($farm_code) {
        if ($farm_code):
            $this->db->select('MAX(qr_to) AS qr_to');
            $this->db->where('farm_code', $farm_code);
            $query = $this->db->get($this->table);
            return $query->row();
        endif;

        return false;
    }

    /**
     * Get Min-Max Qrcode by farm
     * @param type $farm_code
     * @return boolean
     */
    public function get_farm_qrcode($farm_code) {
        if ($farm_code):
            $this->db->select('MIN(qr_from) AS qrmin, MAX(qr_to) AS qrmax');
            $this->db->where('farm_code', $farm_code);
            $query = $this->db->get($this->table);
            return $query->row();
        endif;
        return FALSE;
    }

}
