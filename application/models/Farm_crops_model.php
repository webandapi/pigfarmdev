<?php

class Farm_crops_model extends FT_Model {

    public $table = 'farm_crops';
    var $key = 'id';
    var $key1 = 'farm_id';
    var $key2 = 'crops_id';
    var $params = array('farm_id', 'crops_id');

    /**
     * Update data
     * @param type $farm_id
     * @param type $data
     * @return boolean|int
     */
    public function update($farm_id = '', $data = array()) {
        if ($farm_id) {
            $this->del_rule(array('farm_id' => $farm_id));
            if ($data) {
                $this->farm_certificate_model->create($farm_id, $data);
            }
        }
        return false;
    }

    public function delete($where = array()) {
        $this->db->where($where);
        $this->db->delete($this->table);
        return TRUE;
    }

}
