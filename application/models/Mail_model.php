<?php
class Mail_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'mail';
    var $key = 'id';
    var $key1 = 'from_user';
    var $key2 = 'to_user';
    var $params=array('id','from_user', 'to_user', 'title', 'content','send','has_read');
     function my_create($data = array())
    {
      $this->check_param($data,$this->params);
        if($this->db->insert($this->table, $data))
        {
          return $insert_id = $this->db->insert_id();
           return TRUE; 
        }else{
            return FALSE;
        }
    }
    function get_info_($id)
    {
        $this->db->where(array($this->key=>$id));
        $query = $this->db->get($this->table);
        if ($query->num_rows())
        {
            $result=$query->row();
            $result->from_user=$this->user_model->get_username_by_userid($result->from_user);
            $result->to_user=$this->user_model->get_username_by_userid($result->to_user);
            return $result;
        }
        
        return false;
    }
    function my_update($id,$user_id,$data)
    {
        if(!$this->check_exists(array('id'=>$id)))
        {
            echo json_encode(array('messageCode'=>404,'message'=>'This mail is not exists.'));
            exit();
        }
        if(!$this->check_exists(array('id'=>$id,$this->key2=>$user_id)))
        {
            echo json_encode(array('messageCode'=>404,'message'=>'This mail is not belong to current user.'));
            exit();
        }
       $this->update($id, $data);
       return true;
    }
    function get_mail_list_by_user_id($user_id)
    {
        $this->db->where(array($this->key2=>$user_id));
        $query = $this->db->get($this->table);
        $result=$query->result();
        foreach ($result as $key => $value) {
            $value->from_user=$this->user_model->get_username_by_userid($value->from_user);
            $value->to_user=$this->user_model->get_username_by_userid($value->to_user);
        }
        return $result;
    }
    
    function get_mail_what_user_read_list($user_id)
    {

        $this->db->where(array($this->key2=>$user_id,'has_read'=>1));
        $query = $this->db->get($this->table);
        $readmailList=$query->result();
        foreach ($readmailList as $key => $value) {
            $value->from_user=$this->user_model->get_username_by_userid($value->from_user);
            $value->to_user=$this->user_model->get_username_by_userid($value->to_user);
       }
        return $readmailList;
    }   
    function ft_get_current_weekday($date) {
      date_default_timezone_set('Asia/Ho_Chi_Minh');
      $date=strtotime($date);
      $weekday = date("l",$date);
      $weekday = strtolower($weekday);
      switch($weekday) {
          case 'monday':
              $weekday = 'Thứ hai';
              break;
          case 'tuesday':
              $weekday = 'Thứ ba';
              break;
          case 'wednesday':
              $weekday = 'Thứ tư';
              break;
          case 'thursday':
              $weekday = 'Thứ năm';
              break;
          case 'friday':
              $weekday = 'Thứ sáu';
              break;
          case 'saturday':
              $weekday = 'Thứ bảy';
              break;
          default:
              $weekday = 'Chủ nhật';
              break;
      }
      return $weekday.', '.date($this->lang->line('date_format2'),$date);
  }


}