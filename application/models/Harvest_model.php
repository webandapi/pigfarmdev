<?php

class Harvest_model extends FT_Model {

    public $table = 'harvest';
    var $key = 'id';
    var $params = array('packaging_date', 'package_user_id','planting_id');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

   /**
     * Get farm_id by harvest
     * @param type $diary_id
     * @return boolean
     */
    public function get_farm_by_harvest($harvest_id) {
        if ($harvest_id):
            $sql = "SELECT pl.farm_id FROM {$this->table} ha JOIN planting pl ON ha.planting_id = pl.id WHERE ha.id = {$harvest_id}";
            $query = $this->db->query($sql);
            return $query->row();
        endif;
        return false;
    }

}
