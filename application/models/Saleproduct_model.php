<?php

class Saleproduct_model extends FT_Model {

    //ten bang du lieu
    public $table = 'sale_product';
    public $key1 = 'farm_id';
    public $key2 = 'product_id';
    var $params = array('farm_id', 'product_id');

    //for API method
    function my_create($farm_id = '', $data = array()) {
        if ($farm_id) {
            foreach ($data as $key => $product) {
                if (!$this->product_model->my_check_exists($product->id)) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'the id: ' . $product->id . ' in table: ' . $this->table . ' is not exists!'));
                    $this->farm_model->delete($farm_id);
                    exit();
                }
            }
            foreach ($data as $key => $product) {
                $uf = $this->get_sale_product_info($farm_id, $product->id);
                if ($uf) {
                    
                } else {
                    $sale_product = array(
                        "farm_id" => $farm_id,
                        "product_id" => $product->id,
                    );
                    $this->db->insert('sale_product', $sale_product);
                }
            }
        }
        return false;
    }

    //for webform method
    function create_($farm_id = '', $data = array()) {
        if ($farm_id) {
            foreach ($data as $key => $value) {
                $product = '';
                if (gettype($value) == 'object') {
                    $product = $value->id;
                } else {
                    $product = $value;
                }
                if (!$this->product_model->my_check_exists($product)) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'the id: ' . $product . ' in table: ' . $this->table . ' is not exists!'));
                    $this->farm_model->delete($farm_id);
                    exit();
                }
                $uf = $this->get_sale_product_info($farm_id, $value);
                if ($uf) {
                    
                } else {
                    $sale_product = array(
                        "farm_id" => $farm_id,
                        "product_id" => $value,
                    );
                    $this->db->insert('sale_product', $sale_product);
                }
            }
        }
        return false;
    }

    function my_update($farm_id = '', $data = array()) {
        //var_dump($farmname);
        //var_dump($data);
        if ($farm_id) {
            $this->del_rule(array('farm_id' => $farm_id));
            if ($data) {
                foreach ($data as $key => $value) {
                    //echo 'aaa';
                    $product = '';
                    if (gettype($value) == 'object') {
                        $product = $value->id;
                    } else {
                        $product = $value;
                    }
                    if (!$this->product_model->my_check_exists($product)) {
                        echo json_encode(array('messageCode' => 404, 'message' => 'the id: ' . $product . ' in table: ' . $this->table . ' is not exists!'));
                        exit();
                    }
                    $sale_product = array(
                        "farm_id" => $farm_id,
                        "product_id" => $product,
                    );
                    $this->db->insert('sale_product', $sale_product);
                }
            }
            return true;
        }
        return false;
    }

    function get_sale_product_info($farm_id, $product_id) {
        if (!$farm_id || !$product_id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key1] = $farm_id;
        $where[$this->key2] = $product_id;
        return $this->get_info_rule($where);
    }

    function get_products_by_farm_id($farm_id = '') {
        $farmproductCol = $this->get_sale_product_list($farm_id);
        //var_dump($farmproductCol);
        $farmproductData = array();
        foreach ($farmproductCol as $key => $value) {
            //echo '________';
            //var_dump($value);
            $farmproductData[] = $this->product_model->get_basic_info($value->product_id);
        }
        return $farmproductData;
    }

    function get_sale_product_list($farm_id = '') {
        $this->db->where(array($this->key1 => $farm_id));
        $query = $this->db->get($this->table);
        return $query->result();
    }

    /**
     * Get list sale products by user Id
     * @param type $userID
     * @return boolean
     */
    function get_saleproducts_by_user($userID) {
        if ($userID) {
            $this->db->where(array('user_id' => $userID));
            $query = $this->db->get($this->table);
            $result = $query->result();
            $listSaleProducts = array();
            foreach ($result as $key => $pro) {
                $data = new stdClass;
                $farm = $this->farm_model->get_basic_info($pro->farm_id);
                $data->farm = $farm->name;
                $data->name = $pro->name;
                $cover = $farm->cover ? $this->config->config['upload_path'] . 'farm/' . $farm->cover : '';
                $data->cover = $cover;
                $data->price = $pro->price;
                $data->unit = $pro->unit;
                $listSaleProducts[] = $data;
            }
            return $listSaleProducts;
        }
        return false;
    }

    function check_exists_($product_id, $farm_id) {
        if ($product_id) {
            if ($this->check_exists(array('product_id' => $product_id, 'farm_id' => $farm_id))) {
                echo json_encode(array('messageCode' => 500, 'message' => $this->lang->line('product_is_sale')));
                exit();
            }
        }
        return true;
    }

}
