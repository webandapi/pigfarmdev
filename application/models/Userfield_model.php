<?php

class Userfield_model extends FT_Model {
    #field_id=term_id
    //ten bang du lieu

    public $table = 'user_field';
    public $key1 = 'user_id';
    public $key2 = 'field_id';
    var $params = array('user_id', 'field_id');

    /**
     * Insert data
     * @param type $user_id
     * @param type $data
     * @return boolean
     */
    function create($user_id = '', $data = array()) {
        if ($user_id) {
            foreach ($data as $key => $field) {
                $field = (array) $field;
                if (!$this->field_model->my_check_exists($field['id'])) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'the id: ' . $field['id'] . ' in table: ' . $this->table . ' is not exists!'));
                    $this->user_model->delete($user_id);
                    exit();
                }
            }
            foreach ($data as $key => $field) {
                $field = (array) $field;
                $uf = $this->get_user_field_info($user_id, $field['id']);
                if ($uf) {
                    
                } else {
                    $user_field = array(
                        "user_id" => $user_id,
                        "field_id" => $field['id'],
                    );
                    $this->db->insert('user_field', $user_field);
                }
            }
        }
        return false;
    }

    public function add_4_expert_with_all_fields($value = '') {
        $expertList = array(136, 137, 138);
        $fieldList = $this->field_model->get_list_();

        foreach ($fieldList as $key => $field)
            foreach ($expertList as $key => $user_id) {
                $userFieldObj = $this->get_post_rate_info($user_id, $field->id);
                if ($userFieldObj) {
                    
                } else {
                    $userFieldObj = array(
                        "user_id" => $user_id,
                        "field_id" => $field->id,
                    );
                    //echo 123;
                    $this->db->insert('user_field', $userFieldObj);
                }
            }
        return true;
    }

    /**
     * Update data
     * @param type $user_id
     * @param type $data
     * @return boolean
     */
    function update($user_id = '', $data = array()) {
        if ($user_id) {
            $this->del_rule(array('user_id' => $user_id));
            if ($data) {
                $data = (array) $data;
                foreach ($data as $key => $field) {
                    $field = (array) $field;
                    if (!$this->field_model->my_check_exists($field['id'])) {
                        echo json_encode(array('messageCode' => 404, 'message' => 'the id: ' . $field['id'] . ' in table: ' . $this->user_model->table . ' is not exists!'));
                        exit();
                    }
                }
                foreach ($data as $key => $field) {
                    $field = (array) $field;
                    $uf = $this->get_user_field_info($user_id, $field['id']);
                    if ($uf) {
                        
                    } else {
                        $user_field = array(
                            "user_id" => $user_id,
                            "field_id" => $field['id'],
                        );
                        $this->db->insert('user_field', $user_field);
                    }
                }
            }
            return true;
        }
        return false;
    }

    function getFieldsByUserId($user_id = '') {
        $userfieldCol = $this->get_user_field_list_by_user_id($user_id);
        $UserFieldData = array();
        foreach ($userfieldCol as $key => $value) {
            $UserFieldData[] = $value->field_id;
        }
        return $UserFieldData;
    }

    function getFieldsSlugByUserId($user_id = '') {
        $userfieldCol = $this->get_user_field_list_by_user_id($user_id);
        $UserFieldData = array();
        foreach ($userfieldCol as $key => $value) {
            $UserFieldData[] = $this->field_model->get_info($value->field_id)->slug;
        }
        return $UserFieldData;
    }

    /**
     * Lay danh sach field
     * $input : mang cac du lieu dau vao
     */
    function get_user_field_list($user_id = '') {
        $this->db->where(array('user_id' => $user_id));
        $query = $this->db->get($this->table);
        return $query->result();
    }

}
