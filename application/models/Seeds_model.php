<?php

class Seeds_model extends FT_Model {

    public $table = 'seeds';
    var $key = 'id';
    var $params = array('name', 'crops_id', 'farm_id', 'origin', 'date_of_purchase', 'expiration_date', 'amount', 'seed_cost', 'pesticide_costs', 'fertilizer_cost', 'harvesting_costs', 'expected_productivity', 'total_cost');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

}
