<?php
class Searchrank_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'search_rank';
    var $key = 'id';
    var $params=array('id', 'query', 'count', 'result');

    function check_query($query,$resultCount)
    {
        if($this->check_exists(array('query'=>$query))){
        	$searchQuery=$this->get_info_rule(array('query'=>$query));
        	$this->update($searchQuery->id,array('count'=>$searchQuery->count+1,'result'=>$resultCount));
        	return true;
        }else{
        	$this->create(array('query'=>$query,'result'=>$resultCount));
        	return true;
        }
        return false;

    }
}