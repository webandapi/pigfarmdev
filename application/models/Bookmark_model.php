<?php

class Bookmark_model extends FT_Model {

    //ten bang du lieu
    public $table = 'bookmark';
    var $key = 'id';
    var $params = array('id', 'user_id', 'post_id', 'date');

    /**
     * Check bookmark is exist
     * @param type $post_id
     * @param type $user_id
     * @return boolean
     */
    function check_exists_($post_id, $user_id) {
        if ($this->check_exists(array('post_id' => $post_id, 'user_id' => $user_id)))
            return 1;

        return 0;
    }

    /**
     * Get bookmarks by user Id
     * @param type $user_id
     * @return boolean|\stdClass
     */
    function get_bookmarks_by_user($user_id, $limit, $offset, $os = '', $version = '') {
        if ($user_id) {

            $this->db->where(array('user_id' => $user_id));
            $this->db->order_by('id', 'DESC');
            $query = $this->db->get($this->table, $limit, $offset);
            $result = $query->result();

            $bookmarks = array();

            foreach ($result as $key => $bookmark) {

                $post = $this->post_model->get_post_basic_info($user_id, $bookmark->post_id, $os, $version);
                $post['bookmark'] = 1;
                $bookmarks[] = $post;
            }

            return $bookmarks;
        }

        return false;
    }

    /**
     * Delete bookmark
     * @param type $user_id
     * @param type $data
     * @return boolean
     */
    function my_delete($user_id, $post_id) {
        $this->bookmark_model->del_rule(array('user_id' => $user_id, 'post_id' => $post_id));
        return true;
    }

}
