<?php

class Post_model extends FT_Model {

    //ten bang du lieu
    public $table = 'wp_posts';
    public $key = 'ID';
    var $params = array('ID', 'view_count');

    /**
     * Check exists
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get total posts
     * @return type
     */
    public function get_total_post() {
        $total = wp_count_posts();
        return $total->publish;
    }

    /**
     * Get tags
     * @return type
     */
    public function get_tags() {
        $tags = get_tags();
        return $tags;
    }

    /**
     * Get view
     * @param type $id
     * @return boolean
     */
    public function get_view($id = '') {
        if (!$id) {
            return FALSE;
        }
        $count = $this->get_info($id, 'view_count')->view_count;
        return (int) $count;
    }

}
