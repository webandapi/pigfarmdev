<?php

class Supportdevice_model extends FT_Model {

    public $table = 'support-device';
    var $key = 'id';
    var $params = array('id', 'deviceid', 'ipserver', 'username', 'password', 'usersql', 'passsql', 'dateactive', 'warantytime', 'idsupport', 'idfarm', 'isdefault', 'created', 'updated');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        $result = parent::get_list($input);
        $data = [];
        foreach ($result as $key => $value) {
            $this->db->reset_query();
            $data[] = $this->supportdevice_model->get_basic_info($value->id);
        }
        return $data;
    }

    /**
     * Get devices info
     * @param type $id
     * @return type
     */
    public function get_basic_info($id) {
        $device = $this->supportdevice_model->get_info($id, array('*'));

        $supporter = $this->supporter_model->get_info($device->idsupport, array('name'));

        $farm = $this->farm_model->get_info($device->idfarm, array('name'));

        if ($supporter)
            $device->supporter = $supporter;

        if ($farm)
            $device->farm = $farm;

        return $device;
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get device info
     * @param type $id
     * @return boolean
     */
    function get_device_info($id) {
        if ($id) {
            $this->db->reset_query();
            return $this->get_info($id, '*');
        }
        return false;
    }

    /**
     * Get device by farm ids
     * @param type $farmids
     * @return boolean
     */
    public function get_device_by_farm($farmids) {
        if ($farmids):
            $this->db->select('support-device.*, farm.name, supporter.name as supporter');
            $this->db->from($this->table);
            $this->db->where_in('idfarm', $farmids);
            $this->db->join('farm', 'farm.id = support-device.idfarm');
            $this->db->join('supporter', 'supporter.id = support-device.idsupport');
            $query = $this->db->get();
            return $query->result();
        endif;
        return false;
    }

}
