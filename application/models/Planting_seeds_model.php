<?php

class Planting_seeds_model extends FT_Model {

    public $table = 'planting_seeds';
    var $key = 'id';
    var $params = array('planting_id', 'seeds_id');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($planting_id, $data = array()) {
        if ($planting_id) {
            $this->del_rule(array('planting_id' => $planting_id));
            $i = 0;
            if ($data) {
                foreach ($data as $val) {
                    if ($this->planting_seeds_model->create(array('planting_id' => $planting_id, 'seeds_id' => $val)))
                        $i++;
                }
            }
            return $i;
        }
        return false;
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

}
