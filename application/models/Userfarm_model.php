<?php

class Userfarm_model extends FT_Model {

    public $table = 'users_farm';
    var $key = 'id';
    var $params = array('farm_id', 'username', 'pw', 'fullname', 'email', 'phone', 'modules_in', 'status', 'date_of_birth', 'gender', 'created');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get sum
     * @param type $field
     * @param type $where
     * @return type
     */
    public function get_total($input = array()) {
        return parent::get_total($input);
    }

    /**
     * Get user info
     * @param type $username
     * @param type $field
     * @return boolean
     */
    function get_user_info($username, $field = '') {
        if (!$username)
            return false;

        $where = array();
        $where['username'] = $username;

        return $this->get_info_rule($where, $field);
    }

    /**
     * Get user basic info
     * @param type $user_id
     * @return boolean
     */
    function get_user_basic_info($user_id) {
        if ($user_id) {
            $this->db->reset_query();
            return $this->get_info($user_id, '*');
        }
        return false;
    }

    /**
     * Get count user by farm
     * @param type $farm_in
     * @return boolean
     */
    public function get_user_by_farm($farm_in) {
        if ($farm_in):
            $this->db->select('users_farm.*');
            $this->db->from($this->table);
            $this->db->where_in('farm_id', $farm_in);
            $query = $this->db->get();
            return $query->result();
        endif;
        return false;
    }

}
