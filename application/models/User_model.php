<?php

class User_model extends FT_Model {

    public $table = 'user';
    var $key = 'user_id';
    var $params = array('user_id', 'username', 'firstname', 'lastname', 'pw', 'super', 'address', 'email', 'phone', 'usertype_id', 'status', 'date_of_birth', 'gender', 'avatar', 'language','flags');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        $this->db
                ->from($this->table)
                ->join('user_field', 'user_field.user_id = user.user_id')
                ->join('profile', 'profile.user_id = user.user_id')
                ->where('user.user_id', $id);

        /*
         * get the original DELETE SQL, in our case:
         * "DELETE FROM `cookies` JOIN `recipes` ON `recipes`.`cookie`=`cookies`.`id` WHERE `recipes`.`rating`='BAD'"
         */
        $sql = $this->db->delete($this->table);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get sum
     * @param type $field
     * @param type $where
     * @return type
     */
    public function get_total($input = array()) {
        return parent::get_total($input);
    }

    /**
     * Get user info
     * @param type $username
     * @param type $field
     * @return boolean
     */
    function get_user_info($username, $field = '') {
        if (!$username)
            return false;

        $where = array();
        $where['username'] = $username;

        return $this->get_info_rule($where, $field);
    }

    /**
     * Get user basic info
     * @param type $user_id
     * @return boolean
     */
    function get_user_basic_info($user_id) {
        if ($user_id) {
            $this->db->reset_query();
            return $this->get_info($user_id, '*');
        }
        return false;
    }

    function get_username_by_userid($user_id) {
        if ($user_id) {
            return $this->get_info($user_id, 'username')->username;
        }
        return false;
    }

}
