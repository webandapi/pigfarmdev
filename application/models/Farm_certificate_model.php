<?php

class Farm_certificate_model extends FT_Model {

    public $table = 'farm_certificate';
    var $key = 'id';
    var $key1 = 'farm_id';
    var $key2 = 'certificate_id';
    var $params = array('farm_id', 'certificate_id');

    /**
     * Insert data
     * @param type $farm_id
     * @param type $data
     * @return boolean
     */
    public function create($farm_id = '', $data = array()) {
        if ($farm_id) {
            foreach ($data as $val) {
                $certificate_id = gettype($val) == 'object' ? $val->id : $val;

                // check certificate is exists
                if (!$this->certificate_model->check_exists(array('id' => $certificate_id))) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'Không tìm thấy chứng nhận được yêu cầu!'));
                    exit();
                }

                // check farm is exists
                if (!$this->farm_model->check_exists(array('id' => $farm_id))) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'Không tìm thấy trang trại được yêu cầu!'));
                    exit();
                }

                // check farm - certificate is exists
                if ($this->farm_certificate_model->check_exists(array($this->key1 => $farm_id, $this->key2 => $certificate_id))) {
                    echo json_encode(array('messageCode' => 404, 'message' => 'Thông tin trang trại - chứng nhận bị trùng lặp!'));
                    exit();
                }

                // insert data
                $this->db->insert($this->table, array('farm_id' => $farm_id, 'certificate_id' => $certificate_id));
            }
        }
        return false;
    }

    /**
     * Update data
     * @param type $farm_id
     * @param type $data
     * @return boolean|int
     */
    public function update($farm_id = '', $data = array()) {
        if ($farm_id) {
            $this->del_rule(array('farm_id' => $farm_id));
            if ($data) {
                $this->farm_certificate_model->create($farm_id, $data);
            }
        }
        return false;
    }

}
