<?php

class Planting_model extends FT_Model {

    public $table = 'planting';
    var $key = 'id';
    var $params = array('name', 'sowing_time', 'expected_harvest_time', 'status', 'farm_id', 'seeds_id');

    /*
     * Insert data
     */

    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Update data
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Delete data
     * @param type $id
     * @return type
     */
    public function delete($id) {
        return parent::delete($id);
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     * @return type
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Get list diaries by farm_id
     * @param type $farm_in
     * @param type $field
     * @return type
     */
    public function get_diaries_by_farm($farm_in = array()) {
        if ($farm_in):
            $this->db->select('planting.name AS planting, diaries.*');
            $this->db->from($this->table);
            $this->db->join('diaries', 'planting.id = diaries.planting_id');
            $this->db->where('status', 0);
            $this->db->where_in('farm_id', $farm_in);
            $query = $this->db->get();
            return $query->result();
        endif;
        return false;
    }

    /**
     * Get list incidents by farm_id
     * @param type $farm_in
     * @param type $field
     * @return type
     */
    public function get_incidents_by_farm($farm_in = array()) {
        if ($farm_in):
            $this->db->select('planting.name AS planting, incidents.*');
            $this->db->from($this->table);
            $this->db->join('incidents', 'planting.id = incidents.planting_id');
            $this->db->where('status', 0);
            $this->db->where_in('farm_id', $farm_in);
            $query = $this->db->get();
            return $query->result();
        endif;
        return false;
    }

    /**
     * Get harvest list by farm
     * @param type $farm_in
     * @return boolean
     */
    public function get_harvest_by_farm($farm_in = array()) {
        if ($farm_in):
            $this->db->select('planting.name AS planting, harvest.*');
            $this->db->from($this->table);
            $this->db->join('harvest', 'planting.id = harvest.planting_id');
            $this->db->where('status', 0);
            $this->db->where_in('farm_id', $farm_in);
            $query = $this->db->get();
            return $query->result();
        endif;
        return false;
    }

    /**
     * Get planting by farm
     * @param type $farm_in
     * @return boolean
     */
    public function get_planting_by_farm($farm_in = array()) {
        if ($farm_in):
            $this->db->select('id,name');
            $this->db->from($this->table);
            $this->db->where_in('farm_id', $farm_in);
            $query = $this->db->get();
            return $query->result();
        endif;
        return false;
    }

}
