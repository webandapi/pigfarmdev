<?php
class Regisproduct_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'regis_product';
    var $key = 'id';
    var $params=array('id','serial', 'user_id','farm_id', 'product_id', 'name', 'cover', 'note','img1','img2','img3','img4','img5');
    function check_exists_($regisproduct_id,$user_id='')
    {
         if($user_id)
        {
            if(!$this->check_exists(array('id'=>$product_id,'user_id'=>$user_id))){
                echo json_encode(array('messageCode'=>500,'message'=>$this->lang->line('message_this_product_belong_with_this_user')));
            exit();
            }
        }else{
            if(!$this->check_exists(array('id'=>$regisproduct_id))){
                echo json_encode(array('messageCode'=>500,'message'=>$this->lang->line('message_not_exists_product')));
                exit();
            }
        }
	    
        return true;
    }
    function check_exists_serial($serial,$user_id='')
    {
         if($user_id)
        {
            if(!$this->check_exists(array('serial'=>$serial,'user_id'=>$user_id))){
                echo json_encode(array('messageCode'=>500,'message'=>$this->lang->line('message_this_product_not_belong_with_this_user')));
            exit();
            }
        }else{
            if(!$this->check_exists(array('serial'=>$serial))){
                echo json_encode(array('messageCode'=>500,'message'=>$this->lang->line('message_not_exists_product')));
                exit();
            }
        }
        return true;
    }

    function get_info($id, $field = '')
    {
        if (!$id)
        {
            return FALSE;
        }
        
        $where = array();
        $where[$this->key] = $id;
        
        $regisproduct=$this->get_info_rule($where, $field);
        if($regisproduct){
            return $regisproduct;
        }else{
            return false;
        }
    }
    function get_info_by_serial($serial, $field = '')
    {
        if (!$serial)
        {
            return FALSE;
        }
        
        $where = array();
        $where['serial'] = $serial;
        
        $regisproduct=$this->get_info_rule($where, $field);
        if($regisproduct){
            $regisproduct->imgs=[$regisproduct->img1,$regisproduct->img2,$regisproduct->img3,$regisproduct->img4,$regisproduct->img5];
            unset($regisproduct->img1);
            unset($regisproduct->img2);
            unset($regisproduct->img3);
            unset($regisproduct->img4);
            unset($regisproduct->img5);
            $regisproduct->product=$this->product_model->get_info($regisproduct->product_id);
            unset($regisproduct->product_id);
            $regisproduct->farm=$this->farm_model->get_info($regisproduct->farm_id);
            unset($regisproduct->farm_id);
            $regisproduct->user=$this->user_model->get_user_basic_info($regisproduct->user_id);
            unset($regisproduct->user_id);
            return $regisproduct;
        }else{
            return false;
        }
    }
    function get_basic_info($id, $field = '')
    {
        if (!$id)
        {
            return FALSE;
        }
        
        $where = array();
        $where[$this->key] = $id;
        
        $regisproduct=$this->get_info_rule($where, $field);
        return $regisproduct;
    }
    function get_regisproduct_list_by_product_id($product_id)
    {
        $this->db->where(array('product_id'=>$product_id));
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }
    function get_regisproduct_list()
    {
        //$this->db->where(array('product_id'=>$product_id));
        $query = $this->db->get($this->table);
        $result = $query->result();
        return $result;
    }


}