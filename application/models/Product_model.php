<?php

class Product_model extends FT_Model {

    public $table = 'product';
    var $key = 'id';
    var $params = array('id', 'field_id', 'name', 'cover', 'note', 'level');

    function check_exists_($product_id, $field_id = '') {
        if ($field_id) {
            if (!$this->check_exists(array('id' => $product_id, 'field_id' => $field_id))) {
                echo json_encode(array('messageCode' => 500, 'message' => $this->lang->line('message_this_product_not_in_field')));
                exit();
            }
        } else {
            if (!$this->check_exists(array('id' => $product_id))) {
                echo json_encode(array('messageCode' => 500, 'message' => $this->lang->line('message_not_exists_product')));
                exit();
            }
        }
        return true;
    }

    function get_info($id, $field = '') {
        if (!$id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key] = $id;

        $product = $this->get_info_rule($where, $field);
        if ($product) {
            $product->field = $this->field_model->get_basic_info($product->field_id);
            unset($product->field_id);
            return $product;
        } else {
            return false;
        }
    }

    function get_basic_info($id, $field = '') {
        if (!$id) {
            return FALSE;
        }

        $where = array();
        $where[$this->key] = $id;

        $product = $this->get_info_rule($where, $field);
        $product->field = $this->field_model->get_basic_info($product->field_id);
        unset($product->field_id);
        unset($product->note);
        return $product;
    }

    function get_product_list_by_field_id($field_id) {
        $this->db->where(array('field_id' => $field_id));
        $query = $this->db->get($this->table);
        $result = $query->result();
        foreach ($result as $key => $product) {
            $product->field = $this->field_model->get_basic_info($product->field_id);
            unset($product->field_id);
            unset($product->note);
        }
        return $result;
    }

    function create_products_with_fields($value = '') {
        $termTaxonomyList = $this->field_model->get_list();
        foreach ($termTaxonomyList as $key => $value) {
            if (!$this->check_exists(array('field_id' => $value->id))) {
                $this->create(array('name' => $value->name, 'field_id' => $value->id, 'level' => $value->level));
            }
        }
        return true;
    }

    /**
     * Get list data
     * @param type $input
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Get list products
     * @return type
     */
    function get_product_list() {
        $query = $this->db->get($this->table);
        $result = $query->result();
        foreach ($result as $key => $product) {
            $product->field = $this->field_model->get_basic_info($product->field_id);
            unset($product->field_id);
            unset($product->note);
        }
        return $result;
    }

}
