<?php
class Readpost_model extends FT_Model
{
	#post_id=term_id
    //ten bang du lieu
    public $table = 'read_post';
    public $key1 = 'user_id';
    public $key2 = 'post_id';
    var $params=array( 'user_id','post_id');
    //for API method
	function my_create($user_id='',$post_id = array())
	{
		if($user_id)
		{
			if(!$this->post_model->my_check_exists($post_id))
			{
				echo json_encode(array('messageCode'=>404,'message'=>'the id: '.$post_id.' in table: '.$this->post_model->table.' is not exists!'));
				exit();
			}
			if($user_id!=''&&$post_id!='')
			{
				if($this->check_exists(array('user_id'=>$user_id,'post_id'=>$post_id)))
				{
					echo json_encode(array('messageCode'=>500,'message'=>'Already exist!'));
					exit();
				}
		          $read_post = array(
		         "user_id" => $user_id,
		         "post_id" => $post_id);
		        $this->db->insert('read_post',$read_post);
		        return true;
	        }
			else{
				echo json_encode(array('messageCode'=>404,'message'=>'check input user_id and post_id!'));
				exit();
			}
	    
        }
	    return false;
        
	}
	function check_read_post($user_id='',$post_id = array())
	{
		if($user_id!=''&&$post_id!='')
		{
			if($this->check_exists(array('user_id'=>$user_id,'post_id'=>$post_id)))
			{
				return true;
			}
		}
		else{
			echo json_encode(array('messageCode'=>404,'message'=>'check input user_id and post_id!'));
			exit();
		}
		return false;
	}
	function get_post_what_user_read_list($user_id)
	{

		$this->db->where(array('user_id'=>$user_id));
		$query = $this->db->get($this->table);
		$readPostList=$query->result();
		foreach ($readPostList as $key => $value) {
	       //$value->username=$this->user_model->get_info($value->user_id)->username;
	       unset($value->user_id);
	   }
		return $readPostList;
	}	


}