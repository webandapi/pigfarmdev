<?php

class Harvest_distributor_model extends FT_Model {

    public $table = 'harvest_distributors';
    var $key = 'id';
    var $params = array('harvest_id', 'ditributor_id');

    
    /**
     * Insert harvest-distributor
     * @param type $harvest_id
     * @param type $data
     * @return boolean|int
     */
    public function update($harvest_id, $data = array()) {
        if ($harvest_id) {
            $this->del_rule(array('harvest_id' => $harvest_id));
            $i = 0;
            if ($data) {
                foreach ($data as $val) {
                    if ($this->harvest_distributor_model->create(array('harvest_id' => $harvest_id, 'distributor_id' => $val)))
                        $i++;
                }
            }
            return $i;
        }
        return false;
    }

}
