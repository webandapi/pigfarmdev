<?php
class Rate_model extends FT_Model
{
    //ten bang du lieu
    public $table = 'rate';
    public $key1 = 'farmer_id';
    public $key2 = 'expert_id';
    var $params=array('farmer_id', 'expert_id', 'star', 'comment', 'time');
    //for API method
	function create($data=array())
	{
		if($data['star']<1||$data['star']>5) 
		{
			$message=json_encode(array('messageCode'=>500,'message'=>'Star must be 1,2,3,4 or 5.'));
          echo ($message);
          exit();
		}
		if($data)
		{
			$this->check_param($data,$this->params);

			if($this->db->insert($this->table, $data))
			{
			   return TRUE;
			}else{
				return FALSE;
			}
		}
		return false;
	}

	function update_($data)
	{
		if (!$data)
		{
			return FALSE;
		}
		if($data['star']<1||$data['star']>5) 
		{
			$message=json_encode(array('messageCode'=>500,'message'=>'Star must be 1,2,3,4 or 5.'));
          echo ($message);
          exit();
		}
		$this->check_param($data,$this->params);
		$where = array();
	 	$where['farmer_id'] = $data['farmer_id'];
	 	$where['expert_id'] = $data['expert_id'];
	 	$array=array();
	 	$array['star'] = $data['star'];
	 	$array['comment'] = $data['comment'];
	 	$array['time'] = $data['time'];
	    $this->update_rule($where, $array);
	 	
	 	return TRUE;
	}
	function get_rate_of_expert_list($expert_id)
	{
		$this->db->where(array('expert_id'=>$expert_id));
		$query = $this->db->get($this->table);
		return $query->result();
	}	

}