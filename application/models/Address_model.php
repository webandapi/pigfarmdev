<?php
class Address_model extends FT_Model
{
    //ten bang du lieu
    public $table1 = 'devvn_tinhthanhpho';
    public $table2 = 'devvn_quanhuyen';
    public $table3 = 'devvn_xaphuongthitran';

    public $key1 = 'province_id';
    public $key2 = 'district_id';
    public $key3 = 'ward_id';


    public function gettinhthanhpho_list()
    {
		$query = $this->db->get($this->table1);
		//echo $this->db->last_query();
		return $query->result();
    }
    public function getquanhuyen_list()
    {
		$query = $this->db->get($this->table2);
		//echo $this->db->last_query();
		return $query->result();
    }
    public function getxaphuong_list()
    {
		$query = $this->db->get($this->table3);
		//echo $this->db->last_query();
		return $query->result();
    }

    public function get_district_by_province_id($id='')
    {
        $this->check_exists_province($id);
        $this->db->where(array($this->key1=>$id));
        $query = $this->db->get($this->table2);
        return $query->result();
    }

    function check_exists_province($id)
    {
        $this->db->where(array($this->key1=>$id));
        //thuc hien cau truy van lay du lieu
        $query = $this->db->get($this->table1);
        if($query->num_rows() > 0){
            return TRUE;
        }else{
            echo json_encode(array('messageCode'=>404,'message'=>'Province is not exists!'));
            exit();
        }
    }
    public function get_ward_by_district_id($id='')
    {
        $this->check_exists_district($id);
        $this->db->where(array($this->key2=>$id));
        $query = $this->db->get($this->table3);
        return $query->result();
    }

    function check_exists_district($id)
    {
        $this->db->where(array($this->key2=>$id));
        //thuc hien cau truy van lay du lieu
        $query = $this->db->get($this->table2);
        if($query->num_rows() > 0){
            return TRUE;
        }else{
            echo json_encode(array('messageCode'=>404,'message'=>'District is not exists!'));
            exit();
        }
    }

}