<?php

class Usertype_model extends FT_Model {

    public $table = 'usertype';
    var $key = 'id';
    var $params = array('id', 'name', 'note');

    /**
     * Get data list
     * @return type
     */
    public function get_list($input = array()) {
        return parent::get_list($input);
    }

    /**
     * Check exist
     * @param type $where
     */
    public function check_exists($where = array()) {
        return parent::check_exists($where);
    }

    /**
     * Insert data
     * @param type $data
     * @return type
     */
    public function create($data = array()) {
        return parent::create($data);
    }

    /**
     * Uodate datas
     * @param type $id
     * @param type $data
     * @return type
     */
    public function update($id, $data) {
        return parent::update($id, $data);
    }

    /**
     * Check record name is exist
     * @return type
     */
    public function check_usertype_exist($name) {
        if ($name) {
            $this->db->where(array('name' => $name));
            $query = $this->db->get($this->table);
            return $query->row();
        }
        return false;
    }

    /**
     * Get data info
     * @param type $id
     * @return boolean
     */
    public function get_usertype_info($id) {
        if ($id) {
            $this->db->where(array('id' => $id));
            $query = $this->db->get($this->table);
            return $query->row();
        }
        return FALSE;
    }

}
