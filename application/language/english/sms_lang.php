<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sms_lang
 *
 * @author Duc Quyen
 */
defined('BASEPATH') OR exit('No direct script access allowed');

//object
$lang['farm'] = 'farm';
$lang['name_farm'] = 'Name of the farm';
$lang['add_farm'] = 'Add farm';
$lang['product'] = 'Product';
$lang['product_name'] = 'product name';
$lang['farm_size'] = 'Farm size';
$lang['size'] = 'size';
$lang['location'] = 'location';
$lang['latitude'] = 'latitude';
$lang['longitude'] = 'longtitude';
$lang['option'] = 'option';
$lang['list_of_farms'] = 'List of farms';
$lang['you_have_no_farm'] = 'You have no farm';
$lang['view_detail'] = 'View details';
$lang['update_farm'] = 'Update farm';
$lang['farm_details'] = 'Farm details';
$lang['farm_not_found'] = 'No farm information found';
$lang['farm_information'] = 'farm information';

//activity
$lang['edit'] = 'Edit';
$lang['add'] = 'Add';
$lang['delete'] = 'Delete';
$lang['update'] = 'Update';
$lang['image'] = 'image';
$lang['update_success'] = 'Update success';
$lang['wrong'] = 'Wrong';
$lang['notfound'] = 'Not found';
$lang['view'] = 'View';
$lang['message_not_exists_user'] = 'User is not exists';
$lang['message_username_null'] = 'Username is null';
$lang['message_token_null'] = 'Token is null.';
$lang['message_refresh_key_is_null'] = 'refresh key is null.';
$lang['message_token_invalid'] = 'Token is invalid';
$lang['message_token_out_of_date'] = 'Token is out of date';
$lang['total'] = 'total';
$lang['comment'] = 'comment';
$lang['average'] = 'average';
$lang['medium'] = 'medium';
$lang['small'] = 'small';
$lang['big'] = 'big';
$lang['time'] = 'time';
$lang['noposttoday'] = 'no post today!';
$lang['gmt'] = 'Y-m-d H:i:s';
$lang['cannot_update_email'] = 'Can not update Email';

// message
$lang['welcome'] = 'Welcome';
$lang['congratulation'] = 'Congratulation';
$lang['failed'] = 'Failed';
$lang['success'] = 'Success';
$lang['error'] = 'Error';
$lang['save_success'] = 'save success';
$lang['missing_query'] = 'Missing query';
$lang['accept'] = 'Accept';
$lang['notaccept'] = 'Not accept';
$lang['wrongcurrentpassword'] = 'Current password is wrong';
$lang['you_are'] = 'You are';
$lang['professional_information'] = 'Professional information';
$lang['terms_of_use'] = 'Terms of use';
$lang['copyright'] = 'Fman keeps content on this site';
$lang['login_success'] = 'Login success';
$lang['login_failed'] = 'Login failed';
$lang['message_not_exists_farm'] = 'farm is not exists';
$lang['message_not_exists_product'] = 'product is not exists';
$lang['product_not_exists'] = 'This product is not exists!';
$lang['message_missing_param'] = 'Missing param';
$lang['this_user_not_in_any_field'] = 'This user is not in any field';
$lang['missing_data'] = 'Missing data';
$lang['message_this_farm_not_belong_to_user'] = 'This farm is not belong to current user';
$lang['message_this_product_not_in_field'] = 'This product is not in this field';
$lang['new_pass_not_difference'] = 'New password is not difference';
$lang['not_exists_field'] = 'Require field is not exists';
$lang['message_this_mail_not_belong_to_user'] = 'This mail is not belong to current user.';
$lang['message_already_exists'] = 'already exists';
$lang['put_username'] = 'Please put in username';
$lang['value_not_correct'] = 'Value is not correct, just accept 1/0 value';
$lang['check_param'] = 'Check input param';


//Registered Product
$lang['regis_product'] = 'Registered Product';
$lang['missing_serial'] = 'Missing Serial';
$lang['not_exists_regisproduct'] = 'This product is not exists';
$lang['message_this_regisproduct_not_belong_to_user'] = 'This registered product is not belong to current user';

// field-category
$lang['agricultural'] = 'agricultural';
$lang['aquacultural'] = 'aquacultural';
$lang['fresh-vegetable'] = 'fresh-vegetable';

//main menu
$lang['logo_name'] = 'Sensor Management System';
$lang['mainnav_device'] = 'Device';
$lang['mainnav_sensor'] = 'Sensor';
$lang['mainnav_user'] = 'User Management';
$lang['mainnav_admin'] = 'Admin Management';
$lang['mainnav_data'] = 'Data';
$lang['mainnav_setting'] = 'Setting';
$lang['mainnav_dashboard'] = 'Dashboard';
$lang['mainnav_help'] = 'Help';
$lang['username'] = 'User Name';
$lang['logout'] = 'Log out';
$lang['myaccount'] = 'My Account';
$lang['account'] = 'Account';
$lang['update_account'] = 'Update account';
$lang['account_infomation'] = 'Account information';
$lang['agri_contact'] = 'Agricultural Contact';
$lang['function'] = 'Function';
$lang['contact'] = 'Contact';
$lang['news'] = 'News';
$lang['article_tutorial'] = 'Article tutorial';
$lang['device_store'] = 'Device store';
$lang['introduce'] = 'Introduce';

// admin data gridview
$lang['username'] = 'User Name';
$lang['lastname'] = 'Last Name';
$lang['firstname'] = 'First Name';
$lang['field'] = 'Field';
$lang['user'] = 'User';
$lang['home'] = 'Home';
$lang['level'] = 'Level';
$lang['language'] = 'Language';
$lang['phone'] = 'Phone number';
$lang['usertype'] = 'User Type';
$lang['gender'] = 'Gender';
$lang['female'] = 'Female';
$lang['male'] = 'Male';
$lang['dateofbirth'] = 'Day of birth';
$lang['avatar'] = 'Avatar';
$lang['admin'] = 'Admin';
$lang['device'] = 'Device';
$lang['device_type'] = 'Device Type';
$lang['message_not_exists_devicetype'] = 'Not exists this devicetype';

//profile
$lang['profile'] = 'Profile';
$lang['certificate'] = 'Certificate';
$lang['note'] = 'Note';
$lang['exp'] = 'Experience';
$lang['office'] = 'Office';
$lang['exp'] = 'Experience';
$lang['star'] = 'Star';
$lang['rate'] = 'Rate';
$lang['postrate'] = 'Post rate';
$lang['your_rate'] = 'Your rate';
$lang['rate_success'] = 'rate success';
$lang['rate_failed'] = 'Rate failed';
$lang['rate_exists'] = 'This rating is already exists';
$lang['rate_not_exists'] = 'This rating is not exists';
$lang['rate_not_found'] = 'Rating not found';
$lang['update_rate_success'] = 'Update rate success';
$lang['update_rate_failed'] = 'Update rate failed';

//post
$lang['id'] = 'id';
$lang['date'] = 'Date';
$lang['author'] = 'Author';
$lang['title'] = 'Title';
$lang['post'] = 'Post';
$lang['by'] = 'By';
$lang['hotposts'] = 'Hot posts';
$lang['todaypost'] = 'Today Post';
$lang['oldpost'] = 'Old Posts';
$lang['connectedpost'] = 'Connected posts';
$lang['readpost'] = 'Read posts';
$lang['in_wp'] = 'In wordpress';
$lang['search_rank'] = 'search rank';
$lang['empty_postid'] = 'Missing post id';
$lang['user_empty_field'] = 'Current user does not have any field!';
$lang['post_not_exists'] = 'Post with current id is not exists';
$lang['post_id_missing'] = 'Missing post id';
$lang['read_more'] = 'Read more';
$lang['search'] = 'Search';
$lang['categories'] = 'Categories';
$lang['posts_popular'] = 'The popular posts';
$lang['posts_useful'] = 'Useful posts on cultivation, animal husbandry';
$lang['experts_on_this_field'] = 'Experts on this field';
$lang['experts_confirmed_article_not_correct'] = 'You have confirmed this article is not correct';
$lang['experts_confirmed_article_correct'] = 'You have confirmed this article is correct';
$lang['opinion_of_the_expert'] = 'Opinion of the expert';
$lang['agree'] = 'Agree';
$lang['disagree'] = 'Disagree';
$lang['related_posts'] = 'Related posts';
$lang['can_not_view_posts_in_category'] = 'You can not view posts in this category';

//title_box
$lang['dashboard_sensor'] = 'Sensor statistical at collector';
$lang['device_channel'] = 'Channel';
$lang['device_min'] = 'Min';
$lang['device_max'] = 'Max';
$lang['device_avg'] = 'AVG';
$lang['device_title_weather'] = 'Weather forecast';

//popup for setting device show dashboard
$lang['pop_devicedashboard_title'] = 'Please choose device showed on dashboard';
$lang['pop_devicedashboard_save_success'] = 'Save success';

//chart
$lang['chart'] = 'Chart';

//dashboard
$lang['device_no_data'] = 'Device does not have data';

//device
$lang['message_not_exists_device'] = 'Device is not exists';
$lang['message_this_device_not_belong_to_user'] = 'Device is not belong to this user';
$lang['message_this_device_is_already_in_a_farm'] = 'This device is already in one farm';
$lang['message_this_device_is_already_active'] = 'This device is already active';
$lang['list_devices'] = 'List of devices';
$lang['user_not_loged'] = 'You are not logged in';
$lang['user_not_loged_or_enter_serial_device'] = 'You are not logged in or enter your device serial number';
$lang['device_list_not_found'] = 'Device list not found';
$lang['enter_into_serial'] = 'Enter into serial';
$lang['temperature'] = 'Temperature';
$lang['air'] = 'air';
$lang['land'] = 'land';
$lang['humidity'] = 'Humidity';
$lang['light'] = 'Light';
$lang['figures_from_the_farm'] = 'Figures from the farm';
$lang['activation_date'] = 'Activation date';
$lang['date_of_manufacture'] = 'Date of manufacture';
$lang['warranty_expiration_date'] = 'Warranty expiration date';

//popup chart for every device
$lang['popchart_filter'] = 'Filter by';
$lang['popchart_filter_date'] = 'Date';
$lang['popchart_filter_week'] = 'Week';
$lang['popchart_filter_month'] = 'Month';
$lang['popchart_button_filter'] = 'Filter';
$lang['popchart_msg_error_input_month'] = 'Please enter month again (next month should be greater than the previous month)';
$lang['device_active'] = 'Active Date';
$lang['device_end_warranty'] = 'Warranty expiration date';
$lang['device_info'] = 'Device information';
$lang['device_user'] = 'User\'s device information';
$lang['device_user_fullname'] = 'Full name';
$lang['device_user_area'] = 'Area';
$lang['pdfexport_time'] = 'Time';
$lang['data_empty'] = 'Not enough data for rendering chart by';
$lang['popchart_sensor_all'] = 'All sensors';

//menu user login
$lang['usernav_logout'] = 'Log out';
$lang['usernav_login'] = 'Login';
$lang['usernav_login_with'] = 'Login with';
$lang['usernav_changepass'] = 'Change password';
$lang['usernav_register'] = 'Register';

//Login
$lang['text_username'] = 'Username';
$lang['text_password'] = 'Password';
$lang['text_forgot_password'] = 'Forgot password';
$lang['remember_password'] = 'Remember password';
$lang['check_your_username_or_password'] = 'Check your username or password';
$lang['missing_username_password'] = 'Missing username or password';

//Admin page
$lang['text_manager_admin'] = 'Admin management';
$lang['text_manager_user'] = 'User management';
$lang['text_manager_device'] = 'Device management';
$lang['text_manager_sensor'] = 'Sensor management';

//Change password
$lang['text_old_password'] = 'Old password';
$lang['text_new_password'] = 'New password';
$lang['text_confirm_password'] = 'Confirm password';

//Forgot password
$lang['forgot_password'] = 'Forgot Password';
$lang['reset_password_success'] = 'Change password successfully';
$lang['reset_password_failed'] = 'Change password failed';
$lang['send_reset_password_mail_success'] = 'An email has been sent to your mailbox';
$lang['reset_password'] = 'Change password';
$lang['forgot_password_body_text'] = 'Click the link below to reset your password: ';
$lang['send_mail_error'] = 'Unable to send email ';
$lang['send_mail_success'] = 'Email has been sent successfully';
$lang['not_exists_email'] = 'Your email does not exists in our system ';
$lang['email_already_exists'] = 'Email has been used';
$lang['text_email'] = 'Email address';
$lang['text_send'] = 'Send';
$lang['text_reset_password'] = 'Forgot password';
$lang['text_reset_password_content'] = 'Your new password is:';
$lang['text_reset_password_success_message'] = 'Your new password has sent via email address:';
$lang['text_reset_password_error_message'] = 'Your email is not exist. Please try again';


//For show message
$lang['message_error_login'] = 'This username or password is incorrect';
$lang['message_error_confirm_password'] = 'Wrong confirm password';
$lang['message_success_change_password'] = 'Your password has changed, please login again';
$lang['message_error_old_password'] = 'Your old password is incorrect';
$lang['message_success_logout'] = 'Logout success';
$lang['message_deactive_user'] = 'Your account has been locked. Please contact admin to active acount again.';
$lang['message_forgot_password'] = 'Your password has been resent. Please check your inbox to receive new password';
$lang['message_required_username'] = 'Forgot password';
$lang['message_required_password'] = 'Forgot password';
$lang['message_empty_username'] = 'You have not enter username';
$lang['message_empty_password'] = 'You have not enter password';
$lang['message_deny_access'] = 'You don\'t have permission to access this page';
$lang['message_already_exists_user'] = 'Username already exists';
$lang['message_not_exists_user'] = 'This user is not exists!';

//user
$lang['name'] = "Full name/Organization";
$lang['admin_name'] = "Full name";
$lang['username'] = "Username";
$lang['password'] = "Password";
$lang['address'] = "Address";
$lang['status'] = "Status";
$lang['email'] = 'Email';
$lang['send_email_success'] = 'Send email success';
$lang['send_email_failed'] = 'Send email failed';
$lang['please_check_email'] = 'Please check your email';
$lang['mail'] = 'Mail';
$lang['mailbox'] = 'Mail box';
$lang['date_of_birth'] = 'Birthday';
$lang['active'] = 'Active';
$lang['deactive'] = 'Deactive';
$lang['date_format'] = 'l, F j, Y';
$lang['date_format2'] = 'd/m/Y H:i:s';
$lang['male'] = 'Male';
$lang['female'] = 'Female';
$lang['user_info'] = 'User info';
$lang['info'] = "Info";
$lang['fullname'] = "Full name";
$lang['expert'] = "Expert";
$lang['expertinfo'] = "expert info";
$lang['expert-list'] = 'Expert list';
$lang['not_expert'] = 'this user is not an expert';
$lang['expert_not_exists'] = 'This expert is not exists';
$lang['farmer'] = "Farmer";
$lang['user_not_expert'] = 'This user is not an expert';
$lang['farmer_missing'] = 'Missing farmer';
$lang['expert_missing'] = 'Missing expert';
$lang['username_missing'] = 'Missing username';
$lang['user_not_have_profile'] = 'This user does not have profile';
$lang['user_not_verify'] = 'Your account has not been verified';
$lang['user_not_exists'] = 'This user is not exists';
$lang['register_success'] = 'Register success';
$lang['register_failed'] = 'Register failed';
$lang['expert_information'] = 'Expert information';
$lang['expert_rating'] = 'Expert rating';
$lang['not_update'] = 'Not update';

//sensor
$lang['sensor'] = 'Sensor';
$lang['sensor_type'] = 'Sensor type';
$lang['sensor_name'] = 'Sensor name';
$lang['chart_color'] = 'Chart sensor color';
$lang['sensor_unit'] = 'Unit';
$lang['message_not_exists_sensor'] = 'Sensor is not exists';
$lang['message_this_sensor_not_belong_to_devicetype'] = 'This sensor is not belong to this device type';

//device
$lang['device_id_wasp'] = 'Device ID';
$lang['device_name'] = 'Device name';
$lang['show_dashboard'] = 'Showed on dashboard';

//collector
$lang['mainnav_collector'] = "Collector";
$lang['collector'] = 'Collector';
$lang['id_collector'] = 'Collector ID';
$lang['collector_name'] = 'Collector name';
$lang['collector_info'] = "Collector information";
$lang['collector_area'] = 'Province/City';
$lang['collector_date_active'] = 'Date active';
$lang['collector_date_end_warranty'] = 'Date end warranty';
$lang['collector_serial'] = "Collector serial";
$lang['collector_status'] = "Status";

$lang['try_it_now'] = "Try it now";

//login fb,gg
$lang['wrong_app_id'] = 'Wrong app id';
$lang['check_user_error'] = 'Error in check user info';
$lang['check_token_error'] = 'Error in check access token of app';
$lang['missing_token_or_password'] = 'Missing access token or password';
$lang['access_token_is_invalid'] = 'access_token is invalid';

//update api
$lang['password_not_update'] = 'Password can not update in this api';
$lang['not_update_with_null'] = 'Can not update with null value';
$lang['update_avatar_error'] = 'Update avatar got error';
$lang['level_id_not_exists'] = 'Level id is not exists';
$lang['input_null'] = 'Input null';

//...
$lang['check_input_id'] = 'Check input id';
$lang['field_id_missing'] = 'field id is missing';
$lang['is_not_exists'] = 'is not exists';
$lang['in_table'] = 'in table';
$lang['update_farm_cover_error'] = 'Update farm cover got error';
$lang['save_farm_error'] = 'Save farm cover got error';
$lang['missing_field_input'] = 'Missing field input';
$lang['check_input_params'] = 'Check input params';
$lang['not_found_data'] = 'Not found data';
$lang['params_id_null'] = 'Params id is null';
$lang['refresh_key_is_invalid'] = 'refresh_key is invalid';

$lang['product_is_sale'] = 'This product has been sold';
$lang['bookmark_failed'] = 'Bookmark failed';
