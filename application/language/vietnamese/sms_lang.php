<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sms_lang
 *
 * @author Nhat Hoang
 */
defined('BASEPATH') OR exit('No direct script access allowed');

//object
$lang['farm'] = 'Trang trại';
$lang['name_farm'] = 'Tên trang trại';
$lang['add_farm'] = 'Thêm trang trại';
$lang['product'] = 'Sản phẩm';
$lang['product_name'] = 'tên sản phẩm';
$lang['farm_size'] = 'Diện tích';
$lang['size'] = 'kích thước';
$lang['location'] = 'Địa điểm';
$lang['latitude'] = 'vĩ độ';
$lang['longitude'] = 'kinh độ';
$lang['option'] = 'tùy chọn';
$lang['list_of_farms'] = 'Danh sách trang trại';
$lang['you_have_no_farm'] = 'Bạn chưa có trang trại nào';
$lang['view_detail'] = 'Xem chi tiết';
$lang['update_farm'] = 'Cập nhật trang trại';
$lang['farm_details'] = 'Chi tiết trang trại';
$lang['farm_not_found'] = 'Không tìm thấy thông tin trang trại';
$lang['farm_information'] = 'thông tin trang trại';


//activity
$lang['edit'] = 'Chỉnh sửa';
$lang['add'] = 'Thêm';
$lang['delete'] = 'Xóa';
$lang['update'] = 'Cập nhật';
$lang['image'] = 'hình ảnh';
$lang['update_success'] = 'Cập nhật thành công';
$lang['wrong'] = 'Sai';
$lang['notfound'] = 'Không tìm thấy';
$lang['view'] = 'Xem';
$lang['message_not_exists_user'] = 'Tài khoản không tồn tại';
$lang['message_username_null'] = 'Tên đăng nhập trống';
$lang['message_token_null'] = 'Token trống.';
$lang['message_refresh_key_is_null'] = 'refresh key trống.';
$lang['message_token_invalid'] = 'Token không hợp lệ';
$lang['message_token_out_of_date'] = 'Token hết hạn';
$lang['total'] = 'Tổng';
$lang['comment'] = 'bình luận';
$lang['average'] = 'trung bình cộng';
$lang['medium'] = 'trung bình';
$lang['small'] = 'nhỏ';
$lang['big'] = 'lớn';
$lang['time'] = 'thời gian';
$lang['noposttoday'] = 'hôm nay không có bài viết nào!';
$lang['gmt'] = 'Y-m-d H:i:s';
$lang['cannot_update_email'] = 'Không thể thay đổi Email';

// message
$lang['welcome'] = 'Chào mừng';
$lang['congratulation'] = 'Chúc mừng';
$lang['failed'] = 'Lỗi';
$lang['success'] = 'Thành công';
$lang['error'] = 'Lỗi';
$lang['save_success'] = 'lưu thành công';
$lang['missing_query'] = 'Thiếu từ khóa';
$lang['accept'] = 'Chấp nhận';
$lang['notaccept'] = 'Không chấp nhận';
$lang['wrongcurrentpassword'] = 'Mật khẩu hiện tại không đúng';
$lang['you_are'] = 'Bạn là';
$lang['professional_information'] = 'Thông tin chuyên môn';
$lang['terms_of_use'] = 'Điều khoản sử dụng';
$lang['copyright'] = 'Fman giữ bản quyền nội dung trên website này';
$lang['login_success'] = 'Đăng nhập thành công';
$lang['login_failed'] = 'Đăng nhập thất bại';
$lang['message_not_exists_farm'] = 'không tìm thấy trang trại';
$lang['message_not_exists_product'] = 'không tìm thấy sản phẩm';
$lang['product_not_exists'] = 'Sản phẩm này không tồn tại!';
$lang['message_missing_param'] = 'thiếu tham số';
$lang['this_user_not_in_any_field'] = 'người dùng này chưa thuộc lĩnh vực nào';
$lang['missing_data'] = 'thiếu dữ liệu';
$lang['message_this_farm_not_belong_to_user'] = 'trang trại này không thuộc về user hiện tại';
$lang['message_this_product_not_in_field'] = 'sản phẩm này không nằm trong lĩnh vực này';
$lang['new_pass_not_difference'] = 'Mật khẩu mới trùng mật khẩu cũ';
$lang['not_exists_field'] = 'Lĩnh vực không tồn tại';
$lang['message_this_mail_not_belong_to_user'] = 'thư này không thuộc về user hiện tại.';
$lang['message_already_exists'] = 'đã tồn tại';
$lang['put_username'] = 'Vui lòng điền vào tên đăng nhập';
$lang['value_not_correct'] = 'Giá trị không đúng, chỉ chấp nhận 1 hoặc 0';
$lang['check_param'] = 'Kiểm tra các giá trị đưa vào';

//Registered Product
$lang['regis_product'] = 'Registered Product';
$lang['missing_serial'] = 'Thiếu Serial';
$lang['not_exists_regisproduct'] = 'Sản phẩm này không tồn tại';
$lang['message_this_regisproduct_not_belong_to_user'] = 'Sản phẩm này không thuộc về người dùng hiện tại';
// field-category
$lang['agricultural'] = 'nông nghiệp';
$lang['aquacultural'] = 'thủy sản';
$lang['fresh-vegetable'] = 'rau sạch';

//main menu
$lang['logo_name'] = 'Hệ thống quản lý môi trường ';
$lang['mainnav_device'] = 'Thiết bị';
$lang['mainnav_sensor'] = 'Cảm biến';
$lang['mainnav_user'] = 'Người dùng';
$lang['mainnav_admin'] = 'Quản trị viên';
$lang['mainnav_data'] = 'Dữ liệu';
$lang['mainnav_setting'] = 'Cài đặt';
$lang['mainnav_dashboard'] = 'Dashboard';
$lang['mainnav_help'] = 'Trợ giúp';

$lang['logout'] = 'Đăng xuất';
$lang['myaccount'] = 'Xem tài khoản';
$lang['account'] = 'Tài khoản';
$lang['update_account'] = 'Cập nhật tài khoản';
$lang['account_infomation'] = 'Thông tin tài khoản';
$lang['agri_contact'] = 'Danh bạ nông nghiệp';
$lang['function'] = 'Tính năng';
$lang['contact'] = 'Liên hệ';
$lang['news'] = 'Tin tức';
$lang['article_tutorial'] = 'Bài viết hướng dẫn';
$lang['device_store'] = 'Cửa hàng thiết bị';
$lang['introduce'] = 'Giới thiệu';

// admin data gridview
$lang['username'] = 'Tên đăng nhập';
$lang['lastname'] = 'Họ';
$lang['firstname'] = 'Tên';
$lang['field'] = 'Lĩnh vực';
$lang['user'] = 'Người dùng';
$lang['home'] = 'Trang chủ';
$lang['level'] = 'Cấp độ';
$lang['language'] = 'Ngôn ngữ';
$lang['phone'] = 'Điện thoại';
$lang['usertype'] = 'Loại người dùng';
$lang['gender'] = 'Giới tính';
$lang['female'] = 'Nữ';
$lang['male'] = 'Nam';
$lang['dateofbirth'] = 'Ngày sinh';
$lang['avatar'] = 'Ảnh đại diện';
$lang['admin'] = 'Quản trị viên';
$lang['device'] = 'Thiết bị';
$lang['device_type'] = 'Loại thiết bị';
$lang['message_not_exists_devicetype'] = 'Không tìm thấy thiết bị này';

//profile
$lang['profile'] = 'Thông tin';
$lang['certificate'] = 'Chứng nhận';
$lang['note'] = 'Ghi chú';
$lang['exp'] = 'Kinh Nghiệm';
$lang['office'] = 'Văn phòng';
$lang['exp'] = 'Kinh nghiệm';
$lang['star'] = 'Sao';
$lang['rate'] = 'Đánh giá';
$lang['postrate'] = 'Xác nhận bài viết';
$lang['your_rate'] = 'Đánh giá của bạn';
$lang['rate_success'] = 'đánh giá thành công';
$lang['rate_failed'] = 'Đánh giá thất bại';
$lang['rate_exists'] = 'Đánh giá đã tồn tại';
$lang['rate_not_exists'] = 'Đánh giá không tồn tại';
$lang['rate_not_found'] = 'Không tìm thấy đánh giá';
$lang['update_rate_success'] = 'Cập nhật đánh giá thành công';
$lang['update_rate_failed'] = 'Cập nhật đánh giá thất bại';

//post
$lang['id'] = 'Mã';
$lang['date'] = 'Ngày đăng';
$lang['author'] = 'Người đăng';
$lang['title'] = 'tiêu đề';
$lang['post'] = 'Bài viết';
$lang['by'] = 'Bởi';
$lang['hotposts'] = 'Tin nóng';
$lang['todaypost'] = 'Tin trong ngày';
$lang['oldpost'] = 'Tin cũ';
$lang['connectedpost'] = 'Bài viết đã kết nối';
$lang['readpost'] = 'Tin đã đọc';
$lang['in_wp'] = 'Trong wordpress';
$lang['search_rank'] = 'Top tìm kiếm';
$lang['empty_postid'] = 'Không tìm thấy Id bài viết';
$lang['user_empty_field'] = 'Không tìm thấy lĩnh vực của người dùng hiện tại!';
$lang['post_not_exists'] = 'Bài viết không tồn tại';
$lang['post_id_missing'] = 'Thiếu id bài viết';
$lang['read_more'] = 'Xem thêm';
$lang['search'] = 'Tìm kiếm';
$lang['categories'] = 'Danh mục';
$lang['posts_popular'] = 'Các bài viết xem nhiều nhất';
$lang['posts_useful'] = 'Các bài viết hữu ích về trồng trọt, chăn nuôi';
$lang['experts_on_this_field'] = 'Các chuyên gia về lĩnh vực này';
$lang['experts_confirmed_article_not_correct'] = 'Bạn đã xác nhận bài viết này là không đúng';
$lang['experts_confirmed_article_correct'] = 'Bạn đã xác nhận bài viết này là đúng';
$lang['opinion_of_the_expert'] = 'Ý kiến của chuyên gia';
$lang['agree'] = 'Đồng ý';
$lang['disagree'] = 'Không đồng ý';
$lang['related_posts'] = 'Các bài viết liên quan';
$lang['can_not_view_posts_in_category'] = 'Bạn không thể xem tin trong chuyên mục này';

//title_box
$lang['dashboard_sensor'] = 'Trạng thái các cảm biến tại bộ thu thập dữ liệu';
$lang['device_channel'] = 'Channel';
$lang['device_min'] = 'Min';
$lang['device_max'] = 'Max';
$lang['device_avg'] = 'AVG';
$lang['device_title_weather'] = 'Dự báo thời tiết';

//popup for setting device show dashboard
$lang['pop_devicedashboard_title'] = 'Chọn điểm thu thập dữ liệu hiển thị trên dashboard';
$lang['pop_devicedashboard_save_success'] = 'Thay đổi thiết lập thành công';

//chart
$lang['chart'] = 'Biểu đồ';

//dashboard
$lang['device_no_data'] = 'Điểm thu thập chưa có dữ liệu';

//device
$lang['message_not_exists_device'] = 'Không tìm thấy thiết bị này';
$lang['message_this_device_not_belong_to_user'] = 'Thiết bị không thuộc về người dùng này';
$lang['message_this_device_is_already_in_a_farm'] = 'Thiết bị này đã thuộc về một trang trại';
$lang['message_this_device_is_already_active'] = 'Thiết bị này đã được kích hoạt';
$lang['list_devices'] = 'Danh sách thiết bị';
$lang['user_not_loged'] = 'Bạn chưa đăng nhập tài khoản';
$lang['user_not_loged_or_enter_serial_device'] = 'Bạn chưa đăng nhập hoặc vui lòng nhập số serial của thiết bị';
$lang['device_list_not_found'] = 'Không tìm thấy danh sách thiết bị';
$lang['enter_into_serial'] = 'Nhập vào serial';
$lang['temperature'] = 'Nhiệt độ';
$lang['air'] = 'không khí';
$lang['land'] = 'đất';
$lang['humidity'] = 'Độ ẩm';
$lang['light'] = 'Ánh sáng';
$lang['figures_from_the_farm'] = 'Các số liệu từ trang trại';
$lang['activation_date'] = 'Ngày kích hoạt';
$lang['date_of_manufacture'] = 'Ngày sản xuất';
$lang['warranty_expiration_date'] = 'Ngày hết hạn bảo hành';


//popup chart for every device
$lang['popchart_filter'] = 'Hiển thị theo';
$lang['popchart_filter_date'] = 'Ngày';
$lang['popchart_filter_week'] = 'Tuần';
$lang['popchart_filter_month'] = 'Tháng';
$lang['popchart_button_filter'] = 'Lọc';
$lang['popchart_msg_error_input_month'] = 'Vui lòng nhập lại tháng (tháng sau phải lớn hơn tháng trước)';
$lang['device_active'] = 'Ngày kích hoạt';
$lang['device_end_warranty'] = 'Hạn bảo hành';
$lang['device_info'] = 'Điểm thu thập';
$lang['device_user'] = 'Đơn vị sử dụng';
$lang['device_user_fullname'] = 'Họ và tên';
$lang['device_user_area'] = 'Khu vực';
$lang['pdfexport_time'] = 'Thời gian';
$lang['data_empty'] = 'Không có đủ dữ liệu đọc thông tin theo';
$lang['popchart_sensor_all'] = 'Tất cả cảm biến';

//menu user login
$lang['usernav_logout'] = 'Thoát';
$lang['usernav_login'] = 'Đăng nhập';
$lang['usernav_login_with'] = 'Đăng nhập qua';
$lang['usernav_changepass'] = 'Đổi mật khẩu';
$lang['usernav_register'] = 'Đăng ký';

//Login
$lang['text_username'] = 'Tên tài khoản';
$lang['text_password'] = 'Mật khẩu';
$lang['text_forgot_password'] = 'Quên mật khẩu';
$lang['remember_password'] = 'Ghi nhớ mật khẩu';
$lang['check_your_username_or_password'] = 'Vui lòng kiểm tra tên đăng nhập hoặc mật khẩu';
$lang['missing_username_password'] = 'Thiếu tên đăng nhập hoặc mật khẩu';

//Admin page
$lang['text_manager_admin'] = 'Quản lý người quản trị';
$lang['text_manager_user'] = 'Quản lý người dùng';
$lang['text_manager_device'] = 'Quản lý điểm thu thập dữ liệu';
$lang['text_manager_sensor'] = 'Quản lý cảm biến điểm thu thập dữ liệu';

//Change password
$lang['text_old_password'] = 'Mật khẩu cũ';
$lang['text_new_password'] = 'Mật khẩu mới';
$lang['text_confirm_password'] = 'Nhập lại mật khẩu';

//Forgot password
$lang['forgot_password'] = 'Quên mật khẩu';
$lang['reset_password_success'] = 'Đổi mật khẩu thành công';
$lang['reset_password_failed'] = 'Đổi mật khẩu không thành công';
$lang['send_reset_password_mail_success'] = 'Gửi thông tin đặt lại mật khẩu thành công đến email';
$lang['reset_password'] = 'Đổi mật khẩu';
$lang['forgot_password_body_text'] = 'Nhấn vào link sau để đặt lại mật khẩu: ';
$lang['send_mail_error'] = 'Gửi mail không thành công ';
$lang['send_mail_success'] = 'Gửi mail thành công!';
$lang['not_exists_email'] = 'Địa chỉ email không tồn tại trong hệ thống';
$lang['email_already_exists'] = 'Địa chỉ email đã được sử dụng';
$lang['text_email'] = 'Địa chỉ email';
$lang['text_send'] = 'Gửi';
$lang['text_reset_password'] = 'Lấy lại mật khẩu';
$lang['text_reset_password_content'] = 'Mật khẩu mới của bạn là:';
$lang['text_reset_password_success_message'] = 'Mật khẩu mới của bạn đã được gửi lại tại địa chỉ email:';
$lang['text_reset_password_error_message'] = 'Địa chỉ email không tồn tại trong cơ sở dữ liệu. Vui lòng kiểm tra lại.';


//For show message
$lang['message_error_login'] = 'Tên đăng nhập hoặc mật khẩu không đúng';
$lang['message_error_confirm_password'] = 'Mật khẩu xác nhận sai';
$lang['message_success_change_password'] = 'Mật khẩu đã đổi. Vui lòng đăng nhập lại';
$lang['message_error_old_password'] = 'Mật khẩu cũ không đúng';
$lang['message_success_logout'] = 'Thoát thành công';
$lang['message_deactive_user'] = 'Tài khoản của bạn đang bị khóa. Vui lòng liên hệ người quản trị để kích hoạt trở lại.';
$lang['message_forgot_password'] = 'Mật khẩu của bạn đã được gửi lại. Vui lòng kiểm tra email để lấy lại.';
$lang['message_required_username'] = 'Quên mật khẩu';
$lang['message_required_password'] = 'Quên mật khẩu';
$lang['message_empty_username'] = 'Bạn chưa nhập tên tài khoản';
$lang['message_empty_password'] = 'Bạn chưa nhập mật khẩu';
$lang['message_deny_access'] = 'Bạn không có quyền truy cập vào trang này';
$lang['message_already_exists_user'] = 'Tên đăng nhập đã tồn tại';
$lang['message_not_exists_user'] = "Người dùng không tồn taị";

//user
$lang['name'] = "Họ tên/Tổ chức";
$lang['admin_name'] = "Họ tên";
$lang['username'] = "Tên đăng nhập";
$lang['password'] = "Mật khẩu";
$lang['address'] = "Địa chỉ";
$lang['status'] = "Trạng thái kích hoạt";
$lang['email'] = 'Email';
$lang['send_email_success'] = 'Gửi email thành công';
$lang['send_email_failed'] = 'Có lỗi khi gửi email';
$lang['please_check_email'] = 'vui lòng kiểm tra email';
$lang['mail'] = 'thư';
$lang['mailbox'] = 'Hộp thư';
$lang['date_of_birth'] = 'Ngày sinh';
$lang['active'] = 'Kích hoạt';
$lang['deactive'] = 'Chưa kích hoạt';
$lang['date_format'] = 'd/m/Y';
$lang['date_format2'] = 'd/m/Y H:i:s';
$lang['male'] = 'Nam';
$lang['female'] = 'Nữ';
$lang['user_info'] = 'Thông tin người dùng';
$lang['info'] = "Thông tin";
$lang['fullname'] = "Họ tên";
$lang['expert'] = "Chuyên gia";
$lang['expertinfo'] = "xem thông tin chuyên gia";
$lang['expert-list'] = 'danh sách chuyên gia';
$lang['not_expert'] = 'người dùng này không phải là chuyên gia';
$lang['expert_not_exists'] = 'Chuyên gia này không tồn tại';
$lang['farmer'] = "Nông dân";
$lang['user_not_expert'] = 'Người dùng này không phải chuyên gia';
$lang['farmer_missing'] = 'Thiếu thông tin nông dân';
$lang['expert_missing'] = 'Thiếu thông tin chuyên gia';
$lang['username_missing'] = 'Thiếu tên đăng nhập';
$lang['user_not_have_profile'] = 'Người dùng này chưa có thông tin cá nhân';
$lang['user_not_verify'] = 'Tài khoản của bạn chưa được xác thực';
$lang['user_not_exists'] = 'Tài khoản không tồn tại';
$lang['register_success'] = 'Đăng ký thành công';
$lang['register_failed'] = 'Đăng ký thất bại';
$lang['expert_information'] = 'Thông tin chuyên gia';
$lang['expert_rating'] = 'Đánh giá chuyên gia';
$lang['not_update'] = 'Chưa cập nhật';

//sensor
$lang['sensor'] = 'Cảm biến';
$lang['sensor_type'] = 'Loại cảm biến';
$lang['sensor_name'] = 'Tên cảm biến';
$lang['chart_color'] = 'Màu đồ thị cảm biến';
$lang['sensor_unit'] = 'Đơn vị đo';
$lang['message_not_exists_sensor'] = 'Không tìm thấy cảm biến';
$lang['message_this_sensor_not_belong_to_devicetype'] = 'cảm biến này không thuộc về loại thiết bị này';

//device
$lang['device_id_wasp'] = 'Mã điểm thu thập dữ liệu';
$lang['device_name'] = 'Điểm thu thập dữ liệu';
$lang['show_dashboard'] = 'Hiển thị dashboard';

//collector
$lang['mainnav_collector'] = "Bộ thu thập";
$lang['collector'] = 'bộ thu thập';
$lang['id_collector'] = 'Mã bộ thu thập';
$lang['collector_name'] = 'Tên bộ thu thập';
$lang['collector_info'] = "Thông tin bộ thu thập dữ liệu";
$lang['collector_area'] = 'Khu vực';
$lang['collector_date_active'] = 'Ngày kích hoạt';
$lang['collector_date_end_warranty'] = 'Hạn bảo hành';
$lang['collector_serial'] = "Số seri bộ thu thập";
$lang['collector_status'] = "Trạng thái";

$lang['try_it_now'] = "Dùng thử ngay";

//login fb,gg
$lang['wrong_app_id'] = 'Sai app id';
$lang['check_user_error'] = 'Kiểm tra thông tin người dùng thất bại';
$lang['check_token_error'] = 'Kiểm tra token của ứng dụng thất bại';
$lang['missing_token_or_password'] = 'Thiếu token hoặc mật khẩu';
$lang['access_token_is_invalid'] = 'access_token không khả dụng';

//update api
$lang['password_not_update'] = 'Không thể cập nhật mật khẩu trên api này';
$lang['not_update_with_null'] = 'Không thể cập nhật vì các giá trị bị bỏ trống';
$lang['update_avatar_error'] = 'Cập nhật ảnh đại diện bị lỗi';
$lang['level_id_not_exists'] = 'Level id không tồn tại';
$lang['input_null'] = 'Tham số truyền vào trống';

//....
$lang['check_input_id'] = 'Kiểm tra id nhập';
$lang['field_id_missing'] = 'id trường bị thiếu';
$lang['is_not_exists'] = 'không tồn tại';
$lang['in_table'] = 'trong bảng';
$lang['update_farm_cover_error'] = 'Cập nhật trang bìa nông trại đã lỗi';
$lang['save_farm_error'] = 'Lưu trang trại bị lỗi';
$lang['missing_field_input'] = 'Thiếu thông tin đầu vào';
$lang['check_input_params'] = 'Kiểm tra các tham số đầu vào';
$lang['not_found_data'] = 'Không tìm thấy dữ liệu';
$lang['params_id_null'] = 'Tham số id không có giá trị';
$lang['refresh_key_is_invalid'] = 'refresh_key không hợp lệ';
$lang['product_is_sale'] = 'Sản phẩm này đã được bán';
$lang['bookmark_failed'] = 'Bookmark thất bại';