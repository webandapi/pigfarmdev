<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crops extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý cây trồng, vật nuôi';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if (!$adminfull) {
            $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'));
            $i = 0;
            if ($farms) {
                foreach ($farms as $farm) {
                    $farmcrops = $this->farm_crops_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($farmcrops) {
                        foreach ($farmcrops as $fc) {
                            $i++;
                        }
                    }
                }
            }
            if ($i >= 1)
                $checkfull = true;
        }
        #end check
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name'));
        if ($farms):
            $data = array();
            foreach ($farms as $farm) :
                if ($_GET['farm'] == $farm->id || $_GET['farm'] == '') {
                    $farmcrops = $this->farm_crops_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($farmcrops):
                        foreach ($farmcrops as $fc) {
                            $crops = $this->crops_model->get_info_rule(array('id' => $fc->crops_id));
                            $crops->field = $this->field_model->get_info_rule(array('id' => $crops->field_id));
                            $crops->farm = $this->farm_model->get_info_rule(array('id' => $fc->farm_id), 'id,name');
                            $data[] = $crops;
                        }
                    endif;
                }
            endforeach;
        endif;
        $this->load->view('crops/index', array('crops' => $data, 'farms' => $farms, 'checkfull' => $checkfull));
        $this->load->view('footer');
    }

    /**
     * Insert data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân}
     */
    public function add() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if (!$adminfull) {
            $i = 0;
            if ($farms) {
                foreach ($farms as $farm) {
                    $farmcrops = $this->farm_crops_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($farmcrops) {
                        foreach ($farmcrops as $fc) {
                            $i++;
                        }
                    }
                }
            }
            if ($i >= 1)
                $checkfull = true;
        }
        #end check

        $fields = $this->field_model->get_list(); #field
        $cropsdatalist = $this->crops_model->get_list(); #cropsdatalist
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('crops/add', array('farms' => $farms, 'fields' => $fields, 'cropsdatalist' => $cropsdatalist, 'checkfull' => $checkfull));
        } else { #submit form
            #check admin full feature
            if ($checkfull) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            }

            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'farm',
                    'label' => 'trang trại',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
                array(
                    'field' => 'field',
                    'label' => 'loại cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('crops/add', array('farms' => $farms, 'fields' => $fields));
            } else {
                try {
                    $thisuser = $this->get_current_user();
                    # check this current user is "farmer"
                    if ($thisuser['usertype_id'] == '1') {
                        #check crops exists by name
                        $name = $this->db->escape_str(trim($data['name']));
                        $check_exists = $this->crops_model->get_info_rule(array('name' => $name));
                        # if crops name is exists
                        if ($check_exists && $check_exists->field_id == $data['field']) {
                            if ($check_exists->id) {
                                if ($this->farm_crops_model->create(array('farm_id' => $data['farm'], 'crops_id' => $check_exists->id)))
                                    $this->session->set_flashdata('success', 'Thêm cây trồng, vật nuôi mới thành công');
                                else
                                    $this->session->set_flashdata('failed', 'Không thể thêm cây trồng, vật nuôi mới!');
                            }
                        } else { #else not
                            $entry = array(
                                'name' => $name,
                                'field_id' => $data['field'],
                                'note' => $this->db->escape_str(trim($data['note'])),
                            );
                            $crops_id = $this->crops_model->create($entry);
                            if ($crops_id) {
                                # update farm crops
                                $this->farm_crops_model->create(array('farm_id' => $data['farm'], 'crops_id' => $crops_id));

                                $this->session->set_flashdata('success', 'Thêm cây trồng, vật nuôi mới thành công');
                            } else
                                $this->session->set_flashdata('failed', 'Không thể thêm cây trồng, vật nuôi mới!');
                        }
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải là nông dân nên không được phép thêm cây trồng, vật nuôi!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân 
     * && cây trồng phải thuộc trang trại nông dân đó quản lý}
     */
    /* public function edit() {
      $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
      $fields = $this->field_model->get_list(); #fields
      $thisuser = $this->get_current_user(); # current user
      #show form
      if (!$this->input->post('submit')) {
      $segs = $this->uri->segment_array();
      $id = end($segs);
      if (isset($id)) {
      $ok = false;
      if ($data = $this->crops_model->get_info_rule(array('id' => $id))) {
      if ($this->farm_model->check_exists(array('id' => $data->farm_id, 'user_id' => $thisuser['user_id'])))
      $ok = true;
      }
      if ($ok === true && $thisuser['usertype_id'] == '1')
      $this->load->view('crops/edit', array('farms' => $farms, 'fields' => $fields, 'data' => $data));
      else
      $this->load->view('crops/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
      }
      } else {
      $data = $this->input->post();
      $config = array(
      array(
      'field' => 'name',
      'label' => 'Tên cây trồng, vật nuôi',
      'rules' => 'trim|required',
      'errors' => array(
      'required' => '%s không được để trống.',
      )
      ),
      array(
      'field' => 'farm',
      'label' => 'trang trại',
      'rules' => 'required',
      'errors' => array(
      'required' => 'Chọn %s.',
      )
      ),
      array(
      'field' => 'field',
      'label' => 'loại cây trồng, vật nuôi',
      'rules' => 'trim|required',
      'errors' => array(
      'required' => 'Chọn %s.',
      )
      ),
      );

      #form validate
      $this->form_validation->set_rules($config);
      if ($this->form_validation->run() == FALSE) {
      redirect($_SERVER['HTTP_REFERER']);
      return FALSE;
      } else {
      try {
      $ok = false;
      if ($current = $this->crops_model->get_info_rule(array('id' => $data['id']))) {
      if ($this->farm_model->check_exists(array('id' => $current->farm_id, 'user_id' => $thisuser['user_id'])))
      $ok = true;
      }

      #check permission
      if ($ok == true && $thisuser['usertype_id'] == '1') {
      $entry = array(
      'name' => $this->db->escape_str(trim($data['name'])),
      'farm_id' => $data['farm'],
      'field_id' => $data['field'],
      'note' => $this->db->escape_str(trim($data['note'])),
      );
      if ($this->crops_model->update($data['id'], $entry))
      $this->session->set_flashdata('success', 'Cập nhật cây trồng, vật nuôi thành công');
      else
      $this->session->set_flashdata('failed', 'Không thể cập nhật cây trồng, vật nuôi!');
      } else
      $this->session->set_flashdata('failed', 'Bạn không có quyền thao tác thông tin trang trại người khác!');

      redirect($_SERVER['HTTP_REFERER']);
      return FALSE;
      } catch (Exception $ex) {
      echo 'Caught exception: ', $e->getMessage(), "\n";
      return false;
      }
      }
      }
      $this->load->view('footer');
      } */

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        $farm_id = $_GET['f'];
        if ($id && $farm_id) {
            try {
                $thisuser = $this->get_current_user(); # current user
                $ok = false;
                if ($current = $this->crops_model->get_info_rule(array('id' => $id))) {
                    if ($this->farm_model->check_exists(array('id' => $farm_id, 'user_id' => $thisuser['user_id'])))
                        $ok = true;
                }

                #check permission
                if ($ok == true && $thisuser['usertype_id'] == '1') {
                    if ($this->farm_crops_model->delete(array('crops_id' => $id, 'farm_id' => $farm_id)))
                        $this->session->set_flashdata('success', 'Xóa cây trồng, vật nuôi thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa cây trồng, vật nuôi!');
                } else
                    $this->session->set_flashdata('failed', 'Bạn không có quyền xóa cây trồng của trang trại khác!');

                redirect($this->config->config['base_url'] . "/crops");
                return FALSE;
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
