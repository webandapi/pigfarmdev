<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Farm extends FT_Controller {

    /**
     * __construct
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý trang trại';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List farm / Farm info
     */
    public function index() {
        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if (!$adminfull) {
            $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'));
            if (count($farms) >= 1) {
                $checkfull = true;
            }
        }
        #end check admin full feature


        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'));
        if ($farms):
            foreach ($farms as $farm) {
                $qrcode = $this->qrcode_model->get_farm_qrcode($farm->code);
                $thisfarmtype = $this->farmtype_model->get_info_rule(array('id' => $farm->farmtype_id), 'name');
                $farm->farmtype = $thisfarmtype->name;
                $farm->products = $this->farm_model->get_farm_products($farm->id);
                $farm->certificate = $this->farm_model->get_farm_certificate($farm->id);
            }
        endif;
        $this->load->view('farm/index', array('farms' => $farms, 'checkfull' => $checkfull));
        $this->load->view('footer');
    }

    /**
     * Insert data
     */
    public function add() {

        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if (!$adminfull) {
            $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'));
            if (count($farms) >= 1) {
                $checkfull = true;
            }
        }

        $entry = array();
        $entry['products'] = $this->product_model->get_list(); // products lst
        $entry['provinces'] = $this->province_model->get_list(); #provinces
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('farm/add', array('entry' => $entry, 'checkfull' => $checkfull));
        } else { // Submit form
            #check admin full feature
            if ($checkfull) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            }

            #Else curent user has admin full feature
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'farm_name',
                    'label' => 'Tên trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'farm_size',
                    'label' => 'Diện tích',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'province',
                    'label' => 'tỉnh/TP',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
                array(
                    'field' => 'province',
                    'label' => 'Tỉnh/TP',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
                array(
                    'field' => 'farm_location',
                    'label' => 'Vị trí',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );
            # from validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('farm/add', array('entry' => $entry));
            } else {
                try {

                    $thisuser = $this->get_current_user();
                    # check this current user is "farmer"
                    if ($thisuser['usertype_id'] == '1') {
                        // insert data
                        $entrydb = array(
                            'name' => $this->db->escape_str(trim($data['farm_name'])),
                            'user_id' => $thisuser['user_id'],
                            'province_id' => $data['province'],
                            'size' => $this->db->escape_str(trim($data['farm_size'])),
                            'location' => $this->db->escape_str(trim($data['farm_location'])),
                            'note' => $this->db->escape_str(trim($data['farm_note'])),
                            'latitude' => $this->db->escape_str(trim($data['latitude'])),
                            'longitude' => $this->db->escape_str(trim($data['longitude'])),
                            'water_indicator' => $this->db->escape_str(trim($data['water_indicator'])),
                            'index_of_photosynthesis' => $this->db->escape_str(trim($data['index_of_photosynthesis'])),
                            'show_harvest' => $data['show_harvest']
                        );
                        $farm_id = $this->farm_model->create($entrydb);
                        if ($farm_id) {

                            # total farms by province
                            $currentfarm = $this->farm_model->get_info_rule(array('id' => $farm_id), 'province_id'); #current farm
                            $countfarmsbyprovince = count($this->farm_model->get_list(array('where' => array('province_id' => $currentfarm->province_id)), array('id'))); #count farms by province
                            # get zipcode of this farm province
                            $thisfarmprovince = $this->province_model->get_info_rule(array('id' => $currentfarm->province_id), 'zipcode');
                            $pzipcode = substr($thisfarmprovince->zipcode, 0, 2);
                            #farm order
                            $order = $countfarmsbyprovince ? $countfarmsbyprovince : 1;
                            #create farm code
                            $zerostr = '';
                            for ($i = 1; $i <= 3; $i++) {
                                if ($i <= 3 - strlen($order))
                                    $zerostr .= '0';
                                $fzipcode = $pzipcode . $zerostr . $order;
                            }

                            #update farm data (code && order)
                            $this->farm_model->update($farm_id, array('code' => $fzipcode, 'order' => $order));

                            // create farm - products
                            if ($data['products'])
                                $this->farmproduct_model->create($farm_id, $data['products']);

                            $this->session->set_flashdata('success', 'Thêm trang trại mới thành công');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể thêm trang trại mới');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải là nông dân nên không được phép thêm trang trại!');


                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân 
     * && trang trại hiện tại thuộc quyền sở hữu của của nông dân hiện tại}
     */
    public function edit() {
        $entry = array();
        $entry['products'] = $this->product_model->get_list(); // products lst
        $entry['provinces'] = $this->province_model->get_list(); #provinces
        $user_data = $this->get_current_user(); # current user
        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            if (isset($id)) {
                $data = array();
                $ok = false;
                if ($data = $this->farm_model->get_info_rule(array('id' => $id))) {
                    if ($data->user_id == $user_data['user_id']) {
                        $ok = true;
                    }
                }
                # check permission 
                if ($ok === true && $user_data['usertype_id'] == '1') {
                    $thisproducts = $this->farm_model->get_farm_products($id);
                    if ($thisproducts) {
                        foreach ($thisproducts as $thispro) {
                            $data->products[] = $thispro->id;
                        }
                    }
                    $this->load->view('farm/edit', array('data' => $data, 'entry' => $entry));
                } else
                    $this->load->view('farm/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
            }
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'farm_name',
                    'label' => 'Tên trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'farm_size',
                    'label' => 'Diện tích',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                )
            );

            # form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {
                    # check this current user is farm boss
                    $ok = false;
                    if ($current = $this->farm_model->get_info_rule(array('id' => $data['id']))) {
                        if ($current->user_id == $user_data['user_id']) {
                            $ok = true;
                        }
                    }

                    #check permission
                    if ($ok === true && $user_data['usertype_id'] == '1') {
                        // insert data
                        $entrydb = array(
                            'name' => $this->db->escape_str(trim($data['farm_name'])),
                            'size' => $this->db->escape_str(trim($data['farm_size'])),
                            'note' => $this->db->escape_str(trim($data['farm_note'])),
                            'water_indicator' => $this->db->escape_str(trim($data['water_indicator'])),
                            'index_of_photosynthesis' => $this->db->escape_str(trim($data['index_of_photosynthesis'])),
                            'show_harvest' => $data['show_harvest']
                        );

                        if ($this->farm_model->update($data['id'], $entrydb)) {
                            // update farm - product
                            $this->farmproduct_model->update($data['id'], $data['products']);
                            $this->session->set_flashdata('success', 'Cập nhật trang trại thành công !');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật trang trại !');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không có quyền thao tác thông tin trang trại người khác!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return false;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete farm
     * @return boolean
     */
    function delete() {
        
    }

}
