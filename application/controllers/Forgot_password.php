<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forgot_password extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();
        $pageTitle = 'Quên mật khẩu';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->view('forgot_password/header');
    }

    /**
     * Forgot password
     */
    public function index() {
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('forgot_password/index');
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|callback',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('crops/add', array('farms' => $farms, 'fields' => $fields));
            } else {
                try {
                    $thisuser = $this->get_current_user();
                    # check this current user is "farmer"
                    if ($thisuser['usertype_id'] == '1') {
                        #check crops exists by name
                        $name = $this->db->escape_str(trim($data['name']));
                        $check_exists = $this->crops_model->get_info_rule(array('name' => $name));
                        # if crops name is exists
                        if ($check_exists && $check_exists->field_id == $data['field']) {
                            if ($check_exists->id) {
                                if ($this->farm_crops_model->create(array('farm_id' => $data['farm'], 'crops_id' => $check_exists->id)))
                                    $this->session->set_flashdata('success', 'Thêm cây trồng, vật nuôi mới thành công');
                                else
                                    $this->session->set_flashdata('failed', 'Không thể thêm cây trồng, vật nuôi mới!');
                            }
                        } else { #else not
                            $entry = array(
                                'name' => $name,
                                'field_id' => $data['field'],
                                'note' => $this->db->escape_str(trim($data['note'])),
                            );
                            $crops_id = $this->crops_model->create($entry);
                            if ($crops_id) {
                                # update farm crops
                                $this->farm_crops_model->create(array('farm_id' => $data['farm'], 'crops_id' => $crops_id));

                                $this->session->set_flashdata('success', 'Thêm cây trồng, vật nuôi mới thành công');
                            } else
                                $this->session->set_flashdata('failed', 'Không thể thêm cây trồng, vật nuôi mới!');
                        }
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải là nông dân nên không được phép thêm cây trồng, vật nuôi!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('forgot_password/footer');
    }

}
