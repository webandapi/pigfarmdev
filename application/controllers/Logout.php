<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata('user')) {
            redirect($this->config->config['base_url']);
            exit();
        }
    }

    /**
     * Logout
     */
    public function index() {
        if ($this->session->has_userdata('user'))
            $this->session->unset_userdata('user');
        redirect($this->config->config['base_url']);
    }

}
