<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userfarm extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý nhân viên trang trại';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id'));
        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if (!$adminfull) {
            $i = 0;
            if ($farms) {
                foreach ($farms as $farm) {
                    $usersfarm = $this->userfarm_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($usersfarm) {
                        foreach ($usersfarm as $uf) {
                            $i++;
                        }
                    }
                }
            }
        }
        if ($i >= 1)
            $checkfull = true;

        #end check admin full feature


        $userfarmlst = array();
        if ($farms):
            foreach ($farms as $farm) {
                $userfarm = $this->userfarm_model->get_list(array('where' => array('farm_id' => $farm->id)));
                if ($userfarm) {
                    foreach ($userfarm as $val) {
                        unset($val->farm_id);
                        $userfarmlst[] = $val;
                    }
                }
            }
        endif;
        $this->load->view('userfarm/index', array('users' => $userfarmlst, 'checkfull' => $checkfull));
        $this->load->view('footer');
    }

    /**
     * Insert data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân}
     */
    public function add() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'));
        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if (!$adminfull) {
            $i = 0;
            if ($farms) {
                foreach ($farms as $farm) {
                    $usersfarm = $this->userfarm_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($usersfarm) {
                        foreach ($usersfarm as $uf) {
                            $i++;
                        }
                    }
                }
            }
        }
        if ($i >= 1)
            $checkfull = true;

        #end check admin full feature

        $modules = $this->module_model->get_list();
        if (!$this->input->post('submit')) {
            $this->load->view('userfarm/add', array('farms' => $farms, 'modules' => $modules, 'checkfull' => $checkfull));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required|min_length[3]|is_unique[users_farm.username]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).',
                        'is_unique' => '%s này đã được sử dụng.'
                    )
                ),
                array(
                    'field' => 'password',
                    'label' => 'Mật khẩu',
                    'rules' => 'trim|required|min_length[6]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s phải chứa ít nhất %s kí tự.',
                    )
                ),
                array(
                    'field' => 'cf_password',
                    'label' => 'Nhập lại mật khẩu',
                    'rules' => 'trim|required|matches[password]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'matches' => '%s chưa khớp',
                    )
                ),
                array(
                    'field' => 'fullname',
                    'label' => 'Họ tên',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|is_unique[users_farm.email]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
                array(
                    'field' => 'farm',
                    'label' => 'trang trại',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('userfarm/add', array('farms' => $farms, 'modules' => $modules));
            } else {
                try {
                    $thisuser = $this->get_current_user();
                    # check this current user is "farmer"
                    if ($thisuser['usertype_id'] == '1') {
                        $birthday = explode('/', $data['birthday']);
                        $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];

                        $entry = array(
                            'username' => $this->db->escape_str(trim($data['username'])),
                            'fullname' => $this->db->escape_str(trim($data['fullname'])),
                            'email' => $this->db->escape_str(trim($data['email'])),
                            'phone' => $this->db->escape_str(trim($data['phone'])),
                            'farm_id' => $this->db->escape_str(trim($data['farm'])),
                            'status' => $this->db->escape_str(trim($data['status'])),
                            'gender' => $this->db->escape_str(trim($data['gender'])),
                            'date_of_birth' => $birthday,
                            'pw' => $this->user_model->create_hash($data['password']),
                            'modules_in' => json_encode($data['modules'])
                        );
                        if ($this->userfarm_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm nhân viên mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm nhân viên mới!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải là nông dân nên không được phép thêm nhân viên trang trại !');

                    redirect($this->config->config['base_url'] . '/userfarm/add');
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân
     *  && Nhân viên cần cập nhật là nhân viên của trang trại mà nông dân hiện tại đang quản lý}
     */
    public function edit() {
        $thisuser = $this->get_current_user(); #current user
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user')); #farms by user
        $modules = $this->module_model->get_list();  #modules
        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $ok = false;
                if ($data = $this->userfarm_model->get_info_rule(array('id' => $id))) {
                    if ($farms) {
                        foreach ($farms as $farm) {
                            if ($data->farm_id == $farm->id) {
                                $ok = true;
                                break;
                            }
                        }
                    }
                }
                if ($ok == true && $thisuser['usertype_id'] == '1')
                    $this->load->view('userfarm/edit', array('farms' => $farms, 'modules' => $modules, 'data' => $data));
                else
                    $this->load->view('userfarm/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
            }
        } else { #submit
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).',
                    )
                ),
                array(
                    'field' => 'fullname',
                    'label' => 'Họ tên',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                    )
                ),
                array(
                    'field' => 'farm',
                    'label' => 'trang trại',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {

                    # check permission
                    $ok = false;
                    if ($current = $this->userfarm_model->get_info_rule(array('id' => $data['id']))) {
                        if ($farms) {
                            foreach ($farms as $farm) {
                                if ($current->farm_id == $farm->id) {
                                    $ok = true;
                                    break;
                                }
                            }
                        }
                    }

                    # processing update data
                    if ($ok === true && $thisuser['usertype_id'] == '1') {
                        $reponse = '';
                        $birthday = explode('/', $data['birthday']);
                        $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];
                        $entry = array(
                            'fullname' => $this->db->escape_str(trim($data['fullname'])),
                            'email' => $this->db->escape_str(trim($data['email'])),
                            'phone' => $this->db->escape_str(trim($data['phone'])),
                            'farm_id' => $this->db->escape_str(trim($data['farm'])),
                            'status' => $this->db->escape_str(trim($data['status'])),
                            'gender' => $this->db->escape_str(trim($data['gender'])),
                            'date_of_birth' => $birthday,
                            'modules_in' => json_encode($data['modules'])
                        );
                        if ($this->userfarm_model->update($data['id'], $entry))
                            $this->session->set_flashdata('success', 'Cập nhật nhân viên thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật nhân viên!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không có quyền thao tác với thông tin vừa yêu cầu !');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Delete data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân 
     * && nhân viên được xóa là nhân viên của trang trại mà nông dân hiện tại đang quản lý}
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user')); #farms by user
                $thisuser = $this->get_current_user();
                $ok = false;
                if ($current = $this->userfarm_model->get_info_rule(array('id' => $id))) {
                    if ($farms) {
                        foreach ($farms as $farm) {
                            if ($current->farm_id == $farm->id) {
                                $ok = true;
                                break;
                            }
                        }
                    }
                }

                # check permission
                if ($ok === true && $thisuser['usertype_id'] == '1') {
                    if ($this->userfarm_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa nhân viên thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa nhân viên!');
                } else
                    $this->session->set_flashdata('failed', 'Bạn không có quyền thao tác với thông tin vừa yêu cầu !');

                redirect($this->config->config['base_url'] . '/userfarm');
                return FALSE;
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
