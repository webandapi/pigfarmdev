<?php

$DROP_PRIMARY_KEY = 'ALTER TABLE admin MODIFY `username` VARCHAR(128) NOT NULL;
ALTER TABLE admin DROP PRIMARY KEY;
';

$ALTER_ADMIN = 'ALTER TABLE admin
ADD COLUMN `id` INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY NOT NULL FIRST,
ADD COLUMN `adminrole_id` INT(11) NOT NULL AFTER `permission`,
ADD COLUMN `fullname` VARCHAR(30) NOT NULL AFTER `username`,
ADD COLUMN `email` VARCHAR(30) AFTER `fullname`,
ADD COLUMN `phone` VARCHAR(30) AFTER `email`,
ADD COLUMN `address` VARCHAR(30) AFTER `phone`,
ADD COLUMN `status` tinyint(1) NOT NULL DEFAULT 1 AFTER `address`,
ADD COLUMN `date_of_birth` date AFTER `status`,
ADD COLUMN `gender` TINYINT(1) AFTER `date_of_birth`,
ADD COLUMN `created` TIMESTAMP NOT NULL;';


$CREATE_ADMINROLE = "CREATE TABLE adminrole (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
note TEXT
)";

$CREATE_MODULES = "CREATE TABLE modules (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
note TEXT
)";

$CREATE_USERFARM = "CREATE TABLE users_farm (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
farm_id INT(11) NOT NULL,
username VARCHAR(255) NOT NULL,
pw varchar(128) NOT NULL,
fullname VARCHAR(30) NOT NULL,
email VARCHAR(30),
phone VARCHAR(30),
modules_in text,
status tinyint(1) NOT NULL DEFAULT 1,
date_of_birth date,
gender TINYINT(1),
created TIMESTAMP NOT NULL,	
FOREIGN KEY (farm_id) REFERENCES farm(id)
)";


// cây trồng
$CREATE_Crops = "CREATE TABLE crops (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
seed_cost VARCHAR(255),
pesticide_costs VARCHAR(255),
fertilizer_cost VARCHAR(255),
harvesting_costs VARCHAR(255),
expected_productivity VARCHAR(255),
farm_id INT(11) NOT NULL,
field_id INT(11) NOT NULL,
FOREIGN KEY (field_id) REFERENCES field(id),
FOREIGN KEY (farm_id) REFERENCES farm(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


// hạt giống
$CREATE_SEEDS = "CREATE TABLE seeds (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
crops_id INT(11) NOT NULL,
farm_id INT(11) NOT NULL,
origin VARCHAR(255),
date_of_purchase DATE,
expiration_date DATE,
amount VARCHAR(255),	
FOREIGN KEY (farm_id) REFERENCES farm(id),
FOREIGN KEY (crops_id) REFERENCES crops(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

$ALTER_seeds = 'ALTER TABLE seeds
ADD COLUMN `name` VARCHAR(255) AFTER `id`';

// vụ trồng
$CREATE_planting = "CREATE TABLE planting (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
sowing_time DATE NOT NULL,
expected_harvest_time DATE NOT NULL,
status tinyint(1) NOT NULL DEFAULT 0,
farm_id INT(11) NOT NULL,
FOREIGN KEY (farm_id) REFERENCES farm(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

// vụ trồng - hạt giống
$CREATE_planting_seeds = "CREATE TABLE planting_seeds (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
planting_id INT(11) NOT NULL,
seeds_id INT(11) NOT NULL,
FOREIGN KEY (planting_id) REFERENCES planting(id),
FOREIGN KEY (seeds_id) REFERENCES seeds(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

// lần thu hoạch
$CREATE_harvest = "CREATE TABLE harvest (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
packaging_date DATE NOT NULL,
package_user_id INT(11),
planting_id INT(11) NOT NULL,
FOREIGN KEY (planting_id) REFERENCES planting(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


// lần thu hoạch - nhà phân phối
$CREATE_harvest = "CREATE TABLE harvest_distributors (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
harvest_id INT(11) NOT NULL,
distributor_id INT(11) NOT NULL,
FOREIGN KEY (harvest_id) REFERENCES harvest(id),
FOREIGN KEY (distributor_id) REFERENCES distributors(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

// nhật ký xử lý
$CREATE_diaries = "CREATE TABLE diaries (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255) NOT NULL,
content TEXT NOT NULL,
date DATETIME NOT NULL,
planting_id INT(11) NOT NULL,
FOREIGN KEY (planting_id) REFERENCES planting(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


// sự cố
$CREATE_incidents = "CREATE TABLE incidents (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255) NOT NULL,
content TEXT,
damages TEXT,
created timestamp NOT NULL,
planting_id INT(11) NOT NULL,
FOREIGN KEY (planting_id) REFERENCES planting(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


// qrcode
$CREATE_qrcode = "CREATE TABLE qrcode (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
farm_code varchar(4) NOT NULL,
planting_id INT(11) NOT NULL,
harvest_id INT(11) NOT NULL,
qr_from INT(11),
qr_to INT(11),
FOREIGN KEY (planting_id) REFERENCES planting(id),
FOREIGN KEY (harvest_id) REFERENCES harvest(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

// change farm
$ALTER_ADMIN = 'ALTER TABLE farm
ADD COLUMN `code` VARCHAR(30) AFTER `name`,
ADD COLUMN `farmtype_id` INT(11) AFTER `code`,
ADD COLUMN `show_harvest` tinyint(1) NOT NULL DEFAULT 1,
ADD COLUMN `water_indicator` varchar(255) AFTER `farmtype_id`,
ADD COLUMN `index_of_photosynthesis` VARCHAR(255) AFTER `water_indicator`,
ADD COLUMN `certificate_picture` VARCHAR(255) AFTER `cover`';


// nhà phân phối
$CREATE_distributor = "CREATE TABLE distributors (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
logo VARCHAR(255),
address VARCHAR(255) NOT NULL,
phone VARCHAR(30) NOT NULL,
email VARCHAR(255) NOT NULL,
code VARCHAR(30) NOT NULL
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


// chứng nhận
$CREATE_distributor = "CREATE TABLE certificate (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
note TEXT,
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

// trang trại - chứng nhận
$CREATE_distributor = "CREATE TABLE farm_certificate (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
farm_id INT(11) NOT NULL,
certificate_id INT(11) NOT NULL,
FOREIGN KEY (farm_id) REFERENCES farm(id),
FOREIGN KEY (certificate_id) REFERENCES certificate(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


// tỉnh/thành phố
// chứng nhận
$CREATE_distributor = "CREATE TABLE provinces (
id INT(11) AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(255) NOT NULL,
zipcode INT(10) NOT NULL
)CHARACTER SET utf8 COLLATE utf8_general_ci;";


//QA
$CREATE_qa = "CREATE TABLE qa (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
title VARCHAR(255) NOT NULL,
description TEXT NOT NULL
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

// farm crops
$CREATE_qa = "CREATE TABLE farm_crops (
id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
farm_id INT(11) NOT NULL,
crops_id INT(11) NOT NULL,
FOREIGN KEY (farm_id) REFERENCES farm(id),
FOREIGN KEY (crops_id) REFERENCES crops(id)
)CHARACTER SET utf8 COLLATE utf8_general_ci;";

#remove foreign key of crops table
'ALTER TABLE crops DROP FOREIGN KEY crops_ibfk_2';
