<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();
        $this->check_user_log();

        $pageTitle = 'Thông tin tham khảo';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * User Manuals
     */
    public function user_manuals() {
        $this->load->view('info/manuals');
        $this->load->view('footer');
    }

    /**
     * QA
     */
    public function qa() {
        $qa = $this->qa_model->get_list();
        $this->load->view('info/qa', array('qa' => $qa));
        $this->load->view('footer');
    }

    /**
     * Important
     */
    public function important() {
        $this->load->view('info/important');
        $this->load->view('footer');
    }

}
