<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supportdevice extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý thiết bị';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        # check admin full version
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name', 'latitude', 'longitude'));
        $farm_ids = $farminfo = $location = array();
        if ($farms):
            foreach ($farms as $farm) :
                $total = 0;
                $farmplanting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), array('name', 'seeds_id'));
                if ($farmplanting) {
                    foreach ($farmplanting as $fp) {
                        $seeds = $this->seeds_model->get_info_rule(array('id' => $fp->seeds_id), 'name,total_cost');
                        $fp->seeds = $seeds;
                        $farm->planting[] = $fp;
                        $total += $seeds->total_cost;
                        $farm->total = $total;
                    }
                }
                $farm_ids[] = $farm->id;
            endforeach;
        endif;
        $devices = $this->supportdevice_model->get_device_by_farm($farm_ids);
        if (!$adminfull) {
            if (count($devices) >= 1) {
                $checkfull = true;
            }
        }

        #end check admin full feature
        $this->load->view('supportdevice/index', array('devices' => $devices));
        $this->load->view('footer');
    }

    
    /**
     * Insert data
     */
    public function add() {
        # check admin full version
        $adminfull = $this->check_admin_full();
        $entry = array();
        $entry['supporter'] = $this->supporter_model->get_list();
        $entry['farms'] = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name', 'latitude', 'longitude'));
        if (!$this->input->post('submit')) {
            $this->load->view('supportdevice/add', array('entry' => $entry));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'deviceid',
                    'label' => 'Tên thiết bị',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).'
                    )
                ),
                array(
                    'field' => 'ipserver',
                    'label' => 'Địa chỉ máy chủ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idsupport',
                    'label' => 'Người hỗ trợ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idfarm',
                    'label' => 'Trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                )

            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('supportdevice/add', array('entry' => $entry));
            } else {
                try {
                    $dateactive = explode('/', $data['dateactive']);
                    $dateactive = $dateactive[2] . '-' . $dateactive[1] . '-' . $dateactive[0];

                    $warantytime = explode('/', $data['warantytime']);
                    $warantytime = $warantytime[2] . '-' . $warantytime[1] . '-' . $warantytime[0];
                    $entry = array(
                        'deviceid' => $this->db->escape_str(trim($data['deviceid'])),
                        'ipserver' => $this->db->escape_str(trim($data['ipserver'])),
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'password' => $this->db->escape_str(md5($data['password'])),
                        'usersql' => $this->db->escape_str(trim($data['usersql'])),
                        'passsql' => $this->db->escape_str(md5($data['passsql'])),
                        'isdefault' => $this->db->escape_str($data['isdefault']),
                        'idsupport' => $this->db->escape_str(trim($data['idsupport'])),
                        'idfarm' => $this->db->escape_str(trim($data['idfarm'])),
                        'dateactive' => $dateactive,
                        'warantytime' => $warantytime,
                        'created' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                        'updated' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                    );

                    if ($this->supportdevice_model->create($entry)) {
                        $this->session->set_flashdata('success', 'Thêm thiết bị mới thành công');
                    } else {
                        $this->session->set_flashdata('failed', 'Không thể thêm thiết bị mới!');
                    }

                    redirect($this->config->config['base_url'] . '/supportdevice/add');
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     */
    public function edit() {
        # check admin full version
        $adminfull = $this->check_admin_full();
        $entry = array();
        $entry['supporter'] = $this->supporter_model->get_list();
        $entry['farms'] = $this->farm_model->get_list();

        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->supportdevice_model->check_exists(array('id' => $id));
                if ($check_exist) {
                    $data = $this->supportdevice_model->get_device_info($id);
                }
            }
            $this->load->view('supportdevice/edit', array('data' => $data, 'entry' => $entry));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'deviceid',
                    'label' => 'Tên thiết bị',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).'
                    )
                ),
                array(
                    'field' => 'ipserver',
                    'label' => 'Địa chỉ máy chủ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idsupport',
                    'label' => 'Người hỗ trợ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idfarm',
                    'label' => 'Trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                )

            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('supportdevice/edit', array('id' => $id, 'entry' => $entry));
            } else {
                try {
                    $dateactive = explode('/', $data['dateactive']);
                    $dateactive = $dateactive[2] . '-' . $dateactive[1] . '-' . $dateactive[0];

                    $warantytime = explode('/', $data['warantytime']);
                    $warantytime = $warantytime[2] . '-' . $warantytime[1] . '-' . $warantytime[0];
                    $entry = array(
                        'deviceid' => $this->db->escape_str(trim($data['deviceid'])),
                        'ipserver' => $this->db->escape_str(trim($data['ipserver'])),
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'usersql' => $this->db->escape_str(trim($data['usersql'])),
                        'isdefault' => $this->db->escape_str($data['isdefault']),
                        'idsupport' => $this->db->escape_str(trim($data['idsupport'])),
                        'idfarm' => $this->db->escape_str(trim($data['idfarm'])),
                        'dateactive' => $dateactive,
                        'warantytime' => $warantytime,
                        'updated' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                    );
                    if ($this->supportdevice_model->update($data['id'], $entry)) {
                        $this->session->set_flashdata('success', 'Cập nhật thiết bị thành công');
                    } else {
                        $this->session->set_flashdata('failed', 'Không thể cập nhật thiết bị!');
                    }

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->supportdevice_model->check_exists(array('id' => $id));
                if ($check) {
                    if ($this->supportdevice_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa thiết bị thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa thiết bị!');

                    redirect($this->config->config['base_url'] . "/supportdevice");
                    return FALSE;
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

    public function showFarm($id) {
        $farm = $this->farm_model->get_basic_info($id);
        return $farm->name;
    }

}
