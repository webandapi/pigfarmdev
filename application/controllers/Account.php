<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();
        $this->check_user_log();

        $pageTitle = 'Quản lý tài khoản cá nhân';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Account
     */
    public function index() {
        $thisuser = $this->get_current_user();
        $fields = $this->userfield_model->get_user_field_list($thisuser['user_id']);
        $strfields = '';
        if ($fields) {
            foreach ($fields as $k => $col) {
                $field = $this->field_model->get_info_rule(array('id' => $col->field_id));
                $strfields .= '<p class="btn btn-success btn-flat btn-sm">' . $field->name . '</p>  ';
            }
        }
        $thisuser['userfield'] = $strfields;
        $this->load->view('account/index', array('current' => $thisuser));
        $this->load->view('footer');
    }

    /**
     * Change password
     */
    public function changepassword() {
        $thisuser = $this->get_current_user();
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('account/changepassword', array('current' => $thisuser));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'oldpass',
                    'label' => 'Mật khẩu cũ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'newpass',
                    'label' => 'Mật khẩu mới',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'cfnewpass',
                    'label' => 'Nhập lại mật khẩu mới',
                    'rules' => 'trim|required|matches[newpass]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'matches' => '%s chưa khớp',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('account/changepassword', array('current' => $thisuser));
            } else {
                try {

                    $username = $this->session->userdata('user');
                    $user = $this->user_model->get_user_info($username);

                    #check old password is the same new password
                    if (trim($data['oldpass']) == trim($data['newpass'])) {
                        $this->session->set_flashdata('failed', 'Mật khẩu mới của bạn đã trùng với mật khẩu cũ hiện tại.');
                        redirect($_SERVER['HTTP_REFERER']);
                        return FALSE;
                    }

                    if ($this->user_model->validate_password(trim($data['oldpass']), $user->pw)) {
                        $entry['pw'] = $this->user_model->create_hash(trim($data['newpass']));
                        if ($this->user_model->update($user->user_id, $entry)) {
                            # delete remember
                            if ($this->session->has_userdata('re_username'))
                                $this->session->unset_userdata('re_username');
                            if ($this->session->has_userdata('re_password'))
                                $this->session->unset_userdata('re_password');
                            $this->session->set_flashdata('success', 'Cập nhật mật khẩu mới thành công');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật mật khẩu mới!');
                    } else
                        $this->session->set_flashdata('failed', 'Nhập mật khẩu cũ chưa đúng!');


                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Account update
     */
    public function update() {
        $userfield = $this->field_model->get_list(); # userfields
        $thisuser = $this->get_current_user(); #current user
        $fieldcold = array();
        $fields = $this->userfield_model->get_user_field_list($thisuser['user_id']);
        if ($fields) {
            foreach ($fields as $col) {
                $fieldcold[] = $col->field_id;
            }
        }
        $entry = array(
            'current' => $thisuser,
            'userfield' => $userfield,
            'fieldcold' => $fieldcold
        );
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('account/update', array('data' => $entry));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );
            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('account/update', array('data' => $entry));
            } else {
                try {
                    $birthday = explode('/', $data['birthday']);
                    $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];
                    $entry = array(
                        'firstname' => $this->db->escape_str(trim($data['firstname'])),
                        'lastname' => $this->db->escape_str(trim($data['lastname'])),
                        'address' => $this->db->escape_str(trim($data['address'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'gender' => $this->db->escape_str(trim($data['gender'])),
                        'date_of_birth' => $birthday,
                    );

                    #update account
                    if ($this->user_model->update($thisuser['user_id'], $entry)) {
                        $fieldCol = $data['userfield'];
                        if ($fieldCol) {
                            $fieldCol_ = array();
                            foreach ($fieldCol as $key => $value) {
                                $fieldCol_[]['id'] = $value;
                            }
                        }
                        #update user field
                        $this->userfield_model->update($thisuser['user_id'], $fieldCol_);

                        $this->session->set_flashdata('success', 'Cập nhật thông tin tài khoản thành công');
                    } else
                        $this->session->set_flashdata('failed', 'Không thể cập nhật thông tin tài khoản!');


                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

}
