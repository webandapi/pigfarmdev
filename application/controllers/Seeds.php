<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seeds extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý giống cây trồng, vật nuôi';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name'));
        $checkfull = false;
        $adminfull = $this->check_admin_full();
        if ($farms):
            $data = array();
            $i = 0;
            foreach ($farms as $farm) :
                if ($_GET['farm'] == $farm->id || $_GET['farm'] == '') {
                    $seeds = $this->seeds_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($seeds) {
                        foreach ($seeds as $val) {
                            $i++;
                            $crops = $this->crops_model->get_info_rule(array('id' => $val->crops_id, 'name'));
                            $val->farm = $farm->name;
                            $val->crops = $crops->name;
                            $data[] = $val;
                        }
                    }
                }
            endforeach;
            if ($i >= 1 && !$adminfull)
                $checkfull = true;
        endif;
        $this->load->view('seeds/index', array('seeds' => $data, 'farms' => $farms, 'checkfull' => $checkfull));
        $this->load->view('footer');
    }

    /**
     * Insert data
     * @return boolean
     * Kiểm tra: {Tài khoản người dùng hiện tại phải là nông dân}
     */
    public function add() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $cropslst = array();
        $checkfull = false;
        $adminfull = $this->check_admin_full();
        if ($farms):
            $i = 0;
            foreach ($farms as $farm) :
                if (!$adminfull) {
                    $seeds = $this->seeds_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($seeds) {
                        foreach ($seeds as $s) {
                            $i++;
                        }
                    }
                }

                if ($_GET['farm'] == $farm->id || $_GET['farm'] == '') {
                    $farmcrops = $this->farm_crops_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($farmcrops) {
                        foreach ($farmcrops as $val) {
                            $crops = $this->crops_model->get_info_rule(array('id' => $val->crops_id), 'id,name');
                            $cropslst[] = $crops;
                        }
                    }
                }
            endforeach;
            if ($i >= 1)
                $checkfull = true;
        endif;

        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('seeds/add', array('crops' => $cropslst, 'farms' => $farms, 'checkfull' => $checkfull));
        } else { #submit form
            #check admin full feature
            if ($checkfull) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            }
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên giống cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'crops',
                    'label' => 'cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
                array(
                    'field' => 'farm',
                    'label' => 'trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('seeds/add', array('crops' => $cropslst, 'farms' => $farms));
            } else {
                try {
                    $thisuser = $this->get_current_user(); #current user
                    #check permission
                    if ($thisuser['usertype_id'] == '1') {
                        $date_of_purchase = $this->db->escape_str(trim($data['date_of_purchase']));
                        $date_of_purchase = explode('/', $date_of_purchase);
                        $date_of_purchase = $date_of_purchase[2] . '-' . $date_of_purchase[1] . '-' . $date_of_purchase[0];

                        $expiration_date = $this->db->escape_str(trim($data['expiration_date']));
                        $expiration_date = explode('/', $expiration_date);
                        $expiration_date = $expiration_date[2] . '-' . $expiration_date[1] . '-' . $expiration_date[0];

                        $entry = array(
                            'name' => $this->db->escape_str(trim($data['name'])),
                            'crops_id' => $data['crops'],
                            'farm_id' => $data['farm'],
                            'origin' => $this->db->escape_str(trim($data['origin'])),
                            'date_of_purchase' => $date_of_purchase,
                            'expiration_date' => $expiration_date,
                            'amount' => str_replace('.', '', $this->db->escape_str(trim($data['amount']))),
                            'seed_cost' => str_replace('.', '', $this->db->escape_str(trim($data['seed_cost']))),
                            'pesticide_costs' => str_replace('.', '', $this->db->escape_str(trim($data['pesticide_costs']))),
                            'fertilizer_cost' => str_replace('.', '', $this->db->escape_str(trim($data['fertilizer_cost']))),
                            'harvesting_costs' => str_replace('.', '', $this->db->escape_str(trim($data['harvesting_costs']))),
                            'expected_productivity' => $this->db->escape_str(trim($data['expected_productivity'])),
                            'total_cost' => $this->db->escape_str(trim($data['total_cost'])),
                        );
                        if ($this->seeds_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm giống cây trồng, vật nuôi mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm giống cây trồng, vật nuôi mới!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải là nông dân nên không được phép thêm giống cây trồng, vật nuôi!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && thông tin hạt giống phải thuộc trang trại nông dân đang quản lý}
     */
    public function edit() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $cropslst = array();
        $thisuser = $this->get_current_user();
        if ($farms):
            foreach ($farms as $farm) :
                if ($_GET['farm'] == $farm->id || $_GET['farm'] == '') {
                    $farmcrops = $this->farm_crops_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($farmcrops) {
                        foreach ($farmcrops as $val) {
                            $crops = $this->crops_model->get_info_rule(array('id' => $val->crops_id), 'id,name');
                            $cropslst[] = $crops;
                        }
                    }
                }
            endforeach;
        endif;

        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            if (isset($id)) {
                $ok = false;
                if ($data = $this->seeds_model->get_info_rule(array('id' => $id))) {
                    if ($this->farm_model->check_exists(array('id' => $data->farm_id, 'user_id' => $thisuser['user_id'])))
                        $ok = true;
                }
                #check permission
                if ($ok === true && $thisuser['usertype_id'] == '1')
                    $this->load->view('seeds/edit', array('crops' => $cropslst, 'farms' => $farms, 'data' => $data));
                else
                    $this->load->view('seeds/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
            }
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên giống cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'crops',
                    'label' => 'cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
                array(
                    'field' => 'farm',
                    'label' => 'trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {
                    $ok = false;
                    if ($current = $this->seeds_model->get_info_rule(array('id' => $data['id']))) {
                        if ($this->farm_model->check_exists(array('id' => $current->farm_id, 'user_id' => $thisuser['user_id'])))
                            $ok = true;
                    }
                    #check permission
                    if ($ok == true && $thisuser['usertype_id'] == '1') {
                        $date_of_purchase = $this->db->escape_str(trim($data['date_of_purchase']));
                        $date_of_purchase = explode('/', $date_of_purchase);
                        $date_of_purchase = $date_of_purchase[2] . '-' . $date_of_purchase[1] . '-' . $date_of_purchase[0];

                        $expiration_date = $this->db->escape_str(trim($data['expiration_date']));
                        $expiration_date = explode('/', $expiration_date);
                        $expiration_date = $expiration_date[2] . '-' . $expiration_date[1] . '-' . $expiration_date[0];
                        $entry = array(
                            'name' => $this->db->escape_str(trim($data['name'])),
                            'crops_id' => $data['crops'],
                            'farm_id' => $data['farm'],
                            'origin' => $this->db->escape_str(trim($data['origin'])),
                            'date_of_purchase' => $date_of_purchase,
                            'expiration_date' => $expiration_date,
                            'amount' => str_replace('.', '', $this->db->escape_str(trim($data['amount']))),
                            'seed_cost' => str_replace('.', '', $this->db->escape_str(trim($data['seed_cost']))),
                            'pesticide_costs' => str_replace('.', '', $this->db->escape_str(trim($data['pesticide_costs']))),
                            'fertilizer_cost' => str_replace('.', '', $this->db->escape_str(trim($data['fertilizer_cost']))),
                            'harvesting_costs' => str_replace('.', '', $this->db->escape_str(trim($data['harvesting_costs']))),
                            'expected_productivity' => $this->db->escape_str(trim($data['expected_productivity'])),
                            'total_cost' => $this->db->escape_str(trim($data['total_cost'])),
                        );

                        if ($this->seeds_model->update($data['id'], $entry))
                            $this->session->set_flashdata('success', 'Cập nhật giống cây trồng, vật nuôi thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật giống cây trồng, vật nuôi!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không có quyền thao tác thông tin trang trại người khác!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete($seeds_id = '') {
        $segs = $this->uri->segment_array();
        $id = $seeds_id ? $seeds_id : end($segs);
        if ($id) {
            try {
                $thisuser = $this->get_current_user(); # current user
                $ok = false;
                if ($current = $this->seeds_model->get_info_rule(array('id' => $id))) {
                    if ($this->farm_model->check_exists(array('id' => $current->farm_id, 'user_id' => $thisuser['user_id'])))
                        $ok = true;
                }

                #check permission
                if ($ok == true && $thisuser['usertype_id'] == '1') {
                    if ($this->seeds_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa giống cây trồng, vật nuôi thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa giống cây trồng, vật nuôi!');
                } else
                    $this->session->set_flashdata('failed', 'Bạn không có quyền xóa giống cây trồng, vật nuôi của trang trại khác !');

                redirect($this->config->config['base_url'] . "/seeds");
                return FALSE;
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
