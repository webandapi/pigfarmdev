<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Planting extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý vụ cây trồng, vật nuôi';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name'));
        $adminfull = $this->check_admin_full(); # check admin full
        $checkfull = false;
        if ($farms):
            $plantinglst = array();
            $i = 0;
            foreach ($farms as $farm) :
                if ($_GET['farm'] == $farm->id || $_GET['farm'] == '') {
                    $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name');
                    if ($planting) {
                        foreach ($planting as $val) {
                            $i++;
                            $diaries = $this->diary_model->get_list(array('where' => array('planting_id' => $val->id)));
                            $incidents = $this->incidents_model->get_list(array('where' => array('planting_id' => $val->id)));
                            $seeds = $this->seeds_model->get_info_rule(array('id' => $val->seeds_id), 'name');
                            $val->farm = $farm->name;
                            $val->diaries = $diaries ? $diaries : array();
                            $val->incidents = $incidents ? $incidents : array();
                            $val->seeds = $seeds->name;
                            $plantinglst[] = $val;
                        }
                    }
                }
            endforeach;
            if ($i >= 1 && !$adminfull)
                $checkfull = true;
        endif;
        $this->load->view('planting/index', array('planting' => $plantinglst, 'farms' => $farms, 'checkfull' => $checkfull));
        $this->load->view('footer');
    }

    /**
     * Insert data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân}
     */
    public function add() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $seedslst = array();
        $adminfull = $this->check_admin_full(); # check admin full
        $checkfull = false;
        $thisuser = $this->get_current_user(); #current user
        if ($farms):
            $i = 0;
            foreach ($farms as $farm) {
                if (!$adminfull) {
                    $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id)));
                    if ($planting) {
                        foreach ($planting as $pl) {
                            $i++;
                        }
                    }
                }
                $seeds = $this->seeds_model->get_list(array('where' => array('farm_id' => $farm->id)), 'id,name');
                if ($seeds) {
                    foreach ($seeds as $value) {
                        $seedslst[] = $value;
                    }
                }
            }
            if ($i >= 1)
                $checkfull = true;
        endif;

        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('planting/add', array('farms' => $farms, 'seeds' => $seedslst, 'checkfull' => $checkfull));
        } else { #submit form
            #check admin full feature
            if ($checkfull) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            }
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên vụ cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'sowing_time',
                    'label' => 'Thời gian bắt đầu',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'expected_harvest_time',
                    'label' => 'Thời gian dự kiến kết thúc',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'seeds',
                    'label' => 'giống cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
                array(
                    'field' => 'farm',
                    'label' => 'trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('planting/add', array('farms' => $farms, 'seeds' => $seedslst));
            } else {
                try {
                    if ($thisuser['usertype_id'] == '1') {
                        // thời gian bắt đầu
                        $sowing_time = $this->db->escape_str(trim($data['sowing_time']));
                        $sowing_time = explode('/', $sowing_time);
                        $sowing_time = $sowing_time[2] . '-' . $sowing_time[1] . '-' . $sowing_time[0];

                        // thời gian dự kiến kết thúc
                        $expected_harvest_time = $this->db->escape_str(trim($data['expected_harvest_time']));
                        $expected_harvest_time = explode('/', $expected_harvest_time);
                        $expected_harvest_time = $expected_harvest_time[2] . '-' . $expected_harvest_time[1] . '-' . $expected_harvest_time[0];

                        $entry = array(
                            'name' => $this->db->escape_str(trim($data['name'])),
                            'sowing_time' => $sowing_time,
                            'expected_harvest_time' => $expected_harvest_time,
                            'status' => $this->db->escape_str(trim($data['status'])),
                            'farm_id' => $data['farm'],
                            'seeds_id' => $data['seeds']
                        );
                        if ($this->planting_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm vụ cây trồng, vật nuôi mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm vụ cây trồng, vật nuôi mới!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải là nông dân nên không được phép thêm vụ cây trồng, vật nuôi!');


                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && vụ cây trồng, vật nuôi hiện tại phải thuộc trang trại nông dân đó quản lý}
     */
    public function edit() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $seedslst = array();
        $thisuser = $this->get_current_user();
        if ($farms):
            foreach ($farms as $farm) {
                $seeds = $this->seeds_model->get_list(array('where' => array('farm_id' => $farm->id)), 'id,name');
                if ($seeds) {
                    foreach ($seeds as $value) {
                        $seedslst[] = $value;
                    }
                }
            }
        endif;

        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            if (isset($id)) {
                $ok = false;
                if ($data = $this->planting_model->get_info_rule(array('id' => $id))) {
                    if ($this->farm_model->check_exists(array('id' => $data->farm_id, 'user_id' => $thisuser['user_id'])))
                        $ok = true;
                }
                #check permission
                if ($ok === true && $thisuser['usertype_id'] == '1')
                    $this->load->view('planting/edit', array('farms' => $farms, 'seeds' => $seedslst, 'data' => $data));
                else
                    $this->load->view('planting/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
            }
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên vụ cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'sowing_time',
                    'label' => 'Thời gian bắt đầu',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'expected_harvest_time',
                    'label' => 'Thời gian dự kiến kết thúc',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {

                    $ok = false;
                    if ($current = $this->planting_model->get_info_rule(array('id' => $data['id']))) {
                        if ($this->farm_model->check_exists(array('id' => $current->farm_id, 'user_id' => $thisuser['user_id'])))
                            $ok = true;
                    }

                    #check permission
                    if ($ok == true && $thisuser['usertype_id'] == '1') {
                        // thời gian bắt đầu
                        $sowing_time = $this->db->escape_str(trim($data['sowing_time']));
                        $sowing_time = explode('/', $sowing_time);
                        $sowing_time = $sowing_time[2] . '-' . $sowing_time[1] . '-' . $sowing_time[0];

                        // thời gian dự kiến kết thúc
                        $expected_harvest_time = $this->db->escape_str(trim($data['expected_harvest_time']));
                        $expected_harvest_time = explode('/', $expected_harvest_time);
                        $expected_harvest_time = $expected_harvest_time[2] . '-' . $expected_harvest_time[1] . '-' . $expected_harvest_time[0];

                        $entry = array(
                            'name' => $this->db->escape_str(trim($data['name'])),
                            'sowing_time' => $sowing_time,
                            'expected_harvest_time' => $expected_harvest_time,
                            'status' => $this->db->escape_str(trim($data['status'])),
                            'seeds_id' => $data['seeds']
                        );

                        if ($this->planting_model->update($data['id'], $entry))
                            $this->session->set_flashdata('success', 'Cập nhật vụ cây trồng, vật nuôi thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật vụ cây trồng, vật nuôi!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không có quyền thao tác thông tin trang trại người khác!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Delete data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && vụ cây trồng, vật nuôi hiện tại phải thuộc trang trại của nông dân đó đang sở hữu}
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $thisuser = $this->get_current_user(); # current user
                $ok = false;
                if ($current = $this->planting_model->get_info_rule(array('id' => $id))) {
                    if ($this->farm_model->check_exists(array('id' => $current->farm_id, 'user_id' => $thisuser['user_id'])))
                        $ok = true;
                }

                #check permission
                if ($ok == true && $thisuser['usertype_id'] == '1') {
                    $this->incidents_model->del_rule(array('planting_id' => $id));
                    $this->diary_model->del_rule(array('planting_id' => $id));
                    $this->harvest_model->del_rule(array('planting_id' => $id));
                    #delete harvest - distributor
                    if ($this->planting_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa vụ cây trồng, vật nuôi thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa vụ cây trồng, vật nuôi!');
                } else
                    $this->session->set_flashdata('failed', 'Bạn không có quyền xóa vụ cây trồng, vật nuôi của trang trại khác!');

                redirect($this->config->config['base_url'] . "/planting");
                return FALSE;
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
