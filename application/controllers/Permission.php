<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Access denied!';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $current_user = $this->get_current_admin();

        $this->load->view('header', array('current' => $current_user, 'pageTitle' => $pageTitle));
    }

    /**
     * Show message access denied
     */
    public function index() {
        $this->load->view('permission');
        $this->load->view('footer');
    }

}
