<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class My404 extends FT_Controller {

    public function __construct() {

        /**
         * __construct()
         */
        parent::__construct();
    }

    function index() {
        $this->load->view('errors/header');
        $this->load->view('errors/404');
        $this->load->view('errors/footer');
    }

}
