<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Harvest extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý lần thu hoạch';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        // lấy danh sách trang trại của nông dân hiện tại
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name'));
        $farm_ids = array();
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if ($farms):
            foreach ($farms as $farm) :
                $farm_ids[] = $farm->id;
            endforeach;
        endif;
        $harvest = $this->planting_model->get_harvest_by_farm($farm_ids);
        if ($harvest):
            foreach ($harvest as $h) {
                $user_farm = $this->userfarm_model->get_info_rule(array('id' => $h->package_user_id), 'fullname');
                $qrcode = $this->qrcode_model->get_info_rule(array('harvest_id' => $h->id), 'qr_from,qr_to');
                $thisdistributors = $this->harvest_distributor_model->get_list(array('where' => array('harvest_id' => $h->id)), 'distributor_id');
                if ($thisdistributors) {
                    foreach ($thisdistributors as $val) {
                        $dis = $this->distributor_model->get_info_rule(array('id' => $val->distributor_id), 'name');
                        $h->distributor[] = $dis->name;
                    }
                }
                if ($qrcode)
                    $h->qrcode = $qrcode;
                if ($user_farm)
                    $h->user = $user_farm->fullname;
            }
            if (count($harvest) >= 1 && !$adminfull)
                $checkfull = true;
        endif;
        $this->load->view('harvest/index', array('harvest' => $harvest, 'checkfull' => $checkfull));
        $this->load->view('footer');
    }

    /**
     * Insert data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân && nông dân phải tạo vụ cây trồng, vật nuôi trước đó}
     */
    public function add() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $plantinglst = $userfarmlst = array();
        $farm_ids = array();
        $adminfull = $this->check_admin_full();
        $checkfull = false;
        if ($farms):
            foreach ($farms as $farm) :
                $farm_ids[] = $farm->id;
                $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name'); #planting
                if ($planting) {
                    foreach ($planting as $val) {
                        $plantinglst[] = $val;
                    }
                }
                $userfarm = $this->userfarm_model->get_list(array('where' => array('farm_id' => $farm->id)), 'user_id,user_name,fullname'); #userfarm
                if ($userfarm) {
                    foreach ($userfarm as $user) {
                        $userfarmlst[] = $user;
                    }
                }
            endforeach;
        endif;
        $harvest = $this->planting_model->get_harvest_by_farm($farm_ids);
        $distributors = $this->distributor_model->get_list(); #distributors
        if (count($harvest) >= 1 && !$adminfull)
            $checkfull = true;

        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('harvest/add', array('planting' => $plantinglst, 'userfarm' => $userfarmlst, 'distributors' => $distributors, 'checkfull' => $checkfull));
        } else { #submit form
            #check admin full feature
            if ($checkfull) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            }
            #form data posted
            $data = $this->input->post();

            #form validate config
            $config = array(
                array('field' => 'packaging_date', 'label' => 'Ngày đóng gói', 'rules' => 'trim|required', 'errors' => array('required' => '%s không được để trống.')),
                array('field' => 'planting', 'label' => 'vụ cây trồng, vật nuôi', 'rules' => 'trim|required', 'errors' => array('required' => 'Chọn %s.',)),
                array('field' => 'qrto', 'label' => 'Qrcode đến', 'rules' => 'callback_check_qrcode_to')
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('harvest/add', array('planting' => $plantinglst, 'userfarm' => $userfarmlst, 'distributors' => $distributors));
            } else {
                try {
                    $thisuser = $this->get_current_user(); #current user
                    #check permission
                    if ($thisuser['usertype_id'] == '1') {

                        # ngày đóng gói
                        $packaging_date = explode('/', $data['packaging_date']);
                        $packaging_date = $packaging_date[2] . '-' . $packaging_date[1] . '-' . $packaging_date[0];

                        $entry = array(
                            'packaging_date' => $packaging_date,
                            'package_user_id' => $data['userfarm'],
                            'planting_id' => $data['planting']
                        );

                        #insert data
                        $harvest_id = $this->harvest_model->create($entry);
                        if ($harvest_id) {

                            // harvest - distributor
                            if ($data['distributors'] && is_array($data['distributors'])) {
                                foreach ($data['distributors'] as $dis) {
                                    $this->harvest_distributor_model->create(array('harvest_id' => $harvest_id, 'distributor_id' => $dis));
                                }
                            }

                            // Qrcode
                            if (is_numeric($data['qrfrom']) && is_numeric($data['qrto'])) {
                                $farm = $this->harvest_model->get_farm_by_harvest($harvest_id); #thisfarm
                                $farm_info = $this->farm_model->get_info_rule(array('id' => $farm->farm_id), 'code');

                                #check farm code is set
                                if ($farm_info->code) {
                                    #check max qrcode
                                    $checkcode = $this->qrcode_model->get_maxqrcode_by_farmcode($farm_info->code);
                                    if ($data['qrfrom'] <= $checkcode->qr_to) {
                                        $max = number_format($checkcode->qr_to, '0', ',', '.');
                                        $this->session->set_flashdata('failed', "Thêm lần thu hoạch thành công, nhưng Qrcode không được cập nhật.<br/>QRcode của trang trại bạn đang dùng hiện tại là: {$max}. Vui lòng nhập (Mã QRcode từ) > {$max}!");
                                        redirect($_SERVER['HTTP_REFERER']);
                                        return FALSE;
                                    } else {
                                        #qrcode data
                                        $thisharvest = $this->harvest_model->get_info_rule(array('id' => $harvest_id), 'planting_id'); #this harvest info
                                        $qrcode_entry = array(
                                            'farm_code' => $farm_info->code,
                                            'planting_id' => $thisharvest->planting_id,
                                            'harvest_id' => $harvest_id,
                                            'qr_from' => $this->db->escape_str(trim($data['qrfrom'])),
                                            'qr_to' => $this->db->escape_str(trim($data['qrto']))
                                        );
                                        #insert qrcode
                                        $this->qrcode_model->create($qrcode_entry);
                                    }
                                } else {
                                    $this->session->set_flashdata('failed', "Trang trại của bạn chưa được cấp mã trang trại. Vui lòng liên hệ quản trị viên để được hổ trợ.");
                                    redirect($_SERVER['HTTP_REFERER']);
                                    return FALSE;
                                }
                            }

                            $this->session->set_flashdata('success', 'Thêm lần thu hoạch mới thành công');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể thêm lần thu hoạch mới!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không phải nông dân nên không được phép thêm lần thu hoạch!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Check qrcode to
     * @param type $qrcode
     * @return boolean
     * Kiểm tra: {Qrcode đến không được nhỏ hơn Qrcode từ}
     */
    public function check_qrcode_to() {
        $qrfrom = $this->input->post('qrfrom');
        $qrto = $this->input->post('qrto');
        if ($qrfrom && $qrto) {
            $message = '';
            if ($qrfrom >= $qrto) {
                $message = 'Qrcode đến không đc nhỏ hơn Qrcode từ.';
                $this->form_validation->set_message('check_qrcode_to', $message);
                return FALSE;
            } else {
                return TRUE;
            }
        }
        return TRUE;
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && lần thu hoạch phải thuộc trang trại nông dân đó quản lý}
     */
    public function edit() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $plantinglst = $userfarmlst = $planting_ids = array();
        $thisuser = $this->get_current_user(); #current user
        if ($farms):
            foreach ($farms as $farm) :
                $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name'); #planting
                if ($planting) {
                    foreach ($planting as $val) {
                        $planting_ids[] = $val->id;
                        $plantinglst[] = $val;
                    }
                }
                $userfarm = $this->userfarm_model->get_list(array('where' => array('farm_id' => $farm->id)), 'user_id,user_name,fullname'); #userfarm
                if ($userfarm) {
                    foreach ($userfarm as $user) {
                        $userfarmlst[] = $user;
                    }
                }
            endforeach;
        endif;

        $distributors = $this->distributor_model->get_list(); #distributors
        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            if (isset($id)) {
                $ok = false;
                if ($data = $this->harvest_model->get_info_rule(array('id' => $id))) {
                    if (in_array($data->planting_id, $planting_ids))
                        $ok = true;
                }

                #check permission
                if ($ok === true && $thisuser['usertype_id'] == '1') {
                    $thisplanting = $this->planting_model->get_info_rule(array('id' => $data->planting_id), 'id');
                    $qrcode = $this->qrcode_model->get_info_rule(array('harvest_id' => $id), 'qr_from,qr_to');
                    if ($qrcode)
                        $data->qrcode = $qrcode;

                    #distributors
                    $thisdistributors = $this->harvest_distributor_model->get_list(array('where' => array('harvest_id' => $id)), 'distributor_id');
                    if ($thisdistributors) {
                        foreach ($thisdistributors as $val) {
                            $dis = $this->distributor_model->get_info_rule(array('id' => $val->distributor_id), 'id');
                            $data->distributors[] = $dis->id;
                        }
                    }
                    $this->load->view('harvest/edit', array('planting' => $plantinglst, 'userfarm' => $userfarmlst, 'distributors' => $distributors, 'data' => $data));
                } else
                    $this->load->view('harvest/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
            }
        } else { #submit form
            #form data posted
            $data = $this->input->post();
            $config = array(
                array('field' => 'packaging_date', 'label' => 'Ngày đóng gói', 'rules' => 'trim|required', 'errors' => array('required' => '%s không được để trống.')),
                array('field' => 'qrto', 'label' => 'Qrcode đến', 'rules' => 'callback_check_qrcode_to'),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {
                    $ok = false;
                    if ($current = $this->harvest_model->get_info_rule(array('id' => $data['id']))) {
                        if (in_array($current->planting_id, $planting_ids))
                            $ok = true;
                    }

                    #check permission
                    if ($ok === true && $thisuser['usertype_id'] == '1') {

                        # ngày đóng gói
                        $packaging_date = explode('/', $data['packaging_date']);
                        $packaging_date = $packaging_date[2] . '-' . $packaging_date[1] . '-' . $packaging_date[0];

                        $entry = array(
                            'packaging_date' => $packaging_date,
                            'package_user_id' => $this->db->escape_str(trim($data['userfarm'])),
                                //'planting_id' => $data['planting']
                        );

                        if ($this->harvest_model->update($data['id'], $entry)) {

                            #update harvest - distributor
                            $this->harvest_distributor_model->update($data['id'], $data['distributors']);

                            # update qrcode if this qrcode not set before
                            $thisharvest = $this->qrcode_model->get_info_rule(array('harvest_id' => $data['id']), 'qr_from,qr_to');
                            if (!$thisharvest) {
                                // Qrcode
                                if (is_numeric($data['qrfrom']) && is_numeric($data['qrto'])) {
                                    $farm = $this->harvest_model->get_farm_by_harvest($data['id']); #this farm
                                    $farm_info = $this->farm_model->get_info_rule(array('id' => $farm->farm_id), 'code');

                                    #check farm code is set
                                    if ($farm_info->code) {
                                        #check max qrcode
                                        $checkcode = $this->qrcode_model->get_maxqrcode_by_farmcode($farm_info->code);
                                        if ($data['qrfrom'] <= $checkcode->qr_to) {
                                            $max = number_format($checkcode->qr_to, '0', ',', '.');
                                            $this->session->set_flashdata('failed', "Cập lần thu hoạch thành công, nhưng Qrcode không được cập nhật.<br/>QRcode của trang trại bạn đang dùng hiện tại là: {$max}. Vui lòng nhập (Mã QRcode từ) > {$max}!");
                                            redirect($_SERVER['HTTP_REFERER']);
                                            return FALSE;
                                        } else {
                                            #qrcode data
                                            $thisharvest = $this->harvest_model->get_info_rule(array('id' => $data['id']), 'planting_id'); #this harvest info
                                            $qrcode_entry = array(
                                                'farm_code' => $farm_info->code,
                                                'planting_id' => $thisharvest->planting_id,
                                                'harvest_id' => $data['id'],
                                                'qr_from' => $this->db->escape_str(trim($data['qrfrom'])),
                                                'qr_to' => $this->db->escape_str(trim($data['qrto']))
                                            );
                                            #insert qrcode
                                            $this->qrcode_model->create($qrcode_entry);
                                        }
                                    } else {
                                        $this->session->set_flashdata('failed', "Trang trại của bạn chưa được cấp mã trang trại. Vui lòng liên hệ quản trị viên để được hổ trợ.");
                                        redirect($_SERVER['HTTP_REFERER']);
                                        return FALSE;
                                    }
                                }
                            }

                            $this->session->set_flashdata('success', 'Cập nhật lần thu hoạch thành công');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật lần thu hoạch!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không có quyền cập nhật lần thu hoạch của người khác!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Delete data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && lần thu hoạch phải thuộc trang trại mà nông dân đó quản lý}
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
                $planting_ids = array();
                $thisuser = $this->get_current_user(); #current user
                if ($farms):
                    foreach ($farms as $farm) :
                        $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name'); #planting
                        if ($planting) {
                            foreach ($planting as $val) {
                                $planting_ids[] = $val->id;
                            }
                        }
                    endforeach;
                endif;

                $ok = false;
                if ($current = $this->harvest_model->get_info_rule(array('id' => $id))) {
                    if (in_array($current->planting_id, $planting_ids))
                        $ok = true;
                }
                #check permission
                if ($ok === true && $thisuser['usertype_id'] == '1') {
                    #delete harvest - distributor
                    $this->harvest_distributor_model->del_rule(array('harvest_id' => $id));

                    #delete qrcode
                    $this->qrcode_model->del_rule(array('harvest_id' => $id));

                    #delete harvest
                    if ($this->harvest_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa lần thu hoạch thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa lần thu hoạch!');
                } else
                    $this->session->set_flashdata('failed', 'Bạn không thể xóa lần thu hoạch của người khác!');

                redirect($this->config->config['base_url'] . "/harvest");
                return FALSE;
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
