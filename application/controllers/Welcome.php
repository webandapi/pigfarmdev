<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends FT_Controller {
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation'));

        if ($this->session->has_userdata('user')) {
            redirect($this->config->config['base_url'] . '/dashboard/');
            exit();
        }
    }

    /**
     * User login page
     */
    public function index() {

        # show form
        if (!$this->input->post('submit')) {
            $this->load->view('login/header');
            $this->load->view('login/index');
            $this->load->view('login/footer');
        } else { // submit form
            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'pwd',
                    'label' => 'Mật khẩu',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                )
            );

            # form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('login/header');
                $this->load->view('login/index');
                $this->load->view('login/footer');
            } else {

                // submit
                if (isset($_POST['username']) && isset($_POST['pwd'])) {
                    $login_name = $_POST['username'];
                    $login_password = $_POST['pwd'];

                    $user = $this->user_model->get_user_info($login_name);
                    if ($user) {
                        if ($this->user_model->validate_password($login_password, $user->pw)) {
                            if ($user->usertype_id == 1) {
                                if ($user->status) {
                                    #remember
                                    $remember = (int) $_POST['remember'];
                                    if ($remember == 1)
                                        $this->session->set_userdata(array('re_username' => $user->username, 're_password' => $login_password));

                                    #create token
                                    $token = $this->get_token('HS256', 'JWT', json_encode(array('username' => $user->username, 'password' => $login_password)));
                                    if (!$token)
                                        return false;

                                    $this->token_model->create_($user->user_id, $token['access_token'], $token['refresh_key']);

                                    # set userdata
                                    $this->session->sess_expiration = '14400'; // expires in 4 hours
                                    $this->session->set_userdata(array('user' => $user->username, 'logged_in' => TRUE));
                                    $this->session->access_token = $token['access_token'];
                                    redirect($this->config->config['base_url'] . '/dashboard');
                                } else
                                    $this->session->set_flashdata('reponse', 'Tài khoản này chưa được kích hoạt!');
                            }else {
                                $this->session->set_flashdata('reponse', 'Bạn không phải là nông dân!');
                            }
                        } else
                            $this->session->set_flashdata('reponse', 'Mật khẩu không đúng!');
                    } else
                        $this->session->set_flashdata('reponse', 'Tên đăng nhập không đúng!');

                    redirect($this->config->config['base_url']);
                    return FALSE;
                }
                return false;
            }
        }
    }

    /**
     * Login
     * @return boolean
     */
    public function login() {

        /* check request methos is post */
        if ('POST' == $_SERVER['REQUEST_METHOD']) {

            $username = trim($_POST['username']);
            $pwd = trim($_POST['pwd']);

            if ($username && $pwd) :

                $user = $this->user_model->get_user_info($username);

                if ($user) :
                    if ($this->user_model->validate_password($pwd, $user->pw)) {

                        $this->session->set_userdata(
                                array(
                                    'user' => $username,
                                    'usertype' => $user->usertype_id,
                                    'avatar' => $user->avatar,
                                    'logged_in' => true)
                        );

                        if (!$user->language)
                            $user->language = 2;

                        $language = $this->language_model->get_info($user->language);

                        if ($user->status == -1) {
                            $this->user_model->update($user->user_id, array('status' => 1));
                            //...................... gửi mail mới
                            $mailData = array(
                                'from_user' => $this->system->user_id,
                                'to_user' => $user->user_id,
                                'title' => $this->lang->line('welcome'),
                                'content' => $this->options_model->get_info_rule(array('slug' => 'welcome', 'language' => $user->language))->value
                            );
                            $this->mail_model->create($mailData);
                        }

                        $token = $this->get_token('HS256', 'JWT', json_encode(array('username' => $username, 'password' => $pwd)));
                        if (!$token)
                            return false;


                        $this->token_model->create_($user->user_id, $token['access_token'], $token['refresh_key']);
                        $tokenReturn = $this->token_model->get_info_rule(array('user_id' => $user->user_id));

                        unset($tokenReturn->user_id);
                        unset($tokenReturn->id);
                        unset($tokenReturn->start);

                        $this->user_model->update($user->user_id, array('status' => 1));
                        $this->session->set_userdata(array('language' => $language->slug));
                        $this->session->sess_expiration = '604800'; // expires in 4 hours
                        $this->session->access_token = $token['access_token'];

                        echo json_encode(array('success' => true, 'redirect' => $this->config->config['base_url'] . '/dashboard'));
                        exit();
                    } else {
                        echo json_encode(array('success' => false, 'message' => 'Mật khẩu không đúng!'));
                        exit();
                    }
                else :
                    echo json_encode(array('success' => false, 'message' => 'Tên đăng nhập hoặc mật khẩu không đúng'));
                    exit();
                endif;
            else :
                echo json_encode(array('error' => 'Chưa nhập đủ thông tin đăng nhập'));
                exit();
            endif;
        }


        return false;
    }

    /**
     * User login page
     */
    public function register() {

        # show form
        if (!$this->input->post('submit')) {
            $this->load->view('login/header');
            $this->load->view('register/index');
            $this->load->view('login/footer');
        }  else { // submit form
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'lname',
                    'label' => 'Họ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'firstname',
                    'label' => 'Tên',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'required|min_length[6]|max_length[12]|is_unique[user.username]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s phải lớn hơn 6 ký tự.',
                        'max_length' => '%s phải nhỏ hơn 12 ký tự.',
                        'is_unique' => '%s đã tồn tại.'
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'email',
                    'rules' => 'required|valid_email|is_unique[user.email]',
                    'errors' => array(
                        'required' => '%s không được để trống',
                        'valid_email' => '%s phải hợp lệ.',
                        'is_unique' => '%s đã tồn tại.'
                    )
                ),
                array(
                    'field' => 'pw',
                    'label' => 'Mật khẩu',
                    'rules' => 'required|min_length[8]|max_length[12]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s phải lớn hơn 8 ký tự.',
                        'max_length' => '%s phải lớn hơn 12 ký tự.'
                    )
                ),
                array(
                    'field' => 'pw-confirm',
                    'label' => 'Mật khẩu',
                    'rules' => 'required|matches[pw]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'matches' => '%s phải giống với trường mật khẩu.'
                    )
                )
            );

            # form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('login/header');
                $this->load->view('register/index');
                $this->load->view('login/footer');
            } else {
                try {
                    echo "Vinh Ga";
                    $data = array(
                        'lastname' => $this->db->escape_str(trim($data['lastname'])),
                        'firstname' => $this->db->escape_str(trim($data['firstname'])),
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'pw' => $this->db->create_hash(trim($data['pw']))
                    );
                    var_dump($data);
                    exit;
                    if ($this->user_model->create($data)) {
                        $this->session->set_flashdata('success', 'Đăng kí thành viên mới thành công');
                        redirect($this->config->config['base_url'] . '/register/success');
                    }
                    else
                        $this->session->set_flashdata('failed', 'Không thể thêm thành viên mới!');
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
    }

    public function success() {
        $this->load->view('login/header');
        $this->load->view('register/success');
        $this->load->view('login/footer');
    }

    public function forgotpw() {
        if (!$this->input->post('submit')) {
            $this->load->view('login/header');
            $this->load->view('forgot/index');
            $this->load->view('login/footer');
        }  else { // submit form
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'email',
                    'rules' => 'required|valid_email',
                    'errors' => array(
                        'required' => '%s không được để trống',
                        'valid_email' => '%s phải hợp lệ.'
                    )
                )
            );
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('login/header');
                $this->load->view('forgot/index');
                $this->load->view('login/footer');
            } else {
                // to do change password
            }
        }
    }

    public function forgotsuccess() {
        $pageTitle = 'lấy lại mật khẩu';
        $this->load->view('login/header');
        $this->load->view('forgot/success');
        $this->load->view('login/footer');
    }

}
