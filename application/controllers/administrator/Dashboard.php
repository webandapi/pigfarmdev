<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends FT_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Dashboard';
        $this->load->helper(array('url'));
        
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Administrator Dashboard
     */
    public function index() {
        $data = array();
        $totalexpert = $this->user_model->get_total(array('where' => array('usertype_id' => 2)));
        $totalfarmer = $this->user_model->get_total(array('where' => array('usertype_id' => 1)));
        $totalpost = $this->post_model->get_total_post();
        $totaladmin = $this->admin_model->get_total();
        $data = array(
            'totalexpert' => $totalexpert,
            'totalfarmer' => $totalfarmer,
            'totalpost' => $totalpost,
            'totaladmin' => $totaladmin
        );
        $this->load->view('administrator/index', array('data' => $data));
        $this->load->view('administrator/footer');
    }

}
