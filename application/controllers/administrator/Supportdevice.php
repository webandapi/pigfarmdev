<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supportdevice extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý thiết bị';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $where = array();
        // if (isset($_GET['status']) && $_GET['status'] != '')
        //     $where['where']['status'] = $_GET['status'];

        // if (isset($_GET['usertype']) && $_GET['usertype'] != '')
        //     $where['where']['usertype_id'] = $_GET['usertype'];

        $devices = $this->supportdevice_model->get_list($where);
        $this->load->view('administrator/supportdevice/index', array('devices' => $devices));
        $this->load->view('administrator/footer');
    }

    
    /**
     * Insert data
     */
    public function add() {
        $entry = array();
        $entry['supporter'] = $this->supporter_model->get_list();
        $entry['farms'] = $this->farm_model->get_list();
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/supportdevice/add', array('entry' => $entry));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'deviceid',
                    'label' => 'Tên thiết bị',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).'
                    )
                ),
                array(
                    'field' => 'ipserver',
                    'label' => 'Địa chỉ máy chủ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idsupport',
                    'label' => 'Người hỗ trợ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idfarm',
                    'label' => 'Trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                )

            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/supportdevice/add', array('entry' => $entry));
            } else {
                try {
                    $dateactive = explode('/', $data['dateactive']);
                    $dateactive = $dateactive[2] . '-' . $dateactive[1] . '-' . $dateactive[0];

                    $warantytime = explode('/', $data['warantytime']);
                    $warantytime = $warantytime[2] . '-' . $warantytime[1] . '-' . $warantytime[0];
                    $entry = array(
                        'deviceid' => $this->db->escape_str(trim($data['deviceid'])),
                        'ipserver' => $this->db->escape_str(trim($data['ipserver'])),
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'password' => $this->db->escape_str(md5($data['password'])),
                        'usersql' => $this->db->escape_str(trim($data['usersql'])),
                        'passsql' => $this->db->escape_str(md5($data['passsql'])),
                        'isdefault' => $this->db->escape_str($data['isdefault']),
                        'idsupport' => $this->db->escape_str(trim($data['idsupport'])),
                        'idfarm' => $this->db->escape_str(trim($data['idfarm'])),
                        'dateactive' => $dateactive,
                        'warantytime' => $warantytime,
                        'created' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                        'updated' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                    );

                    if ($this->supportdevice_model->create($entry)) {
                        $this->session->set_flashdata('success', 'Thêm thiết bị mới thành công');
                    } else {
                        $this->session->set_flashdata('failed', 'Không thể thêm thiết bị mới!');
                    }

                    redirect($this->config->config['base_url'] . '/administrator/supportdevice/add');
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     */
    public function edit() {
        $entry = array();
        $entry['supporter'] = $this->supporter_model->get_list();
        $entry['farms'] = $this->farm_model->get_list();

        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->supportdevice_model->check_exists(array('id' => $id));
                if ($check_exist) {
                    $data = $this->supportdevice_model->get_device_info($id);
                }
            }
            $this->load->view('administrator/supportdevice/edit', array('data' => $data, 'entry' => $entry));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'deviceid',
                    'label' => 'Tên thiết bị',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).'
                    )
                ),
                array(
                    'field' => 'ipserver',
                    'label' => 'Địa chỉ máy chủ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idsupport',
                    'label' => 'Người hỗ trợ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                ),
                array(
                    'field' => 'idfarm',
                    'label' => 'Trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.'
                    )
                )

            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/supportdevice/edit', array('id' => $id, 'entry' => $entry));
            } else {
                try {
                    $dateactive = explode('/', $data['dateactive']);
                    $dateactive = $dateactive[2] . '-' . $dateactive[1] . '-' . $dateactive[0];

                    $warantytime = explode('/', $data['warantytime']);
                    $warantytime = $warantytime[2] . '-' . $warantytime[1] . '-' . $warantytime[0];
                    $entry = array(
                        'deviceid' => $this->db->escape_str(trim($data['deviceid'])),
                        'ipserver' => $this->db->escape_str(trim($data['ipserver'])),
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'usersql' => $this->db->escape_str(trim($data['usersql'])),
                        'isdefault' => $this->db->escape_str($data['isdefault']),
                        'idsupport' => $this->db->escape_str(trim($data['idsupport'])),
                        'idfarm' => $this->db->escape_str(trim($data['idfarm'])),
                        'dateactive' => $dateactive,
                        'warantytime' => $warantytime,
                        'created' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                        'updated' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                    );
                    if ($this->supportdevice_model->update($data['id'], $entry)) {
                        $this->session->set_flashdata('success', 'Cập nhật thiết bị thành công');
                    } else {
                        $this->session->set_flashdata('failed', 'Không thể cập nhật thiết bị!');
                    }

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->supportdevice_model->check_exists(array('id' => $id));
                if ($check) {
                    if ($this->supportdevice_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa thiết bị thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa thiết bị!');

                    redirect($this->config->config['base_url'] . "/administrator/supportdevice");
                    return FALSE;
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
