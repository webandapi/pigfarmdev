<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý quản trị viên';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] !== '1') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
            return FALSE;
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $administrators = $where = array();
        if ($_GET['status'] !== NULL && $_GET['status'] !== '')
            $where['where']['status'] = $_GET['status'];
        if ($_GET['role'] !== NULL && $_GET['role'] !== '')
            $where['where']['adminrole_id'] = $_GET['role'];
        $datalst = $this->admin_model->get_list($where);
        if ($datalst):
            foreach ($datalst as $val) :
                $data = new stdClass();
                $data->id = $val->id;
                $data->username = $val->username;
                $data->fullname = $val->fullname;
                $data->email = $val->email;
                $data->phone = $val->phone;
                $data->gender = $val->gender;
                $data->status = $val->status;
                $data->role = $this->get_adminrole_name($val->adminrole_id);
                $administrators[] = $data;
            endforeach;
        endif;
        $roles = $this->adminrole_model->get_list();
        $this->load->view('administrator/admin/index', array('administrators' => $administrators, 'roles' => $roles));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        $roles = $this->adminrole_model->get_list();
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/admin/add', array('roles' => $roles));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required|min_length[3]|is_unique[admin.username]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
                array(
                    'field' => 'password',
                    'label' => 'Mật khẩu',
                    'rules' => 'trim|required|min_length[6]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s phải chứa ít nhất %s kí tự.',
                    )
                ),
                array(
                    'field' => 'cf_password',
                    'label' => 'Nhập lại mật khẩu',
                    'rules' => 'trim|required|matches[password]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'matches' => '%s chưa khớp',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|is_unique[admin.email]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
                array(
                    'field' => 'fullname',
                    'label' => 'Họ tên',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'phone',
                    'label' => 'Điện thoại',
                    'rules' => 'trim|required|numeric|is_unique[admin.phone]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'numeric' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.'
                    )
                ),
                array(
                    'field' => 'adminrole',
                    'label' => 'loại quản trị viên',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/admin/add', array('roles' => $roles));
            } else {
                try {
                    $birthday = explode('/', $data['birthday']);
                    $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];

                    $entry = array(
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'fullname' => $this->db->escape_str(trim($data['fullname'])),
                        'address' => $this->db->escape_str(trim($data['address'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'adminrole_id' => $this->db->escape_str(trim($data['adminrole'])),
                        'status' => $this->db->escape_str(trim($data['status'])),
                        'gender' => $this->db->escape_str(trim($data['gender'])),
                        'date_of_birth' => $birthday,
                        'pw' => $this->user_model->create_hash($data['password'])
                    );
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] == '1') {
                        $new_row = $this->admin_model->create($entry);
                        if ($new_row)
                            $this->session->set_flashdata('success', 'Thêm quản trị viên mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm quản trị viên mới');

                        redirect($this->config->config['base_url'] . '/administrator/admin/add');
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        $roles = $this->adminrole_model->get_list();
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->admin_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->admin_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/admin/edit', array('data' => $data, 'roles' => $roles));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                    )
                ),
                array(
                    'field' => 'fullname',
                    'label' => 'Họ tên',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'phone',
                    'label' => 'Điện thoại',
                    'rules' => 'trim|required|numeric',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'numeric' => '%s không hợp lệ.',
                    )
                ),
                array(
                    'field' => 'adminrole',
                    'label' => 'loại quản trị viên',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                redirect($this->config->config['base_url'] . "/administrator/admin/edit/{$data['admin_id']}");
                return FALSE;
            } else {
                try {
                    $birthday = explode('/', $data['birthday']);
                    $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];

                    $entry = array(
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'fullname' => $this->db->escape_str(trim($data['fullname'])),
                        'address' => $this->db->escape_str(trim($data['address'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'adminrole_id' => $this->db->escape_str(trim($data['adminrole'])),
                        'status' => $this->db->escape_str(trim($data['status'])),
                        'gender' => $this->db->escape_str(trim($data['gender'])),
                        'date_of_birth' => $birthday,
                    );

                    if ($data['admin_id'] !== '1') {
                        $check = $this->admin_model->check_exists(array('id' => $data['admin_id']));
                        $current_user = $this->get_current_admin();
                        if ($current_user['adminrole_id'] == '1') {
                            if ($check) {
                                if ($this->admin_model->update($data['admin_id'], $entry))
                                    $this->session->set_flashdata('success', 'Cập nhật quản trị viên thành công');
                                else
                                    $this->session->set_flashdata('failed', 'Không thể cập nhật quản trị viên!');

                                redirect($this->config->config['base_url'] . "/administrator/admin/edit/{$data['admin_id']}");
                                return FALSE;
                            }
                        }
                    }else {
                        redirect($this->config->config['base_url'] . "/administrator/admin");
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                if ($id !== '1') {
                    $check = $this->admin_model->check_exists(array('id' => $id));
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] == '1') {
                        if ($check) {
                            if ($this->admin_model->delete($id))
                                $this->session->set_flashdata('success', 'Xóa quản trị viên thành công');
                            else
                                $this->session->set_flashdata('failed', 'Không thể xóa quản trị viên');

                            redirect($this->config->config['base_url'] . "/administrator/admin");

                            return FALSE;
                        }
                    }
                }else {
                    redirect($this->config->config['base_url'] . "/administrator/admin");
                    return FALSE;
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

    /**
     * Get adminrole data
     * @param type $id
     * @return boolean
     */
    public function get_adminrole_name($role_id) {
        if ($role_id) {
            $row = $this->adminrole_model->get_info_rule(array('id' => $role_id));
            if ($row)
                return $row->name;
            return FALSE;
        }
        return FALSE;
    }

}
