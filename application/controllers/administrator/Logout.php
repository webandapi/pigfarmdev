<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Logout extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        if (!$this->session->has_userdata('admin')) {
            redirect($this->config->config['base_url'] . '/administrator/login');
            exit();
        }
    }

    /**
     * Logout
     */
    public function index() {
        if ($this->session->has_userdata('admin'))
            $this->session->unset_userdata('admin');
        redirect($this->config->config['base_url'] . '/administrator/login');
    }

}
