<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý người dùng';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $where = array();

        if (isset($_GET['status']) && $_GET['status'] != '')
            $where['where']['status'] = $_GET['status'];

        if (isset($_GET['usertype']) && $_GET['usertype'] != '')
            $where['where']['usertype_id'] = $_GET['usertype'];

        $users = $this->user_model->get_list($where);
        $this->load->view('administrator/user/index', array('users' => $users));
        $this->load->view('administrator/footer');
    }

    /**
     * Expert list
     * @param type $limit
     * @param type $offset
     */
    public function expert($limit, $offset) {
        $status = $_GET['status'];
        $date = $_GET['date'];
        $where = array(
            'where' => array('usertype_id' => 2),
            'order' => array('user_id', 'DESC')
        );
        if (isset($status) && $status != '')
            $where['where']['status'] = $status;

        if ($date) {
            $date = explode('-', $date);
            $form = explode('/', $date[0]);
            $formstr = trim($form[2]) . '-' . trim($form[1]) . '-' . trim($form[0]);
            $to = explode('/', $date[1]);
            $tostr = trim($to[2]) . '-' . trim($to[1]) . '-' . trim($to[0]);
            $where['where']['created'] = "between {$formstr} and {$tostr}";
        }

        $experts = array();
        $experts = $this->user_model->get_list($where);
        $this->load->view('administrator/user/expert', array('experts' => $experts));
        $this->load->view('administrator/footer');
    }

    /**
     * Farmer list
     * @param type $limit
     * @param type $offset
     */
    public function farmer($limit, $offset) {
        $status = $_GET['status'];
        $date = $_GET['date'];
        $flags = $_GET['flags'];
        $where = array(
            'where' => array('usertype_id' => 1),
            'order' => array('user_id', 'DESC')
        );
        if (isset($status) && $status != '')
            $where['where']['status'] = $status;

        if (isset($flags) && $flags != '')
            $where['where']['flags'] = $flags;

        $farmers = array();
        $farmers = $this->user_model->get_list($where);
        $this->load->view('administrator/user/farmer', array('farmers' => $farmers));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     */
    public function add() {
        $usertype = $this->usertype_model->get_list();
        $userfield = $this->field_model->get_list_();
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/user/add', array('usertype' => $usertype, 'userfield' => $userfield));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required|min_length[3]|is_unique[user.username]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).',
                        'is_unique' => '%s này đã được sử dụng.'
                    )
                ),
                array(
                    'field' => 'password',
                    'label' => 'Mật khẩu',
                    'rules' => 'trim|required|min_length[6]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s phải chứa ít nhất %s kí tự.',
                    )
                ),
                array(
                    'field' => 'cf_password',
                    'label' => 'Nhập lại mật khẩu',
                    'rules' => 'trim|required|matches[password]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'matches' => '%s chưa khớp',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|is_unique[user.email]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
                array(
                    'field' => 'usertype',
                    'label' => 'loại người dùng',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/user/add', array('usertype' => $usertype, 'userfield' => $userfield));
            } else {
                try {
                    $birthday = explode('/', $data['birthday']);
                    $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];

                    $entry = array(
                        'username' => $this->db->escape_str(trim($data['username'])),
                        'firstname' => $this->db->escape_str(trim($data['firstname'])),
                        'lastname' => $this->db->escape_str(trim($data['lastname'])),
                        'address' => $this->db->escape_str(trim($data['address'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'usertype_id' => $this->db->escape_str(trim($data['usertype'])),
                        'status' => $this->db->escape_str(trim($data['status'])),
                        'gender' => $this->db->escape_str(trim($data['gender'])),
                        'date_of_birth' => $birthday,
                        'flags' => $data['flags'],
                        'pw' => $this->user_model->create_hash($data['password'])
                    );
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($this->user_model->create($entry)) {

                            $user_ = $this->user_model->get_user_info($this->db->escape_str(trim($data['username'])));

                            //create expert profile
                            if ($data['usertype'] == '2') {
                                $this->profile_model->create(array('user_id' => $user_->user_id));
                            }

                            // create user field
                            $fieldCol_ = array();
                            if ($data['userfield']) {
                                foreach ($data['userfield'] as $val) {
                                    $fieldCol_[]['id'] = $val;
                                }
                            }
                            if ($fieldCol_)
                                $this->userfield_model->create($user_->user_id, $fieldCol_);

                            $this->session->set_flashdata('success', 'Thêm người dùng mới thành công');
                        } else {
                            $this->session->set_flashdata('failed', 'Không thể thêm người dùng mới!');
                        }

                        redirect($this->config->config['base_url'] . '/administrator/user/add');
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     */
    public function edit() {
        $usertype = $this->usertype_model->get_list();
        $userfield = $this->field_model->get_list_();
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = $fieldcold = array();
            if (isset($id)) {
                $check_exist = $this->user_model->check_exists(array('user_id' => $id));
                if ($check_exist) {
                    $data = $this->user_model->get_user_basic_info($id);
                    $fields = $this->userfield_model->get_user_field_list($id);
                    if ($fields) {
                        foreach ($fields as $col) {
                            $fieldcold[] = $col->field_id;
                        }
                    }
                }
            }
            $this->load->view('administrator/user/edit', array('usertype' => $usertype, 'userfield' => $userfield, 'data' => $data, 'field' => $fieldcold));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                    )
                ),
                array(
                    'field' => 'usertype',
                    'label' => 'loại người dùng',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/user/edit', array('usertype' => $usertype));
            } else {
                try {
                    $reponse = '';
                    $birthday = explode('/', $data['birthday']);
                    $birthday = $birthday[2] . '-' . $birthday[1] . '-' . $birthday[0];

                    $entry = array(
                        'firstname' => $this->db->escape_str(trim($data['firstname'])),
                        'lastname' => $this->db->escape_str(trim($data['lastname'])),
                        'address' => $this->db->escape_str(trim($data['address'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'usertype_id' => $this->db->escape_str(trim($data['usertype'])),
                        'status' => $this->db->escape_str(trim($data['status'])),
                        'gender' => $this->db->escape_str(trim($data['gender'])),
                        'date_of_birth' => $birthday,
                        'flags' => $data['flags'],
                    );
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($this->user_model->update($data['user_id'], $entry)) {
                            $fieldCol = $data['userfield'];
                            if ($fieldCol) {
                                $fieldCol_ = array();
                                $i = 1;
                                foreach ($fieldCol as $key => $value) {
                                    $fieldCol_[]['id'] = $value;
                                }
                            }
                            $this->userfield_model->update($data['user_id'], $fieldCol_);
                            $this->session->set_flashdata('success', 'Cập nhật người dùng thành công');
                        } else {
                            $this->session->set_flashdata('failed', 'Không thể cập nhật người dùng!');
                        }

                        redirect($_SERVER['HTTP_REFERER']);
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->user_model->check_exists(array('user_id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] !== '3') {
                    if ($check) {
                        if ($this->user_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa người dùng thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa người dùng!');
                        redirect($this->config->config['base_url'] . "/administrator/user");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

    /**
     * Export users
     */
    public function export() {
        $where = array(
            'where' => array('usertype_id' => 1),
            'order' => array('user_id', 'DESC')
        );

        $this->load->library('PHPExcel/Classes/PHPExcel.php');
        $Excel = new PHPExcel();
        $Excel->setActiveSheetIndex(0);
        $Excel->getActiveSheet()->setTitle('Farmers Report');
        $dataUsers = $this->user_model->get_list($where);
        $dataExcute = array();
        foreach ($dataUsers as $users) {
            if ($user->email != null) {
                array_push($dataExcute, $users);
            }
        }

        //Set column title
//        $Excel->getActiveSheet()->setCellValue('A1', 'User ID');
//        $Excel->getActiveSheet()->setCellValue('B1', 'User Name');
//        $Excel->getActiveSheet()->setCellValue('C1', 'Firstname');
//        $Excel->getActiveSheet()->setCellValue('D1', 'Lastname');
//        $Excel->getActiveSheet()->setCellValue('E1', 'PW');
//        $Excel->getActiveSheet()->setCellValue('F1', 'SUPER');
//        $Excel->getActiveSheet()->setCellValue('G1', 'Address');
        $Excel->getActiveSheet()->setCellValue('H1', 'Email');
//        $Excel->getActiveSheet()->setCellValue('J1', 'Phone');
//        $Excel->getActiveSheet()->setCellValue('K1', 'Usertype_id');
//        $Excel->getActiveSheet()->setCellValue('L1', 'Status');
//        $Excel->getActiveSheet()->setCellValue('M1', 'Date of birth');
//        $Excel->getActiveSheet()->setCellValue('N1', 'Gender');
//        $Excel->getActiveSheet()->setCellValue('O1', 'Avatar');
//        $Excel->getActiveSheet()->setCellValue('P1', 'Language');
//        $Excel->getActiveSheet()->setCellValue('Q1', 'Created');
        //Set size column

        $Excel->getActiveSheet()->getStyle('A1:Q1')->getFont()->setBold(true);

//        $Excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('E')->setWidth(100);
//        $Excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('G')->setWidth(50);
        $Excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
//        $Excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
        //Set row value
        $numRow = 2;
        foreach ($dataExcute as $rowData) {
//            $Excel->getActiveSheet()->setCellValue('A' . $numRow, $rowData->user_id);
//            $Excel->getActiveSheet()->setCellValue('B' . $numRow, $rowData->username);
//            $Excel->getActiveSheet()->setCellValue('C' . $numRow, $rowData->firstname);
//            $Excel->getActiveSheet()->setCellValue('D' . $numRow, $rowData->lastname);
//            $Excel->getActiveSheet()->setCellValue('E' . $numRow, $rowData->pw);
//            $Excel->getActiveSheet()->setCellValue('F' . $numRow, $rowData->super);
//            $Excel->getActiveSheet()->setCellValue('G' . $numRow, $rowData->address);
            $Excel->getActiveSheet()->setCellValue('H' . $numRow, $rowData->email);
//            $Excel->getActiveSheet()->setCellValue('J' . $numRow, $rowData->phone);
//            $Excel->getActiveSheet()->setCellValue('K' . $numRow, $rowData['']);
//            $Excel->getActiveSheet()->setCellValue('L' . $numRow, $rowData['status']);
//            $Excel->getActiveSheet()->setCellValue('M' . $numRow, $rowData['date_of_birth']);
//            $Excel->getActiveSheet()->setCellValue('N' . $numRow, $rowData['gender']);
//            $Excel->getActiveSheet()->setCellValue('O' . $numRow, $rowData['avatar']);
//            $Excel->getActiveSheet()->setCellValue('P' . $numRow, $rowData['language']);
//            $Excel->getActiveSheet()->setCellValue('Q' . $numRow, $rowData['created']);
            $numRow++;
        }

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="UsersData.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($Excel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

}
