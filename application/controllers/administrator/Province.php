<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý tỉnh/TP';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List data
     */
    public function index() {
        $provinces = $this->province_model->get_list();
        $this->load->view('administrator/province/index', array('provinces' => $provinces));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/province/add');
        } else { #submit form
            #form data posted
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên tỉnh/TP',
                    'rules' => 'trim|required|is_unique[provinces.name]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng'
                    )
                ),
                array(
                    'field' => 'zipcode',
                    'label' => 'Zipcode',
                    'rules' => 'trim|required|is_unique[provinces.zipcode]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng'
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/province/add');
            } else {
                try {
                    #check admin role
                    $thisadmin = $this->get_current_admin();
                    if ($thisadmin['adminrole_id'] !== '3') {
                        $entry = array();
                        $entry['name'] = $this->db->escape_str(trim($data['name']));
                        $entry['zipcode'] = $this->db->escape_str(trim($data['zipcode']));
                        
                        if ($this->province_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm tỉnh/TP mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm tỉnh/TP mới!');
                    }
                    redirect($this->config->config['base_url'] . '/administrator/province/add');
                    return false;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->province_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->province_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/province/edit', array('data' => $data));
        } else { // Submit form
            #form data posted
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên tỉnh/TP',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng'
                    )
                ),
                array(
                    'field' => 'zipcode',
                    'label' => 'Zipcode',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return false;
            } else {
                try {
                    #check admin role
                    $thisadmin = $this->get_current_admin();
                    if ($thisadmin['adminrole_id'] !== '3') {
                        $entry = array();
                        $entry['name'] = $this->db->escape_str(trim($data['name']));
                        $entry['zipcode'] = $this->db->escape_str(trim($data['zipcode']));

                        $check_exists = $this->province_model->check_exists(array('id' => $data['id']));
                        if ($check_exists) {
                            if ($this->province_model->update($data['id'], $entry))
                                $this->session->set_flashdata('success', 'Cập nhật tỉnh/TP thành công');
                            else
                                $this->session->set_flashdata('failed', 'Không thể cập nhật tỉnh/TP!');

                            redirect($_SERVER['HTTP_REFERER']);
                            return false;
                        }
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }
    
    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->province_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] !== '3') {
                    if ($check) {
                        if ($this->province_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa Tỉnh/TP thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa Tỉnh/TP');
                        redirect($this->config->config['base_url'] . "/administrator/province");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
