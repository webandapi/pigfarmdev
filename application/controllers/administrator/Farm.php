<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Farm extends FT_Controller {
    /*
     * __construct()
     */

    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý trang trại';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();


        // check admin role
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List data
     * @param type $id
     */
    public function index($id = '') {
        $farms = $this->farm_model->get_list();
        if ($farms):
            foreach ($farms as $farm) :
                $thisuser = $this->user_model->get_info_rule(array('user_id' => $farm->user_id), array('username', 'lastname', 'firstname'));
                $thisfarmtype = $this->farmtype_model->get_info_rule(array('id' => $farm->farmtype_id), 'name');
                $farm->user = ($thisuser->firstname || $thisuser->lastname) ? ($thisuser->lastname . ' ' . $thisuser->firstname) : $thisuser->username;
                $farm->farmtype = $thisfarmtype->name;
                $farm->products = $this->farm_model->get_farm_products($farm->id);
                $farm->certificate = $this->farm_model->get_farm_certificate($farm->id);
            endforeach;
        endif;
        $this->load->view('administrator/farm/index', array('farms' => $farms));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     */
    public function add() {
        $entry = array();
        $entry['farmes'] = $this->user_model->get_list(array('where' => array('usertype_id' => 1))); #farmers lst
        $entry['products'] = $this->product_model->get_list(); #products lst
        $entry['farmtype'] = $this->farmtype_model->get_list(); #farmtype lst
        $entry['certificate'] = $this->certificate_model->get_list(); #certificate lst
        $entry['provinces'] = $this->province_model->get_list(); #provinces
        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/farm/add', array('entry' => $entry));
        } else { #Submit form
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'farm_name',
                    'label' => 'Tên trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'fileToUpload',
                    'rules' => 'callback_check_upload_image'
                ),
                array(
                    'field' => 'farm_boss',
                    'label' => 'Chủ trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'farm_size',
                    'label' => 'Diện tích',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'farm_location',
                    'label' => 'Vị trí',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'province',
                    'label' => 'Tỉnh/TP',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );

            # form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/farm/add', array('entry' => $entry));
            } else {
                try {
                    # ? chưa check thông tin dropdown có tồn tại chưa? nếu không thì set vào giá trị mặc định dc tạo bởi admin
                    // insert data
                    $entrydb = array(
                        'name' => $this->db->escape_str(trim($data['farm_name'])),
                        'water_indicator' => $this->db->escape_str(trim($data['water_indicator'])),
                        'index_of_photosynthesis' => $this->db->escape_str(trim($data['index_of_photosynthesis'])),
                        'farmtype_id' => $data['farmtype'],
                        'user_id' => $data['farm_boss'],
                        'province_id' => $data['province'],
                        'size' => $this->db->escape_str(trim($data['farm_size'])),
                        'location' => $this->db->escape_str(trim($data['farm_location'])),
                        'note' => $this->db->escape_str(trim($data['farm_note'])),
                        'latitude' => $this->db->escape_str(trim($data['latitude'])),
                        'longitude' => $this->db->escape_str(trim($data['longitude'])),
                        'slug' => $this->db->escape_str(trim($data['farm_slug'])),
                        'show_harvest' => $data['show_harvest']
                    );

                    # check current adminrole_id is not {3} - report user
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        $farm_id = $this->farm_model->create($entrydb);
                        if ($farm_id) {

                            # total farms by province
                            $currentfarm = $this->farm_model->get_info_rule(array('id' => $farm_id), 'province_id'); #current farm
                            $countfarmsbyprovince = count($this->farm_model->get_list(array('where' => array('province_id' => $currentfarm->province_id)), array('id'))); #count farms by province
                            # get zipcode of this farm province
                            $thisfarmprovince = $this->province_model->get_info_rule(array('id' => $currentfarm->province_id), 'zipcode');
                            $pzipcode = substr($thisfarmprovince->zipcode, 0, 2);
                            #farm order
                            $order = $countfarmsbyprovince ? $countfarmsbyprovince : 1;
                            #create farm code
                            $zerostr = '';
                            for ($i = 1; $i <= 3; $i++) {
                                if ($i <= 3 - strlen($order))
                                    $zerostr .= '0';
                                $fzipcode = $pzipcode . $zerostr . $order;
                            }

                            #update farm data (code && order)
                            $this->farm_model->update($farm_id, array('code' => $fzipcode, 'order' => $order));

                            # create farm - products
                            if ($data['products'])
                                $this->farmproduct_model->create($farm_id, $data['products']);

                            # create farm - certificate
                            if ($data['certificate'])
                                $this->farm_certificate_model->create($farm_id, $data['certificate']);

                            # Upload certificate picture
                            if ($_FILES['fileToUpload']['name']) {
                                $this->do_upload('certificate', $farm_id, $_FILES);
                            }

                            $this->session->set_flashdata('success', 'Thêm trang trại mới thành công !');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể thêm trang trại mới !');

                        redirect($this->config->config['base_url'] . '/administrator/farm/add');
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        $entry = array();
        $entry['farmes'] = $this->user_model->get_list(array('where' => array('usertype_id' => 1)));  #farmers lst
        $entry['products'] = $this->product_model->get_list(); # products lst
        $entry['farmtype'] = $this->farmtype_model->get_list(); # farmtype lst
        $entry['certificate'] = $this->certificate_model->get_list(); # certificate lst
        $entry['provinces'] = $this->province_model->get_list(); #provinces
        # show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                # check current adminrole_id is not {3} - report user
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] !== '3') {

                    # check farm is exists
                    $check_farm = $this->farm_model->check_exists(array('id' => $id));
                    if ($check_farm) {
                        $data = $this->farm_model->get_info_rule(array('id' => $id));

                        # farm - products
                        $thisproducts = $this->farm_model->get_farm_products($id);
                        if ($thisproducts) {
                            foreach ($thisproducts as $thispro) {
                                $data->products[] = $thispro->id;
                            }
                        }

                        # farm - certificate
                        $thiscertificate = $this->farm_model->get_farm_certificate($id);
                        if ($thiscertificate) {
                            foreach ($thiscertificate as $thiscer) {
                                $data->certificate[] = $thiscer->id;
                            }
                        }
                    }
                }
            }
            $this->load->view('administrator/farm/edit', array('data' => $data, 'entry' => $entry));
        } else { # Submit form
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'farm_name',
                    'label' => 'Tên trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'fileToUpload',
                    'rules' => 'callback_check_upload_image'
                ),
                array(
                    'field' => 'farm_boss',
                    'label' => 'Chủ trang trại',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'farm_size',
                    'label' => 'Diện tích',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            # form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {
                    $entrydb = array(
                        'name' => $this->db->escape_str(trim($data['farm_name'])),
                        'water_indicator' => $this->db->escape_str(trim($data['water_indicator'])),
                        'index_of_photosynthesis' => $this->db->escape_str(trim($data['index_of_photosynthesis'])),
                        'farmtype_id' => $data['farmtype'],
                        'user_id' => $data['farm_boss'],
                        'size' => $this->db->escape_str(trim($data['farm_size'])),
                        'note' => $this->db->escape_str(trim($data['farm_note'])),
                        'slug' => $this->db->escape_str(trim($data['farm_slug'])),
                        'show_harvest' => $data['show_harvest']
                    );

                    # check current adminrole_id is not {3} - report user
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        $check_farm = $this->farm_model->check_exists(array('id' => $data['id']));
                        if ($check_farm) {
                            if ($this->farm_model->update($data['id'], $entrydb)) {

                                # update farm - product
                                $this->farmproduct_model->update($data['id'], $data['products']);

                                # update farm - certificate
                                $this->farm_certificate_model->update($data['id'], $data['certificate']);

                                # Upload certificate picture
                                if ($_FILES['fileToUpload']['name']) {
                                    $this->do_upload('certificate', $data['id'], $_FILES);
                                }

                                $this->session->set_flashdata('success', 'Cập nhật trang trại thành công !');
                            } else
                                $this->session->set_flashdata('failed', 'Không thể cập nhật trang trại !');

                            redirect($_SERVER['HTTP_REFERER']);
                            return false;
                        }
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     */
    public function delete() {
        
    }

}
