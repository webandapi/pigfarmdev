<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->load->helper(array('form', 'url'));
        $this->load->library(array('session', 'form_validation'));

        if ($this->session->has_userdata('admin')) {
            redirect($this->config->config['base_url'] . '/administrator/dashboard/');
            exit();
        }
    }

    /**
     * Login
     * @return boolean
     */
    public function index() {

        # show form
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/login/header');
            $this->load->view('administrator/login/index');
            $this->load->view('administrator/login/footer');
        } else {
            $config = array(
                array(
                    'field' => 'username',
                    'label' => 'Tên đăng nhập',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'pwd',
                    'label' => 'Mật khẩu',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                )
            );

            # validate form
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/login/header');
                $this->load->view('administrator/login/index');
                $this->load->view('administrator/login/footer');
            } else {
                if (isset($_POST['username']) && isset($_POST['pwd'])) {

                    $login_name = $_POST['username'];
                    $login_password = $_POST['pwd'];

                    $admin = $this->admin_model->get_admin_info($login_name);
                    if ($admin) {
                        if ($this->admin_model->validate_password($login_password, $admin->pw)) {
                            if ($admin->status) {
                                $this->session->sess_expiration = '14400'; // expires in 4 hours
                                $this->session->set_userdata(array('admin' => $admin->username, 'logged_in' => TRUE));
                                redirect($this->config->config['base_url'] . '/administrator/dashboard/');
                            } else {
                                $this->session->set_flashdata('reponse', 'Tài khoản này chưa được kích hoạt!');
                                redirect($this->config->config['base_url'] . '/administrator/login');
                            }
                        } else {
                            $this->session->set_flashdata('reponse', 'Mật khẩu không đúng!');
                            redirect($this->config->config['base_url'] . '/administrator/login');
                        }
                    } else {
                        $this->session->set_flashdata('reponse', 'Tên đăng nhập không đúng!');
                        redirect($this->config->config['base_url'] . '/administrator/login');
                    }
                }
                return false;
            }
        }
    }

}
