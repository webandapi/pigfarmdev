<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Farmtype extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Loại trang trại';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] !== '1') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $farmtypes = $this->farmtype_model->get_list();
        $this->load->view('administrator/farmtype/index', array('farmtypes' => $farmtypes));
        $this->load->view('administrator/footer');
    }

    public function add() {
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/farmtype/add');
        } else {
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'farmtype_name',
                    'label' => 'Tên loại trang trại',
                    'rules' => 'required|is_unique[farmtype.name]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng'
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            // Validatetion
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/farmtype/add');
            } else {
                try {
                    $entry = array();
                    $entry['name'] = $this->db->escape_str(trim($data['farmtype_name']));
                    $entry['note'] = $this->db->escape_str(trim($data['farmtype_des']));

                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($this->farmtype_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm loại trang trại mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm loại trang trại mới!');
                        redirect($this->config->config['base_url'] . '/administrator/farmtype/add');
                        return false;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Edit data
     */
    public function edit() {
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->farmtype_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->farmtype_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/farmtype/edit', array('data' => $data));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'farmtype_name',
                    'label' => 'Tên loại trang trại',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            // Validatetion
            if ($this->form_validation->run() == FALSE) {
                redirect($this->config->config['base_url'] . "/administrator/farmtype/edit/{$data['farmtype_id']}");
                return false;
            } else {
                try {
                    $entry = array();
                    $entry['name'] = $this->db->escape_str(trim($data['farmtype_name']));
                    $entry['note'] = $this->db->escape_str(trim($data['farmtype_des']));

                    $check = $this->farmtype_model->check_exists(array('id' => $data['farmtype_id']));
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($check) {
                            if ($this->farmtype_model->update($data['farmtype_id'], $entry))
                                $this->session->set_flashdata('success', 'Cập nhật loại trang trại thành công');
                            else
                                $this->session->set_flashdata('failed', 'Không thể cập nhật loại trang trại!');

                            redirect($this->config->config['base_url'] . "/administrator/farmtype/edit/{$data['farmtype_id']}");
                            return false;
                        }
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->farmtype_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] == '1') {
                    if ($check) {
                        if ($this->farmtype_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa loại trang trại thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa loại trang trại!');

                        redirect($this->config->config['base_url'] . "/administrator/farmtype");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
