<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Qa extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý câu hỏi thường gặp';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $qa = $this->qa_model->get_list();
        $this->load->view('administrator/qa/index', array('qa' => $qa));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     */
    public function add() {
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/qa/add');
        } else {
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'title',
                    'label' => 'Câu hỏi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'description',
                    'label' => 'Nội dung',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            // Validatetion
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/qa/add');
            } else {
                try {
                    $entry = array();
                    $entry['title'] = $this->db->escape_str(trim($data['title']));
                    $entry['description'] = $data['description'];

                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($this->qa_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm câu hỏi mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm câu hỏi mới!');
                        redirect($this->config->config['base_url'] . '/administrator/qa/add');
                        return false;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Edit data
     */
    public function edit() {
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->qa_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->qa_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/qa/edit', array('data' => $data));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'title',
                    'label' => 'Câu hỏi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'description',
                    'label' => 'Nội dung',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            // Validatetion
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return false;
            } else {
                try {
                    $entry = array();
                    $entry['title'] = $this->db->escape_str(trim($data['title']));
                    $entry['description'] = $data['description'];

                    $check = $this->qa_model->check_exists(array('id' => $data['id']));
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($check) {
                            if ($this->qa_model->update($data['id'], $entry))
                                $this->session->set_flashdata('success', 'Cập nhật câu hỏi thành công');
                            else
                                $this->session->set_flashdata('failed', 'Không thể cập nhật câu hỏi!');

                            redirect($_SERVER['HTTP_REFERER']);
                            return false;
                        }
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->qa_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] !== '3') {
                    if ($check) {
                        if ($this->qa_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa câu hỏi thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa câu hỏi!');

                        redirect($this->config->config['base_url'] . "/administrator/qa");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
