<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Supporter extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý người hỗ trợ';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $where = array();
        $supporter = $this->supporter_model->get_list($where);
        $this->load->view('administrator/supporter/index', array('supporter' => $supporter));
        $this->load->view('administrator/footer');
    }

    
    /**
     * Insert data
     */
    public function add() {
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/supporter/add', array());
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Họ và tên',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).'
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|valid_email|is_unique[supporter.email]',
                    'errors' => array(
                        'valid_email' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.'
                    )
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/supporter/add', array());
            } else {
                try {
                    $entry = array(
                        'name' => $this->db->escape_str(trim($data['name'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'description' => $this->db->escape_str(trim($data['description'])),
                        'created' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                        'updated' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                    );

                    if ($this->supporter_model->create($entry)) {
                        $this->session->set_flashdata('success', 'Thêm người hỗ trợ mới thành công');
                    } else {
                        $this->session->set_flashdata('failed', 'Không thể thêm người hỗ trợ mới!');
                    }

                    redirect($this->config->config['base_url'] . '/administrator/supporter/add');
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     */
    public function edit() {
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->supporter_model->check_exists(array('id' => $id));
                if ($check_exist) {
                    $data = $this->supporter_model->get_supporter_info($id);
                }
            }
            $this->load->view('administrator/supporter/edit', array('data' => $data));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Họ và tên',
                    'rules' => 'trim|required|min_length[3]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'min_length' => '%s quá ngắn (ít nhất %s kí tự).'
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|valid_email',
                    'errors' => array(
                        'valid_email' => '%s không hợp lệ.'
                    )
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/supporter/edit', array('id' => $id));
            } else {
                try {
                    $entry = array(
                        'name' => $this->db->escape_str(trim($data['name'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                        'description' => $this->db->escape_str(trim($data['description'])),
                        'updated' => $this->db->escape_str(trim(date('Y-m-d H:i:s'))),
                    );
                    if ($this->supporter_model->update($data['id'], $entry)) {
                        $this->session->set_flashdata('success', 'Cập nhật người hỗ trợ thành công');
                    } else {
                        $this->session->set_flashdata('failed', 'Không thể cập nhật người hỗ trợ!');
                    }

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->supporter_model->check_exists(array('id' => $id));
                if ($check) {
                    if ($this->supporter_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa người hỗ trợ thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa người hỗ trợ!');

                    redirect($this->config->config['base_url'] . "/administrator/supporter");
                    return FALSE;
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
