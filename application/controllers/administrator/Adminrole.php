<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Adminrole extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Phân quyền quản trị viên';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] !== '1') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $roles = $this->adminrole_model->get_list();
        $this->load->view('administrator/adminrole/index', array('roles' => $roles));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     */
    public function add() {
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/adminrole/add');
        } else {
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'adminrole_name',
                    'label' => 'Tên phân quyền',
                    'rules' => 'required|is_unique[adminrole.name]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng'
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            // Validatetion
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/adminrole/add');
            } else {
                try {
                    $entry = array();
                    $entry['name'] = $this->db->escape_str(trim($data['adminrole_name']));
                    $entry['note'] = $this->db->escape_str(trim($data['adminrole_des']));

                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] == '1') {
                        if ($this->adminrole_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm phân quyền mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm phân quyền mới!');
                        redirect($this->config->config['base_url'] . '/administrator/adminrole/add');
                        return false;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Edit data
     */
    public function edit() {
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->adminrole_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->adminrole_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/adminrole/edit', array('data' => $data));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'adminrole_name',
                    'label' => 'Tên phân quyền',
                    'rules' => 'required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            // Validatetion
            if ($this->form_validation->run() == FALSE) {
                redirect($this->config->config['base_url'] . "/administrator/adminrole/edit/{$data['adminrole_id']}");
                return false;
            } else {
                try {
                    $entry = array();
                    $entry['name'] = $this->db->escape_str(trim($data['adminrole_name']));
                    $entry['note'] = $this->db->escape_str(trim($data['adminrole_des']));

                    $check = $this->adminrole_model->check_exists(array('id' => $data['adminrole_id']));
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] == '1') {
                        if ($check) {
                            if ($this->adminrole_model->update($data['adminrole_id'], $entry))
                                $this->session->set_flashdata('success', 'Cập nhật phân quyền thành công');
                            else
                                $this->session->set_flashdata('failed', 'Không thể cập nhật phân quyền!');

                            redirect($this->config->config['base_url'] . "/administrator/adminrole/edit/{$data['adminrole_id']}");
                            return false;
                        }
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->adminrole_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] == '1') {
                    if ($check) {
                        if ($this->adminrole_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa phân quyền thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa phân quyền!');

                        redirect($this->config->config['base_url'] . "/administrator/adminrole");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
