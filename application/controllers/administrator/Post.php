<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();
        $pageTitle = 'Quản lý bài viết';

        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        /* if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }*/
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List data
     */
    public function index() {
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => 500,
            'post_status' => 'publish'
        );
        $query = new WP_Query($args);
        $posts = array();
        if ($query->have_posts()):
            while ($query->have_posts()):$query->the_post();
                global $post;
                $obj = new stdClass();
                $obj->id = get_the_ID();
                $obj->title = get_the_title();
                $obj->url = $this->config->config['ft_url'] . '/post/info/' . get_post_field('post_name', get_post());
                $fields = $this->getPostCategories(get_the_ID());
                if ($fields) {
                    $fieldsCol = '';
                    foreach ($fields as $key => $value) {
                        $field = $this->field_model->get_info($value['id']);
                        if ($field->name) {
                            $fieldsCol .= "<a target='_blank' href='{$this->config->config['ft_url']}/post/category/{$field->slug}'>{$field->name}</a>";
                            if ($key > 0)
                                $fieldsCol .= ' | ' . "<a target='_blank' href='{$this->config->config['ft_url']}/post/category/{$field->slug}'>{$field->name}</a>";
                        }
                    }
                }
                $obj->fields = $fieldsCol;
                $obj->author = get_the_author();
                $obj->view = $this->post_model->get_view(get_the_ID());
                $obj->date = date('m/d/Y', strtotime(get_the_modified_date()));
                $posts[] = $obj;
            endwhile;
        endif;
        $this->load->view('administrator/post/index', array('posts' => $posts));
        $this->load->view('administrator/footer');
    }

    /**
     * List Category
     */
    public function category() {
        $categories = get_categories(array('exclude' => 1, 'hide_empty' => false));
        $this->load->view('administrator/post/category', array('categories' => $categories));
        $this->load->view('administrator/footer');
    }

    public function tag() {
        $tags = $this->post_model->get_tags();
        $this->load->view('administrator/post/tag', array('tags' => $tags));
        $this->load->view('administrator/footer');
    }

}
