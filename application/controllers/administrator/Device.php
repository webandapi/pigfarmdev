<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Device extends FT_Controller {

    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý thiết bị';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List data
     * @return boolean
     */
    function index() {
        $devices = $this->device_model->get_list();
        $this->load->view('administrator/device/index', array('devices' => $devices));
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $user = $this->user_model->get_user_info($this->session->userdata('user'));
        $segs = $this->uri->segment_array();
        $id = end($segs);
        $devices = $this->device_model->get_info_rule_(array('id' => $id));
        $data = array($devices);
        foreach ($data as $key => $value) {
            $serial = $value->serial;
        }
        // var_dump($serial);die;
        if ($serial) {
            $this->device_model->check_exists_serial($serial);
            $devicesId = $this->device_model->get_device_id_by_serial($serial);
            // var_dump($devicesId);die;
            if (!$this->device_model->is_current_user($devicesId, $user->user_id)) {
                echo json_encode(array($this->messageCode => $this->codeFailed, $this->message => $this->lang->line('delete') . ' ' . $this->lang->line('failed'), 'data' => $this->lang->line('message_this_device_not_belong_to_user')));
                return false;
            }
            $this->device_model->check_exists_($devicesId);
            if ($this->device_model->update($devicesId, array('farm_id' => '', 'status' => '0'))) {
                // die;
                redirect($this->config->config['base_ci_url'] . 'device');
                $message = json_encode(array($this->messageCode => 200, $this->message => $this->lang->line('delete') . ' ' . $this->lang->line('success'), 'data' => $this->device_model->get_device_list_by_user_id($payloadData['user']->user_id)));
            } else {
                $message = json_encode(array($this->messageCode => $this->codeFailed, $this->message => $this->lang->line('delete') . ' ' . $this->lang->line('failed'), 'data' => $this->device_model->get_device_list_by_user_id($payloadData['user']->user_id)));
            }
        } else {
            echo json_encode(array($this->messageCode => 500, $this->message => $this->lang->line('failed') . '! ' . $this->lang->line('check_input_id'), 'data' => $this->NullObj));
            return false;
        }
        echo $message;
        return true;
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        $this->load->view('administrator/device/add');
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        $this->load->view('administrator/device/edit');
        $this->load->view('administrator/footer');
    }

}
