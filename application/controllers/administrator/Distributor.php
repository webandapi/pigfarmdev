<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý nhà phân phối';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
            return FALSE;
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $distributors = $this->distributor_model->get_list();
        $this->load->view('administrator/distributor/index', array('distributors' => $distributors));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/distributor/add');
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên nhà phân phối',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'fileToUpload',
                    'rules' => 'callback_check_upload_image'
                ),
                array(
                    'field' => 'code',
                    'label' => 'Mã số doanh nghiệp',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'location',
                    'label' => 'Địa chỉ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email|is_unique[distributors.email]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
                array(
                    'field' => 'phone',
                    'label' => 'Điện thoại',
                    'rules' => 'trim|required|numeric|is_unique[distributors.phone]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'numeric' => '%s không hợp lệ.',
                        'is_unique' => '%s này đã được sử dụng.'
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/distributor/add');
            } else {
                try {
                    $entry = array(
                        'name' => $this->db->escape_str(trim($data['name'])),
                        'code' => $this->db->escape_str(trim($data['code'])),
                        'location' => $this->db->escape_str(trim($data['location'])),
                        'latitude' => $this->db->escape_str(trim($data['latitude'])),
                        'longitude' => $this->db->escape_str(trim($data['longitude'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                    );
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        $distributor_id = $this->distributor_model->create($entry);
                        if ($distributor_id) {
                            if ($_FILES['fileToUpload']['name']) {
                                $this->do_upload('distributors', $distributor_id, $_FILES);
                            }
                            $this->session->set_flashdata('success', 'Thêm nhà phân phối mới thành công');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể thêm nhà phân phối mới');

                        redirect($this->config->config['base_url'] . '/administrator/distributor/add');
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->distributor_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->distributor_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/distributor/edit', array('data' => $data));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'name',
                    'label' => 'Tên nhà phân phối',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'fileToUpload',
                    'rules' => 'callback_check_upload_image'
                ),
                array(
                    'field' => 'code',
                    'label' => 'Mã số doanh nghiệp',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'location',
                    'label' => 'Địa chỉ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'valid_email' => '%s không hợp lệ.',
                    )
                ),
                array(
                    'field' => 'phone',
                    'label' => 'Điện thoại',
                    'rules' => 'trim|required|numeric',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'numeric' => '%s không hợp lệ.',
                    )
                ),
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {
                    $entry = array(
                        'name' => $this->db->escape_str(trim($data['name'])),
                        'code' => $this->db->escape_str(trim($data['code'])),
                        'location' => $this->db->escape_str(trim($data['location'])),
                        'latitude' => $this->db->escape_str(trim($data['latitude'])),
                        'longitude' => $this->db->escape_str(trim($data['longitude'])),
                        'email' => $this->db->escape_str(trim($data['email'])),
                        'phone' => $this->db->escape_str(trim($data['phone'])),
                    );
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] !== '3') {
                        if ($this->distributor_model->update($data['id'], $entry)) {
                            if ($_FILES['fileToUpload']['name']) {
                                $this->do_upload('distributors', $data['id'], $_FILES);
                            }
                            $this->session->set_flashdata('success', 'Cập nhật nhà phân phối thành công');
                        } else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật nhà phân phối');

                        redirect($_SERVER['HTTP_REFERER']);
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->distributor_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] !== '3') {
                    if ($check) {
                        if ($this->distributor_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa nhà phân phối thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa nhà phân phối!');
                        redirect($this->config->config['base_url'] . "/administrator/distributor");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
