<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devicetype extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý loại thiết bị';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List data 
     */
    public function index() {
        $devicetype = $this->devicetype_model->get_list();
        $this->load->view('administrator/devicetype/index', array('devicetype' => $devicetype));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        $this->load->view('administrator/devicetype/add');
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        $this->load->view('administrator/devicetype/edit');
        $this->load->view('administrator/footer');
    }

}
