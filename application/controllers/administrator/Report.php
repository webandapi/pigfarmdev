<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Thống kê';
        $this->load->helper(array('url'));
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        
    }

    /**
     * Post report
     * @return boolean
     */
    public function post() {
        $this->load->view('administrator/report/post', array('posts' => $posts));
        $this->load->view('administrator/footer');
    }

}
