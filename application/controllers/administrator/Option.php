<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Option extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();
        $this->check_admin_log();

        $pageTitle = 'Quản lý cài đặt';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] !== '1') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
            return FALSE;
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $options = $this->options_model->get_list();
        $this->load->view('administrator/option/index', array('options' => $options));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        // View
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/option/add');
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'setting_name',
                    'label' => 'Tên cài đặt',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'setting_key',
                    'label' => 'Key',
                    'rules' => 'trim|required|is_unique[options.slug]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng.',
                    )
                ),
                array(
                    'field' => 'setting_value',
                    'label' => 'Giá trị',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/option/add');
            } else {
                try {
                    $entry = array(
                        'name' => $this->db->escape_str(trim($data['setting_name'])),
                        'slug' => $this->db->escape_str(trim($data['setting_key'])),
                        'value' => $this->db->escape_str(trim($data['setting_value']))
                    );
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] == '1') {
                        $new_row = $this->options_model->create($entry);
                        if ($new_row)
                            $this->session->set_flashdata('success', 'Thêm cài đặt mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm cài đặt mới');

                        redirect($this->config->config['base_url'] . '/administrator/option/add');
                        return FALSE;
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     * @return boolean
     */
    public function edit() {
        // View
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->options_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->options_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/option/edit', array('data' => $data));
        } else { // Submit
            $data = $this->input->post();

            $config = array(
                array(
                    'field' => 'setting_name',
                    'label' => 'Tên cài đặt',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'setting_key',
                    'label' => 'Key',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'setting_value',
                    'label' => 'Giá trị',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {
                    $entry = array(
                        'name' => $this->db->escape_str(trim($data['setting_name'])),
                        'slug' => $this->db->escape_str(trim($data['setting_key'])),
                        'value' => $this->db->escape_str(trim($data['setting_value']))
                    );

                    $check = $this->options_model->check_exists(array('id' => $data['option_id']));
                    $current_user = $this->get_current_admin();
                    if ($current_user['adminrole_id'] == '1') {
                        if ($check) {
                            if ($this->options_model->update($data['option_id'], $entry))
                                $this->session->set_flashdata('success', 'Cập nhật cài đặt thành công');
                            else
                                $this->session->set_flashdata('failed', 'Không thể cập nhật cài đặt!');

                            redirect($_SERVER['HTTP_REFERER']);
                            return FALSE;
                        }
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->options_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] == '1') {
                    if ($check) {
                        if ($this->options_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa cài đặt thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa cài đặt');
                        redirect($this->config->config['base_url'] . "/administrator/option/");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
