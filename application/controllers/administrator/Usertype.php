<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Usertype extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Phân quyền người dùng';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        $usertype = $this->usertype_model->get_list();
        $this->load->view('administrator/usertype/index', array('usertype' => $usertype));
        $this->load->view('administrator/footer');
    }

    /**
     * Insert data
     */
    public function add() {
        // View
        if (!$this->input->post('submit')) {
            $this->load->view('administrator/usertype/add');
        } else { // Submit
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'usertype_name',
                    'label' => 'Tên phân quyền',
                    'rules' => 'trim|required|is_unique[usertype.name]',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                        'is_unique' => '%s này đã được sử dụng'
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                $this->load->view('administrator/usertype/add');
            } else {
                try {
                    $entry = array();
                    $entry['name'] = $data['usertype_name'];
                    $entry['note'] = $data['usertype_des'];

                    if ($this->usertype_model->create($entry))
                        $this->session->set_flashdata('success', 'Thêm phân quyền mới thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể thêm phân quyền mới!');

                    redirect($this->config->config['base_url'] . '/administrator/usertype/add');
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Update data
     */
    public function edit() {
        // View
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            $data = array();
            if (isset($id)) {
                $check_exist = $this->usertype_model->check_exists(array('id' => $id));
                if ($check_exist)
                    $data = $this->usertype_model->get_info_rule(array('id' => $id));
            }
            $this->load->view('administrator/usertype/edit', array('data' => $data));
        } else { // Submit
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'usertype_name',
                    'label' => 'Tên phân quyền',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() == FALSE) {
                redirect($this->config->config['base_url'] . "/administrator/usertype/edit/{$data['usertype_id']}");
                return false;
            } else {
                try {
                    $entry = array();
                    $entry['name'] = $data['usertype_name'];
                    $entry['note'] = $data['usertype_des'];

                    $check = $this->usertype_model->check_exists(array('id' => $data['usertype_id']));
                    if ($check) {
                        if ($this->usertype_model->update($data['usertype_id'], $entry))
                            $this->session->set_flashdata('success', 'Cập nhật phân quyền thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật phân quyền!');

                        redirect($this->config->config['base_url'] . "/administrator/usertype/edit/{$data['usertype_id']}");
                    }
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }

        // load footer
        $this->load->view('administrator/footer');
    }

    /**
     * Delete data
     * @return boolean
     */
    public function delete() {
        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $check = $this->usertype_model->check_exists(array('id' => $id));
                $current_user = $this->get_current_admin();
                if ($current_user['adminrole_id'] == '1') {
                    if ($check) {
                        if ($this->usertype_model->delete($id))
                            $this->session->set_flashdata('success', 'Xóa loại người dùng thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể xóa loại người dùng');
                        redirect($this->config->config['base_url'] . "/administrator/usertype/");
                        return FALSE;
                    }
                }
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
