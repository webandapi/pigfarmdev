<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends FT_Controller {
    /*
     * __construct()
     */

    public function __construct() {
        parent::__construct();

        $this->check_admin_log();

        $pageTitle = 'Quản lý sản phẩm';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_admin();
        if ($user_data['adminrole_id'] == '3') {
            $this->session->set_flashdata('failed', 'Xin lỗi! Bạn không có quyền truy cập trang này.');
            redirect($this->config->config['base_url'] . '/administrator/permission');
        }
        $this->load->view('administrator/header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * List data
     */
    public function index() {
        $products = $this->product_model->get_list();
        if ($products):
            foreach ($products as $product) :
                $product->field = $this->field_model->get_basic_info($product->field_id, array('id', 'name'));
            endforeach;
        endif;
        $this->load->view('administrator/product/index', array('products' => $products));
        $this->load->view('administrator/footer');
    }

    public function add() {
        $this->load->view('administrator/product/add');
        $this->load->view('administrator/footer');
    }

    public function edit() {
        $this->load->view('administrator/product/edit');
        $this->load->view('administrator/footer');
    }

    public function delete() {
        
    }

}
