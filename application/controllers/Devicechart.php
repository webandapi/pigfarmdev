<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Devicechart extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Thiết Bị Đồ thị- Device Chart';
        $this->load->helper(array('url'));

        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Device Chart
     */
    public function index() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name', 'latitude', 'longitude'));
        $farm_ids = $farminfo = $location = array();
        if ($farms):
            foreach ($farms as $farm) :

                #show farms map
                $loc = array();
                $loc[] = "<img src='{$this->config->config['assets_path']}/images/demo/map_demo.jpg' />";
                $loc[] = $farm->latitude;
                $loc[] = $farm->longitude;
                $location[] = $loc;

                $total = 0;
                $farmplanting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), array('name', 'seeds_id'));
                if ($farmplanting) {
                    foreach ($farmplanting as $fp) {
                        $seeds = $this->seeds_model->get_info_rule(array('id' => $fp->seeds_id), 'name,total_cost');
                        $fp->seeds = $seeds;
                        $farm->planting[] = $fp;
                        $total += $seeds->total_cost;
                        $farm->total = $total;
                    }
                }
                $farm_ids[] = $farm->id;
            endforeach;
        endif;
        $devices = $this->supportdevice_model->get_device_by_farm($farm_ids);
        // Get device default
        $device = [];
        if ($devices) { 
            foreach ($devices as $val) {
                if ($val->isdefault) {
                    $device[0] = $val->deviceid;
                    $device[1] = $val->ipserver;
                    break;
                }
            }
        }
        if ($_POST['item-select-device']) {
            $dt = explode('---',$_POST['item-select-device']);
            $device[0] = $dt[0];
            $device[1] = $dt[1];
        }
        $this->load->view('devicechart/index', array('farms' => $farms, 'devices' => $devices, 'device' => $device));
        $this->load->view('footer');
    }

    /**
     * Device Chart
     */
    public function detail() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name', 'latitude', 'longitude'));
        $farm_ids = $farminfo = $location = array();
        if ($farms):
            foreach ($farms as $farm) :

                #show farms map
                $loc = array();
                $loc[] = "<img src='{$this->config->config['assets_path']}/images/demo/map_demo.jpg' />";
                $loc[] = $farm->latitude;
                $loc[] = $farm->longitude;
                $location[] = $loc;

                $total = 0;
                $farmplanting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), array('name', 'seeds_id'));
                if ($farmplanting) {
                    foreach ($farmplanting as $fp) {
                        $seeds = $this->seeds_model->get_info_rule(array('id' => $fp->seeds_id), 'name,total_cost');
                        $fp->seeds = $seeds;
                        $farm->planting[] = $fp;
                        $total += $seeds->total_cost;
                        $farm->total = $total;
                    }
                }
                $farm_ids[] = $farm->id;
            endforeach;
        endif;
        $devices = $this->supportdevice_model->get_device_by_farm($farm_ids);
        // Get device default
        $device = [];
        if ($devices) { 
            foreach ($devices as $val) {
                if ($val->isdefault) {
                    $device[0] = $val->deviceid;
                    $device[1] = $val->ipserver;
                    break;
                }
            }
        }
        if ($_POST['item-select-device']) {
            $dt = explode('---',$_POST['item-select-device']);
            $device[0] = $dt[0];
            $device[1] = $dt[1];
        }
        $this->load->view('devicechart/detail', array('devices' => $devices, 'device' => $device));
        $this->load->view('footer');
    }


}
