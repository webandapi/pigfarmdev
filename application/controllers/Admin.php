<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends FT_Controller {

    /**
     * __construct()
     * @return boolean
     */
    public function __construct() {

        parent::__construct();

        $this->load->helper(array('url'));

        if ($this->session->has_userdata('admin'))
            redirect($this->config->config['base_url'] . '/administrator/dashboard');
        else
            redirect($this->config->config['base_url'] . '/administrator/login');

        return FALSE;
    }

    /**
     * Index page
     */
    public function index() {
        
    }

}
