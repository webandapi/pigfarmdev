<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Diary extends FT_Controller {

    /**
     * __construct()
     */
    public function __construct() {
        parent::__construct();

        $this->check_user_log();

        $pageTitle = 'Quản lý nhật lý xử lý';
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
        $this->load->library('session');
        $path_info = $this->uri->uri_string();
        $user_data = $this->get_current_user();
        $this->load->view('header', array('current' => $user_data, 'pageTitle' => $pageTitle, 'path_info' => $path_info));
    }

    /**
     * Get list data
     */
    public function index() {
        // lấy danh sách trang trại của nông dân hiện tại
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name'));
        $farm_ids = array();
        if ($farms):
            foreach ($farms as $farm) :
                $farm_ids[] = $farm->id;
            endforeach;
        endif;
        $diaries = $this->planting_model->get_diaries_by_farm($farm_ids);
        $this->load->view('diary/index', array('diaries' => $diaries));
        $this->load->view('footer');
    }

    /**
     * Insert data
     * @return boolean
     */
    public function add() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $plantinglst = array();
        if ($farms):
            foreach ($farms as $farm) :
                $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name');
                if ($planting) {
                    foreach ($planting as $val) {
                        $plantinglst[] = $val;
                    }
                }
            endforeach;
        endif;

        #show form
        if (!$this->input->post('submit')) {
            $this->load->view('diary/add', array('planting' => $plantinglst));
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'title',
                    'label' => 'Tiêu đề',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'content',
                    'label' => 'Nội dung xử lý',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'date',
                    'label' => 'Ngày',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'time',
                    'label' => 'Giờ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'planting',
                    'label' => 'vụ cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('diary/add', array('planting' => $plantinglst));
            } else {
                try {
                    $thisuser = $this->get_current_user();
                    if ($thisuser['usertype_id'] == '1') {
                        $date = explode('/', $data['date']);
                        $time = explode(' ', $data['time']);
                        $time = explode(':', $time[0]);
                        $date = $date[2] . '-' . $date[1] . '-' . $date[0] . ' ' . $time[0] . ':' . $time[1] . ':00';

                        $entry = array(
                            'title' => $this->db->escape_str(trim($data['title'])),
                            'content' => trim($data['content']),
                            'date' => $date,
                            'planting_id' => $data['planting'],
                        );
                        if ($this->diary_model->create($entry))
                            $this->session->set_flashdata('success', 'Thêm nhật ký xử lý mới thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể thêm nhật ký xử lý mới!');
                    } else
                        $this->session->set_flashdata('success', 'Bạn không phải là nông dân nên không có quyền thêm nhật ký xử lý');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Update data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && nhật ký xử lý phải thuộc vụ cây trồng, vật nuôi của trang trại nông dân đang sở hữu}
     */
    public function edit() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $plantinglst = $planting_ids = array();
        $thisuser = $this->get_current_user();
        if ($farms):
            foreach ($farms as $farm) :
                $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name');
                if ($planting) {
                    foreach ($planting as $val) {
                        $planting_ids[] = $val->id;
                        $plantinglst[] = $val;
                    }
                }
            endforeach;
        endif;

        #show form
        if (!$this->input->post('submit')) {
            $segs = $this->uri->segment_array();
            $id = end($segs);
            if (isset($id)) {
                $ok = false;
                if ($data = $this->diary_model->get_info_rule(array('id' => $id))) {
                    if (in_array($data->planting_id, $planting_ids))
                        $ok = true;
                }

                #check permission
                if ($ok === true && $thisuser['usertype_id'] == '1')
                    $this->load->view('diary/edit', array('planting' => $plantinglst, 'data' => $data));
                else
                    $this->load->view('diary/edit', array('permission' => "Bạn không có quyền thao tác với thông tin vừa yêu cầu !"));
            }
        } else {
            $data = $this->input->post();
            $config = array(
                array(
                    'field' => 'title',
                    'label' => 'Tiêu đề',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'content',
                    'label' => 'Nội dung xử lý',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'date',
                    'label' => 'Ngày',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'time',
                    'label' => 'Giờ',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => '%s không được để trống.',
                    )
                ),
                array(
                    'field' => 'planting',
                    'label' => 'vụ cây trồng, vật nuôi',
                    'rules' => 'trim|required',
                    'errors' => array(
                        'required' => 'Chọn %s.',
                    )
                ),
            );

            #form validate
            $this->form_validation->set_rules($config);
            if ($this->form_validation->run() == FALSE) {
                redirect($_SERVER['HTTP_REFERER']);
                return FALSE;
            } else {
                try {

                    $ok = false;
                    if ($current = $this->diary_model->get_info_rule(array('id' => $data['id']))) {
                        if (in_array($current->planting_id, $planting_ids))
                            $ok = true;
                    }
                    #check permission
                    if ($ok === true && $thisuser['usertype_id'] == '1') {
                        $date = explode('/', $data['date']);
                        $time = explode(' ', $data['time']);
                        $time = explode(':', $time[0]);
                        $date = $date[2] . '-' . $date[1] . '-' . $date[0] . ' ' . $time[0] . ':' . $time[1] . ':00';

                        $entry = array(
                            'title' => $this->db->escape_str(trim($data['title'])),
                            'content' => trim($data['content']),
                            'date' => $date,
                            'planting_id' => $data['planting'],
                        );

                        if ($this->diary_model->update($data['id'], $entry))
                            $this->session->set_flashdata('success', 'Cập nhật nhật ký thành công');
                        else
                            $this->session->set_flashdata('failed', 'Không thể cập nhật nhật ký!');
                    } else
                        $this->session->set_flashdata('failed', 'Bạn không có quyền cập nhật nhật ký xử lý của người khác!');

                    redirect($_SERVER['HTTP_REFERER']);
                    return FALSE;
                } catch (Exception $ex) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                    return false;
                }
            }
        }
        $this->load->view('footer');
    }

    /**
     * Delete data
     * @return boolean
     * Kiểm tra: {tài khoản người dùng hiện tại phải là nông dân 
     * && nhật ký xử lý phải thuộc vụ cây trồng, vật nuôi của trang trại nông dân đang sở hữu}
     */
    public function delete() {
        $farms = $this->farm_model->get_farmlist_by_userid($this->session->userdata('user'), array('id', 'name')); #farms
        $planting_ids = array();
        
        if ($farms):
            foreach ($farms as $farm) :
                $planting = $this->planting_model->get_list(array('where' => array('farm_id' => $farm->id, 'status' => 0)), 'id,name');
                if ($planting) {
                    foreach ($planting as $val) {
                        $planting_ids[] = $val->id;
                    }
                }
            endforeach;
        endif;

        $segs = $this->uri->segment_array();
        $id = end($segs);
        if ($id) {
            try {
                $thisuser = $this->get_current_user();
                $ok = false;
                if ($current = $this->diary_model->get_info_rule(array('id' => $id))) {
                    if (in_array($current->planting_id, $planting_ids))
                        $ok = true;
                }
                #check permission
                if ($ok == true && $thisuser['usertype_id'] == '1') {
                    if ($this->diary_model->delete($id))
                        $this->session->set_flashdata('success', 'Xóa nhật ký thành công');
                    else
                        $this->session->set_flashdata('failed', 'Không thể xóa nhật ký!');
                } else
                    $this->session->set_flashdata('failed', 'Bạn không thể xóa nhật ký của người khác!');

                redirect($this->config->config['base_url'] . "/diary");
                return FALSE;
            } catch (Exception $ex) {
                echo 'Caught exception: ', $e->getMessage(), "\n";
                return FALSE;
            }
        }
        return FALSE;
    }

}
