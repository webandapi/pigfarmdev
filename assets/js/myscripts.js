(function ($, root, undefined) {

    $(function () {

        $(".seeds-wrap input.opeinput, .seeds-wrap input#amount").keyup(function (e) {
            var total = 0;
            var num = $('#amount').val() ? parseInt($('#amount').val()) : 1;

            $(".seeds-wrap input.opeinput").each(function () {
                var iv = $(this).val();
                var val = parseInt(iv);
                if (val && val > 0)
                    total = total + val;
            });

            if (total > 0) {
                $(".seeds-wrap #cost-message").text('Tổng kinh phí dự kiến cho giống cây trồng, vật nuôi này là: ' + parseInt(total * num).toLocaleString() + ' vnđ');
                $('.seeds-wrap input#total-cost').val(total * num);
            } else {
                $(".seeds-wrap #cost-message").text('');
                $('.seeds-wrap input:hidden').val(total);
            }

            e.preventDefault();
        });

//        $('.seeds-wrap input.opeinput, .seeds-wrap input#amount').keyup(function (event) {
//            if (event.which >= 37 && event.which <= 40)
//                return;
//
//            $(this).val(function (index, value) {
//                return value
//                        .replace(/\D/g, "")
//                        .replace(/\B(?=(\d{3})+(?!\d))/g, ".");
//            });
//        });
    });

})(jQuery, this);
